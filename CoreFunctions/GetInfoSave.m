function Info_Saved=GetInfoSave()
global Info
names=fieldnames(Info);
for NameIndex=1:numel(names)
    if(~strcmp(names{NameIndex},'Fig') && ~strcmp(names{NameIndex},'h') && ~strcmp(names{NameIndex},'cr') && ~strcmp(names{NameIndex},'Design') && ~strcmp(names{NameIndex},'app')  && ~strcmp(names{NameIndex},'Drivers'))
        Info_Saved.(names{NameIndex})=Info.(names{NameIndex});
    end
end
end