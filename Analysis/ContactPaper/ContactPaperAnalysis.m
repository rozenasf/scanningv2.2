%% Load and orgenize the scans!!!

%WHERE ARE THE MULTI SIDED BRIDGE SCANS???
%MERGE DEBARGHYA ANALYSIS TO THE MAIN CODE
%ANALYZE THE PROFILES as function of BG
%WHERE IS THE GRAPH OF W/Rc=2 antisymmetrized as funciton of all E\H BG's? 

global Info
BGList=[-9.90,-8.50,-7.22,-6.00,-4.85,-3.78,-2.79,-1.88,-1.07,-0.37,0.20,0.60,0.80,1.20,1.77,2.47,3.28,4.19,5.18,6.25,7.40,8.62,9.90];
W_Rc_Bare=[0.25,0.50,0.75,1.00,1.25,1.50,1.75,2.00,2.50,3.00,3.50,4.00,6.00];
W_Rc_Extended=[W_Rc_Bare;-W_Rc_Bare];W_Rc_Extended=W_Rc_Extended(:)';
Beta_Sum = 0.51; %now it is a free parameter, but it changes significanlty around the contacts

[X,Y]=meshgrid(1:1:13,902:911);XEdge_BGlist_Holes_wrc_List=0.01*X+Y;

ScansList={...
    'XY_BG_Holes'               ,[867.01:0.01:867.09],struct('W_Rc',[0,-1,1,-2,2,-4,4,-6.3,6.3]),...
    'XY_BG_Electrons'           ,[862.01,862.02,863.01,863.02,863.03,864.01,864.02,945.01,945.02],struct('W_Rc',[0,-1,1,-2,2,-4,4,6.3,-6.3]),...
    'XY_BG_Holes_BG_spec'               ,[957.01:0.01:957.07],struct('W_Rc',[0,-1,1,-2,2,-4,4,],'BG',[-1.07]),...
    'XY_BG_Electrons_spec'           ,[955.01,956.01:0.01:956.06],struct('W_Rc',[0,-1,1,-2,2,-4,4],'BG',[2.47]),...
%     'Xcenter_BG'                ,[874.01:0.01:874.23],struct('BG',[BGList],'W_Rc',0),...
%     'Xcenter_Magnet_Holes'      ,[875.01:0.01:875.26],struct('BG',-8.5    ,'W_Rc',W_Rc_Extended),...
%     'Xcenter_Magnet_Electrons'  ,[876.01:0.01:876.26],struct('BG',9.9     ,'W_Rc',W_Rc_Extended),...
%     'XEdge_Magnet_Holes'        ,[880.01:0.01:880.27],struct('BG',-8.5    ,'W_Rc',[0,W_Rc_Extended]),...
%     'XEdge_Magnet_Holes_0_5'    ,[881.01:0.01:881.27],struct('BG',-3.78   ,'W_Rc',[0,W_Rc_Extended]) ...
%     'XEdge_Magnet_Electrons_0_5',[884.01:0.01:884.28],struct('BG', 5.181  ,'W_Rc',[0,0,W_Rc_Extended]) ...
%     'XEdge_Magnet_Electrons'    ,[885.01:0.01:885.28],struct('BG', 9.9    ,'W_Rc',[0,0,W_Rc_Extended]) ...
%     'XEdge_Beta_Sum'            ,[887],struct('BG',9.9,'W_Rc',[0]),...
%     'Xcenter_Beta_Sum',          [888],struct('BG',9.9,'W_Rc',[0]),...
%     'XEdge_BGlist_wrc_P1',        889+[1:1:23]/100,struct('BG',[BGList],'W_Rc',1),...
%     'XEdge_BGlist_wrc_0',        890+[1:1:23]/100,struct('BG',[BGList],'W_Rc',0)...
%     'XEdge_BGlist_wrc_P1_5',      891+[1:1:23]/100,struct('BG',[BGList],'W_Rc',1.5)...
%     'XEdge_BGlist_wrc_P2',        892+[1:1:23]/100,struct('BG',[BGList],'W_Rc',2)...
%     'XEdge_BGlist_wrc_P3',        893+[1:1:23]/100,struct('BG',[BGList],'W_Rc',3)...
%     'XEdge_BGlist_wrc_N2',        901+[1:1:23]/100,struct('BG',[BGList(1),BGList],'W_Rc',2),...
%     'XY_BG_Electrons_one_contacts'           ,[898.01:0.01:898.13],struct('W_Rc',[0,1,-1,1.5,-1.5,2,-2,3,-3,4,-4,6,-6]),...
%     'XY_BG_Holes_one_contacts'           ,[899.01:0.01:899.13],struct('W_Rc',[0,1,-1,1.5,-1.5,2,-2,3,-3,4,-4,6,-6]),...
%      'XEdge_BGlist_Holes_wrc', XEdge_BGlist_Holes_wrc_List,struct('BG',[BGList(1),BGList(1:12)],'W_Rc',[1,-1,1.5,-1.5,2,-2,3,-3,4,-4]), ...
%     'Xcenter_wrc_BG', [946.01,947.01,948.01,949.01], struct('W_Rc',[0,W_Rc_Extended],'BG',[5.18,-3.78,2.47,-1.07]),... Debargha figures
%     'XY_Center_BG_Wrc',       950+0.01*(1:46),struct('BG',[BGList,BGList],'W_Rc',[BGList*0+1.6,BGList*0-1.6]),...
%     'XY_Wrc_small_Electrons', 956+0.01*1:6,struct('BG',2.47)...
%     'XY_BG_Holes_one_contacts'           ,[899.01:0.01:899.13],struct('W_Rc',[0,1,-1,1.5,-1.5,2,-2,3,-3,4,-4,6,-6]),...
%     'XEdge_BGlist_Holes_wrc', XEdge_BGlist_Holes_wrc_List,struct('BG',[BGList(1),BGList(1:12)],'W_Rc',[1,-1,1.5,-1.5,2,-2,3,-3,4,-4]),...
%     'XY_bridge',[935+0.01*[1:6],936+0.01*[1:4]],struct('BG',-2,'W_Rc',-1.6*(-1).^(1:10),'CalibrationPoint',[1,1,0,0,-1,-1,1,1,-1,-1]),...
%     'Curveture_WRc',[998+0.01*[1:89],999+0.01*[1:37]],struct('BG',-2,'W_Rc',[0,[0.25:0.25:6],[0.25:0.25:4],[0.25:0.25:3],[0.25:0.25:1.5],[0.25:0.25:1]]),...
%     'XY_bridge_lower_excitation', [1005.01:0.01:1005.06], struct('BG',-2,'W_Rc',-1.6*(-1).^(1:10),'CalibrationPoint',[1,1,0,0,-1,-1,1,1,-1,-1]),...
%     'XY_bridge_heigher_excitation', [1006.01:0.01:1006.04], struct('BG',-2,'W_Rc',-1.6*(-1).^(1:10),'CalibrationPoint',[1,1,0,0,-1,-1,1,1,-1,-1]),...
% 'XY_bridge_lower_excitation_Full', [1007+0.01*[1:6],1008+0.01*[1:6],1009+0.01*[1:6]], struct('BG',-2,'W_Rc',-1.6*(-1).^(1:10),'CalibrationPoint',[1,1,0,0,-1,-1,1,1,-1,-1]),...
% 'XY_bridge_lower_excitation_new_standard', [1014.01:0.01:1014.04], struct('BG',-2,'W_Rc',-1.6*(-1).^(1:10),'CalibrationPoint',[1,1,0,0,-1,-1,1,1,-1,-1]),...
% 'XY_bridge_lower_excitation_new_standard2', [1015.01:0.01:1015.04], struct('BG',-2,'W_Rc',-1.6*(-1).^(1:10),'CalibrationPoint',[1,1,0,0,-1,-1,1,1,-1,-1]),...
%    'XY_bridge_heigher_excitation_75K', [1400.01:0.01:1006.04], struct('BG',-2,'W_Rc',-1.6*(-1).^(1:10),'CalibrationPoint',[1,1,0,0,-1,-1,1,1,-1,-1])... ...%     'XY_Wrc_small_Electrons', 957+0.01*1:6,struct('BG',2.47)...
    };
%Where is the up down middle up down scans for profiles? W/Rc=1.6, BG=~2
%for Lmr=1.5W

%scans 946.01,947,948,949 are X scans in the center as function of W/Rc for
%different BG

%Where is the up down middle up down scans for profiles? W/Rc=1.6, BG=~2
%for Lmr=1.5W

Scans=[];
SuperAxis=[];
tic
for i=1:numel(ScansList)/3;
    List=ScansList{3*i-1};
    Scans.(ScansList{3*i-2})=[];
    
    
    if(size(List,1)==1)
        for j=1:numel(List)
            Scans.(ScansList{3*i-2}){j}=LoadScan(List(j));
            Scans.(ScansList{3*i-2}){j}.Info=Info;
            Scans.(ScansList{3*i-2}){j}.Info.Scan=rmfield(Scans.(ScansList{3*i-2}){j}.Info.Scan,'idgr');
        end
    else
        for k=1:size(List,1)
            for j=1:size(List,2)
                Scans.(ScansList{3*i-2}){k,j}=LoadScan(List(k,j));
                Scans.(ScansList{3*i-2}){k,j}.Info=Info;
                Scans.(ScansList{3*i-2}){k,j}.Info.Scan=rmfield(Scans.(ScansList{3*i-2}){k,j}.Info.Scan,'idgr');
                
            end
        end
        
    end
    SuperAxis.(ScansList{3*i-2})=ScansList{3*i};
end
fprintf('Time for full load:%0.1f\n',toc)
%
RangeRight =[8.25,9.5];
RangeMid   =[-4,4];
RangeLeft  =[-9.5,-8.25];
InterpolationPoints=[-7.25,7.25];
%% Nice 2D as function of W/Rc = 0,1,2,4,6.3
% BG=-8.5 - "XY_BG_Holes" and BG=9.9 - "XY_BG_Electrons"
ScanNameList={'XY_BG_Holes','XY_BG_Electrons','XY_BG_Holes_BG_spec','XY_BG_Electrons_spec'};

for j=1:numel(ScanNameList)
    ScanName=ScanNameList{j};
    figure(j);clf;colormap(jet(16));shading interp
    suptitle(ScanNameList{j}(7:end))
    
    for i=1:numel(Scans.(ScanName))
        subplot(5,2,i+1*(i>1))
        UnPackScan(Scans.(ScanName){i})
        Beta_Sum=0.51;
        R = CenterIDataAxis(BreakIData(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15),[1,2],1e6);
        pcolor(R);shading interp;axis equal;
        hold on
        contour(R,40,'LineColor','k')
        TXYL(sprintf('R [ohm], W/Rc=%0.2f',SuperAxis.(ScanName).W_Rc(i)),'X(scaled) [um]','Y [um]');
        ScaleColorFromIData(R.roi(1,-7,7))
    end
end
%% X cuts close to the edge
if(1)
    %% X cuts close to the edge as function of BG for focusing
    %W/Rc = 1,1.5,2,3
    %% X cuts close to the edge as function of BG for focusing
    %W/Rc = 1,1.5,2,3
%     figure(6);clf;
%     ScanNameList={'XEdge_BGlist_wrc_1'};
%     UnPackScan(Scans.XEdge_Beta_Sum{1});
%     BetaSum = Beta_Sum;
%     Data=[];
%     for j=1:numel(ScanNameList)
%         R=[];
%         %             subplot(2,4,j);
%         ScanName=ScanNameList{j};
%         for i=1:numel(Scans.(ScanName))
%             UnPackScan(Scans.(ScanName){i})
%             R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./BetaSum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
%             Data(i,:)=R.data;
%             plot(R,'-','linewidth',3);hold on
%         end
%         xlim([4,9]); ylim([-10,150]);
%         ChangePlotColor(@(n)jet(23))
%         
%     end
    
    %% X cuts close to the edge as function of W/Rc for focusing
    %BG=-8.5,9.9,-3.78,5.181
    
    
    figure(5);clf;
    ScanNameList={'XEdge_Magnet_Holes','XEdge_Magnet_Holes_0_5','XEdge_Magnet_Electrons_0_5','XEdge_Magnet_Electrons'};
    for j=1:numel(ScanNameList)
        R=[];
        subplot(2,4,j);
        ScanName=ScanNameList{j};
        for i=1:numel(Scans.(ScanName))-2
            UnPackScan(Scans.(ScanName){i})
            Beta_Sum=0.51;
            R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
            plot(R,'-');hold on
        end
        ChangePlotColor(@(n)jet(28))
        TXYL(sprintf('Edge cuts, Density = %0.2e',Vbg_to_n(SuperAxis.(ScanName).BG,0.7)/1e4),'$X$ (Scaled) $[\mu m]$','$R [Ohm]$')
        %             [dRdX,Sharvin_Right,Sharvin_Left,Vsd]=AnalyzeX_Cut(RangeMid,RangeRight,RangeLeft,InterpolationPoints);
        subplot(2,4,j+4);hold on
        %             [Xo,Yo1]=SortXY(Vbg_to_n(SuperAxis.(ScanName).W_Rc(1:end-2),0.7)/1e4,Sharvin_Right);
        %             [Xo,Yo2]=SortXY(Vbg_to_n(SuperAxis.(ScanName).W_Rc(1:end-2),0.7)/1e4,Sharvin_Left);
        %             plot(Xo,Yo1,'.');plot(Xo,Yo2,'.');
        R={};
        for i=1:numel(Scans.(ScanName))-2
            UnPackScan(Scans.(ScanName){i})
            Beta_Sum=0.51;
            if(numel(find(W_Rc_Bare==abs(SuperAxis.(ScanName).W_Rc(i))))>0)
                R{find(W_Rc_Bare==abs(SuperAxis.(ScanName).W_Rc(i)))}{sign(SuperAxis.(ScanName).W_Rc(i))/2+1.5} = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
            end
        end
        for i=1:numel(R)
            Sym=R{i}{2}-R{i}{1};Sym.data=Sym.data+fliplr(Sym.data);
            plot(Sym,'.-','linewidth',1)
        end
        ChangePlotColor(@(a)hsv(13));
        TXYL(sprintf('Symmetrized',Vbg_to_n(SuperAxis.(ScanName).BG,0.7)/1e4),'$X$ (Scaled) $[\mu m]$','$\Delta R [Ohm]$')
    end
    %% X cuts close to the edge as functin of BG at different W/Rc
    figure(9);clf
for Extra=1:5
    subplot(2,3,Extra)
j=1+(Extra-1)*2;
for i=2:10
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j,i});Beta_Sum=0.51;
R1 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j+1,i});Beta_Sum=0.51;
R2 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
IData=R1-R2;
plot(IData.roi(1,-6.5,6.5),'o-','linewidth',3)

hold on
end
TXYL(['W/Rc=',num2str(SuperAxis.XEdge_BGlist_Holes_wrc.W_Rc(j))],'X [um]','\Delta R')
end
end
suptitle(['Magnetic field W/Rc = ',sprintf('%0.2f,',W_Rc_Bare(1:12))])
%% X cuts through the center

if(1)
    %% 	Lmr: X cuts as function of BG no magnetic field
    %W/Rc = 1,2,4,6
    ScanName='Xcenter_BG';
    figure(3);clf;subplot(2,2,1);
    for i=1:numel(Scans.(ScanName))
        UnPackScan(Scans.(ScanName){i})
        Beta_Sum=0.51;
        R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
        plot(R,'-');hold on
    end
    ChangePlotColor(@jet)
    Fit_Right=Fit_Line(0,'X',RangeRight);Fit_Mid=Fit_Line(1,'X',RangeMid);Fit_Left=Fit_Line(0,'X',RangeLeft);
    Sarvin_Right=ExtrapulateFit(Fit_Right,InterpolationPoints(2))-ExtrapulateFit(Fit_Mid,InterpolationPoints(2));
    Sarvin_Left=-(ExtrapulateFit(Fit_Left,InterpolationPoints(1))-ExtrapulateFit(Fit_Mid,InterpolationPoints(1)));
    R_sharvin_th =h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(BGList,0.7)))/5.0e-6;
    ylim([-200,200]);
    ClearFit
    TXYL('X - Line cut through the center for different doping','$ $Xs (scaled) $[\mu m]$','$R$ [Ohm]')
    Kf=SqrtSign(pi*Vbg_to_n(BGList,0.7));
    %
    subplot(2,2,2);cla
    RhoXX=[Fit_Mid.a]*5;
    plot(Vbg_to_n(BGList,0.7)/1e4,1e6*h_./(2*qe_^2.*abs(Kf).*RhoXX),'o--','linewidth',1,'markersize',5)
    TXYL('$ $Mean free path $l_{mr}=\frac{h}{2e^2\rho_{xx}\sqrt{\pi n}}$','Density [1/cm^2]','$l_{mr} [\mu m]$')
    %
    subplot(2,2,3);cla;hold on
    plot(Vbg_to_n(BGList,0.7)/1e4,Sarvin_Left,'o--','linewidth',1,'markersize',5);
    plot(Vbg_to_n(BGList,0.7)/1e4,Sarvin_Right,'o--','linewidth',1,'markersize',5);
    plot(Vbg_to_n(BGList,0.7)/1e4,R_sharvin_th,'o--','linewidth',1,'markersize',5);
    ylim([0,280]);grid
    TXYL('Contact Resistance','$ $Density $[1/cm^2]$','$\Delta R$ [Ohm]','Left','Right','Theoretical')
    %
    subplot(2,2,4);cla;hold on
    plot(Kf/1e6,Sarvin_Right./R_sharvin_th,'o--','linewidth',1,'markersize',5);
    plot(Kf/1e6,Sarvin_Left./R_sharvin_th,'o--','linewidth',1,'markersize',5);
    ylim([0,8])
    Fit_Line(@(x,a)0+4*sqrt(-(x-0.7).*a(1))+a(2),[2.15e-8,30],'X',[-150,-35])
    Fit(1).Range=[-150,0];Fit(2).Range=[-150,0];
    grid
    TXYL('Contact Resistance normelized by theory','$ $Kf $[1/\mu m]$','$\Delta R/R_{Quantum}$','Left','Right','Theoretical')
    %% 	Sharvin : X cuts as function of Magnetic field [0:0.25:2,2.5,3,3.5,4,6] through the center for electron\holes
    %BG=-8.5,9.9, (+-) theoretical half collimation
    figure(4);clf
    ScanNameList={'Xcenter_Magnet_Holes','Xcenter_Magnet_Electrons'};
    ZeroFieldScans=[2,23];
    for j=1:numel(ScanNameList)
        subplot(2,2,j);
        ScanName=ScanNameList{j};
        Vec=[];
        UnPackScan(Scans.Xcenter_BG{ZeroFieldScans(j)})
        Beta_Sum=0.51;
        R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
        plot(R,'-');hold on
        for i=1:numel(Scans.(ScanName))
            
            UnPackScan(Scans.(ScanName){i})
            Beta_Sum=0.51;
            R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
            plot(R,'-');hold on
            
            
        end
        ChangePlotColor(@(n)jet(27))
        if(j==1)
            TXYL('Holes - X line cuts though the center for different magnetic field','$Xs$ (scaled) $[\mu m]$','$R [Ohm]$')
        else
            TXYL('Electrons - X line cuts though the center for different magnetic field','$Xs$ (scaled) $[\mu m]$','$R [Ohm]$')
        end
        [dRdX,Sharvin_Right,Sharvin_Left,Vsd]=AnalyzeX_Cut(RangeMid,RangeRight,RangeLeft,InterpolationPoints);
        subplot(2,2,3);hold on;grid on
        [Xo,Yo]=SortXY([0,SuperAxis.(ScanName).W_Rc],1e6*dRdX*5e-6);
        plot(Xo,Yo,'o--','linewidth',1);
        TXYL('$\rho_{xx}$ as function of W/Rc','$W/Rc$','$\rho_{xx}$ [Ohm]','Holes','Electrons')
        
        %
        subplot(2,4,6+j);hold on
        [Xo,Yo1]=SortXY([0,SuperAxis.(ScanName).W_Rc],Sharvin_Right);
        [Xo,Yo2]=SortXY([0,SuperAxis.(ScanName).W_Rc],Sharvin_Left);
        R_sharvin_th =h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(SuperAxis.(ScanName).BG,0.7)))/5.0e-6;
        plot(Xo,(Yo1+fliplr(Yo1))/2./R_sharvin_th,'.--','linewidth',1,'markersize',10);
        plot(Xo,(Yo2+fliplr(Yo2))/2./R_sharvin_th,'.--','linewidth',1,'markersize',10);
        grid
        TXYL('Contact Resistance (Symmetrized)','W/Rc','R/R_{Quantum}','Right','Left')
    end
    %% MR and Sharvin(vs w/rc) at different BGs
    for i = 1:4
LoadScan(946.01+i-1);
Beta_HS15_Hset = BreakIData(Beta_HS15_Hset);
BetaSum =0.51;
R = Beta_HS15_Hset/BetaSum.*AVS15./mean(IgrAC15.data(:));


R = CenterIDataAxis(R,[1],1e6)
figure(113); clf
plot_with_color(R,1);

 [dRdX,Sharvin_Right,Sharvin_Left,Vsd]=AnalyzeX_Cut(RangeMid,RangeRight,RangeLeft,InterpolationPoints);
 n=abs(Vbg_to_n(Info.Channels.BG,0.7)); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    
    r_c=p_F./qe_./B;
    
    W_Rc=5e-6./r_c
    figure(114)
    subplot(2,2,i)
 plot(W_Rc,(dRdX+fliplr(dRdX))/2*5e-6*1e6,'o--')
 RR(i,:)=(dRdX+fliplr(dRdX))/2*5e-6*1e6;
 legend(num2str(Info.Channels.BG));
 TXYL(' ','w/rc','\rho_{xx}');
 
 figure(115);
 subplot(2,2,i);
 R_sharvin_th =h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(Info.Channels.BG,0.7)))/5.0e-6;
 plot(W_Rc,(Sharvin_Right+fliplr(Sharvin_Right))/2./R_sharvin_th ,W_Rc,(Sharvin_Left+fliplr(Sharvin_Left))/2./R_sharvin_th,'o--');
 legend(num2str(Info.Channels.BG));
 TXYL(' ','w/rc','R_{contact}/R_{Sharvin}');
 
 SR(i,:) =(Sharvin_Right+fliplr(Sharvin_Right))/2./R_sharvin_th;
 SL(i,:) = (Sharvin_Left+fliplr(Sharvin_Left))/2./R_sharvin_th;
end

end
%% High quality 2D around a contact:
if(1)
    %% 	BG=9.9, W/Rc=0,1,1.5,2,3,4,6
    ScanNameList={'XY_BG_Electrons_one_contacts','XY_BG_Holes_one_contacts'};
    
    for j=1:numel(ScanNameList)
        ScanName=ScanNameList{j};
        figure(6+j);clf;colormap(jet(16));shading flat
        suptitle(ScanNameList{j}(7:end))
        
        for i=1:numel(Scans.(ScanName))/2+1
            subplot(3,3,i)
            UnPackScan(Scans.(ScanName){2*i-1})
            Beta_Sum=0.51;
            R = CenterIDataAxis(BreakIData(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15),[1,2],1e6);
            WW=WorkPoint+Vg_loc;
            pcolor(R);shading flat;axis equal;
            hold on
            contour(R,20,'LineColor','k');
            TXYL(sprintf('R [ohm], W/Rc=%0.2f',SuperAxis.(ScanName).W_Rc(2*i-1)),'X(scaled) [um]','Y [um]');
            ScaleColorFromIData(R.roi(1,-7,7))
            grid on
            
        end
    end
    %%  BG=-8.5, W/Rc=0,1,1.5,2,3,4,6
    %% 	BG= theoretical half collimation
    %%  BG= - theoretical half collimation
    
end
%% XY_Profile for electrons or holes at few W_Rc
for k=1:2
figure(9+k);clf;colormap(jet)
ScanNameList={'XY_BG_Electrons','XY_BG_Holes'};
W_RcList=[1,2,4,6];
Beta_Sum = 0.51;
for J=1:k+2
    try
     subplot(2,2,J);cla
    ScanName=ScanNameList{k};
    UnPackScan(Scans.(ScanName){J*2});Beta_Sum=0.51;
    R1 = CenterIDataAxis(BreakIData(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15),[1,2],1e6);
    UnPackScan(Scans.(ScanName){J*2+1});Beta_Sum=0.51;
    R2 = CenterIDataAxis(BreakIData(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15),[1,2],1e6);
    Data=R1-R2;Data=Data.roi(1,-7.0,7.0);Data=Data.roi(2,-2.5,2.5)
    for i=1:size(Data,1)
       Data.data(i,:)=smooth( Data.data(i,:),3);
    end
    for i=1:size(Data,2)
       Data.data(:,i)=smooth( Data.data(:,i),3);
    end
Sign=sign(mean(mean(diff(Data.data'),1),1));
    surf(Sign.*diff(Data.data'));
TXYL(['W/Rc=',num2str(W_RcList(J))],'X[um]','Y[um]')
Ax=gca;Ax.DataAspectRatio(1)=1;Ax.DataAspectRatio(2)=Ax.DataAspectRatio(1);
    end
    suptitle(ScanNameList{k}(7:end))
end
end

%% Profiles
if(1)
    %Multi sided bridge where Lmr=1.5*W and W/Rc=1.6?
    
    %% 2D Profiles of EY for electrons and holes:
for k=1:2
    figure(10+k);clf;colormap(jet)
    ScanNameList={'XY_BG_Electrons','XY_BG_Holes'};
    W_RcList=[1,2,4,6];
    for J=1:4
        try
            subplot(2,2,J);cla
            ScanName=ScanNameList{k};
            UnPackScan(Scans.(ScanName){J*2});Beta_Sum=0.51;
            R1 = CenterIDataAxis(BreakIData(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15),[1,2],1e6);
            UnPackScan(Scans.(ScanName){J*2+1});Beta_Sum=0.51;
            R2 = CenterIDataAxis(BreakIData(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15),[1,2],1e6);
            Data=R1-R2;Data=Data.roi(1,-7.0,7.0);Data=Data.roi(2,-2.5,2.5)
            for i=1:size(Data,1)
                Data.data(i,:)=smooth( Data.data(i,:),3);
            end
            for i=1:size(Data,2)
                Data.data(:,i)=smooth( Data.data(:,i),3);
            end
            
            Sign=sign(mean(mean(diff(Data'),1),1));
            surf(Sign.*diff(Data'));
            TXYL(['W/Rc=',num2str(W_RcList(J))],'X[um]','Y[um]')
            Ax=gca;Ax.DataAspectRatio(1)=1;Ax.DataAspectRatio(2)=Ax.DataAspectRatio(1);
        end
        suptitle(ScanNameList{k}(7:end))
    end
end
    %% 2D Profiles of EY as functio of BG for W/Rc=1.6
    Shift=23;
figure(13);clf
for i=1:23;

UnPackScan(Scans.XY_Center_BG_Wrc{i});Beta_Sum=0.5;R=Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15;
R1=R.mean(1);
UnPackScan(Scans.XY_Center_BG_Wrc{i+Shift});Beta_Sum=0.5;R=Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15;
R2=R.mean(1);
R1.data=smooth(R1.data,3)
R2.data=smooth(R2.data,3)
RHall=diff(R1-R2);
RHall=RHall*sign(mean(RHall));

plot(CenterIDataAxis(RHall,1,1e6),'o','linewidth',3)
hold on
end;
ChangePlotColor(jet)
 Fit_Line(2,'X',[-1.5,1.5])
 clf
Curv=-[Fit.a]./[Fit.dY_0]*(5)^2;
plot(BGList,Curv,'.','markersize',25);grid;ylim([-1.5,3]);xlim([-9.8,9.9])
TXYL('Curveture -a/c*W^2 @ W/Rc=1.6', 'BG','Curveture')

end
%% more figures from the previous session:
if(1)
%% Lmr as function of BG:

figure(14);clf
List=405:411;
W_RcList=[4,-4,2,-2,1,-1,0];
Phi={};
for i=1:numel(List)
    LoadScan(List(i))
    Phi{i}=(Beta_HS15_Hset-Beta_HS2_Hset)./(2*(Beta_HS15_Hset+Beta_HS2_Hset))+0.5;
end
%

Symmetrized={};
Symmetrized{1}=Phi{7};
Symmetrized{2}=(Phi{5}+Phi{6})/2;
Symmetrized{3}=(Phi{4}+Phi{3})/2;
Symmetrized{4}=(Phi{2}+Phi{1})/2;
%
Slopes=[];RhoXX=[];
for i=1:4
set(gca, 'ColorOrder', hsv(21), 'NextPlot', 'replacechildren');
plot(Symmetrized{i},'linewidth',1);
RhoXX(i,:)=Fit_Line(1,'X',[-2.5e-5,-1.6e-5],'a')*Info.Channels.AVS15./IgrAC15.mean(1).data;
end
% 1/RhoXX
for i=1:4
% plot(RampBG,h_./(8*qe_^2*sqrt(pi*abs(Vbg_to_n(RampBG',0.7))))./(RhoXX(1,:)*5e-6)','o-','linewidth',3)
plot(Vbg_to_n(RampBG',0.7)*1e-4,1./(RhoXX(i,:)*5e-6)','o-','linewidth',3)
hold on
end
hold off
TXYL('scans 405:411,$\frac{1}{\rho_{xx}}$','n [$\frac{1}{cm}$]','$\frac{1}{\rho_{xx}} [\frac{1}{Ohm}]$',...
    '$\\frac{w}{R_c}=%d$',{[0,1,2,4]})
% title('scans 405:411,$l_{mr}=\frac{h}{2e^2\rho_{xx}\sqrt{\pi n}}$','Interpreter','latex','fontsize',24)
set(gca,'FontSize',24)
xlim(Vbg_to_n([-9.8,9.2],0.7)*1e-4);
% Lmr:
for i=1:4
% plot(RampBG,h_./(8*qe_^2*sqrt(pi*abs(Vbg_to_n(RampBG',0.7))))./(RhoXX(1,:)*5e-6)','o-','linewidth',3)
plot(RampBG',1e6*h_./(2*qe_^2*sqrt(pi*abs(Vbg_to_n(RampBG',0.7))))./(RhoXX(i,:)*5e-6)','o-','linewidth',3)
hold on
end
hold off
TXYL('$ $scans 405:411,$l_{mr}=\frac{h}{2e^2\rho_{xx}\sqrt{\pi n}}$','$ $BG [V]','$l_{mr} [\mu m]$',...
    '$\\frac{w}{R_c}=%d$',{[0,1,2,4]})
% title('scans 405:411,$l_{mr}=\frac{h}{2e^2\rho_{xx}\sqrt{\pi n}}$','Interpreter','latex','fontsize',24)
set(gca,'FontSize',24)

% xlim(Vbg_to_n([-9.8,9.2],0.7)*1e-4);
end