classdef Keithley2400_Driver < handle
    properties
        Communication
        Compliance_I
        Compliance_V
        Range_I
        Range_V
        RAW
       % SourceMode %I or V
        ActiveSet
        Tic
    end
    %M3
    methods
        function obj=Keithley2400_Driver(ComPort)
            BaudRate=38400;
            if(isnumeric(ComPort))
                obj.Communication=serial(['COM',num2str(ComPort)],'BaudRate',BaudRate);
            else
                obj.Communication=serial(ComPort,'BaudRate',BaudRate);
            end
            fopen(obj.Communication);
            obj.ActiveSet=0;
            obj.Compliance_V=obj.Query('SENS:VOLT:PROT:LEV?\n');
            obj.Compliance_I=obj.Query('SENS:CURR:PROT:LEV?\n');
            obj.ActiveSet=1;
            %obj.SourceMode='?';
            disp('Connection to Keithly2400 OK!');
            obj.Tic=tic;
        end
        function Init(obj)
            fprintf(obj.Communication,'SENS:CURR:RANG:AUTO OFF');
            fprintf(obj.Communication,'SOUR:VOLT:RANG:AUTO OFF');
        end
        function out=Get(obj,Channel)
            
            flushinput(obj.Communication);
            fprintf(obj.Communication,'READ?\n');
            obj.RAW=fgets(obj.Communication);
            
            names=regexp(Channel,'_','split');
            SplitRaw=regexp(obj.RAW,',','split');
            switch names{end}
                case 'I'
                    out=str2double(SplitRaw{2});
                case 'V'
                    out=str2double(SplitRaw{1});
                otherwise
                    error('Keithley 2400 channel name must end with _I/V');
            end
            obj.Tic=tic;
        end
        function Set(obj,Channel,Value)
            names=regexp(Channel,'_','split');
            switch names{end}
                case 'V'
                    %if(Value<=obj.Range_V)
                        fprintf(obj.Communication,'SOUR:VOLT:LEV %E\n',Value);
                    %else
                    %   error(sprintf('Keithley2400 voltage cant be above range! %e>%e',Value,obj.Range_V))
                    %end
                case 'I'
                    if(Value<=obj.Range_I)
                        fprintf(obj.Communication,'SOUR:CURR:LEV %E\n',Value);
                    else
                        error(sprintf('Keithley2400 current cant be above range! %e>%e',Value,obj.Range_I))
                    end
            end
            obj.Tic=tic;
        end
       function out=Toc(obj)
            out=toc(obj.Tic);
        end
%         function set.SourceMode(obj,Value)
%             if(obj.ActiveSet)
%                 fprintf(obj.Communication,'SOUR:FUNC?\n');
%                 Input=fgets(obj.Communication);
%                 obj.SourceMode=Input(1);
%                 fprintf('SourceMode is %s\n',obj.SourceMode);
%             else
%                 obj.SourceMode=Value;
%             end
%         end
        function set.Compliance_I(obj,Value)
            if(obj.ActiveSet)
                fprintf(obj.Communication,'SENS:CURR:PROT %E\n',Value);
                Input=obj.Query('SENS:CURR:PROT:LEV?\n');
                if(Input==Value)
                    obj.Compliance_I=Input;
                    fprintf('Compliance_I set OK %e\n',obj.Compliance_I);
                else
                    error(sprintf('Problem setting Compliance_I! %e != %e',str2num(obj.RAW),Value));
                end
            else
                obj.Compliance_I=Value;
            end
        end
        function set.Compliance_V(obj,Value)
            if(obj.ActiveSet)
                fprintf(obj.Communication,'SENS:VOLT:PROT %E\n',Value);
                Input=obj.Query('SENS:VOLT:PROT:LEV?\n');
                if(Input==Value)
                    obj.Compliance_V=Input;
                    fprintf('Compliance_V set OK %e\n',obj.Compliance_V);
                else
                    error(sprintf('Problem setting Compliance_V! %e != %e',str2num(obj.RAW),Value));
                end
            else
                obj.Compliance_V=Value;
            end
        end
        function Input=Query(obj,Command)
            fprintf(obj.Communication,Command);
            obj.RAW=fgets(obj.Communication);
            Input=str2num(obj.RAW);
        end
        function set.Range_I(obj,Value)
            if(obj.ActiveSet)
                Input1=obj.Query(sprintf('SOUR:CURR:RANG %E\n SOUR:CURR:RANG? \n',Value));
                Input2=obj.Query(sprintf('SENS:CURR:RANG %E\n SENS:CURR:RANG? \n',Value));
                if(Input1==Input2)
                    obj.Range_I=Input1;
                    fprintf('Range_I is set to %e\n',obj.Range_I);
                else
                    Input1
                    Input2
                    error('Problem Keithley Range!');
                end
            else
                obj.Range_I=Value;
            end
            
        end
        function set.Range_V(obj,Value)
            if(obj.ActiveSet)
                Input1=obj.Query(sprintf('SOUR:VOLT:RANG %E\n SOUR:VOLT:RANG? \n',Value));
                Input2=obj.Query(sprintf('SENS:VOLT:RANG %E\n SENS:VOLT:RANG? \n',Value));
                if(Input1==Input2)
                    obj.Range_V=Input1;
                    fprintf('Range_V is set to %e\n',obj.Range_V);
                else
                    error('Problem Keithley Range!');
                end
            else
                obj.Range_V=Value;
            end
        end

        function RAW=GetRaw(obj)
            flushinput(obj.Communication);
            fprintf(obj.Communication,'READ?\n');
            RAW=fgets(obj.Communication);
            obj.RAW=RAW;
        end
        
        function delete(obj)
            disp('DELETING KEITHLY2400!');
            fclose(obj.Communication);
            try delete(obj);end
        end
        
    end
end