%%
while(1)
    TelegramGetLoop()
    try
    LoadScan();
    PlotScan();
    TelegramSend(3)
end
pause(5)
end
                                                                                                                                                                                    %% Last plateau -8.5

                                                                                                                                                                                    LoadScan(395);Beta_Sum=358.492e-3;

                                                                                                                                                                                    R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
                                                                                                                                                                                    R1A=R1;
                                                                                                                                                                                    R1A.data=-(R1.data-fliplr(R1.data))/2;
                                                                                                                                                                                    R1A_RC=(R1A)./(B./abs(Vbg_to_n(-8.5,0.7)*qe_))
                                                                                                                                                                                    %clf
                                                                                                                                                                                     plot([0,linspace(0.25,5,40)],R1A.roi(1,0,1).data,'.--','MarkerSize',15);
                                                                                                                                                                                    %plot([0,linspace(0.25,5,40)],R1A_RC.roi(1,0,1).data,'.--','MarkerSize',15);
                                                                                                                                                                                    %plot(R1,'.--','MarkerSize',10);
                                                                                                                                                                                    TXYL('Last Plateau','W/Rc','R [Ohm]')

                                                                                                                                                                                    %% Last plateau 9.9

                                                                                                                                                                                    LoadScan(396);Beta_Sum=358.492e-3;

                                                                                                                                                                                    R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
                                                                                                                                                                                    R1A=R1;
                                                                                                                                                                                    R1A.data=(R1.data-fliplr(R1.data))/2;
                                                                                                                                                                                    R1A_RC=R1A./(B./(Vbg_to_n(9.9,0.7)*qe_))
                                                                                                                                                                                    %clf
                                                                                                                                                                                     plot([0,linspace(0.25,5,40)],R1A.roi(1,0,1).data,'.--','MarkerSize',15);
                                                                                                                                                                                    %plot([0,linspace(0.25,5,40)],R1A_RC.roi(1,0,1).data,'.--','MarkerSize',15);
                                                                                                                                                                                    TXYL('Last Plateau Electron side','W/Rc','R [Ohm]')
%%
figure(2)
% while(1)
    LoadScan(387);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
    LoadScan(388);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
    plot(R1-R2)
%     hold on
%     plot(R2)
%     hold off
%     pause(10)
% end
%%
figure(1)
LoadScan(385);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(386);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
 plot(-R1+R2)

 figure(2)
% plot(R2-R1)
%%
title('Scan 385 386 369 370  W/Rc=1,BG=-9.9/-3.5');
%%
LoadScan(371);Beta_Sum=358.492e-3;RH1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
 LoadScan(372);Beta_Sum=358.492e-3;RH2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
% RHS=RH1-RH2;plot(RHS)
% pcolor(RH2)
%
RangeX=[-26,-14]*1e-6;
RangeY=[-20,-17.5]*1e-6;
plot(RH2.roi(1,RangeX(1),RangeX(2)).roi(2,RangeY(1),RangeY(2)).mean(2));
FixX=Fit_Line(1);

plot(RH2.roi(1,RangeX(1),RangeX(2)).roi(2,RangeY(1),RangeY(2)).mean(1));
FixY=Fit_Line(1);

New=RH2*1;
X=New.axes{1}.data;
Y=New.axes{2}.data;

% FixX.fit(1)=FixX.fit(1)+0.3e6;
% FixY.fit(1)=FixY.fit(1)-0.3e6;

New.data=New.data-repmat(FixX.Func(X)',1,size(New,2))-repmat(FixY.Func(Y),size(New,1),1)
pcolor(New.roi(1,-27e-6,-14e-6))
%  caxis([-10,-3])
axis equal
colormap(jet)
%%
surf(New)
Ax=gca;
Ax.DataAspectRatio(1)=30;Ax.DataAspectRatio(2)=Ax.DataAspectRatio(1);

% zlim([-10,0]);caxis([-10,0])
zlim([-5,2]);caxis(zlim)
%%
RH2
%%
for i = 1:size(RH2,2)
figure(4);clf; 
plot(RH2.slice_index(2,i))
 fit = Fit_Line(1,'X',[-2.6,-1.4]*1e-5); 
 RH2.data(:,i) = RH2.data(:,i) - fit.Func(RH2.axes{1}.data)';

end
figure(3)
clf
pcolor(RH2)


%%
LoadScan(383);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(384);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
plot(R1-R2)
%%
LoadScan(369);Beta_Sum=358.492e-3;RH1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(370);Beta_Sum=358.492e-3;RH2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;

LoadScan(375);Beta_Sum=358.492e-3;RE1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(376);Beta_Sum=358.492e-3;RE2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;

figure(98);
contour(RH1,40);
figure(1);clf
subplot(2,1,1)
contour(RH1-RH2,40)
title({'Antisymmetrized Scan 369,370 BG=-3.6','MagneticField = +-12mT, W/Rc=1'});
subplot(2,1,2)
contour(RE1-RE2,40)
title({'Antisymmetrized Scan 375,376 BG=5','MagneticField = +-12mT, W/Rc=1'});
hold off
%%
clf
subplot(3,2,1)
LoadScan(369);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(370);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
pcolor(R1-R2);axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 369,370 BG=-3.6','MagneticField = +-12mT, W/Rc=1'});
subplot(3,2,3)
LoadScan(371);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(372);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
pcolor(R1-R2);axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 371,372 BG=-3.6','MagneticField = +-24mT, W/Rc=2'});
subplot(3,2,5)
LoadScan(373);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(374);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
pcolor(R1-R2);axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 373,374 BG=-3.6','MagneticField = +-48mT, W/Rc=4'});
subplot(3,2,2)
LoadScan(375);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(376);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
pcolor(R1-R2);axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 375,376 BG=5','MagneticField = +-12mT, W/Rc=1'});
subplot(3,2,4)
LoadScan(377);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(378);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
pcolor(R1-R2);axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 375,376 BG=5','MagneticField = +-24mT, W/Rc=1'});
colormap(jet)
%%
List=369:376;
clf
for i=List(2:end)
LoadScan(i);plot(WorkPoint.slice_index(2,1)); hold on
end
%%
clf
subplot(3,2,1)
LoadScan(369);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(370);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
surf(R1-R2);%axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 369,370 BG=-3.6','MagneticField = +-12mT, W/Rc=1'});
subplot(3,2,3)
LoadScan(371);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(372);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
surf(R1-R2);%axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 371,372 BG=-3.6','MagneticField = +-24mT, W/Rc=2'});
subplot(3,2,5)
LoadScan(373);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(374);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
surf(R1-R2);%axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 373,374 BG=-3.6','MagneticField = +-48mT, W/Rc=4'});
subplot(3,2,2)
LoadScan(375);Beta_Sum=358.492e-3;R1=-Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(376);Beta_Sum=358.492e-3;R2=-Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
surf(R1-R2);%axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 375,376 BG=5','MagneticField = +-12mT, W/Rc=1'});
subplot(3,2,4)
LoadScan(377);Beta_Sum=358.492e-3;R1=-Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(378);Beta_Sum=358.492e-3;R2=-Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
surf(R1-R2);%axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 375,376 BG=5','MagneticField = +-12mT, W/Rc=1'});
colormap(jet)
%%
clf
subplot(3,2,1)
LoadScan(369);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(370);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
contour(R1-R2,50);axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 369,370 BG=-3.6','MagneticField = +-12mT, W/Rc=1'});
subplot(3,2,3)
LoadScan(371);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(372);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
contour(R1-R2,50);axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 371,372 BG=-3.6','MagneticField = +-24mT, W/Rc=2'});
subplot(3,2,5)
LoadScan(373);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(374);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
contour(R1-R2,50);axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 373,374 BG=-3.6','MagneticField = +-48mT, W/Rc=4'});
subplot(3,2,2)
LoadScan(375);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(376);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
contour(R1-R2,50);axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 375,376 BG=5','MagneticField = +-12mT, W/Rc=1'});
subplot(3,2,4)
LoadScan(377);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(378);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
contour(R1-R2,50);axis equal;ylim([-21e-6,-16.5e-6]);
title({'Antisymmetrized Scan 375,376 BG=5','MagneticField = +-12mT, W/Rc=1'});
hold on
plot([-12.2e-6,-12.2e-6],[-20.9,-15.9]*1e-6,'k')
plot([-28.8e-6,-28.8e-6],[-20.9,-15.9]*1e-6,'k')
plot([-12.2e-6,-28.8e-6],[-20.9,-20.9]*1e-6,'k')
plot([-12.2e-6,-28.8e-6],[-15.9,-15.9]*1e-6,'k')
hold off
%%
LoadScan(352);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(353);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
%% Hall effect Y cut
clf
LoadScan(363);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;plot(diff(R1));hold on
LoadScan(361);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;plot(diff(R1)*4)
% LoadScan(362);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
%%

n=abs(Vbg_to_n(-9.9,0.7)); %units of m-2
    p_F=hbar_*sqrt(pi*n);
%     B = 17.4e-3*
    r_c=p_F./qe_./B;
    W_Rc=5e-6./r_c
    B/120e-3*10
%% Hall effect Y cut
LoadScan(361);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
LoadScan(362);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
%%
clf;plot(R1,'linewidth',3);hold on;plot(R2,'linewidth',3);
legend('W/Rc=1 [B=-17.3mT]','W/Rc=-1 [B=-17.3mT]')
ylabel('R Hall [Ohms]')
title('Scans 361,362')
%%
clf;plot(diff(R1-R2)/2*130e-9,'linewidth',3);
legend('W/Rc=1 [B=-17.3mT]','W/Rc=-1 [B=-17.3mT]')
ylabel('Ey')
title('Scans 361,362')
%%
plot(R1-R2)
%%
while(1);LoadScan(352);PlotScan();pause(10);end
%%
subplot(3,1,2)
LoadScan(351);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15; contour(R1,60);colormap(jet);axis equal;title('W/Rc=0 Scan 351');
subplot(3,1,1)
LoadScan(352);Beta_Sum=358.492e-3;R1=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15; contour(R1,60);colormap(jet);axis equal;title('W/Rc=-1 Scan 352');
subplot(3,1,3)
LoadScan(353);Beta_Sum=358.492e-3;R2=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15; contour(R2,60);colormap(jet);axis equal;title('W/Rc=1 Scan 353');
%% High resulution 2D images
LoadScan();figure(1);clf

%R=((Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5).*AVS15./IgrAC15; % non bridge
Beta_Sum=358.492e-3;R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15; %  bridge
 plot(R.slice_index(2,3)+210)
 hold on
%  
% hold on
% LoadScan(343);
% R=((Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5).*AVS15./IgrAC15;
% plot_with_color(R,1,@jet)
% hold on
%
LoadScan(344);
R=((Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5).*AVS15./IgrAC15;
% plot_with_color(R,1,@jet)
 plot(R.slice_index(2,3))
hold off
%%
figure()
LoadScan(348);figure(1);
R=Beta_HS15_Hset.*AVS15./IgrAC15;
plot_with_color(R,1,@jet)
hold off
%% Sharvin resistance
LoadScan(308);clf
figure(1);
R=((Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5)*(2.5e-3)./IgrAC15;
XLocation=[-11.3,-29.3]*1e-6;
plot_with_color(R,1,@jet)
%
[Slope,Extrapulate]=Fit_Line(1,'X',[-26.4,-15]*1e-6,'a','Eval',XLocation);
Top=Fit_Line(1,'X',[XLocation(1),XLocation(1)+1e-6],'AVR');
Bot=Fit_Line(1,'X',[XLocation(2)-1e-6,XLocation(2)],'AVR');

R_sharvin_th = h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(RampBG,0.7)))/5.0e-6;

figure(3);
plot(RampBG,Top-Extrapulate(1,:),'o--','linewidth',3);  hold on
plot(RampBG,-(Bot-Extrapulate(2,:)),'o--','linewidth',3)
plot(RampBG,R_sharvin_th,'o--','linewidth',3);          hold off
ylim([0,200]);title('Contact resistance of the 5um channel, scan 308');
xlabel('BG');ylabel('R [Ohm]')
figure(2);
plot(RampBG,Slope*5e-6,'o--','linewidth',3);
ylim([0,50]);xlabel('BG');ylabel('R_{per square} [Ohm]')
%% magneto resistance
List=332:336
figure(2);clf;
for i=1:numel(List)
LoadScan(List(i),L)
figure(1)
    n=abs(Vbg_to_n(Info.Channels.BG,0.7)); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    r_c=p_F./qe_./B;
    W_Rc=5e-6./r_c;
    
R=((Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5)*(2.5e-3)./IgrAC15;
XLocation=[-11,-30]*1e-6;
figure(1);clf
plot_with_color(R,1,@jet)
% plot_with_color(((Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5)*(2.5e-3),1,@jet);
%
[Slope,Extrapulate]=Fit_Line(1,'X',([-5,5]-20)*1e-6,'a','Eval',XLocation);
Top=Fit_Line(1,'X',[XLocation(1),XLocation(1)+2e-6],'AVR');
Bot=Fit_Line(1,'X',[XLocation(2)-2e-6,XLocation(2)],'AVR');

R_sharvin_th = h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(Info.Channels.BG,0.7)))/5.0e-6;

figure(i+1);
subplot(2,1,1)
%plot(B,Top-Extrapulate(1,:),'o--','linewidth',3);  hold on
SharvinJump=0.5*((Top-Extrapulate(1,:))-(Bot-Extrapulate(2,:)));
SharvinJump=0.5*(SharvinJump+fliplr(SharvinJump));
plot(W_Rc,SharvinJump,'-','linewidth',3);hold on
% plot(W_Rc,R_sharvin_th*ones(size(B)),'-','linewidth',3);          hold off
% ylim([0,60]);
title(sprintf('Contact resistance of the 5um channel, scan %d, BG=%0.1f',Info.ScanNumber,Info.Channels.BG));
xlabel('W/Rc');ylabel('R [Ohm]');
Ax=gca;Ax.XTick=[-10:-1,-0.5,0,0.5,1:10];grid
subplot(2,1,2)
plot(W_Rc,0.5*(Slope+fliplr(Slope))*5e-6,'-','linewidth',3);
title(sprintf('RhoXX, scan %d, BG=%0.1f',Info.ScanNumber,Info.Channels.BG));
%ylim([0,20]);
xlabel('W/Rc');ylabel('R_{per square} [Ohm]');
Ax=gca;Ax.XTick=[-10:-1,-0.5,0,0.5,1:10];grid
end
%%
while(1)
LoadScan(14,L)
Plot1D_with_color(1,'Idc','Hset','Rgr15')
figure(2)
pcolor(Idc.slice_index(3,1))
colormap jet
figure(3)
pcolor(Hset.slice_index(3,1))
colormap jet
pause(5)
end
%%
LoadScan(11,L)
figure(99)
PlotScan()
%%

figure(4)
SNR=Hset.slice_index(1,1)*1;
SNR.data=(squeeze(mean(Hset.data,1)./std(Hset.data,1))/sqrt(0.2)/5e-3);
% plot_with_color(SNR,1,@jet)
pcolor(SNR)
colormap(jet(128))
%%
VsdList=Info.Scan.Axis.Vs_Ramp;
CombinedPeakData=[];
for i=1:size(Hset,2)
HData=Hset.mean(3).slice_index(2,i);
[Y,X,W,P]=findpeaks(HData.data,HData.axes{1}.data);[~,IDX]=sort(P);IDX=fliplr(IDX);PositivePeakData=[X(IDX(1:10))',Y(IDX(1:10))'];
[Y,X,W,P]=findpeaks(-HData.data,HData.axes{1}.data);[~,IDX]=sort(P);IDX=fliplr(IDX);NegativePeakData=[X(IDX(1:10))',-Y(IDX(1:10))'];
PeakData=[PositivePeakData;NegativePeakData];
[~,IDX]=sort(PeakData(:,1));IDX=flip(IDX);
PeakData=PeakData(IDX,:);
    plot(HData);
    hold on
    plot(PeakData(:,1),PeakData(:,2),'o');
    %hold off
    CombinedPeakData(:,i)=PeakData(:,1);
end
hold off
%%

figure(9);
LoadScan(24,L);
plot(Idc,'linewidth',3);
hold on
LoadScan(25,L);
plot(Idc,'linewidth',3);
hold off
AddTitle('Graphene BG Change, shift 5.6mV')
legend('-2V','+2V')
%%
LoadScan(34,L);
plot(Hbg.data,Hset.data,'o');
Fit_Line(1)
xlabel('Hbg');ylabel('Hset');
Title('Inverse Beta')
%%
LoadScan(40,L);
PlotScan();
AddTitle();
%%
plot_with_color(Rgr15,1,@jet)
%%
figure(8)
LoadScan([],L)
% plot_with_color(1./((LowNoise/1e8)./Hset),1,@jet)
plot(1./((LowNoise/1e8)./Hset));AddTitle
%%
figure(9);
LoadScan(102,L);
plot(Idc,'linewidth',3);
hold on
LoadScan(103,L);
plot(Idc,'linewidth',3);
LoadScan(104,L);
plot(Idc,'linewidth',3);
LoadScan(105,L);
plot(Idc,'linewidth',3);
hold off
AddTitle('Graphene BG Change, shift 5.6mV')
legend('0','+1V','+2V','-2V')

%%
clf
LoadScan([114],L);plot(Hbg.data,Hset.data,'.','markersize',10);
hold on
LoadScan([115],L);plot(Hbg.data,Hset.data,'.','markersize',10);
LoadScan([116],L);plot(Hbg.data,Hset.data,'.','markersize',10);
hold off
%%
while(1)
    try
        LoadScan([],L)
        PlotScan();
        AddGDS(GDS,[1e-6*0.9,0,0,0.9*1e-6,4.01e-3,2.085e-3],0)
    end
    
    pause(5);
end

        %%
        while(1)
            try
        LoadScan([],L)
         PlotScan();

%pcolor((Beta_Hbg_Hset+Beta_HRest_Hset))
colormap(jet)

%  AddGDS(GDS,[1e-6*0.9,0,0,0.9*1e-6,4e-3,2.07e-3],1)
AddGDS(GDS,[1e-6*1,0,0,0.9*1e-6,4.0115e-3,2.09e-3],1)

            end
            pause(5); end 
        
   
        %%
               %while(1);
            
                clf
        LoadScan([],L)
         PlotScan();

%pcolor((Beta_Hbg_Hset+Beta_HRest_Hset))
colormap(jet)

%  AddGDS(GDS,[1e-6*0.9,0,0,0.9*1e-6,4e-3,2.07e-3],1)
%   AddGDS(GDS,[1e-6*1,0,0,0.9*1e-6,-50e-6,-10e-6],1)

%             end
%             pause(5)
%         end

%%
clf
while(1)
    
        LoadScan([],L)
        PlotScan();
%         AddGDS(GDS,[1.2e-6*1,0,0,1*1e-6,-57e-6-5.5e-6,4.2e-6],0)
%         F=gcf;
% Children=F.Children;
% for i=1:numel(Children)
%      if(strcmp(class(Children(i)),'matlab.graphics.axis.Axes'))
%         ylim(Children(i),[-22,-10]*1e-6);
%      end
% end
        figure(98);
        plot_with_color((Beta_HS15_Hset-Beta_HS2_Hset)./Beta_Sum,1);
        
         figure(99);
         plot_with_color(Beta_HRest_Hset,2);
    pause(5)
 end
%%
 List=[793:798]
% List=[]
figure(9);clf
for i=List
   LoadScan(i,L);
   plot(1./Beta_Sum)
   hold on
end
hold off
% xlim([0,30e-6]);
Fit_Line();
%%
while(1)
    LoadScan([],L)
figure(7)
Phi=(Beta_HS15_Hset-Beta_HS2_Hset./(Beta_Sum));
plot(Phi)
pause(5)
end

%%
List=238:243
figure(10);clf
for i=List
   LoadScan(i,L);
   plot(Hset./LowNoise)
   hold on
end
hold off
%%
List=[244,245,246]
figure(10);clf
for i=List
   LoadScan(i,L);
   plot(Hset./LowNoise)
   hold on
end
hold off
%%
% while(1)
clf
LoadScan([474],L)
figure(7)
surfc(((Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5)*(7.5e-3)./IgrAC15);
%surfc(WorkPoint);
colormap(jet);
title('')
AddTitle(sprintf('BG=%0.1f',Info.Channels.BG))
%  pause(10);
% end
%%
LoadScan(308);
clf
R=((Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5)*(2.5e-3)./IgrAC15;

i=1;
clf
R_Per_SquereList=[];
TopJump=[];
BotJump=[];
XLocation=[-13.5,-35]*1e-6;
 for i=1:size(R,2)
   RSliced=slice_index(R,2,i);
   plot(RSliced)
   Xlim=xlim;
  
xlim([-3.15e-05,-1.79e-05]);
Slope=Fit_Line(1);
R_Per_Squere=Slope.a*5e-6;
R_Per_SquereList(i)=R_Per_Squere;
xlim([XLocation(1),XLocation(1)+1e-6]);
Top=Fit_Line(1);
TopList=Top.AVR

ExtrapulatedTop=polyval(Slope.fit,[XLocation(1)]);
TopJump(i)=ExtrapulatedTop-TopList;
xlim([XLocation(2)-1e-6,XLocation(2)]);
Bot=Fit_Line(1);
BotList=Bot.AVR

ExtrapulatedBot=polyval(Slope.fit,[XLocation(2)]);
BotJump(i)=ExtrapulatedBot-BotList;
xlim(Xlim)
 end
 %
%  plot(BG.slice_index(1,1).data,R_Per_SquereList,'o--')
clf
 plot(RampBG,-TopJump,'o--','linewidth',3)
 hold on
 plot(RampBG,BotJump,'o--','linewidth',3)
 %
 R_sharvin_th = h_./(4*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(RampBG,0.7)))/5.0e-6;
%  figure()
 plot(RampBG,R_sharvin_th,'o--','linewidth',3)
 hold off
 ylim([0,200])
 title('Contact resistance of the 5um channel, scan 308');
 xlabel('BG')
 ylabel('R [Ohm]')
 %%
%  figure(11)
  plot(RampBG,R_sharvin_th-(BotJump-TopJump)/2,'o--','linewidth',3);
  ylabel('Addition to sharvin')
  xlabel('BG')
  ylim([-100,20]);
  hold on
  plot([-10,10],[0,0])
  grid
  title('Scan 308, the addiditon from expected sharvin')
  hold off
%%
plot_with_color(R,1,@jet);
Xlim=xlim;
ExtrapulatedTop=[];ExtrapulatedBot=[];
xlim([-3.15e-05,-1.79e-05]);
Slope=Fit_Line(1,'all');
R_Per_Squere=fliplr([Slope.a])*5e-6;
xlim(Xlim)
clf 
plot(RampBG,1./R_Per_Squere,'o--','linewidth',3);
%ylim([0,60])
title('4-probe Sigma Scan: 308');
ylabel('1./R_{per square} [1/Ohm]');
xlabel('BG [V]')
%%

Kf=sqrt(abs(pi*Vbg_to_n(RampBG,0.7)));
%plot(RampBG,h_./(2*qe_.^2.*Kf.*R_Per_Squere),'o--','linewidth',3);
plot(RampBG,1./R_Per_Squere,'o--','linewidth',3);
%ylim([0,40])
% xlabel('X
%%
xlim([-13.5,-12.5]*1e-6);
Top=Fit_Line(1,'all');
TopList=fliplr([Top.AVR]);
for i=1:numel(Top)
ExtrapulatedTop(i)=polyval(Slope(numel(Top)-i+1).fit,[-35e-6]);
end
xlim([-36,-35]*1e-6);
Bot=Fit_line(1,'all');
BotList=fliplr([Bot.AVR]);
for i=1:numel(Bot)
ExtrapulatedBot(i)=polyval(Slope(numel(Bot)-i+1).fit,[-13.5e-6]);
end

xlim(Xlim)

%%
i=15;
plot(R.slice_index(2,i),'linewidth',3);

ylabel('R [Ohm]')
AddTitle(sprintf('BG=%0.2f',RampBG(i)))
%%
pcolor(R)
%%
LoadScan([])
Phi = (Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5;
figure(3);
plot_with_color(Phi,1);
%%
%xlim = ([-3,-1.8]*1e-6);
w = 5e-6;
Rgr = (2.5e-3)./IgrAC15;
Fit_Line(1,'all');
dphi = [Fit.a];
rhoxx = dphi.*Rgr.*w;
figure(4);
% plot_with_color(rhoxx,2);
plot(rhoxx.slice_index(1,1),'o--','linewidth',2);
title('rhoxx')
ylabel('R (ohms)')
Kf=sqrt(abs(pi*Vbg_to_n(10,0.7)));
lmfp = 1./rhoxx/Kf/2/qe_^2*h_/um_
figure(5)
% plot_with_color(lmfp,2);
plot(lmfp.slice_index(1,1),'o--','linewidth',2);

title('lmfp')
ylabel('lmfp (um)')
%%
LoadScan(471)
figure(889);clf
Kf=sqrt(abs(pi*Vbg_to_n(BG,0.5)));

BetaSum =  0.1747;
NormBG = Beta_Hbg_Hset./BetaSum;

NormBG2 = Hbg./HS2 ;
dVdn = 1./ (Vbg_to_n(1,0.5)-Vbg_to_n(0,0.5));
%plot(Kf,(1./Beta_Hbg_Hset.data))
plot(Kf,1./NormBG2.data); hold on
Vf=1.2e6;
plot(Kf,dVdn./(pi*hbar_*Vf./(2*Kf*qe_)))
xlabel('kf');

hbar_*Vf*sqrt(pi)*(Vbg_to_n(1,0.5))







%%
plot(Kf,1./(hbar_*Vf*Kf))

xlabel('kf');
ylabel('Beta_Hbg_Hset/BetaSum');
figure(78); clf
plot(WorkPoint)


%%
B=Magnet_I*0.12/10; %field in Tesla
r_c=zeros(1,length(B));


for i=2:length(B)
    n=abs(Vbg_to_n(10,0.7)); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    r_c(i)=p_F/qe_/B(i);
end
%%
figure(90);plot(w./r_c,rhoxx.slice_index(1,1).data,'o--','linewidth',2);
title('rhoxx')
ylabel('R (ohms)')
xlabel('W/rc')
%legend('BG = 9.9','BG = -9.9')
%%

figure(91);plot(w./r_c,lmfp.slice_index(1,1).data,'o--','linewidth',2);
title('lmfp')
ylabel('lmfp (um)')
xlabel('W/rc')
%legend('BG = 9.9','BG = -9.9')
%%
  while(1);
     LoadScan();
    PlotScan();
    AddGDS(GDS,[1e-6*1,0,0,0.82*1e-6,-49e-6,-5e-6],0)
     pause(5)
  end
    %%
    clf
    pcolor(Beta_HS15_Hset)
    
    %%
    LoadScan(454);
    R = (0.5 + (Beta_HS15_Hset-Beta_HS2_Hset)./(2.*Beta_Sum));
    figure(45);
    %surfc(R);
    %contour(R,50);
    pcolor(R)
    colormap jet
    AddGDS(GDS,[1e-6*1,0,0,0.82*1e-6,-50e-6,-4e-6],0)
    %AddGDS(GDS,[1e-6*1,0,0,0.82*1e-6,-55e-6,0e-6],0)
%     pause(5);
% end
%%
clf
pcolor(WorkPoint*3*1000);axis equal
AddGDS(GDS,[1.05e-6*1,0,0,1.01*1e-6,-54e-6,-4.7e-6],0)
%%
LoadScan(486,L);
Phi = (Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5;
clf
plot(Phi)
%%
while(1)
clf
LoadScan([],L)
PlotScan();
%AddGDS(GDS,[1.05e-6*1,0,0,1.01*1e-6,-54e-6,-4.7e-6],0)
pause(10);
end
%%
LoadScan([],L)
Phi = (Beta_HS15_Hset+Beta_HS2_Hset)./(2*(Beta_HS15_Hset+Beta_HS2_Hset+Beta_HRest_Hset))+0.5;
plot_with_color(Phi,1,@jet)
%%
clf
LoadScan([],L)
surf((Beta_HS15_Hset-Beta_HS2_Hset)./(Beta_HS15_Hset+Beta_HS2_Hset))
%% Sharvin - 9-5 channel - fast  Scan 496
clf
LoadScan(496,L);
Phi = (Beta_HS15_Hset-Beta_HS2_Hset)./(2*(Beta_HS15_Hset+Beta_HS2_Hset))+0.5;
Phi.*10e-3
plot_with_color(Phi.*10e-3,1,@jet)
Ax=gca;
set(Ax.Children,'linewidth',2)

out1=Fit_Line(1,'X',[-21,-13.4]*1e-6);
out2=Fit_Line(1,'X',[-8.4,-0.35]*1e-6);
I=IgrAC15.mean(1).data;
for i=1:numel(out1)
    Expected(i)=out1(i).Func(-8.4e-6);
    Actual(i)=out2(i).Func(-8.4e-6);
    Delta(i)=Actual(i)-Expected(i);
    
end
R_Sharvin=Delta./I;

R_sharvin_th = h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(RampBG,0.5)))*(1/5.0e-6-1/9.0e-6);

% hold on
% plot([-8.4,-8.4]*1e-6,[min(min(Phi.data*10e-3)),max(max(Phi.data*10e-3))])
% plot([-8.4,-8.4]*1e-6-5e-6,[min(min(Phi.data*10e-3)),max(max(Phi.data*10e-3))])
% hold off
%  TXYL('Scan 498', 'Xs [um]','Voltage[V]')
plot(RampBG,R_Sharvin,'--.','linewidth',3,'MarkerSize',50);
hold on
plot(RampBG,R_sharvin_th,'--','linewidth',3);
hold off
%Beta sum turns out to be: 0.3072
%% Sharvin - 9-5 channel - Medium quality -bridge Scan 500
Factor=1;

LoadScan(500,L);
figure(2);clf
Beta_Sum=0.3072; %from scan 496
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;

plot_with_color(R,1,@jet);Ax=gca;set(Ax.Children,'linewidth',2)

out1=Fit_Line(1,'X',[-21,-13.4]*1e-6);RhoXX_9_1=[out1.a];
out2=Fit_Line(1,'X',[-8.4,-0.35]*1e-6);RhoXX_5_1=[out2.a];
RampBG_Dinamic1=RampBG(1:numel(RhoXX_9_1));
I=IgrAC15.mean(1).data;R_Sharvin=[];
for i=1:numel(RhoXX_9_1)
    R_Sharvin(i)=out2(i).Func(-8.4e-6)-out1(i).Func(-8.4e-6);
end
R_sharvin_th = Factor*h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(RampBG_Dinamic1,0.5)))*(1/5.0e-6-1/9.0e-6);
figure(1);
clf;plot(RampBG_Dinamic1,R_Sharvin,'.','linewidth',3,'MarkerSize',30); hold on
%hold on;plot(RampBG_Dinamic1,R_sharvin_th,'--','linewidth',3);hold off
%
hold on
LoadScan(501,L);
figure(3);clf
Beta_Sum=0.3072; %from scan 496
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;

plot_with_color(R,1,@jet);Ax=gca;set(Ax.Children,'linewidth',2)

out1=Fit_Line(1,'X',[-21,-13.4]*1e-6);RhoXX_9_2=[out1.a];
out2=Fit_Line(1,'X',[-8.4,-0.35]*1e-6);RhoXX_5_2=[out2.a];
RampBG_Dinamic2=RampBG(1:numel(RhoXX_9_2));
I=IgrAC15.mean(1).data;R_Sharvin=[];
for i=1:numel(RhoXX_9_2)
    R_Sharvin(i)=out2(i).Func(-8.4e-6)-out1(i).Func(-8.4e-6);
end
R_sharvin_th = Factor*h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(RampBG_Dinamic2,0.5)))*(1/5.0e-6-1/9.0e-6);
figure(1);
plot(RampBG_Dinamic2(2:end),R_Sharvin(2:end),'.','linewidth',3,'MarkerSize',30);
%hold on;plot(RampBG_Dinamic2(2:end),R_sharvin_th(2:end),'--','linewidth',3);hold off
%
hold on
LoadScan(502,L);
figure(3);clf
Beta_Sum=0.3072; %from scan 496
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;

plot_with_color(R,1,@jet);Ax=gca;set(Ax.Children,'linewidth',2)

out1=Fit_Line(1,'X',[-21,-13.4]*1e-6);RhoXX_9_3=[out1.a];
out2=Fit_Line(1,'X',[-8.4,-0.35]*1e-6);RhoXX_5_3=[out2.a];
RampBG_Dinamic3=RampBG(1:numel(RhoXX_9_3));
I=IgrAC15.mean(1).data;R_Sharvin=[];
for i=1:numel(RhoXX_9_3)
    R_Sharvin(i)=out2(i).Func(-8.4e-6)-out1(i).Func(-8.4e-6);
end
R_sharvin_th = Factor*h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(RampBG_Dinamic3,0.5)))*(1/5.0e-6-1/9.0e-6);
figure(1);
plot(RampBG_Dinamic3(2:end),R_Sharvin(2:end),'.','linewidth',3,'MarkerSize',30);
%hold on;plot(RampBG_Dinamic3(2:end),R_sharvin_th(2:end),'--','linewidth',3);hold off
plot(XS+0.5,YS,'--','linewidth',3)
TXYL('Sharvin Scans 500,501,502','BG Value','R [Ohm]')
%%
plot(RampBG_Dinamic1,RhoXX_9_1*9.2e-6,'.--b','linewidth',3,'MarkerSize',30);hold on; plot(RampBG_Dinamic1,RhoXX_5_1*5e-6,'.--r','linewidth',3,'MarkerSize',30);
plot(RampBG_Dinamic2(2:end),RhoXX_9_2(2:end)*9.2e-6,'.--b','linewidth',3,'MarkerSize',30); plot(RampBG_Dinamic2(2:end),RhoXX_5_2(2:end)*5e-6,'.--r','linewidth',3,'MarkerSize',30);hold off
%%
while(1)
    try
    TelegramGetLoop();
    end
     try
     LoadScan();
%     PlotScan();
% LoadScan();Beta_Sum=0.379;R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
 figure(3);clf
% plot_with_color(R,1,@jet)
plot(Time.data(:)/3600,1./Beta_Sum.data(:));
     TelegramSend(3);
     
     end
     pause(20)
end