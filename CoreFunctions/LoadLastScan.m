function LoadLastScan()
    %addpath('C:\Users\asafr\Documents\idata');
    %addpath('C:\Users\asafr\Documents\labmate');
    %addpath(genpath('Q:\users\asafr\scanning\ScanningV2.1'));
    global Info;
    txt = clipboard('paste');txt=txt(9:end);txt=strrep(txt,'%20',' ');
    subindex = @(A,r) A(r);
    try Info = load(txt);
    catch
        out=dir('C:\Users\asafr\downloads'); [i,j]=sort([out.datenum]); txt=['C:\Users\asafr\downloads\',out(j(end)).name];
        Info = load(txt);
    end
    Info=Info.Info_Saved;
    AllowedFunctions={'Plot2D','Plot','PlotErrorBar','PlotApproach','Plot1D_with_color','Plot1D'};
    for i=1:numel(Info.Scan.Get)
       for j=1:numel(Info.Scan.Get{i})
          if(isa(Info.Scan.Get{i}{j},'function_handle'))
              FunctionName=func2str(Info.Scan.Get{i}{j});FunctionName=FunctionName(4:subindex(findstr(FunctionName,'('),2)-1);
              if(ismember(FunctionName,AllowedFunctions))
                  Info.Scan.Get{i}{j}();
              end
          end
       end
    end
end
%File:\\\Q:\Measurements\Asaf R\Scanning\AsafR4D1-231017\Scan_488_Xs0_Ys0_2017_10_23_16_14_02.mat