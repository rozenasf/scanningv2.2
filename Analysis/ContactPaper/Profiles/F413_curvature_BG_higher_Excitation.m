% Scans [1014.01:0.01:1014.04], [1015.01:0.01:1015.04]
% [[1:0.25:3,3.5:0.5:6],[1:0.25:3],[1:0.25:2.5],[1:0.25:2]];

Y={};Traces={};
%ScanList=1017:1083; %1017:1083 LONG OVERNIGHT LOW EXITATION SUM=7.5mV
ScanList=1162:1172;
Range=[-1.5,1.5];
Curveture=[];CurvetureSTD=[];
BGList=[];BGListFull=[];BetaSumList=[];
for i=1:numel(ScanList)
    LoadScan(ScanList(i)+0.01);BGListFull(i)=Info.Scan.UserData.BG;
    BetaSumList(i)=1./Info.Scan.UserData.Initial_Beta_Sum;
end
BGList_unique=uniquetol(BGListFull,1e-3);
for i=1:numel(BGList_unique)
        index=find( abs(BGList_unique(i)-BGListFull)<1e-3 );
        
        CurvetureSTDList=[];
        for j=1:numel(index)
            CurvetureSTDList(j)=GetCurvetureFromDSB(ScanList(index(j)),Range);
        end
        [Curveture(i),WRcList(i),BG]=GetCurvetureFromDSB(ScanList(index),Range);
        %BGList(i)=BG;
        if(numel(index)>1)
        CurvetureSTD(i)=std(CurvetureSTDList)./sqrt(numel(index)-1);
        else
            CurvetureSTD(i)=nan;
        end
        
end
    
figure(413);clf
% WRcList_unique=unique(WRcList);Curveture_AVR=[];
Lmr_BG_Fit=[-9.38362e-05,-4.20564e-03,-6.39272e-02,-4.72690e-01,-3.20175e+00,2.54587e+00];
Lmr=polyval(Lmr_BG_Fit,BGList_unique);
errorbar(Lmr/5,Curveture,CurvetureSTD,'.','markersize',20,'linewidth',2);
TXYL('curveture in Ey','lMR/W','curveture')
Export.Figure3.Panel3.Curve1=[Lmr/5;Curveture;CurvetureSTD];
%1016 is the first one
ylim([-2,4]);grid