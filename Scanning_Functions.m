function S = Scanning_Functions( )
%global Info
%global S;
S.Set=@Set;
S.Get=@Get;
S.RunScan=@RunScan;
S.ResumeScan=@ResumeScan;
S.InitiateFlags=@InitiateFlags;
S.UpdatePreScanParameters=@UpdatePreScanParameters;
S.AdvancePosition=@AdvancePosition;
S.EndOfRunScan=@EndOfRunScan;
S.InitiateidgrObjects=@InitiateidgrObjects;
S.CreateScanOutput=@CreateScanOutput;
S.UpdateData=@UpdateData;
S.AddDataToScan=@AddDataToScan;
S=Motors_Functions(S);
%%
    function out=Set(varargin)
        global Info; 
        out=nan;
        NumberOfInput=nargin;VariableIn=varargin;
        if(NumberOfInput==1);VariableIn={VariableIn{1},0};NumberOfInput=2;end
        
        for i=1:(NumberOfInput/2)
            
            OKflag=1;
            Channel=VariableIn{i*2-1};Value=VariableIn{i*2};
            if(isa(Value,'function_handle'))
                Value=Value();
            end
            
            if(isa(Channel,'function_handle'))
                if(nargin(varargin{i})==1)
                    varargin{i}(Value);
                else
                    varargin{i}();    
                end
            else           
                %----Check Range-----%
                if(~InChannelRange(Channel,Value))
                    RangeReact(Channel);error('InChannelRange in SET');
                end
                if(isfield(Info,'MaxJump') && isfield(Info.MaxJump,Channel) && (abs(GetFromMemory(Channel)-Value)>abs(Info.MaxJump.(Channel)) ) )
                    ListOfValues=linspace(GetFromMemory(Channel),Value, ceil(2+abs(GetFromMemory(Channel)-Value)./abs(Info.MaxJump.(Channel))) );
                    ListOfValues=ListOfValues(2:end);
                    for IDXListValues=1:numel(ListOfValues)
                        out=S.Set(Channel,ListOfValues(IDXListValues));
                    end
                    return;
                end
                Driver=GetDriver(Channel);
                if(~isempty(Driver))
                    if(ismethod(Driver,'Set')) %will work as a variable if no Get method
                        Driver.Set(Channel,Value);
                    end
                else
                    switch Channel
                        %---------------------------------------%
                        case Info.ChannelType.Variables
                            %dont do anything, it is a variable. just place in
                            %memory
                            %---------------------------------------%
                        case Info.ChannelType.ClosedLoop
                            S.ClosedLoop(Channel,Value)
                            %---------------------------------------%
                        case Info.ChannelType.CourseSteps
                            Value=round(Value);
                            if(Value~=0);Info.cr.set(Channel,Value);end
                            %---------------------------------------%
                        case Info.ChannelType.RegularSet
                            if(~isnan(Value))
                                Info.cr.set(Channel,Value);
                            else
                                error('Value cant be nan!');
                            end
                        case 'Time'
                            Value=toc(Info.Scan.Runtime.Tic);
                            UpdateTimeAxes(Value);
                        otherwise
                            fprintf('Unknown SET channel %s.\n If it is a regular channel, add it to Info.ChannelType.RegularSet\n',Channel);
                            OKflag=0;
                    end
                end
                if(OKflag)
                    %fprintf('Set:%s,%e\n',Channel,Value);
                    Info.SetChannels.(Channel)=Value;
                    Info.Channels.(Channel)=Value;
                    if(Info.Flags.RunningScan && Info.Scan.Runtime.StepType=='G')
                        S.AddDataToScan(Channel,Info.SetChannels.(Channel))
                    end
                    drawnow;pause(1e-3);
                end
            end
        end
    end
%%
    function Value=Get(varargin) 
        global Info; 
        Value=[];
        for i=1:nargin
            if(iscell(varargin{i}))
                for j=1:numel(varargin{i})
                    if(~isnumeric(varargin{i}{j}))
                        Get(varargin{i}{j});
                    else
                        for k=1:(varargin{i}{j}-1)
                           Get(varargin{i}{j-1}); 
                        end
                    end
                end
                Value{i}=[];
                continue;
            end

            if(isa(varargin{i},'function_handle'))
                Value{i}=varargin{i}();
            else
                Channel=varargin{i};
                OKflag=1;
                Value{i}=nan;
                Driver=GetDriver(Channel);
                if(~isempty(Driver))
                    if(ismethod(Driver,'Get')) %will work as a variable if no Get method
                        Value{i}=Driver.Get(Channel);
                    else
                        Value{i}=GetFromMemory(Channel);
                    end
                    
                    %if getting a driver, Init and get all channels
                elseif(isfield(Info.Drivers,Channel))
                    Driver=Info.Drivers.(Channel);
                    if(ismethod(Driver,'Init'))
                        Driver.Init()
                    end
                    for ChannelIndex=1:numel(Info.ChannelType.(Channel))
                        S.Get(Info.ChannelType.(Channel){ChannelIndex});
                    end
                    OKflag=0;
                else
                    switch Channel
                        %---------------------------------------%
                        case [Info.ChannelType.RegularGet,Info.ChannelType.CourseLocation]
                            Value{i}=Info.cr.get(Channel);
                            %---------------------------------------%
                        case {'Save','SaveCompress'}
                            a=tic;
                            Info_Saved=GetInfoSave();
                            GetSaveName();
                            if(strcmp('SaveCompress',Channel))
                                save(Info.Scan.SaveName,'Info_Saved');
                            else
                                savefast(Info.Scan.SaveName,'Info_Saved');
                            end
                            Value{i}=toc(a);
                            OKflag=0;
                            %---------------------------------------%
                        case Info.ChannelType.Variables
                            Value{i}=GetFromMemory(Channel);
                            %---------------------------------------%
                        case 'Time'
                            Value{i}=toc(Info.Scan.Runtime.Tic);
                        otherwise
                            fprintf('Unknown GET channel %s\n If it is a regular channel, add it to Info.ChannelType.RegularGet\n',Channel);
                            OKflag=0;
                    end
                end
                if(isfield(Info.Scaling,Channel));Value{i}=Value{i}*Info.Scaling.(Channel);end
                if(OKflag)
                    %fprintf('Get:%s,%e\n',Channel,Value{i});
                    Info.GetChannels.(Channel)=Value{i};
                    Info.Channels.(Channel)=Value{i};
                    if(Info.Flags.RunningScan && Info.Scan.Runtime.StepType=='G')
                        S.AddDataToScan(Channel,Info.GetChannels.(Channel))
                    end
                %----Check Range-----%
                    if(~InChannelRange(Channel,Value{i}))
                        Info.Flags.Stop=1;
                        RangeReact(Channel);
                        error(['InChannelRange in GET, Channel:',Channel]);
                    end
                    drawnow;pause(1e-3);
                end
            end
        end
        if(nargin==1);Value=Value{1};end
    end
%%
    function RunScan()
        
        global Info L;
        
        if(Info.Flags.Stop)
            disp('Info.Flags.Stop is 1. This is for your protection!');
            disp('If you know what you are doing, Set Info.Flags.Stop to 0');
            disp('This might happen from an interlock or arduino pull');
            error('Info.Flags.Stop is 1!!!!');
            return
        end
        
        
        %if(L.Bind);try TelegramGet();end;end
        
%         if(isfield(Info.Scan,'Polygons'))
%             [x,y]=meshgrid(Info.Scan.Axis.Xs,Info.Scan.Axis.Ys);
%             count=0;
%             for i=1:numel(x)
%                    count=count+InPolygons(x(i),y(i),Info.Scan.Polygons); 
%             end
%             input(sprintf('will skip %d out of %d points. OK?',count,numel(x)));
%         end
        if(~CheckScanStartDrivers());return;end %will check ScanStart in drivers
        CheckSetRangeBeforeScan(); %will check range of Start,End,Axis
        CheckStartingToleranceBeforeScan();%will check starting tolerance of Start,Axis. these channels will not be checked again
        
        UpdatePreScanParameters(); %before the oncleanup object
        superscan = Info.Flags.SuperScan;
        obj1=onCleanup(@EndOfRunScan);
        InitiateFlags();Info.Flags.InScan=1; 
        Info.Flags.SuperScan = superscan;
        
        for i=1:numel(Info.Scan.Runtime.StartNames)
            Set(Info.Scan.Runtime.StartNames{i},Info.Scan.Start.(Info.Scan.Runtime.StartNames{i}));
        end
        Info.Flags.RunningScan=1;
        
        S.Get('Save');
        Info.Flags.NormalEnd=MainScanLoop();

        %it will save again here because of @EndOfRunScan
    end

    function NormalEnd=MainScanLoop()
       global Info; 
       
       NormalEnd=0;
       while(Info.Flags.InScan)
           tic
            % Set the axis
            
            %Set all of the Set channels
            for i=1:(Info.Scan.Runtime.DimStart+1)
                if(~Info.Flags.InScan);return;end
                S.Set(Info.Scan.Runtime.AxisNames{i},...
                    Info.Scan.Axis.(Info.Scan.Runtime.AxisNames{i})(Info.Scan.Runtime.Position(i)));
                if(Info.Scan.WaitTimes(i)>0)
                    pause(Info.Scan.WaitTimes(i));
                    %S.Set('wait',Info.Scan.WaitTimes(i));
                end
            end
            if(~Info.Flags.InScan);return;end
            Info.Scan.Runtime.StepType='S'; %Dont document the Channels in scan (only for preget now)
            %Set all of the PreGet channels, after Setting the chanenls.
            %dont document these values in scan
            if(isfield(Info.Scan,'PreGet'))
                Info.Scan.Runtime.RelevantPreGet={ Info.Scan.PreGet{1} };
                for i=1:(Info.Scan.Runtime.DimStart)
                    Info.Scan.Runtime.RelevantPreGet=[Info.Scan.Runtime.RelevantPreGet,{ Info.Scan.PreGet{i+1} }];
                end
                
                Info.Scan.Runtime.GetDim=1;
                for i=1:numel(Info.Scan.Runtime.RelevantPreGet)
                    S.Get(Info.Scan.Runtime.RelevantPreGet{i});
                end
                
            end
            Info.Scan.Runtime.StepType='G'; %Change to Get type, so the chanenls will be documented in Scan
             
            %Get all Get Channels
            Info.Scan.Runtime.RelevantGet={ Info.Scan.Get{1} };
            for i=1:(Info.Scan.Runtime.DimEnd)
                Info.Scan.Runtime.RelevantGet=[Info.Scan.Runtime.RelevantGet,{ Info.Scan.Get{i+1} }];
            end

            for i=1:numel(Info.Scan.Runtime.RelevantGet)
                Info.Scan.Runtime.GetDim=i;
                S.Get(Info.Scan.Runtime.RelevantGet{i});
            end           
            
            Info.Scan.Runtime.GetDim=1; %return to normality
            if(~Info.Flags.InScan);return;end %sanity check to exit if problem
% fprintf('\n')
            if(AdvancePosition());NormalEnd=1;return;end
            
            if(Info.Flags.StopRequest);disp('Got stop request');return;end
            if(Info.Flags.PauseRequest);input('Got pause request,press any key to continue');Info.Flags.PauseRequest=0;end
       end
    end
%%
    function EndOfRunScan()
        global Info; 
        fprintf('EndOfRunScan()\n');
         

        Info.Flags.RunningScan=0;
        try Get('Save');end
        
 
        %pause(0.5); %why?
        for i=1:numel(Info.Scan.Runtime.EndNames)
            try
                Set(Info.Scan.Runtime.EndNames{i},Info.Scan.End.(Info.Scan.Runtime.EndNames{i}));
            catch
                Info.Flags.NormalEnd=0;
                fprintf('Problem setting END Scan\n');
            end
        end
        try
            Info.Scan.ElapsedTime=toc(Info.Scan.Runtime.Tic);
            fprintf('ElapsedTime: %0.2f\n',Info.Scan.ElapsedTime);
        end
        
        Info.objects=[];
        if(~Info.Flags.NormalEnd)
            disp('Abnormal scan end');
            %try DocSend('Abnormal scan end');end
            Info.Flags.Stop=1;
        end
        try Get('SaveCompress');catch;disp('Probelm saving final file');end
        Info.Flags.InScan=0;
        if(Info.Flags.SuperScan > 0)
            Info.Flags.SuperScan = Info.Flags.SuperScan+1;
        end
        %try DocSend({BuildToSave(1),['<font color="red" size="6">',repmat('#',1,10),'</font>']});end
if(~Info.Flags.NormalEnd)
        try TelegramSend('Abnormal scan end');end
else
    %try TelegramSend('Done.');end
end

    end
%%
    function UpdatePreScanParameters()
        global Info; 

        %if(~isfield(Info,'ScanNumber'));Info.ScanNumber=1;else Info.ScanNumber=Info.ScanNumber+1;end
        %Change of behavier. always check what is the last scan and add 1
        %100418 AsafR
        if(isfield(Info.Flags,'SuperScan') && Info.Flags.SuperScan >= 2)
            Info.ScanNumber=FindLastScanNumber();
        else
            Info.ScanNumber=FindLastScanNumber()+1;
        end
        
        if(~isfield(Info.Scan,'Start'));Info.Scan.Start=struct();end
        if(~isfield(Info.Scan,'End'));Info.Scan.End=struct();end
        
        Info.ScanNumber
        Info.Scan.idgr=[];
        Info.Scan.Runtime=[];
        Info.Scan.Runtime.AxisNames=fieldnames(Info.Scan.Axis);
        Info.Scan.Runtime.StartNames=fieldnames(Info.Scan.Start);
        Info.Scan.Runtime.EndNames=fieldnames(Info.Scan.End);
        
        Info.Scan.Runtime.Dimentions=numel(Info.Scan.Runtime.AxisNames);
        Info.Scan.Runtime.Position=ones(1,Info.Scan.Runtime.Dimentions);
        Info.Scan.Runtime.PositionIndex=1;
        Info.Scan.Runtime.GetDim=1;
        StringAxisNames=[];
        for i=1:numel(Info.Scan.Runtime.AxisNames)
            Info.Scan.Runtime.MaxPosition(i)=numel(Info.Scan.Axis.(Info.Scan.Runtime.AxisNames{i}));
            StringAxisNames=[StringAxisNames,Info.Scan.Runtime.AxisNames{i},'_'];
        end
        %Info.Scan.Runtime.MaxPositionCum=cumprod([1,Info.Scan.Runtime.MaxPosition(1:end-1)]);
        %Info.Scan.Runtime.DataNames=[];%?
        Info.Scan.StartDateTime=[StringAxisNames,datestr(now,'yyyy_mm_dd_HH_MM_SS')];
        Info.Scan.Runtime.DimEnd=0;
        Info.Scan.Runtime.DimStart=(Info.Scan.Runtime.Dimentions-1);
        Info.Scan.Runtime.Tic=tic;
        Info.Scan.Runtime.Now=now;
        if(~isfield(Info.Scan,'WaitTimes'));Info.Scan.WaitTimes=zeros(1,Info.Scan.Runtime.Dimentions);end
        
        if(Info.Scan.Runtime.Dimentions~=numel(Info.Scan.Get))
           error('size of get list must match the axis!');
        end
        if(Info.Scan.Runtime.Dimentions<numel(Info.Scan.WaitTimes))
            error('too many wait times');
        end
        ZeroWait=zeros(1,Info.Scan.Runtime.Dimentions);
        ZeroWait(1:numel(Info.Scan.WaitTimes))=Info.Scan.WaitTimes;
        Info.Scan.WaitTimes=ZeroWait;
        Info.Scan.Runtime.ChannelAverage=struct();
        Info.Scan.Runtime.Now=now;
        Info.Scan.Runtime.StepType='S';
        Info.Scan.Runtime.Equations=fileread(which('Equations_Driver.m'));
    end
%%
    function InitiateFlags()
        global Info; 
        if(~isfield(Info.Flags,'LastDacParametersUpdate'));Info.Flags.LastDacParametersUpdate=[];end
        Info.Flags=BuildStruct(...
            'InScan',0,'RunningScan',0,'NormalEnd',0,'Stop',...
            0,'StopRequest',0,'PauseRequest',0,'LastDacParametersUpdate',Info.Flags.LastDacParametersUpdate,'SuperScan',0);
    end

%%
    function EndOfScan=AdvancePosition()
        global Info; 
        EndOfScan=0; 
%         %Normelize the data if was averaged
%         names=fieldnames(Info.Scan.idgr);
%         for i=1:numel(names)
%             if(isfield(Info.Scan.Runtime.ChannelAverage,names{i}))
%                 value=Info.Scan.idgr.(names{i}).data(Info.Scan.Runtime.PositionIndex);
%                 Info.Scan.idgr.(names{i}).data(Info.Scan.Runtime.PositionIndex)=value/Info.Scan.Runtime.ChannelAverage.(names{i});
%             end
%         end
        %check end of scan
       
        
        if(isequal(Info.Scan.Runtime.MaxPosition,Info.Scan.Runtime.Position));EndOfScan=1;return;end

        
        

              
        Info.Scan.Runtime.Position(1)=Info.Scan.Runtime.Position(1)+1;
        Info.Scan.Runtime.PositionIndex=Info.Scan.Runtime.PositionIndex+1;
        for i=1:(Info.Scan.Runtime.Dimentions-1)
            if(Info.Scan.Runtime.Position(i)>Info.Scan.Runtime.MaxPosition(i))
                Info.Scan.Runtime.Position(i)=1;
                Info.Scan.Runtime.Position(i+1)=Info.Scan.Runtime.Position(i+1)+1;
            end
        end
        
        Info.Scan.Runtime.DimEnd=0;
        for i=1:(Info.Scan.Runtime.Dimentions-1)
            if(Info.Scan.Runtime.Position(i)==Info.Scan.Runtime.MaxPosition(i))
                Info.Scan.Runtime.DimEnd=Info.Scan.Runtime.DimEnd+1;
            else
                break;
            end
        end
        Info.Scan.Runtime.DimStart=0;
        for i=1:(Info.Scan.Runtime.Dimentions-1)
            if(Info.Scan.Runtime.Position(i)==1)
                Info.Scan.Runtime.DimStart=Info.Scan.Runtime.DimStart+1;
            else
                break;
            end
        end
        
    end
%%
    function InitiateidgrObjects(name,Data,StartAxisDim)
        global Info;
        AxisNames=fieldnames(Info.Scan.Axis);
        %IDX=find(Info.Scan.SaveName=='\');ScanName=Info.Scan.SaveName(IDX(end):end-4);
        j=1;
        for i=StartAxisDim:numel(AxisNames)
            Ax{j}=iaxis(AxisNames{i}, Info.Scan.Axis.(AxisNames{i}));
            j=j+1;
        end
        Info.Scan.idgr.(name) = idata(name, Data, Ax, {0}, '');
        Info.Scan.std_idgr.(name) = idata(name, Data, Ax, {0}, '');
    end
%%
    function CreateScanOutput(name,varargin)
        global Info;
        
        StartAxisDim=1;
        if(Info.Flags.RunningScan)
            StartAxisDim=Info.Scan.Runtime.GetDim;
        end
        
        Data=nan(prod(Info.Scan.Runtime.MaxPosition( StartAxisDim : end )),1);
        if(numel(Info.Scan.Runtime.MaxPosition( StartAxisDim : end ) )>1);Data=reshape(Data,Info.Scan.Runtime.MaxPosition( StartAxisDim : end ) );end
        InitiateidgrObjects(name,Data,StartAxisDim);
    end
%%
    function UpdateData(name,Value)
        global Info;
        ReducedChannelDim=Info.Scan.Runtime.Dimentions-Info.Scan.idgr.(name).dim;
        PositionIndex=ReducedPositionIndex(ReducedChannelDim);
        if(isnan(Info.Scan.idgr.(name).data(PositionIndex)))
            Info.Scan.Runtime.ChannelAverage.(name)=Value;
            Info.Scan.idgr.(name).data(PositionIndex)=Info.Scan.Runtime.ChannelAverage.(name);
        else
            Info.Scan.Runtime.ChannelAverage.(name)=[Info.Scan.Runtime.ChannelAverage.(name),Value];
            Info.Scan.idgr.(name).data(PositionIndex)=mean(Info.Scan.Runtime.ChannelAverage.(name));
            Info.Scan.std_idgr.(name).data(PositionIndex)=std(Info.Scan.Runtime.ChannelAverage.(name));
        end
        
    end
%%
function AddDataToScan(Channel,Value)
    global Info 
    if(isnumeric(Value) && ~isempty(Value))
        if(~isfield(Info.Scan.idgr,Channel))
            S.CreateScanOutput(Channel);
        end
        S.UpdateData(Channel,Value);
    end
end
end
