classdef MatlabFeedBack_Driver < handle
    properties 
       ScanSizes
       FeedbackShift
       Attenuator
       Averaging
       
       Output
       
       DataCoarse
       DataFine
       
       CoarsePeak 
       FinePeak 

       FitCoarse
       FitFine
       
       ComPort
       ArduWrapObj
       Graphics
    end
    methods
        %-------------------------%
        %% Basic driver commands %%
        %-------------------------%
        function obj=MatlabFeedBack_Driver(varargin)
%            obj.data=[];
            obj.Output=nan;
            obj.FeedbackShift=0;
            obj.ScanSizes=[1,0.2];
            obj.ComPort='COM18';
            obj.Attenuator=43.55;%?
            obj.Averaging=100;
            
            for i=1:numel(varargin)/2
                obj.(varargin{i*2-1})=varargin{i*2};
            end
            
            obj.ArduWrapObj=ArduWrap(obj.ComPort);
            InitializeFigure(obj,777);
        end
        function Configure(obj,varargin)
            for i=1:numel(varargin)/2
                obj.(varargin{i*2-1})=varargin{i*2};
            end
            %Send relevant data, not window center and width
        end
        function out=Get(obj,Channel)
            global Info S;
            switch Channel
                case 'WorkPoint'
                    out=obj.GetOutput();
                case 'Feedback'
%                     S.Set('WorkPoint_Initial',GetFromMemory('WorkPoint'));

%                     SearchAround=GetFromMemory('WorkPoint')-obj.FeedbackShift;
                    if((Info.Flags.RunningScan) && (Info.Scan.Runtime.PositionIndex==1))
                        S.Set('Vg_Initial',GetFromMemory('Vg'));
                    end
                    if((~Info.Flags.RunningScan || Info.Scan.Runtime.PositionIndex==1) || 1) %remove the 1 thing! || 1
                        SearchAround=GetFromMemory('WorkPoint')-obj.FeedbackShift;
                    else
                        Position=Info.Scan.Runtime.Position;
                        for i=1:Info.Scan.Runtime.Dimentions
                            if(Position(i)>1)
                                Position(i)=Position(i)-1;
                                break;
                            end
                        end
                        SearchAround=GetFromScan('WorkPoint',Position)-obj.FeedbackShift; 
                        % set alweys to the relevant Vg
                        if(GetFromMemory('Vg')~=GetFromScan('Vg_loc',Position))
                            S.Set('Vg_Ramp',GetFromScan('Vg_loc',Position));
                        end
                        %if the previous point is too large
                        if(abs(SearchAround)>0.1)
                            S.Set('Vg_Ramp',GetFromMemory('Vg')+SearchAround);
                            SearchAround=0;
                        end
                    end
                    
                    %SearchAround=0;
                    if(GetFromMemory('KeepFeedback')==0)
                       obj.DataCoarse=GetSweep(obj,SearchAround,obj.ScanSizes(1)*2.5,obj.Averaging); %*2.5
                    [peak,loc,width,p] = findpeaks(obj.DataCoarse(2,:),obj.DataCoarse(1,:));
                     [~,Idx]=max(p);
                        obj.CoarsePeak=[loc(Idx),peak(Idx)];
                     else
                        obj.CoarsePeak=[SearchAround,0];
                     end
                     
                    obj.DataCoarse=GetSweep(obj,obj.CoarsePeak(1),obj.ScanSizes(1),obj.Averaging);
                    [peak,loc,width,p] = findpeaks(obj.DataCoarse(2,:),obj.DataCoarse(1,:));
                     [~,Idx]=max(p);
                     
                     if(GetFromMemory('KeepFeedback')==0)
                        obj.CoarsePeak=[loc(Idx),peak(Idx)];
                     else
                        obj.CoarsePeak=[SearchAround,0];
                     end
%                     obj.FitCoarse=polyfit(obj.DataCoarse(1,:),obj.DataCoarse(2,:),2);
%                     obj.CoarsePeak=roots(polyder(obj.FitCoarse));
                  
%                     obj.CoarsePeak=max([obj.CoarsePeak,min(obj.DataCoarse(1,:))]);
%                     obj.CoarsePeak=min([obj.CoarsePeak,max(obj.DataCoarse(1,:))]);
                    
%                     obj.DataFine=GetSweep(obj,obj.CoarsePeak,obj.ScanSizes(2),obj.Averaging);

%                     obj.DataFine=GetSweep(obj,obj.CoarsePeak(1),0.4*width(Idx),obj.Averaging);
                    obj.DataFine=GetSweep(obj,obj.CoarsePeak(1),1*width(Idx),obj.Averaging*4);
                    
                    obj.FitFine=polyfit(obj.DataFine(1,:),obj.DataFine(2,:),4);
                    
                    R=roots(polyder(obj.FitFine));
                    R=R(polyval(polyder(polyder(obj.FitFine)),R)<0);
                    [~,IDX2]=min(abs(R-obj.CoarsePeak(1)));

                    obj.FinePeak=R(IDX2);
      
%                     obj.FitFine=polyfit(obj.DataFine(1,:),obj.DataFine(2,:),2);obj.FinePeak=roots(polyder(obj.FitFine));
                    
                    
                    obj.FinePeak=max([obj.FinePeak,min(obj.DataFine(1,:))]);
                    obj.FinePeak=min([obj.FinePeak,max(obj.DataFine(1,:))]);
                    
                    out=obj.FinePeak+obj.FeedbackShift;
%                     SetOutput(obj,out);
            if(imag(out)~=0);fprintf('out is complex! out=%0.2f\n',out);out=0;end
            if(isnan(out));  fprintf('out is nan!\n');out=0;end
            if(abs(out)>0.2);fprintf('abs(out) is more then 0.2! out=%0.2f\n',out);out=0;end
             
            if(GetFromMemory('KeepFeedback')==0)
                S.Set('WorkPoint',out);
                S.Set('FeedbackWidth',width(Idx));
                S.Set('FeedbackAmp',p(Idx));
            else
                S.Set('WorkPoint',SearchAround+obj.FeedbackShift);
                S.Set('FeedXStart',obj.DataCoarse(1,1));
                S.Set('FeedXEnd',obj.DataCoarse(1,end));
                for i=1:31
                    S.Set(sprintf('FeedCoarse%d',i),obj.DataCoarse(2,i));
                end
            end
            try UpdateGraphics(obj);end
            
            S.Set('Vg_loc',GetFromMemory('Vg')); %Document the Vg at this point
     
            end
        end
        function Set(obj,Channel,Value)
           switch Channel
               case 'WorkPoint'
                   OK=obj.SetOutput(Value);
               case 'FeedbackShift'
                   obj.FeedbackShift=Value;
           end
        end

        %--------------------------------%
        %% specific high level commands %%
        %--------------------------------%

        function OK=SetOutput(obj,Value)
            OK=1;
            obj.ArduWrapObj.AppendMessage('v',Value*obj.Attenuator, 'f','B');
            obj.ArduWrapObj.AppendMessage('g',[],      '', 'f');
            [Data_out,Opcode_out]=Quary(obj.ArduWrapObj);
            if(abs(Data_out(2)-Value*obj.Attenuator)>1e-6)
                error('Problem setting Voltage of feedback %e,%e',Value,Data_out(2));
            end
            obj.Output=Data_out(2)/obj.Attenuator;
        end
        
        function Value=GetOutput(obj)
            obj.ArduWrapObj.AppendMessage('g',[],      '', 'f');
            [Data_out,Opcode_out]=Quary(obj.ArduWrapObj);
            obj.Output=Data_out(1);
            Value=obj.Output/obj.Attenuator;
        end
        function Zero(obj)
            global S
            OK=obj.SetOutput(0);
            S.Set('WorkPoint',0);
        end
        function output=GetSweep(obj,Mid,Width,Averaging)
            obj.ArduWrapObj.Clear
            Limits=(Mid+[-Width,Width]/2)*obj.Attenuator;
            
            obj.ArduWrapObj.AppendMessage('v',Limits(1), 'f','B');
            obj.ArduWrapObj.AppendMessage('c',['n',Averaging],'BL','B');
            obj.ArduWrapObj.AppendMessage('c',[115,Limits(1)], 'Bf', 'B');
            obj.ArduWrapObj.AppendMessage('c',[101,Limits(2)], 'Bf', 'B');
            obj.ArduWrapObj.AppendMessage('w',[], '', 'B');
            [Data_out,Opcode_out]=Quary(obj.ArduWrapObj);
            
            for i=0:30
                obj.ArduWrapObj.AppendMessage('s',[73,i],'BL','f')
            end
             [Data_out,Opcode_out]=Quary(obj.ArduWrapObj);
             output=[linspace(Limits(1),Limits(2),31)./obj.Attenuator;Data_out];
            %Format [Vlist;Ilist]
        end
        %--------------------------------%
        %% Low level commands, arduwrap %%
        %--------------------------------%
        
        
        %--------------------------------%
        %%          Graphics            %%
        %--------------------------------%
        function InitializeFigure(obj,FigNo)
            figure(FigNo);
            clf;
            obj.Graphics=[];
           obj.Graphics.CoarseLine=plot(1,1,'o');
           hold on
%            obj.Graphics.CoarseLineFit=plot(1,1,'-');
           obj.Graphics.FineLine=plot(1,1,'.r','markersize',10);
           obj.Graphics.FineLinefit=plot(1,1,'m');
           obj.Graphics.PeakCoarse=plot(1,1,'*');
           obj.Graphics.PeakFine=plot(1,1,'*r');
           obj.Graphics.Output=plot(1,1,'g');
           obj.Graphics.Title=title(sprintf('OUTPUT=%0.3f',0));
           hold off
        end
        function UpdateGraphics(obj)
            %Coarse
            obj.Graphics.CoarseLine.XData=obj.DataCoarse(1,:);obj.Graphics.CoarseLine.YData=obj.DataCoarse(2,:);
            obj.Graphics.PeakCoarse.XData=obj.CoarsePeak(1);
            obj.Graphics.PeakCoarse.YData=obj.CoarsePeak(2);%polyval(obj.FitCoarse,obj.CoarsePeak);
            X=linspace(obj.DataCoarse(1,1),obj.DataCoarse(1,end),100);
%             obj.Graphics.CoarseLineFit.XData=X;obj.Graphics.CoarseLineFit.YData=polyval(obj.FitCoarse,X);
            %Fine
            obj.Graphics.FineLine.XData=obj.DataFine(1,:);obj.Graphics.FineLine.YData=obj.DataFine(2,:);
            obj.Graphics.PeakFine.XData=obj.FinePeak;obj.Graphics.PeakFine.YData=polyval(obj.FitFine,obj.FinePeak);
            X=linspace(obj.DataFine(1,1),obj.DataFine(1,end),100);
            obj.Graphics.FineLinefit.XData=X;obj.Graphics.FineLinefit.YData=polyval(obj.FitFine,X);
            %WorkPoint
            obj.Graphics.Output.XData=GetFromMemory('WorkPoint')*[1,1];
            obj.Graphics.Output.YData=[max([obj.DataCoarse(2,:),obj.DataFine(2,:)]),min([obj.DataCoarse(2,:),obj.DataFine(2,:)])];
            %
            obj.Graphics.Title.String=sprintf('OUTPUT=%0.3f',GetFromMemory('WorkPoint'));
        end
        function ResetDac(obj)
            obj.ArduWrapObj.AppendMessage('t',[],      '', 'B');
            [Data_out,Opcode_out]=Quary(obj.ArduWrapObj)
        end
        %-------------delete-------------%
        function delete(obj)
           delete(obj.ArduWrapObj);
           disp('disconnected from feedback!');
        end
    end

end