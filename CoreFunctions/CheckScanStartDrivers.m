function OK=CheckScanStartDrivers()
    global Info
    OK=1;
    DriverNames=fieldnames(Info.Drivers);
    for i=1:numel(DriverNames)
        Driver=Info.Drivers.(DriverNames{i});
        if(ismethod(Driver,'ScanStart'))
            if(~Driver.ScanStart())
                OK=0;
                disp(['stopped at ScanStart in Driver ',DriverNames{i}]);
                return;
            end
        end
    end
end