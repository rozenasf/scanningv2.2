function out=SprintfA(N)
out='';
for i=1:numel(N)
    if(isnan(N(i)))
        out=[out,'NaN,'];
        continue;
    end
    if(N(i)==0)
        out=[out,'0,'];
        continue;
    end    
    MP='-+';
    Exp=floor(log10(N(i))/3)*3;
    if(Exp==0)
        out=[out,sprintf('%0.3f',N(i)/10^Exp)];
    else
        out=[out,sprintf('%0.3fe%c%d',N(i)/10^Exp,MP((sign(Exp)+1)/2+1),abs(Exp))];
    end
    out=[out,','];
end
out=out(1:end-1);
end