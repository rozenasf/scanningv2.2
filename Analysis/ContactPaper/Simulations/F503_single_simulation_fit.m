Data4=load('CollimationV12.mat');Data4=Data4.Data;
%%
R=XEdgeFromSimulation(Data4.SliceIndex('lMR',5));
clf;i=5;
    R{i}.data=R{i}.data;
    plot(R{i}*77,'linewidth',3);hold on;
 Extra=3;i=2;
j=1+(Extra-1)*2;
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j,i});Beta_Sum=0.51;
R1 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j+1,i});Beta_Sum=0.51;
R2 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
IData=R1-R2;
IData.data=(IData.data+fliplr(IData.data))/2;
plot(IData,'-','linewidth',3);
grid;TXYL('Hall Reistance','X','$R_{Hall} [Ohm]$','Measurement','Simulation [arb. units]');ylim([0,30])