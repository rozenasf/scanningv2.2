function Message=BuildMessage(opcode,SendData,Sendformat,Reciveformat)
    [BinnaryOutput,MessageLength]=Data2Format(['B',Sendformat],[opcode*1,SendData]);
    ExpectedLength=FormatLength(['B',Reciveformat]);
    Message=[char(255), char(MessageLength+32), char(ExpectedLength+32), BinnaryOutput ,char(13)];
end