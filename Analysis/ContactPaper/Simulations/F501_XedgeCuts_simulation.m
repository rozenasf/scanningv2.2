
Data=load('CollimationV1.mat');Data=Data.Data;
Data2=load('CollimationV2.mat');Data2=Data2.Data;
Data3=load('CollimationV3.mat');Data3=Data3.Data;
Data4=load('CollimationV4.mat');Data4=Data4.Data;
%%
DataLmr=Data.SliceIndex('lMR',1);
Data3Lmr=Data3.SliceIndex('lMR',1);
SData=Data.Sym('WRc').SliceIndex('lMR',1);
AData=Data.ASym('WRc').SliceIndex('lMR',1);
SData2=Data2.Sym('WRc');
AData2=Data2.ASym('WRc');
%%
figure(18)
R=XEdgeFromSimulation(Data3Lmr);clf;for i=1:numel(R);plot(R{i});hold on;end;
%%
R=XEdgeFromSimulation(Data2);clf;for i=5:11;plot(R{i});hold on;end;
%%
PeakList={};
for j=1:9
  clf  
R=XEdgeFromSimulation(Data4.SliceIndex('lMR',j));
for i=1:numel(R);
    plot(R{i});hold on
end

PeaksLocations=[-5.2,-2.75,0];

for i=1:numel(PeaksLocations)
   out{i}=Fit_Line(2,'X',[PeaksLocations(i)-1,PeaksLocations(i)+1]) ;
   PeakList{i}(j,:)=[out{i}.dY_0];   
end
for i=1:numel(PeaksLocations)
    PeakList{i}(j,:)=PeakList{i}(j,:)./PeakList{3}(j,:);
end

end
PeakListSimulation=PeakList;
%%

 %%
 figure(20)
R=XEdgeFromSimulation(Data4.SliceIndex('lMR',5));
clf;i=5;
    R{i}.data=R{i}.data;
    plot(R{i}*77,'linewidth',3);hold on;
 Extra=3;i=2;
j=1+(Extra-1)*2;
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j,i});Beta_Sum=0.51;
R1 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j+1,i});Beta_Sum=0.51;
R2 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
IData=R1-R2;
IData.data=(IData.data+fliplr(IData.data))/2;
plot(IData,'-','linewidth',3)
 %%
 figure(19)
PeakList=[];

PeaksLocations=[-5.2,-2.75,0];

for Extra=3
    j=1+(Extra-1)*2;
    clf
for i=2:8
%2:10
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j,i});Beta_Sum=0.51;
R1 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j+1,i});Beta_Sum=0.51;
R2 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
IData=R1-R2;
% plot(IData.roi(1,-6.5,6.5),'-','linewidth',3)
IData.data=(IData.data+fliplr(IData.data))/2
plot(IData,'-','linewidth',3)
hold on
end
ChangePlotColor(@jet)
% PeakList=[];
for i=1:numel(PeaksLocations)
   out{i}=Fit_Line(2,'X',[PeaksLocations(i)-1,PeaksLocations(i)+1]) ;
   PeakList{i}=[out{i}.dY_0];
   
end
for i=1:numel(PeaksLocations)
PeakList{i}=PeakList{i}./PeakList{3}
end
end
%%
figure(21);clf

contour(smooth2(PeakListSimulation{1},3),'ShowText','on');
colormap(jet);
TXYL('P1','Collimation','lMR');
figure(22)
contour(smooth2(PeakListSimulation{2},3),'ShowText','on');
colormap(jet);
TXYL('P2','Collimation','lMR');

%%
figure(21);clf
contour(-smooth2(PeakListSimulation{1},3),-PeakList{1});
colormap(jet(9));
TXYL('1','Collimation','lMR');
figure(22); clf
contour(smooth2(PeakListSimulation{2},3),PeakList{2});
colormap(jet(9));
TXYL('2','Collimation','lMR');
%%
figure(23);clf
MaxY=10;cc1={};cc2={};
SmoothedPeakList{1}=smooth(PeakList{1},3);
SmoothedPeakList{2}=smooth(PeakList{2},3);
for i=1:numel(PeakList{1})
[cc1{i},~]=contour(180./(pi./Data4.axes.CollimationFWHM),Data4.axes.lMR,-smooth2(PeakListSimulation{1},3),[-SmoothedPeakList{1}(i),-SmoothedPeakList{1}(i)]);
Index=find(cc1{i}(1,:)>0);cc1{i}=cc1{i}(:,Index);
[cc2{i},~]=contour(180./(pi./Data4.axes.CollimationFWHM),Data4.axes.lMR,smooth2(PeakListSimulation{2},3),[SmoothedPeakList{2}(i),SmoothedPeakList{2}(i)]);
Index=find((cc2{i}(2,:)<=10).* ((cc2{i}(1,:)>=1)));
cc2{i}=cc2{i}(:,Index);
end
clf
  for i=1:numel(cc1)
     plot(cc1{i}(1,:),cc1{i}(2,:));hold on
  end
 for i=1:numel(cc1)
    plot(cc2{i}(1,:),cc2{i}(2,:),'-');hold on
 end
 ChangePlotColor(@(a)jet(7))

 
 LmrList=[];CollimationList=[];
 for i=1:numel(cc1)
  [xi,yi]=polyxpoly(cc1{i}(1,:),cc1{i}(2,:),cc2{i}(1,:),cc2{i}(2,:));
  plot(xi,yi,'k.')
  LmrList(i)=yi;CollimationList(i)=xi;
 end
%  CollimationList=180./pi./(interp1(1:numel(Data4.axes.CollimationFWHM),Data4.axes.CollimationFWHM,CollimationList));
% LmrList=interp1(1:numel(Data4.axes.lMR),Data4.axes.lMR,LmrList);
Ax=gca;Lines=Ax.Children;for i=1:numel(Lines);Lines(i).LineWidth=3;Lines(i).MarkerSize=25;end;
TXYL('P1 and P2 as function of BG','Collimation','Lmr','BG=%0.2f',{RelevantBGList(1:end-2)});
%%
T=@(t,kfd)exp(-pi*kfd*sin(t).^2);
kfdList=linspace(0.5,1.5,10);FWHMList=[];
clf

for i=1:numel(kfdList)
t=linspace(-pi/2,pi/2,100);plot(t,T(t,kfdList(i))-0.5);Fit_Line(@(x)exp(-x.^2));
FWHMList(i)=diff(Fit.X_0)/pi*180;
end
Kfd_Data=interp1(FWHMList,kfdList,CollimationList);
BGList=[];
for i=2:8
   BGList(i-1)=Scans.XEdge_BGlist_Holes_wrc{j,i}.Info.Channels.BG 
end
Kf=SqrtSign(pi*Vbg_to_n(BGList,0.7));

plot(-Kf,Kfd_Data,'o--');Fit_Line(1,'X',[1,1.5]*1e8)
TXYL('$k_Fd$ collimation parameters as funciton of $k_F$','$k_F$','$k_Fd$');
legend(sprintf('Slope=%0.1f [nm]',Fit.a*1e9),'location','northwest');