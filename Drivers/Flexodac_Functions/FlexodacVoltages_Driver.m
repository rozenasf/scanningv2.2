classdef FlexodacVoltages_Driver < handle
    properties
        Voltages
    end
    
    methods
        function obj=FlexodacVoltages_Driver()
            
        end

        function Set(obj,Channel,Value)
            global Info
            Info.Flexodac.F.do_ramp( GetShocketFromName(Channel) ,Value);
            obj.Voltages.(Channel)=Value;
        end
        function out=Get(obj,Channel)
            out= obj.Voltages.(Channel);
        end
        %
        %         %-----
        %         function out=Get(obj,Channel)
        %                 out=GetFromMemory(Channel);
        % %             out=nan;
        %         end
        %         function Set(obj,Channel,Value)
        %
        %         end
        
    end
end