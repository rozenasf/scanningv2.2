%%
figure(23);clf
MaxY=10;cc1={};cc2={};
SmoothedPeakList{1}=smooth(PeakList{1},3);
SmoothedPeakList{2}=smooth(PeakList{2},3);
for i=1:numel(PeakList{1})-2
[cc1{i},~]=contour(180./(pi./Data4.axes.CollimationFWHM),1./Data4.axes.lMR,-smooth2(PeakListSimulation{1},3),[-SmoothedPeakList{1}(i),-SmoothedPeakList{1}(i)]);
Index=find(cc1{i}(1,:)>0);cc1{i}=cc1{i}(:,Index);
[cc2{i},~]=contour(180./(pi./Data4.axes.CollimationFWHM),1./Data4.axes.lMR,smooth2(PeakListSimulation{2},3),[SmoothedPeakList{2}(i),SmoothedPeakList{2}(i)]);
Index=find((cc2{i}(2,:)<=20).* ((cc2{i}(1,:)>=1)));
cc2{i}=cc2{i}(:,Index);
end
clf
  for i=1:numel(cc1)
     plot(cc1{i}(1,:),cc1{i}(2,:));hold on
  end
 for i=1:numel(cc1)
    plot(cc2{i}(1,:),cc2{i}(2,:),'-');hold on
 end
 ChangePlotColor(@(a)jet(7))

 
 LmrList=[];CollimationList=[];
 for i=1:numel(cc1)
  [xi,yi]=polyxpoly(cc1{i}(1,:),cc1{i}(2,:),cc2{i}(1,:),cc2{i}(2,:));
  plot(xi,yi,'k.')
  LmrList(i)=1./yi;CollimationList(i)=xi;
 end
%  CollimationList=180./pi./(interp1(1:numel(Data4.axes.CollimationFWHM),Data4.axes.CollimationFWHM,CollimationList));
% LmrList=interp1(1:numel(Data4.axes.lMR),Data4.axes.lMR,LmrList);
Ax=gca;Lines=Ax.Children;for i=1:numel(Lines);Lines(i).LineWidth=3;Lines(i).MarkerSize=25;end;
TXYL('P1 and P2 as function of BG','Collimation','1./Lmr','BG=%0.2f',{RelevantBGList(1:end-3)},'location','west');