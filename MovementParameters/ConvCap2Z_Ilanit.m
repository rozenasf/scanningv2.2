    function out=ConvCap2Z()
        global Info S;
        persistent FitDataX;
        persistent FitDataY;
        if(~isempty('FitDataX'))
            load('ZsCapFitData','FitDataX','FitDataY');disp('loaded')
        end
        
        Info.ChannelType.CapUm={'ZsCapXum','ZsCapYum'};
        S.CreateScanOutput('ZsCapXum')
        S.CreateScanOutput('ZsCapYum')
        
        out=0;
        data=Info.Scan.idgr.ZsCapX.data;
%         data=data(~isnan(data));
        intXdata=interp1(FitDataX(:,2),FitDataX(:,1),data);
        data=Info.Scan.idgr.ZsCapY.data;
%         data=data(~isnan(data));
        intYdata=interp1(FitDataY(:,2),FitDataY(:,1),data);
        
%         Info.Scan.idgr.ZsCapXum=Info.Scan.idgr.ZsCapX;
        
        
        Info.Scan.idgr.ZsCapXum.data=intXdata;
        
%         Info.Scan.idgr.ZsCapYum=Info.Scan.idgr.ZsCapY;
        
        Info.Scan.idgr.ZsCapYum.data=intYdata;
        

    end