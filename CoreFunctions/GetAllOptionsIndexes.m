function out=GetAllOptionsIndexes(Size)
    a=[];
    [a{1:numel(Size)}]=ind2sub(Size,1:prod(Size));
    out=cell2mat(a')';
    