function Changed=UpadateDacInformation()
global Info S;
XMLDumpFile='D:\Our LabView 2010\dump.xml';
D=dir('D:\Our LabView 2010\dump.xml');q=D.datenum;
Changed=0;
try if(Info.Flags.LastDacParametersUpdate==q);return;end;end
Changed=1;

try Info.cr.init('Sample');catch fprintf('Dac information was changed, but problem in INIT!!!\n');end
Info.Dac=ReadDecadacInformation(XMLDumpFile);
Info.Flags.LastDacParametersUpdate=q;
disp('Decadac XML loaded!!!')
CalculateScalingFromDac();
S.Get('FVS2','FVS15','AVS2','AVS15');
disp('Loaded frequancy and amplitudes of S15 and S2');
end
