global Info S;
%Retract along the Z-axis
%initZ=S.Get('Zr');
S.Set('Zc',3500e-6);
%% Correct the angle manually
%% Continue approaching Z, finer
Info.Range.Beta=[0,1.0/350];
D_ZApprCoarse('Gate2',linspace(600e-3,400e-3,5),'Zc',linspace(S.Get('Zr'),4600e-6,10),'Vsd',30e-3);

Info.Flags.Stop=0;
DocSend({1,2,3,4});
%% Finer still
Info.Flags.Stop=0;
Info.Range.Beta=[0,1.0/250];
D_ZApprCoarse('Gate2',linspace(600e-3,400e-3,7),'Zc',linspace(S.Get('Zr'),S.Get('Zr')+400e-6,12),'Vsd',30e-3);
%% Finer still 2
DocSend({1,2,3,4});

Info.Flags.Stop=0;
Info.Range.Beta=[0,1.0/150];
D_ZApprCoarseAll('Gate2',linspace(600e-3,400e-3,7),'Zc',linspace(S.Get('Zr'),S.Get('Zr')+200e-6,40),'Vsd',30e-3);
%% Last Z movement
DocSend({1,2,3,4});

Info.Flags.Stop=0;
Info.Range.Beta=[0,1.0/80];
D_ZApprCoarseAll('Gate2',linspace(600e-3,400e-3,7),'Zc',linspace(S.Get('Zr'),S.Get('Zr')+100e-6,20),'Vsd',30e-3);

%%
DocSend({1,2,3,4});
Info.Flags.Stop=0;
Info.Range.Beta=[0,1.0/50];
%% Scan along Y-axis
D_YCourseScanFast('Gate2',400e-3,'Yc',linspace(S.Get('Yr'),3200e-6,40),'Vsd',30e-3);
%% Rescan in the opposite direction
D_YCourseScanFast('Gate2',400e-3,'Yc',linspace(S.Get('Yr'),3800e-6,30),'Vsd',30e-3);
%% Last Z movement


Info.Flags.Stop=0;
Info.Range.Beta=[0,1.0/50];
D_ZApprCoarseAll('Gate2',linspace(700e-3,550e-3,7),'Zc',linspace(S.Get('Zr'),S.Get('Zr')+20e-6,5),'Vsd',30e-3);
