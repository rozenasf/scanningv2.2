%% PLot Ey
ScanNumber = 2403;
SmoothN=1;

LoadScan(ScanNumber,L,'NoScale');
Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
% R = R.slice_index_range(2,1:7);
RR = R.*0; 
for i = 1: numel(R.axes{2}.data)
    RR.data(:,i) = smooth(R.data(:,i),SmoothN);
end 
figure(77);
YMean=(6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end)))./W;
plot(YMean,RR.diff(1,1).mean(2).data/1e9,'o-','linewidth',3);
xlim([-0.4,0.5])
ylabel('Ey (V/nm)'); 
xlabel('Y/W');

% YMean=6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end))./W;
%  plot(YMean,diff(smooth(R.mean(2).data,3)),'o-','linewidth',3)
%%
%% PLot Ey
ScanList=[2401:2412];RRFull={};
figure(78);clf
for j=1:numel(ScanList)
ScanNumber = 2407;
SmoothN=1;

LoadScan(ScanList(j),L,'NoScale');
Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
%     R = R.slice_index_range(2,1:8);
RR = R.*0; 
for i = 1: numel(R.axes{2}.data)
    RR.data(:,i) = smooth(R.data(:,i),SmoothN);
end 
RRFull{mod(j+1,2)+1}=RR.data;

YMean=(6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end)))./W;
if((mod(j+1,2)+1)==2)
    RR.data=RRFull{1}-RRFull{2};
plot(YMean,RR.diff(1,1).mean(2).data/1e9,'o-','linewidth',3);
xlim([-0.4,0.5])
ylabel('Ey (V/nm)'); 
xlabel('Y/W');
hold on
end
end
%% Average Ey using dummy
ScanStartNumber = 2390 ; % 95-96 w/rc = 5  %2392-94 w/rc = 8   %2390-91 w/rc = 7      %2374-75
ScanEndNumber = 2391;
SmoothN=3;
counter = 1;
for i = ScanStartNumber:ScanEndNumber
    ScanNumber = i;
    LoadScan(ScanNumber,L,'NoScale');
    Beta_Sum=1/1.2;
    W = 9e-6;
    R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
%     R = R.slice_index_range(2,1:12);   %.................................remove this if needed
    if(ScanNumber == ScanStartNumber)
        Ravg = R.*0;
        Ravg = Ravg.diff(1,1);
    end
%     R = R.slice_index_range(2,1:12);
    RR = R.*0;
    for j = 1: numel(R.axes{2}.data)
        RR.data(:,j) = smooth(R.data(:,j),SmoothN);
    end
    Ravg = Ravg + RR.diff(1,1);     
    counter = counter+1;
end
Ravg = Ravg./counter;
YMean=6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end))./W;
% figure(559); hold on;
figure(105); hold on;
% surf(-Ravg.mean(2));
plot(YMean,Ravg.mean(2).data/1e9,'o-','linewidth',3);
ylabel('Ey (V/nm)'); 
xlabel('Y/W');
%%

LoadScan([2377],L,'NoScale');
SmoothN=3;
Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;

R = R.slice_index_range(2,1:7);
% R=RPA.roi(3,1,2).mean(3).data;
YMean=6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end))./W;
figure(17)
%  R1 = diff(smooth(R.mean(2).data,3));
 plot(YMean,diff(smooth(R.mean(2).data,3)),'o-','linewidth',3)
%  figure(17)
%   R2 = diff(smooth(fliplr(R.mean(2).data),3));
%  plot(YMean,R2-R1,'-','linewidth',3) 
 
% plot(YMean,1/1.8*diff(smooth(R(:,4),SmoothN)-smooth(R(:,5),SmoothN))./Beta_Sum,'linewidth',3)
%%
LoadScan([2372],L,'NoScale');
SmoothN=3;
Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
R = R.slice_index_range(2,1:12);
% R=RPA.roi(3,1,2).mean(3).data;
YMean=6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end))./W;
figure(10);
R1 = -diff(smooth(R.mean(2).data,3));
%  plot(YMean,diff(smooth(R.mean(2).data,3)),'o-','linewidth',3)


LoadScan([2373],L,'NoScale');
SmoothN=3;
Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
R = R.slice_index_range(2,1:15);
% R=RPA.roi(3,1,2).mean(3).data;
YMean=6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end))./W;
figure(10);
R2 = diff(smooth(R.mean(2).data,3));
%  plot(YMean,diff(smooth(R.mean(2).data,3)),'o-','linewidth',3)

figure(54); hold on
plot(YMean,R1+R2,'o-','linewidth',3)
% TXYL('Ey at w/rc = 4.1 BG = -9.9 (+-B)','Y/W','Ey')
%%
LoadScan([2373],L,'NoScale');
SmoothN=3;
Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
% R=RPA.roi(3,1,2).mean(3).data;
YMean=6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end))./W;
% R1 = diff(smooth(R.mean(2).data,3));
%%

%% 3D plots of Ey
LoadScan([2372],L,'NoScale');
SmoothN=3;
Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
% R=RPA.roi(3,1,2).mean(3).data;
YMean=6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end))./W;
RR = R.*0;
for i = 1: numel(R.axes{2}.data)
    RR.data(:,i) = smooth(R.data(:,i),3);
end
R2372 = -RR.diff(1,1);
figure(887);
surf(-R2372); colormap jet;

%%  average scans 2373 -> 2375

LoadScan([2373],L,'NoScale');
SmoothN=3;
Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
% R=RPA.roi(3,1,2).mean(3).data;
YMean=6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end))./W;
RR = R.*0;
for i = 1: numel(R.axes{2}.data)
    RR.data(:,i) = smooth(R.data(:,i),3);
end
R2373 = -RR.diff(1,1);

LoadScan([2374],L,'NoScale');

Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
% R=RPA.roi(3,1,2).mean(3).data;
YMean=6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end))./W;
RR = R.*0;
for i = 1: numel(R.axes{2}.data)
    RR.data(:,i) = smooth(R.data(:,i),3);
end
R2374 = -RR.diff(1,1);

LoadScan([2375],L,'NoScale');

Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
% R=RPA.roi(3,1,2).mean(3).data;
YMean=6e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end))./W;

RR = R.*0;
for i = 1: numel(R.axes{2}.data)
    RR.data(:,i) = smooth(R.data(:,i),3);
end
R2375 = -RR.diff(1,1);


Ravg = (R2374+R2373+R2375)./3;

figure(888);
surf(Ravg); colormap jet;

%  plot(YMean,diff(smooth(R.mean(2).data,3)),'-','linewidth',3)
%  figure(17)
%%
%plots +B
Start = 2401;
End = 2413;
figure(788);clf;
for i = Start:2:Endsour
ScanNumber = i;
SmoothN=1;

LoadScan(ScanNumber,L,'NoScale');
Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
% R = R.slice_index_range(2,1:7);
RR = R.*0; 
for i = 1: numel(R.axes{2}.data)
    RR.data(:,i) = smooth(R.data(:,i),SmoothN);
end 
figure(788);hold on
YMean=(5.8e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end)))./W;
plot(YMean,RR.diff(1,1).mean(2).data/1e9,'o-','linewidth',3);
xlim([-0.43,0.5])
ylabel('Ey (V/nm)'); 
xlabel('Y/W');
TXYL('BG = -9.9 only +B ','Y/W','Ey (V/nm)','w/rc = 2','3','4','5','6','7')
end


%plots -B

figure(789);clf;
for i = (Start+1):2:End
ScanNumber = i;


LoadScan(ScanNumber,L,'NoScale');
Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
% R = R.slice_index_range(2,1:7);
RR = R.*0; 
for i = 1: numel(R.axes{2}.data)
    RR.data(:,i) = smooth(R.data(:,i),SmoothN);
end 
figure(789);hold on
YMean=(5.8e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end)))./W;
plot(YMean,RR.diff(1,1).mean(2).data/1e9,'o-','linewidth',3);
xlim([-0.43,0.5])
ylabel('Ey (V/nm)'); 
xlabel('Y/W');
TXYL('BG = -9.9 only -B ','Y/W','Ey (V/nm)','w/rc = 2','3','4','5','6','7')
end

%antisymm data
figure(800);clf;
ScanList=[Start:End];RRFull={};
for j=1:numel(ScanList)


LoadScan(ScanList(j),L,'NoScale');
Beta_Sum=1/1.2;
W = 9e-6;
R=Beta_HS15_Hset./Beta_Sum.*AVS15./IgrAC15;
%     R = R.slice_index_range(2,1:8);
RR = R.*0; 
for i = 1: numel(R.axes{2}.data)
    RR.data(:,i) = smooth(R.data(:,i),SmoothN);
end 
RRFull{mod(j+1,2)+1}=RR.data;

YMean=(5.8e-6+0.5*(Beta_HS15_Hset.axes{1}.data(1:end-1)+Beta_HS15_Hset.axes{1}.data(2:end)))./W;
if((mod(j+1,2)+1)==2)
    RR.data=RRFull{1}-RRFull{2};
plot(YMean,RR.diff(1,1).mean(2).data/1e9,'o-','linewidth',3);
xlim([-0.45,0.5])
TXYL('BG = -9.9 antisymm ','Y/W','Ey (V/nm)','w/rc = 2','3','4','5','6','7')
hold on
end
end
