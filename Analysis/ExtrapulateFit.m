function out=ExtrapulateFit(Fit,point)
    for i=1:numel(Fit)
       out(i)=Fit(i).Func( point);
    end
end