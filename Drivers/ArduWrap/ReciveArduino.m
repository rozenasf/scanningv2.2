function [opcode,out]=ReciveArduino(Communication,Reciveformat,N)
    if(~exist('N','var'));N=1;end
    ExpectedLength=FormatLength(['B',Reciveformat]);
    Recived=fread(Communication,ExpectedLength*2*N);
    Readout=sscanf(char(Recived'),'%02X')';
    Data=Format2Data(repmat(['B',Reciveformat],1,N),Readout);
    opcode=char(Data(1));
    out=Data; %ignoring opcodes
   out(1:(1+numel(Reciveformat)):numel(out))=[];
end