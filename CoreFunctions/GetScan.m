function GetScan(Channel,Dim)
global Info S
if(~exists('Dim'));Dim=1;end
Info.Scan=rmfield(Info.Scan,'Start');
Info.Scan=rmfield(Info.Scan,'End');
Info.Scan=rmfield(Info.Scan,'WaitTimes');
for i=1:numel(Info.Scan.Get)
    if(i==Dim)
        Info.Scan.Get{i}={Channel};
    else
        Info.Scan.Get{i}={};
    end
end
S.RunScan();
end