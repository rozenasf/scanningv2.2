function out=GetActiveFigures()
    global Info;out={};
    for i=1:numel(Info.Fig)
        if(isa(Info.Fig{i},'handle') && isvalid(Info.Fig{i}))
            out{end+1}=i;
        end
    end
end