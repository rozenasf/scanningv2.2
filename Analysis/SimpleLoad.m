     if(0)  
        %%
global Info S L;
%load ([pwd,'\Parameters.mat']);
%L.MainPath='H:\Measurements\Scanning SET technique\Data\';
L.MainPath='C:\Users\asafr\Google Drive\witz\phd\measurments\Scanning SET technique'
L.ActiveNotebook='F5_Manchester1';
L.Bind=1;
L.File=[];
Info.ScanNumber=FindLastScanNumber()+1;
        %%
        
[idgr,std_idgr]=LoadScan([],L);
     end
%%
idgr={};
T=[];
for i=293:305
    idgr{i}=LoadScan(i,L);
    a=regexp(Info.Scan.StartDateTime,'_','split');
    b=[a{end-5},'_',a{end-4},'_',a{end-3},'_',a{end-2},'_',a{end-1},'_',a{end}];
    T(i)=datenum(datetime(b,'InputFormat','yyyy_MM_dd_HH_mm_ss'));
    
end
Tini=T(293);
for i=293:305
    T(i)=(T(i)-Tini)*86400;
end
%%
figure(1)
for i=293:305
   plot(idgr{i}.Time.data(:)+T(i),idgr{i}.ZsCapY.data(:))
   hold on
end
hold off
%%

description={...
'Dummy',...    
'0.4-0.9',... 
'0.4-0.9',... 
'0.8-2',... 
'Dummy',... 
'2-3',... 
'Dummy',... 
'Dummy',...
'3-4',... 
'4-5',... 
'Dummy',... 
'5-6',... 
'zero heater + Dummy'... 
};
%%
FigSend(1,'ZsCapY_Vs_Time')
%%
for i=293:305
   figure(2)
    plot(idgr{i}.Hgr1);
   xlim([1.02,1.13])
   drawnow
   Fit_Line(2,'all')
   idgr{i}.PeakLocations=fliplr([Fit.dY_0]);
end
%%
figure(4)
for i=293:305
   plot(idgr{i}.Time.data(10,1:numel(idgr{i}.PeakLocations))+T(i),1./idgr{i}.PeakLocations)
   hold on
end
hold off
%%
legend(description)
%%
figure(3)
FigSend(3,'Hgr1_peak_Vs_Time')
%%
figure(1)

for i=293:305
    idgr{i}.PeakLocations=[];
   plot(idgr{i}.Beta_Sum)
   %hold on
   %xlim([1.12,1.3])
   drawnow
   Fit_Line(0,'all')
   idgr{i}.PeakLocations=fliplr([Fit.fit]);
end
%hold off
%%
figure(5)
for i=293:305
   plot(idgr{i}.Beta_Sum)
   hold on
end
hold off
%title('Heating from 0.4 to 6V')
%%
figure(7)
set(0,'DefaultAxesColorOrder',jet(256))'
%%

plot(idgr{305}.R1)
%%
figure(7)

for i=296:304
    idgr{i}.PeakLocations=[];
   plot(idgr{i}.R1)
   hold on
   %xlim([1.12,1.3])
   drawnow
   %Fit_Line(0,'all')
   %idgr{i}.PeakLocations=fliplr([Fit.fit]);
end
hold off