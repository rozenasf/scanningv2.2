classdef FeedBack_Driver < handle
    properties 
       UseLastWorkpoint 
    end
    methods
        function obj=FeedBack_Driver()
            obj.UseLastWorkpoint=0;
        end
        function out=Get(obj,Channel)
            global Info S;
            
            if(~Info.Flags.RunningScan || Info.Scan.Runtime.PositionIndex==1 || obj.UseLastWorkpoint)
                WorkPoint=GetFromMemory('WorkPoint');
                %obj.UseLastWorkpoint=0;
            else
                Position=Info.Scan.Runtime.Position;
                for i=1:Info.Scan.Runtime.Dimentions
                    if(Position(i)>1)
                        Position(i)=Position(i)-1;
                        break;
                    end
                end
                WorkPoint=GetFromScan('WorkPoint',Position);
            end
            S.Set('WorkPoint_Initial',WorkPoint);
            Info.cr.set('FdBck',WorkPoint);
            out=Info.cr.get('FdBck');
        end
        function Zero(~)
            global Info
            Info.cr.init('FdBck');
        end
        function Center(~)
            global Info S;
            WorkPoint=GetFromMemory('WorkPoint');
            if(abs(WorkPoint)>0.175)
                error('somthing is wrong, center is trying to move more than 175mV');
            end
            Vg=GetFromMemory('Vg');
            NewVg=Vg+WorkPoint;
            S.Set('Vg',NewVg);
            S.Set('WorkPoint',0);
            S.Get('WorkPoint')
        end
        function SetUseLastWorkpoint(obj)
            obj.UseLastWorkpoint=1;
        end
    end

end