    function out=PlotErrorBarInverse(Fig,varargin )
        out=0;global Info;figureN(Fig);
        in=RegExpChannels(varargin);
        for i=1:numel(in)/2
            idgr=1./Info.Scan.idgr.(in{2*i-1}).slice_index(1,Info.Scan.Runtime.MaxPosition(1));
            idgr_err=Info.Scan.idgr.(in{2*i}).slice_index(1,Info.Scan.Runtime.MaxPosition(1))./Info.Scan.idgr.(in{2*i-1}).slice_index(1,Info.Scan.Runtime.MaxPosition(1)).^2;
            for j=3:numel(Info.Scan.Runtime.Position)
                idgr=idgr.slice_index(2,Info.Scan.Runtime.Position(j));
                idgr_err=idgr_err.slice_index(2,Info.Scan.Runtime.Position(j));
            end
            errorbar(idgr,idgr_err)
            hold on
        end
        Zlist=Info.Scan.Axis.(Info.Scan.Runtime.AxisNames{2});
        plot(Zlist,Zlist*0+Info.Range.Beta(2),'r');
        hold off
    end