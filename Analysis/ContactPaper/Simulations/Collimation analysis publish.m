%% 
% The goal of this document is to esimate the pn junction collimation effect 
% by comparing measured collimation with simulation
% 
% Sharvin resistance of a channel (the two sides): $\frac{\pi h}{4e^2 k_F 
% W}$
% 
% for a perfect but rough P-N junciton 
% 
% $$|t(\theta)|=cos^2(\theta)$$
% 
% so the total transmission is $\frac{4e^2}{h}\frac{k_F}{2}$ for a perpenicular 
% collimated beam, or (my interpretetion) a rejection of 0.5 for all particles, 
% hence twice the natural sharvin resistnace. for a smooth P-N junction (or wide 
% P-N with rough edges) the tranmission probability is 
% 
% $$|t(\theta)|=e^{-\pi k_F \cdot d \cdot sin^2\theta}$$
% 
% so that the total transmission is $\frac{2e^2}{\pi h}\sqrt{\frac{k_F}{d}}$, 
% for a directed beam. In term of rejection:
% 
% $$T=\int_{-\pi}^{\pi}{\frac{1}{2\pi}e^{-\pi k_F \cdot d \cdot sin^2\theta}}d\theta\approx\frac{1}{2\pi}\sqrt{\frac{1}{k_Fd}}$$
% 
% when the collimation is large. if it is small, need to solve numerically.

F507_Contact_Resistance_BG_For_MLX
%% 
% plotting the Hall resistance close to the edge of the channel, for different 
% BG at W/Rc = 2. by comparing with simulation, this is closer to W/Rc=1.8. this 
% difference is important in the following Lmr analysis, but less important for 
% the collimation estimation.

F502_Xedge_BG_WRc_2_Measurement
%% 
% There are two numbers extracted from this measurement, $\frac{P_1}{P_C}$ 
% and $\frac{P_2}{P_C}$ where $P_C$ is the peak at $X=0$
% 
% extracting the collimation parameters:

clf;plot(RelevantBGList,smooth(PeakList{1},3),'o--');hold on;plot(RelevantBGList,smooth(PeakList{2},3),'o--');TXYL('Collimation paramters','BG','Parameter','P1','P2')
%% 
% The simulation is run to map out the phase space Lmr and collimation. 
% the collimation angle $\Delta\theta_{emitted}$ is defined as the FWHM of the 
% gaussian distributed beam.
% 
% manually fitting the lowest BG (to get a feel of the fit quality - 3 fitting 
% parameters, cant expect less then perfect... ) 

F503_single_simulation_fit

%% 
% From the simulation we can create two maps describing P1 and P2 parameters:

F504_Simulation_Lmr_Collimation_maps
%% 
% Plotting the relevant contures for each of the BG values. The Black points 
% is the solution for this equation system

F505_Calculate_Collimation_Lmr_From_simulation_and_measurement
%% 
% Finally the width of the P-N juction is the slope:

F506_kFd_fit
%% 
% and the Lmr is very small. this might change because W/Rc is not callibrated 
% correctly (the center peak is very sensative to interference, and can mistake 
% it to be low Lmr).
% 
%