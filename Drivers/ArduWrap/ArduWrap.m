classdef ArduWrap < handle
    properties
        Communication
        Message
        SendFormat
        ReceiveFormat
        OpcodesLocations
        Data
        Opcode
        RawInput
        
    end
    methods
        function obj=ArduWrap(ComPort)
            obj.Communication=serial(ComPort,'BaudRate', 115200);
            fopen(obj.Communication);
        end
       
        %--------------------------------%
        %%            Main               %%
        %--------------------------------%
        function [Data_out,Opcode_out]=Quary(obj)
            flushinput(obj.Communication);
            fwrite(obj.Communication,obj.Message);
            obj.RawInput=fread(obj.Communication,FormatLength(obj.ReceiveFormat)*2);
            ParseReceive(obj);
            Opcode_out=obj.Opcode;
            Data_out=obj.Data;
            obj.Message=[];obj.ReceiveFormat=[];obj.OpcodesLocations=[];
        end
        function AppendMessage(obj,opcode,SendData,Sendformat,Reciveformat)
            Message_p=BuildMessage(opcode,SendData,Sendformat,Reciveformat);
            obj.Message=[obj.Message,Message_p];
            obj.ReceiveFormat=[obj.ReceiveFormat,'B',Reciveformat];
            obj.OpcodesLocations=[obj.OpcodesLocations,1,zeros(1,numel(Reciveformat))];
        end
        function ParseReceive(obj)
            Readout=sscanf(char(obj.RawInput'),'%02X')';
            out=Format2Data(obj.ReceiveFormat,Readout);
            obj.Opcode=   char(out(obj.OpcodesLocations==1));
            obj.Data  =   out (obj.OpcodesLocations==0);
        end
        
        function Clear(obj)
            obj.Message=[];obj.ReceiveFormat=[];obj.OpcodesLocations=[];
            obj.Opcode=[];obj.RawInput=[];obj.Data=[];
        end
        
        function delete(obj)
            fclose(obj.Communication);
            delete(obj.Communication);
            disp('disconnected from feedback!');
        end
    end
    
end