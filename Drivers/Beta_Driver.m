classdef Beta_Driver < handle
    methods
        function out=Get(~,Channel)
            global Info S;
            BreakText=regexp(Channel,'_','split');
            switch(numel(BreakText))
                case 3
                    Names=fieldnames(Info.Scan.Axis);
                    if (Info.Flags.RunningScan && Info.Scan.Runtime.GetDim == 2 && ...
                            (strcmp('Vg_Ramp',Names{1}) || strcmp('Vg',Names{1})) )
                        out=FitData(BreakText{3},BreakText{2},1,Info.Scan.Runtime.Position);
                    else
                        out=GetFromMemory(BreakText{2})/GetFromMemory(BreakText{3});
                    end
                case 2 %Beta_Sum
                    if(GetFromMemory('FVS15')~=GetFromMemory('FVS2'))
                        out=0;
                        List={'Beta_Hbg_Hset','Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset'};
                        for i=1:(numel(List))
                            out=out+GetFromMemory(List{i});
                        end
                    else
                        if(sign(GetFromMemory('AVS15'))==sign(GetFromMemory('AVS2')))
                            out=0;
                            List={'Beta_Hbg_Hset','Beta_HRest_Hset','Beta_HS15_Hset'};
                            for i=1:(numel(List))
                                out=out+GetFromMemory(List{i});
                            end
                        else
                            out=nan;
                        end
                    end
                    S.Set('InverseBetaSum',1./out);
            end
        end
    end
end