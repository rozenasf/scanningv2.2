classdef OpenLoopMovement < handle
    properties
        Xc_Initial
        Yc_Initial
        Nx
        Ny
        StartingTolerance
    end
    
    methods
        
        function obj=OpenLoopMovement()
            obj.StartingTolerance=2e-6;
            obj.Nx=1;obj.Ny=1;
        end
        
        function Set(obj,Channel,Value)
            global S Info
            Axis=fieldnames(Info.Scan.Axis);
            if    ( (numel(Axis)==2) &&  strcmp(Axis{1},'X_OpenLoop') && strcmp(Axis{2},'Y_OpenLoop') )
                
                XY_Channel(obj,Channel,Value);
                
            elseif( (numel(Axis)==1) &&  strcmp(Axis{1},'X_OpenLoop') )
                
                X_Channel(obj,Channel,Value)
                
            elseif( (numel(Axis)==1) &&  strcmp(Axis{2},'Y_OpenLoop') ) 
                
                Y_Channel(obj,Channel,Value)
                
            else
                error('Axis type doesnt match OpenLoop coarse scan!');
            end
            
        end
        function XY_Channel(obj,Channel,Value)
            global S;
            switch Channel
                case 'X_OpenLoop'
                    if(Value>1)
                        S.Set('Xo',obj.Nx);
                        S.Get('Xr');
                    end
                case 'Y_OpenLoop'
                    if(Value>1)
                        S.Set('Xc',obj.Xc_Initial);
                        S.Set('Yo',obj.Ny);
                        S.Get('Yr');
                    else
                        obj.Xc_Initial=S.Get('Xr');
                        obj.Yc_Initial=S.Get('Yr');
                    end
                case 'Goto_Origin_OpenLoop'
                    S.Set('Xc',obj.Xc_Initial);
                    S.Set('Yc',obj.Yc_Initial);
            end
        end
        function X_Channel(obj,Channel,Value)
            global S;
            switch Channel
                case 'X_OpenLoop'
                    if(Value>1)
                        S.Set('Xo',obj.Nx);
                        S.Get('Xr');
                    else
                        obj.Xc_Initial=S.Get('Xr');
                    end
                case 'Goto_Origin_OpenLoop'
                    S.Set('Xc',obj.Xc_Initial);
            end
        end
        function Y_Channel(obj,Channel,Value)
            global S;
            switch Channel
                case 'Y_OpenLoop'
                    if(Value>1)
                        S.Set('Yo',obj.Yx);
                        S.Get('Yr');
                    else
                        obj.Yc_Initial=S.Get('Yr');
                    end
                case 'Goto_Origin_OpenLoop'
                    S.Set('Yc',obj.Yc_Initial);
            end
        end
    end
end