function out_Handle=AddGDS(GDS,Matrix,Type)
%GDS=ExtractLayers(gds_load([L.MainPath,L.ActiveNotebook,'\FullDesign_17032018_beamer.GDS']),'MainSymbol',[0,1,2,3]);
Ax=gca;

Matrix=sprintf('matrix(%e,%e,%e,%e,%e,%e)',Matrix);

%[1,0,0,1,4100.5,2240]*1e-6
if(~exist('Type'));Type=0;end
F=gcf;
Children=F.Children;
for i=1:numel(Children)
    
    if(strcmp(class(Children(i)),'matlab.graphics.axis.Axes'))
        axes(Children(i));
        Xlim=xlim();Ylim=ylim();
        hold on
        if(Type)
            PlotGDS(applyTransform(GDS,Matrix));
        else
            PlotGDSOutline(applyTransform(GDS,Matrix));
        end
        hold off
        xlim(Xlim);ylim(Ylim);
    end
end

end