function Shocket_0_F=GetShocketFromName(Channel)
    global Info
    names=(Info.ChannelType.FlexodacVoltages);
    if(numel(names)~=16);error('Must be 16 names!');end
    Location=find(strcmp(names,Channel));
    if(numel(Location)~=1);error('cant find channel / duplicate channel');end
    Shocket_0_F=Location-1;
end