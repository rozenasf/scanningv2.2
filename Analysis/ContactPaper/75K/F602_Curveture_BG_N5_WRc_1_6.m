%1313 is the BG=-5, 1315 is the BG=-9.9 perliminary
%new set starts at : 1331 up to : ?
%------------------------------------%
%   RUN F601 to get the ExportLmr!   %
%------------------------------------%
Data=[];
ScaleY=1.2207; % from scan 1329 see notebook

BaseScanList={};
BaseScanList{1}=1331:1346; %-9.9 -> 1.07
BaseScanList{2}=1348:1353; %-5  -> 2
BaseScanList{3}=1356:1373; %-2  -> 2.43
BaseScanList{4}=1375:1377; %-1.5 -> 2.32
BaseScanList{5}=1383:1398; %-2 at 7.5mV sum -> 2.76
out={};
for l=1:4
    Beta_Sum_List=[];EyMasked=[];k=0;
for  BaseScan=BaseScanList{l}
    k=k+1;
RSmooth={};R={};
figure(1);clf;figure(2);clf
for i=1:4
    LoadScan(BaseScan+0.01*i,L,'NoScale')
    BetaSum = 0.46;
    R{i} = CenterIDataAxis(Beta_HS15_Hset./BetaSum .*AVS15./IgrAC15,[1,2],1e6);
    figure(1);hold on
    plot_with_color(R{i},1)
    hold on
    plot(R{i}.mean(2),'linewidth',3)
    hold off
    RSmooth{i} = smooth(R{i}.mean(2).data,1);
    Beta_Sum_List(end+1)=Info.Scan.UserData.Initial_Beta_Sum;
end
M=jet(4);ChangePlotColor(@(~)M(ones(5,1)*[1:4],:))
Y=(R{1}.axes{1}.data(1:end-1)+R{1}.axes{1}.data(2:end))/2;
Y=Y* ScaleY; %From lior analysis in Heatup 75K -> scaling XY and psf see notebook
%
Boundaries=[-0.0,0.0];ErfPar=4;
R_Mask=(1+erf(ErfPar*(Y+Boundaries(2))));
L_Mask=(1-erf(ErfPar*(Y+Boundaries(1))));
Total_Mask=R_Mask+L_Mask;
Ey1=-(diff(RSmooth{1}-RSmooth{2}));
Ey2=-(diff(RSmooth{3}-RSmooth{4}));
figure(3);clf
    plot(Y,Ey1,'o--');
hold on
 plot(Y,Ey2,'o--');
 plot(Y,R_Mask);plot(Y,L_Mask);
hold off
figure(4);clf
EyMasked(:,k)=(Ey1.*R_Mask'+Ey2.*L_Mask')./Total_Mask';
% plot(Y,(EyMasked+flipud(EyMasked))/2,'o--');
plot(Y-0.00,EyMasked(:,k),'o--');
 ylim([0,10])
 Fit_Line(2,'X',[-1.25,1.25])
Curvature = -[Fit.a]./[Fit.Y_0]*5^2;
%ylim([0,[Fit.Y_0]*1.2])
TXYL(sprintf('curvature=%0.1f',Curvature),'Y [um]','Hall R [ARB]');
end
%%
figure(602);subplot(2,2,l);
CurList=[];
%for i=1:size(EyMasked,2)
Data(l,:)=mean(EyMasked,2);
    plot(Y,Data(l,:),'o');
    out{l}=Fit_Line(2,'X',[-1.25,1.25])
    Curvature = -[out{l}.a]./[out{l}.Y_0]*5^2;
    CurList(end+1)=Curvature;
    ylim([0,[Fit.Y_0]*1.2])
    TXYL(sprintf('curvature=%0.2f@BG=%0.1f[V]',Curvature,Info.Channels.BG),'Y [um]','Hall R [ARB]');
    xlim([-3,3])
    P1=patch([-3,-2.5,-2.5,-3],[0,0,1,1]*max([Fit.Y_0]*1.2),'k');P1.FaceAlpha=0.5;P1.EdgeAlpha=0;
    P2=patch([3,2.5,2.5,3],[0,0,1,1]*max([Fit.Y_0]*1.2),'k');P2.FaceAlpha=0.5;P2.EdgeAlpha=0;
    %end
end
%%
BG2LMR=[-1.05305119e-06,-4.38744380e-05,-7.57166633e-04,-7.14722794e-03,-4.19879822e-02,-1.71069158e-01,-4.94630562e-01,4.27898584e-01];
Colors='rgb';
figure(6021);clf
out={};
k=0;BGLL=[-9.9,-5,-1.6];
for i=[1,2,4]
k=k+1;
   subplot(3,2,k*2-1);
   fit=polyfit(Y/5,Data(i,:),2);MAX=polyval(fit,-fit(2)/(2*fit(1)));
   plot(Y/5,Data(i,:)./MAX,Colors(k),'linewidth',3);hold on
   patch([-0.5,-0.5,-0.6,-0.6],[0,1.05,1.05,0],'k','FaceAlpha',0.3)
    patch(-[-0.5,-0.5,-0.6,-0.6],[0,1.05,1.05,0],'k','FaceAlpha',0.3)
out{i}=Fit_Line(2,'X',[-1.25,1.25]/5);
ylim([0,1.05])
XX=linspace(-1.5,1.5,100);

   plot(XX,out{i}(1).Func(XX),'k--','linewidth',2);
   ylim([0,1.05]);xlim([-1,1])
   grid('minor')

TXYL(sprintf('$k_F=%0.0f \\ [1/\\mu m]$, curvature: %0.1f , Lmr/W: %0.2f ',-sqrt(pi*abs(Vbg_to_n(BGLL(k),0.6)))/1e6,(out{i}(1).a./out{i}(1).dY_0),polyval(BG2LMR,BGLL(k))),'$Y/W$','$Ey [arb]$');
end
subplot(2,2,2);cla
for i=1:5
    IDX=find(ExportLmr{i}.BG<0.3);
    plot(-sqrt(pi*abs(Vbg_to_n(ExportLmr{i}.BG(IDX),0.6)))/1e6,ExportLmr{i}.Lmr(IDX)/5e-6,'o-','linewidth',3);hold on
end
% xlim([-10,0.2])
ChangePlotColor
BGListCurvatures=[-9.9,-5,-2,-1.6];
k=0;
for i=[1,2,4]
    k=k+1;
plot(-sqrt(pi*abs(Vbg_to_n(BGListCurvatures(i)*[1,1],0.6)))/1e6,[0,1.5],[Colors(k),'-'],'linewidth',5)
end
TXYL('normalized mean free path @ W/R_c','$k_F [1/\mu m]$','$\frac{l_{MR}}{W} [\mu m]$','$W/R_c=%d$',{[0,1,2,4,6]},'location','southwest')
hLegend = findobj(gcf, 'Type', 'Legend');hLegend.FontSize=8;
subplot(2,2,4)
k=0;
for i=[1,2,6]
    k=k+1;
    IDX=find(abs(ExportMR{i}.WRc)~=4);
    if(i==1);
        plot(ExportMR{i}.WRc(IDX),ExportMR{i}.Lmr(IDX)./5e-6,[Colors(k),'.'],'linewidth',3,'markersize',30);hold on
    else
        plot(ExportMR{i}.WRc(IDX),ExportMR{i}.Lmr(IDX)./5e-6,[Colors(k),'.'],'linewidth',3,'markersize',20);hold on
    end
end
% TXYL('normalized mean free path @ k_F','$W/Rc$','$\frac{l_{MR}}{W} [\mu m]$','$k_F=%d \\ [1/\\mu m]$',{[-140,-102,-64]},'location','northwest')
TXYL('normalized mean free path @ k_F','$W/Rc$','$\frac{l_{MR}}{W} [\mu m]$')
% ylim([0.5,1.5])

hLegend = findobj(gcf, 'Type', 'Legend');hLegend.FontSize=8;