function ZeroFlexodac()
global Info S;
VoltagesNames=(Info.ChannelType.FlexodacVoltages);
S.Set('AC',0);
for i=1:numel(VoltagesNames)
    S.Set(VoltagesNames{i},0);
    S.Set(['A_',VoltagesNames{i}],0);
    if(isnan(GetFromMemory(['F_',VoltagesNames{i}])))
        S.Set(['F_',VoltagesNames{i}],100);
    else
        S.Set(['F_',VoltagesNames{i}],GetFromMemory(['F_',VoltagesNames{i}]));
    end
end
end