function RampChannel(Channel,Target,Rate,MaxStep)
    global Info S;
    RampTic=tic;
    Start=GetFromMemory(Channel);
    if(isnan(Start));error('Cant ramp from nan!!!! set the channel manualy first');end
    ValueSet=Start;
    Direction=sign(Target-Start);
    i=1;
    MaxMeasuredStep=0;
    while(  Direction*ValueSet < Direction*Target  )
        i=i+1;
        S.Set(Channel,ValueSet);
        pause(0.1);
        Time=toc(RampTic);
        
        SetTimeValue=Time*Rate*Direction+Start;
        DeltaTimeValue=SetTimeValue-ValueSet;
        
        DeltaABS = min(abs([DeltaTimeValue,MaxStep]));
        ValueSet=ValueSet+Direction*DeltaABS;
        MaxMeasuredStep=max([MaxMeasuredStep,DeltaABS]);
    end
    S.Set(Channel,Target);
    
    fprintf('Done in %d steps, average step is %d, max step is %d\n',...
        i,(Target-Start)/i,MaxMeasuredStep*Direction);
end