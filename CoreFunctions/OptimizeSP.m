function SP=OptimizeSP(in)
    SP=[1,1;1,2;2,2;2,3;3,3;3,4;4,4];
    ASP=SP(:,1).*SP(:,2);
    OptionSP=find(diff(ASP<in))+1;
    SP=SP(OptionSP,:);
    if(isempty(SP));SP=[1,1];end
end