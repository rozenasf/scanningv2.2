function out=smooth2(in,N)
for i=1:size(in,1)
    in(i,:)=smooth(in(i,:),N);
end
for i=1:size(in,2)
    in(:,i)=smooth(in(:,i),N);
end
out=in;
end
