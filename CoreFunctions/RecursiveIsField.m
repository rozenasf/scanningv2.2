function Field=RecursiveIsField(Base,varargin)
    Field=[];
    for i=1:numel(varargin)
        if(isfield(Base,varargin{i}))
            Base=Base.(varargin{i});
        else
            return;
        end
    end
    Field=Base;
end