function out=InPolygons(x,y,Polygons)
out=0;
    for i=1:numel(Polygons)
        if(inpolygon(x,y,Polygons{i}(:,1),Polygons{i}(:,2)))
            out=1;return; 
        end
    end
end