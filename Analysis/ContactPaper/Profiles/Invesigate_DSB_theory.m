clf
FactorOfGaussian=2.4; %2.7,5.7
X=linspace(-2.5,2.5,1001);
Y=exp(-X.^2*FactorOfGaussian);%2.7
plot(X,Y-0.5);
Fit_Line(@(x)exp(-x.^2));
diff(Fit.X_0)

%% DSB
Curveture=1.25;Range=0.6;
clf
X=linspace(-3.5,3.5,101);
Ey=1;Hall=[];
for i=1:numel(X)
    if(abs(X(i))>=2.5);Hall(i)=0;else
Hall(i)=quad(@(x)1-Curveture*0.04*x.^2,-2.5,X(i));
    end
end
Y=exp(-X.^2*FactorOfGaussian);%;
Y=Y./sum(Y);
YT=2*conv(Y,Hall,'same');
Ey=diff(YT);
X2=(X(2:end)+X(1:end-1))/2;
MaskR=erf((X2+0)*4)+1;
MaskL=erf((-X2+0)*4)+1;
Total=MaskR+MaskL;
plot((X(2:end)+X(1:end-1))/2,(Ey.*MaskL+fliplr(Ey).*MaskR)./Total);
ylim([0,max(Ey)*1.2])
hold on
% SSB:
X=linspace(-3.5,3.5,101);
Ey=1;
for i=1:numel(X)
    if(abs(X(i))>=2.5);Hall(i)=0;else
Hall(i)=quad(@(x)1-Curveture*0.04*x.^2,-2.5,X(i));
    end
end
MID=Hall(X==0);
for i=1:numel(X)
    if(abs(X(i))>=2.5);Hall(i)=0;else
Hall(i)=Hall(i)-MID;
    end
end
Y=exp(-X.^2*FactorOfGaussian);Y=Y./sum(Y);
YT=conv(Y,Hall,'same')-conv(Y,fliplr(Hall),'same');
Ey=diff(YT);
X2=(X(2:end)+X(1:end-1))/2;
plot((X(2:end)+X(1:end-1))/2,Ey);
ylim([0,max(Ey)*1.2])
Fit_Line(2,'X',[-Range/2*5,Range/2*5])
plot([-2.5,-2.5],[0,max(Ey)*1.2],'g')
plot([2.5,2.5],[0,max(Ey)*1.2],'g')
plot([-Range/2*5,-Range/2*5],[0,max(Ey)*1.2],'m--')
plot([Range/2*5,Range/2*5],[0,max(Ey)*1.2],'m--')
ClearFit
title(sprintf('PSF=1um, 0.6 Fit. Real: %0.2f, DSB:%0.2f, SSB:%0.2f',Curveture,-[Fit.a]./[Fit.Y_0]*(5)^2))
legend({'Double sided','Single sided'})
%% Search
% 0.32,0.57 5%
% 0.4,0.68 10%
Tolerance=0.10;Curveture=2;Index=2;
Range=1;MeasuredCurveture=[inf,inf];
while(abs(MeasuredCurveture(Index)-Curveture)>Tolerance)
    Range=Range-0.01
clf
X=linspace(-3.5,3.5,101);
Ey=1;Hall=[];
for i=1:numel(X)
    if(abs(X(i))>=2.5);Hall(i)=0;else
Hall(i)=quad(@(x)1-Curveture*0.04*x.^2,-2.5,X(i));
    end
end
Y=exp(-X.^2*FactorOfGaussian);
YT=2*conv(Y,Hall,'same');
Ey=diff(YT);
X2=(X(2:end)+X(1:end-1))/2;
MaskR=erf((X2+0)*4)+1;
MaskL=erf((-X2+0)*4)+1;
Total=MaskR+MaskL;
plot((X(2:end)+X(1:end-1))/2,(Ey.*MaskL+fliplr(Ey).*MaskR)./Total);
ylim([0,max(Ey)*1.2])
hold on
% SSB:
X=linspace(-3.5,3.5,101);
Ey=1;
for i=1:numel(X)
    if(abs(X(i))>=2.5);Hall(i)=0;else
Hall(i)=quad(@(x)1-Curveture*0.04*x.^2,-2.5,X(i));
    end
end
MID=Hall(X==0);
for i=1:numel(X)
    if(abs(X(i))>=2.5);Hall(i)=0;else
Hall(i)=Hall(i)-MID;
    end
end
Y=exp(-X.^2*FactorOfGaussian);
YT=conv(Y,Hall,'same')-conv(Y,fliplr(Hall),'same');
Ey=diff(YT);
X2=(X(2:end)+X(1:end-1))/2;
MaskR=erf((X2+0)*4)+1;
MaskL=erf((-X2+0)*4)+1;
Total=MaskR+MaskL;
plot((X(2:end)+X(1:end-1))/2,Ey);
ylim([0,max(Ey)*1.2])
Fit_Line(2,'X',[-Range/2*5,Range/2*5])
plot([-2.5,-2.5],[0,max(Ey)*1.2],'g')
plot([2.5,2.5],[0,max(Ey)*1.2],'g')
plot([-Range/2*5,-Range/2*5],[0,max(Ey)*1.2],'m--')
plot([Range/2*5,Range/2*5],[0,max(Ey)*1.2],'m--')
ClearFit
title(sprintf('PSF=1um, 0.6 Fit. Real: %0.2f, DSB:%0.2f, SSB:%0.2f',Curveture,-[Fit.a]./[Fit.Y_0]*(5)^2))
legend({'Double sided','Single sided'})
MeasuredCurveture=-[Fit.a]./[Fit.Y_0]*(5)^2;
%drawnow;
end

%% No bridge:
clf
X=linspace(-3.5,3.5,101);
Ey=1;Curveture=0;Hall=[];
for i=1:numel(X)
    if(abs(X(i))>=2.5);Hall(i)=0;else
Hall(i)=quad(@(x)1-Curveture*0.04*x.^2,-2.5,X(i));
    end
end
MID=Hall(X==0);
for i=1:numel(X)
    if(abs(X(i))>=2.5);Hall(i)=0;else
Hall(i)=Hall(i)-MID+MID*10;
    end
end

% Hall=(X+2.5)/5;
Y=exp(-X.^2*2.7);
YT=conv(Y,Hall,'same')-conv(Y,fliplr(Hall),'same');
Ey=diff(YT);
X2=(X(2:end)+X(1:end-1))/2;
MaskR=erf((X2+0)*4)+1;
MaskL=erf((-X2+0)*4)+1;
Total=MaskR+MaskL;
plot((X(2:end)+X(1:end-1))/2,Ey);
Fit_Line(2,'X',[-1.5,1.5])
ylim([0,max(Ey)*1.2])
hold on
plot([-2.5,-2.5],[0,max(Ey)*1.2],'g')
plot([2.5,2.5],[0,max(Ey)*1.2],'g')
title(sprintf('Curveture %0.2f',-[Fit.a]./[Fit.Y_0]*(5)^2))


%%
% clf
plot(X2,(MaskR.*fliplr(Ey)+MaskL.*(Ey))./Total)
%%
RangeList=linspace(0.1,2.5,50);
Curv=[];
for i=1:numel(RangeList)
    Fit_Line(2,'X',[-RangeList(i),RangeList(i)]);
    Curv(i)=[Fit.a]./[Fit.Y_0]*(5)^2;
end
%
plot(RangeList/5,Curv)