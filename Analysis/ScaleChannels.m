function Info=ScaleChannels(Info)
ChannelsToScale={'Magnet_I',12e-3,'B'; ...
    'Xs'       ,16.77/20,  'Xs_Scaled';...
    'Xs_Ramp'  ,16.77/20,  'Xs_Ramp_Scaled'; ...
    'Ys_Ramp' , 5/6 , 'Ys_Ramp'};
%%

% Axis:
AxisNames=fieldnames(Info.Scan.Axis);
Axis=Info.Scan.Axis;
Info.Scan.Axis=[];
ListChanged=cell(0,3);
for i=1:numel(AxisNames)
    Changed=0;
    for j=1:size(ChannelsToScale,1)
        if(strcmp(ChannelsToScale{j,1},AxisNames{i}))
            Changed=1;
            ListChanged(end+1,:)={i,ChannelsToScale{j,3},Axis.(AxisNames{i})*ChannelsToScale{j,2}};
            Info.Scan.Axis.(ChannelsToScale{j,3})=Axis.(AxisNames{i})*ChannelsToScale{j,2}; %Scale
        end
    end
    if(~Changed)
        Info.Scan.Axis.(AxisNames{i})=Axis.(AxisNames{i});
    end
end
%
% idgr:
Names=fieldnames(Info.Scan.idgr);
for i=1:numel(Names)
    if(strcmp(class(Info.Scan.idgr.(Names{i})),'idata'))
        for j=1:size(ListChanged,1)
            Info.Scan.idgr.(Names{i}).axes{j}.name=ListChanged{j,2};
            Info.Scan.idgr.(Names{i}).axes{j}.data=ListChanged{j,3};
        end
    end
end
% Channels:
for i=1:size(ChannelsToScale,1)
    if(isfield(Info.Channels,ChannelsToScale{i,1}))
        Info.Channels.(ChannelsToScale{i,3})=Info.Channels.(ChannelsToScale{i,1})*ChannelsToScale{i,2};
    end
end
end