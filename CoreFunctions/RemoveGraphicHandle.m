function Data = RemoveGraphicHandle(Data)
% Data = RemoveGraphicHandle(Data)
% Remove new graphics objects in a structure and/or cell
%
% Goal: Remove new graphic objects presented in Data, otherwise it creashs
% when loading the Data under earlier versions of MATLAB
% Note: Only use this function if NewGraphicsSystem() returns TRUE

if isstruct(Data)
    fn = fieldnames(Data);
    for k = 1:length(fn)
        x = Data.(fn{k});
        if isstruct(x) || iscell(x) || IsNewGraphicHandle(x)
            Data.(fn{k}) = RemoveGraphicHandle(x);
        end
    end
elseif iscell(Data)
    for k = 1:length(Data)
        x = Data{k};
        if isstruct(x) || iscell(x) || IsNewGraphicHandle(x)
            Data{k} = RemoveGraphicHandle(x);
        end
    end
elseif IsNewGraphicHandle(Data)
    % Replace with empty
    Data = [];
end
function tf = IsNewGraphicHandle(h)
% Return TRUE if an object h is New Graphics Object
tf = strncmp(class(h),'matlab.ui',9) || ...
     strncmp(class(h),'matlab.graphics',15);
end
end % RemoveGraphicHandle


