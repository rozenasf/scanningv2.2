
LoadScan(2094,L)
BG=GenRatio;
% Vbg=BG;
Ratio=polyval([-7.05e-3,2.59e-1,1.10e1],BG);
AVS14=40e-3;
AVS6=-40e-3./Ratio;
Vsd=(AVS14-AVS6);
RSense=120;
VSenseGain=1e3;
Vsense=Ggr14/VSenseGain*AVS14;
%I=-Vsense/RSense;
I=IgrAC14;

Iavg=I.mean(1);
%R=Vsd./Iavg-RSense; %R graphene with bridge 
R=Vsd./Iavg; %R graphene with bridge with sense
R.data

%%
W=4e-6;
Beta_sum_avg=0.3;

%Phi=(Beta_HS14_Hset/0.3)*0.5+0.5;
Phi=(Beta_HS14_Hset/0.3);

Rxx=Phi.*repmat(R.data,21,1);RXX.name='RXX';
figure(10);plot(Rxx)
Fit_Line(1,'all');

I=Phi;I.data=ones(size(I.data));I.name='1';


dPhidX=[Fit.a];

RhoXX=dPhidX.*I*W; RhoXX.name='RhoXX';



%%
VDirac=-4.47;
CapacitiveBgFactor=5.946e14;

n=CapacitiveBgFactor*(BG-VDirac);
% B=Magnet_I/10*120e-3;

k_F_vec = sqrt(pi*abs(n));

l_mfp=1./RhoXX./k_F_vec/2/qe_^2*h_/um_;
l_mfp.name='l_mfp[um]';


RhoXX.axes{2}.data=n*1e-4;
RhoXX.axes{2}.name='n';

%%
figure(11)
subplot(121)
plot(RhoXX.slice_index(1,1),'o--');
subplot(122)
plot(l_mfp.slice_index(1,1),'o--');
