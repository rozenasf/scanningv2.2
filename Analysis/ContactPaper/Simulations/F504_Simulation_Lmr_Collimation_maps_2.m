 Data4=load('Q:\users\asafr\MagFocus1.mat');Data4=Data4.Data;
%%
figure(1);clf
PeakListSimulation={};
for j=1:size(Data4.data,1)
  clf  
R=XEdgeFromSimulation(Data4.SliceIndex('lMR',j));
for i=1:numel(R);
    plot(R{i});hold on
end
%
% PeaksLocations=[-5.2,-2.75,0];
PeaksLocations=[-5.2,-2.2,0];
out={};
for i=1:numel(PeaksLocations)
   out{i}=Fit_Line(2,'X',[PeaksLocations(i)-1,PeaksLocations(i)+1]) ;
   PeakListSimulation{i}(j,:)=[out{i}.dY_0];   
end
for i=1:numel(PeaksLocations)
    PeakListSimulation{i}(j,:)=PeakListSimulation{i}(j,:)./PeakListSimulation{3}(j,:);
end
%pause(1)

end

%%
figure(1);clf;


contour(180./(pi./Data4.axes.CollimationFWHM),1./Data4.axes.lMR,smooth2(PeakListSimulation{1},3),'ShowText','on');
colormap(jet);
TXYL('P1','Collimation','1./lMR');
Ax=gca;Ax.LineWidth=2;Ax.Children.LineWidth=3;
figure(2);clf;
contour(180./(pi./Data4.axes.CollimationFWHM),1./Data4.axes.lMR,smooth2(PeakListSimulation{2},3),'ShowText','on');
colormap(jet);
TXYL('P2','Collimation \Detlta\Theta','1./lMR [um]');
Ax=gca;Ax.LineWidth=2;Ax.Children.LineWidth=3;