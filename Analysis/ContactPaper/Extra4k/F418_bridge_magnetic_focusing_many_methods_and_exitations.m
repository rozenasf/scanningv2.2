
figure(10);clf
ScanNumber=2080;
% GetMaxSuperScanNumber(ScanNumber);
ScanList=2080+(0.01:0.01:36/100);
ScanList=ScanList(ScanList~=2080.13);
ChannelsToExtract={'Beta_Hbg_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_HRest_Hset','Beta_Sum','Weight_Bridge_Calibration_SmallChange','LowNoise','NLSet','NLLSet','Ggr15','GLgr15','GLLgr15','IgrAC15'};
Data=[];
for i=1:numel(ChannelsToExtract)
    for j=1:6
        Data{j}.(ChannelsToExtract{i})=[];
    end
end
for k=1:numel(ScanList)%5
    LoadScan(ScanList(k));
    for i=1:numel(ChannelsToExtract)
        for j=1:6
            Data{j}.(ChannelsToExtract{i})(:,k)=...
                squeeze(Info.Scan.idgr.(ChannelsToExtract{i}).data(mod(j-1,2)+1,ceil(j/2),:));
        end
    end
    %     Data.(ChannelsToExtract{i})=[];
end
%

n=-(Vbg_to_n(-9.9,0.284));
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    
Mark={' Low',' High'};
for i=1:2
subplot(3,2,i);
Phi=0.5+0.5*(Data{i}.Beta_HS2_Hset-Data{i}.Beta_HS15_Hset)./Data{i}.Beta_Sum;
% Phi=(Phi-flipud(Phi));

imagesc(XList(1:k),WRc,Phi);colorbar;shading flat
title(['Phi',Mark{mod(i-1,2)+1}]);
end

Beta_Sum=3.6;
for i=3:4
subplot(3,2,i);
Phi_Bridge=(Data{i}.Beta_HS2_Hset)./Beta_Sum;
imagesc(XList(1:k),WRc,Phi_Bridge);colorbar;shading flat
title(['Phi bridge',Mark{mod(i-1,2)+1}]);
end
for i=5:6
subplot(3,2,i);
imagesc(XList(1:k),WRc,Data{i}.Weight_Bridge_Calibration_SmallChange);colorbar;shading flat
title(['Bridge Dynamical Voltage',Mark{mod(i-1,2)+1}]);
end
%%
figure(11);clf
pcolor(XList(1:k),WRc,Data{5}.Weight_Bridge_Calibration_SmallChange-Data{6}.Weight_Bridge_Calibration_SmallChange);colorbar;
TTT=Data{5}.Weight_Bridge_Calibration_SmallChange-Data{6}.Weight_Bridge_Calibration_SmallChange;
%%
figure(12)

    n=-(Vbg_to_n(-9.9,0.284));
    Index=5;
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    Antisym=Data{Index}.Weight_Bridge_Calibration_SmallChange-flipud(Data{Index}.Weight_Bridge_Calibration_SmallChange);
    
    HallViscusity=Antisym./Data{Index}.IgrAC15./repmat(RHallTheory',[1,35]);
    HHH{Index}=HallViscusity;
    pcolor(XList(1:k),WRc,HallViscusity)
    colormap(jet)
        colorbar
        shading flat
% pcolor(XList(1:k),WRc,Data{5}.Weight_Bridge_Calibration_SmallChange-Data{6}.Weight_Bridge_Calibration_SmallChange);colorbar;

%%

%  subplot(3,2,1);title('Phi Low Exitation');
%  subplot(3,2,2);title('Phi High Exitation');
mid=0.284;
StepSize=[20,6;3,20;1,60;0.1,200];
List=-9.9;
while(List<=9.9)
   List(end+1)= List(end)+1./StepSize(max(find(StepSize(:,1)>=abs(List(end)-mid))),2);
end
List(end)=9.9;
List=smooth(List,10);
List(find(((List<0.04) & (List>0))))=[];

List(abs(List-mid)>0.2)=[];

plot(List,'o')
