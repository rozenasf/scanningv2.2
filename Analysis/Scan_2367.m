%
LoadScan([2367],L)
Pairs=[1,2];
figure(1);clf
for j=1:1
    %subplot(2,2,j);
Beta_Sum=0.307
Phi=Beta_HS15_Hset/Beta_Sum.*AVS15_e./(AVS15_e-AVS2_e)-AVS2_e./(AVS15_e-AVS2_e); Phi.name='Phi';
%
Phi_A=(Phi.slice_index(3,Pairs(j,1))-Phi.slice_index(3,Pairs(j,2)))/2;
Phi_S=(Phi.slice_index(3,Pairs(j,1))+Phi.slice_index(3,Pairs(j,2)))/2;
%Phi_A=Phi_A.slice_index_range(1,[2:51]);
%Phi_S=Phi_S.slice_index_range(1,[2:51]);
%Phi_S=Phi_S.roi_index(1,[2,51]);
%Phi_S=Phi.slice_index(3,2);
N=1;
[X,Y]=meshgrid(linspace(-3,3,N),linspace(-3,3,N));
contour(Phi_A,5,'linewidth',5);hold on%
%caxis([min(min(Phi_A.data)),max(max(Phi_A.data))])
contour(Phi_S,20,'linewidth',5);hold on
caxis([min(min(Phi_S.data)),max(max(Phi_S.data))])

colormap(jet)
%hold off
axis equal
title(sprintf('Phi @ Magnet=+-%d mT , Scan 2366',Magnet_I(Pairs(j,1))*120/10)) 
AddGDS(GDS,[1,0,0,1,-3,-4.3]*1e-6,0); %for movving from negative to positive X

%AddTitle
end
%TelegramSend(999)
title('Potential and streamlines, BG=4V, 120mT');
