%%
figure(608);clf
R={};
SmoothParameter=3;
MaxDummy=6;
for k=0:MaxDummy-1
    for i=1:4
        LoadScan(k+1515+i/100,L,'NoScale');
        
        Beta_Sum=0.5;
        %I=mean(IgrAC15.data(:));
        I=IgrAC15;
        if(k==0)
            R{i} = CenterIDataAxis(Beta_HS15_Hset./Beta_Sum .*AVS15./I,[1,2],1e6);
        else
            R{i} = R{i}+CenterIDataAxis(Beta_HS15_Hset./Beta_Sum .*AVS15./I,[1,2],1e6);
        end
    end
end
%
for i=1:4
    R{i}.axes{1}.data=R{i}.axes{1}.data*1.2207; %Yhe Y
    R{i}.axes{2}.data=R{i}.axes{2}.data*1.3833; %The X
    %    'Xs_Ramp'  ,16.77/20,  'Xs_Ramp_Scaled'; ...%0.7857 at 75K 16.77/20 at 7K
% %     'Ys_Ramp' , 5/4.1 , 'Ys_Ramp_Scaled'}; %5/6 for 4K, 5/4.1 for 75k
    
   R{i}=R{i}/MaxDummy;
           for j=1:size(R{i},1)
            R{i}.data(j,:)=smooth(R{i}.data(j,:),SmoothParameter);
        end
        for j=1:size(R{i},2)
            R{i}.data(:,j)=smooth(R{i}.data(:,j),SmoothParameter);
        end
end
%

Traces={};

% subplot(2,2,1)
Y=R{1}.axes{1}.data;
Y=(Y(1:end-1)+Y(2:end))/2;
%
for i=1:2
    Traces{i}=-diff(R{2*i-1}.data-R{2*i}.data);
%      Traces{i}=mean(Traces{i},2);
%      plot(Y,Traces{i},'.');hold on
end
%
% ylim([0,3.5])
%
Boundaries=[-0.0,0.0];ErfPar=4;
R_Mask=ones(15,1)*(1+erf(ErfPar*(Y+Boundaries(2))));
%M_Mask=ones(6,1)*(erf(ErfPar*(Y+Boundaries(2)))-erf(ErfPar*(Y+Boundaries(1))));
L_Mask=ones(15,1)*(1-erf(ErfPar*(Y+Boundaries(1))));
Total_Mask=R_Mask+L_Mask;
%
% plot(Y,R_Mask*1.7) %Right bridge
% plot(Y,M_Mask*1.7) %Left bridge
% plot(Y,L_Mask*1.7) %Mid bridge
ylim([0,3.5])
%
% subplot(2,2,2)
Profile=(R_Mask'.*(Traces{1})+L_Mask'.*(Traces{2}))./(Total_Mask');

Export.Figure3.Panel2.Curve1.X=[R{1}.axes{2}.data];
Export.Figure3.Panel2.Curve1.Y=Y;
Export.Figure3.Panel2.Curve1.Data=[Profile];


surf(Export.Figure3.Panel2.Curve1.X-mean(Export.Figure3.Panel2.Curve1.X),Export.Figure3.Panel2.Curve1.Y,Export.Figure3.Panel2.Curve1.Data/3.2);
    colormap(jet);
%      TXYL('T = 75K','X','Ey')
  Ax=gca;
 Ax.DataAspectRatio(2)=3;
 Ax.DataAspectRatio(1)=Ax.DataAspectRatio(2);
% Ax.View=[-60,12.5];
 zlim([0.0,1.1]);xlim([-1.8,1.8]);ylim([-3.5,3.5]);
% CopyFigure();
view([-3,2,1.5])
title('F608');