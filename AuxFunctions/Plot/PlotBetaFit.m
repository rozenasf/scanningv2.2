function out=PlotBetaFit(FIG,varargin)
varargin=RegExpChannels(varargin);
out=1;
global Info
    figure(FIG)
    clf;
            SP=OptimizeSP(numel(varargin));
        for j=1:numel(varargin)
            subplot(SP(1),SP(2),j);
            BreakText=regexp(varargin{j},'_','split');
            Var1=BreakText{3};Var2=BreakText{2};
            IDX=Info.Scan.Runtime.Position(2);
            plot(Info.Scan.idgr.(Var1).data(:,IDX),Info.Scan.idgr.(Var2).data(:,IDX),'o')
            hold on
            fit=polyfit(Info.Scan.idgr.(Var1).data(:,IDX),Info.Scan.idgr.(Var2).data(:,IDX),1);
            Xvec=[min(Info.Scan.idgr.(Var1).data(:,IDX)),max(Info.Scan.idgr.(Var1).data(:,IDX))];
            plot(Xvec,polyval(fit,Xvec));
            title(['Slope: ',SprintfA(fit(1))]);
            xlabel(Var1);ylabel(Var2);
            hold off
            drawnow
        end
end