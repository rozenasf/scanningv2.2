load beckup
%%
addpath('C:\Users\asafr\Documents\idata')
%%

s120 = Scan{129};
V3 = unique(s120.V);
Xs = unique(s120.TargetXr);
Ys = unique(s120.TargetYr);
gr = reshape(s120.Graphete,4,65,7);
Xsa = iaxis('Xs', Xs);
Ysa = iaxis('Ys', Ys);
Vsa = iaxis('V', V3);
idgr = idata('graphite', gr, {Vsa, Xsa, Ysa}, {129}, '');
%%
idgr.map(@(x)sin(x)).pcolor3
%%
idgr.slice_index(1,4).pcolor
%%
idgr.slice_index(1,4).plot_with_color(1,@jet)
%%
idgr.slice_index(1,4).slice(1,2400).plot
%%
idgr.slice_index(1,4).slice(1,2400).plot;
hold on
poly1 = idgr.slice_index(1,4).slice(1,2400).fit('poly1');
plot(poly1)
hold off
poly1
poly1.p1
confint(poly1)
%%
idgr.permute([2 3 1]).roi(1,2200,3000).roi(2,4500,4800).slicer
%%

