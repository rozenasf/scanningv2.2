function ConnectDriver(name,Driver)
global Info
try 
    delete(Info.Drivers.(name));
    disp('deleted previous driver');
end
Info.Drivers.(name)=Driver;
assignin('base',name,Info.Drivers.(name));
end