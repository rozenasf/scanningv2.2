function OverideAxis(ToReplace,Text,Vec)
NewAxis=iaxis(Text,Vec);
s = evalin('base','whos');List={s.class};
for i=1:numel(List)
    if(strcmp(List{i},'idata'))
        OBJ=evalin('base',s(i).name);
        Changed=0;
        for j=1:numel(OBJ.axes)
            if(strcmp(OBJ.axes{j}.name,ToReplace))
                OBJ.axes{j}=NewAxis;
                Changed=1;
            end
        end
        if(Changed);assignin('base',s(i).name,OBJ);end
    end
end
end