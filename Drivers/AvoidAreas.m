function out=AvoidAreas(GetCell)
global Info S
    %Info.Scan.Polygons is a cell array event for one polygon. same with GetCall
    out=[];
    x=GetFromMemory('Xs');
    y=GetFromMemory('Ys');
    if(InPolygons(x,y,Info.Scan.Polygons));return;end
    S.Get(GetCell);
end