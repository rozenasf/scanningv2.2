%% Beta=2.1, Zs=3.075
LoadScan(1638)
DiffData=diff((Beta_HS15_Hset+Beta_HS2_Hset)./Beta_Sum);
DiffData.data=smooth(DiffData.data,5);

suptitle(sprintf('Scan %0.2f',Info.ScanNumber+0.01*Info.Flags.SuperScan))
subplot(1,2,1)
plot(WorkPoint);
out3=Fit_Line(@(x)exp(-x.^2),'X',[-12e-6,-10e-6]);
out4=Fit_Line(4,'X',[-7.2e-6,-6.5e-6]);
ScalingY=5.2e-6/(out4.dX_0-out3.dX_0);
title(sprintf('Assuming peak distance is 5.2um,scale: %0.2f',ScalingY))
 %DiffData.axes{1}.data=DiffData.axes{1}.data*ScalingY;
 subplot(1,2,2)
plot(DiffData);
out1=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-12e-6,-9.5e-6]);
out2=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-8e-6,-6e-6]);
title(sprintf('PSF (including the scaling) Fit 1/a=%0.2e',ScalingY./mean([out1.a,out2.a])))
xlabel('Y not scaled');
ylabel('diff( (S15+S2)/sum)');
mean(1./Beta_Sum.data)
ScalingY
mean([out1.dX_0,out2.dX_0])
%% Beta=2.4, Zs=3.1
LoadScan(1630)
DiffData=diff((Beta_HS15_Hset+Beta_HS2_Hset)./Beta_Sum);
DiffData.data=smooth(DiffData.data,5);

suptitle(sprintf('Scan %0.2f',Info.ScanNumber+0.01*Info.Flags.SuperScan))
subplot(1,2,1)
plot(WorkPoint);
out3=Fit_Line(@(x)exp(-x.^2),'X',[-12e-6,-10e-6]);
out4=Fit_Line(@(x)exp(-x.^2),'X',[-7.2e-6,-6.5e-6]);
ScalingY=5.2e-6/(out4.dX_0-out3.dX_0);
title(sprintf('Assuming peak distance is 5.2um,scale: %0.2f',ScalingY))
 %DiffData.axes{1}.data=DiffData.axes{1}.data*ScalingY;
 subplot(1,2,2)
plot(DiffData);
out1=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-12e-6,-9.5e-6]);
out2=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-8e-6,-6e-6]);
title(sprintf('PSF (including the scaling) Fit 1/a=%0.2e',ScalingY./mean([out1.a,out2.a])))
xlabel('Y not scaled');
ylabel('diff( (S15+S2)/sum)');
mean(1./Beta_Sum.data)
ScalingY
mean([out1.dX_0,out2.dX_0])
%%
LoadScan(1623)
DiffData=diff((Beta_HS15_Hset+Beta_HS2_Hset)./Beta_Sum);
DiffData.data=smooth(DiffData.data,5);

suptitle(sprintf('Scan %0.2f',Info.ScanNumber+0.01*Info.Flags.SuperScan))
subplot(1,2,1)
plot(WorkPoint);
out3=Fit_Line(@(x)exp(-x.^2),'X',[-12e-6,-10e-6]);
out4=Fit_Line(@(x)exp(-x.^2),'X',[-7.2e-6,-6.5e-6]);
ScalingY=5.2e-6/(out4.dX_0-out3.dX_0);
title(sprintf('Assuming peak distance is 5.2um,scale: %0.2f',ScalingY))
 %DiffData.axes{1}.data=DiffData.axes{1}.data*ScalingY;
 subplot(1,2,2)
plot(DiffData);
out1=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-12e-6,-9.5e-6]);
out2=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-8e-6,-6e-6]);
title(sprintf('PSF (including the scaling) Fit 1/a=%0.2e',ScalingY./mean([out1.a,out2.a])))
xlabel('Y not scaled');
ylabel('diff( (S15+S2)/sum)');
mean(1./Beta_Sum.data)
ScalingY
%%
LoadScan(1610)
DiffData=diff((Beta_HS15_Hset+Beta_HS2_Hset)./Beta_Sum);
DiffData.data=smooth(DiffData.data,5);

suptitle(sprintf('Scan %0.2f',Info.ScanNumber+0.01*Info.Flags.SuperScan))
subplot(1,2,1)
plot(WorkPoint);
out3=Fit_Line(@(x)exp(-x.^2),'X',[-12e-6,-10e-6]);
out4=Fit_Line(@(x)exp(-x.^2),'X',[-7.5e-6,-6e-6]);
ScalingY=5.2e-6/(out4.dX_0-out3.dX_0);
title(sprintf('Assuming peak distance is 5.2um,scale: %0.2f',ScalingY))
 %DiffData.axes{1}.data=DiffData.axes{1}.data*ScalingY;
 subplot(1,2,2)
plot(DiffData);
out1=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-12e-6,-9.5e-6]);
out2=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-8e-6,-6e-6]);
title(sprintf('PSF (including the scaling) Fit 1/a=%0.2e',ScalingY./mean([out1.a,out2.a])))
xlabel('Y not scaled');
ylabel('diff( (S15+S2)/sum)');
mean(1./Beta_Sum.data)
ScalingY
%%
% LoadScan(1602.08)
LoadScan(1610)
DiffData=diff((Beta_HS15_Hset+Beta_HS2_Hset)./Beta_Sum);
DiffData.data=smooth(DiffData.data,5);

suptitle(sprintf('Scan %0.2f',Info.ScanNumber+0.01*Info.Flags.SuperScan))
subplot(1,2,1)
plot(WorkPoint);
out3=Fit_Line(@(x)exp(-x.^2),'X',[-12e-6,-10e-6]);
out4=Fit_Line(@(x)exp(-x.^2),'X',[-7.5e-6,-6e-6]);
ScalingY=5.2e-6/(out4.dX_0-out3.dX_0);
title('Assuming peak distance is 5.2um')
 %DiffData.axes{1}.data=DiffData.axes{1}.data*ScalingY;
 subplot(1,2,2)
plot(DiffData);
out1=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-12e-6,-9.5e-6]);
out2=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-8e-6,-6e-6]);
title(sprintf('PSF (including the scaling) Fit 1/a=%0.2e',ScalingY./mean([out1.a,out2.a])))
xlabel('Y not scaled');
ylabel('diff( (S15+S2)/sum)');
mean(1./Beta_Sum.data)
ScalingY
%%
%%
LoadScan(1602.08)
DiffData=diff((Beta_HS15_Hset+Beta_HS2_Hset)./Beta_Sum);
DiffData.data=smooth(DiffData.data,5);

suptitle(sprintf('Scan %0.2f',Info.ScanNumber+0.01*Info.Flags.SuperScan))
subplot(1,2,1)
plot(WorkPoint);
out3=Fit_Line(@(x)exp(-x.^2),'X',[-12e-6,-10e-6]);
out4=Fit_Line(@(x)exp(-x.^2),'X',[-7.5e-6,-6e-6]);
ScalingY=5.2e-6/(out4.dX_0-out3.dX_0);
title('Assuming peak distance is 5.2um')
 %DiffData.axes{1}.data=DiffData.axes{1}.data*ScalingY;
 subplot(1,2,2)
plot(DiffData);
out1=Fit_Line(@(x)sech(1.76*x).^2,'X',[-12e-6,-9e-6]);
out2=Fit_Line(@(x)sech(1.76*x).^2,'X',[-9e-6,-6e-6]);
title(sprintf('PSF (including the scaling) Fit 1/a=%0.2e',ScalingY./mean([out1.a,out2.a])))
xlabel('Y not scaled');
ylabel('diff( (S15+S2)/sum)');
%%
5e-6/diff([-10.5e-6,-7.13e-6])
%%
