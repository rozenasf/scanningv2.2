function UpdateMarkerPosition()
    global Info S;
    Position=S.Get('Location');
    try
        if(abs(Position.Ys0) < 1e-9 && abs(Position.Xs0) < 1e-9)
           Info.Design.Marker.Color =[0 0 1]; 
        else
            Info.Design.Marker.Color=[1 0 0];
        end
        set(Info.Design.Marker,'XData',Position.Xr-Position.Xs0);
        set(Info.Design.Marker,'YData',Position.Yr-Position.Ys0);
        Info.Design.Title.String=sprintf('Z=%10.3f + %3.3f scanner, Range: %3.3f',Position.Zr*1e6,Position.Zs0*1e6,Info.Range.Zc(2)*1e6);
        Info.Design.Range.Position=[min(Info.Range.Xc),min(Info.Range.Yc),diff(Info.Range.Xc),diff(Info.Range.Yc)];
    catch
        PlotGDSMatrix();axis('equal');
        UpdateMarkerPosition();
    end
end