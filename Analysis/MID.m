function out=MID(in)
out=0.5*(in(1:end-1)+in(2:end));
end