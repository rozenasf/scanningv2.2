function Yes=IsIdgr(Channel)
global Info
    Yes=ismember(Channel,fieldnames(Info.Scan.idgr));
end