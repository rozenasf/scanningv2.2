function out=AnalyzeRhoXXSharvinNoBridge(ScanNumber)
global L Info
gu
REdge=-5e-6;LEdge=-16e-6;
CenterRange=[-14e-6,-7e-6];
LeftRange=[-18.5e-6,-17.5e-6];
RightRange=[-3.5e-6,-2.5e-6];
Scaling=(17.3-4.1)/16.8;
Data=LoadScan(ScanNumber,L,'NoScale');
if(~isfield(Info.Scan,'UserData'));out=[];return;end
out.Phi=0.5+0.5*(Data.Beta_HS15_Hset-Data.Beta_HS2_Hset)./Data.Beta_Sum;
out.R=out.Phi*5e-3./Data.IgrAC15.data;
out.BG=Info.Scan.UserData.BG;
out.WRc=Info.Scan.UserData.WRc;
out.BetaSum=Info.Scan.UserData.Beta_Sum;
out.Hset=mean(Data.Hset.data(:));
out.IgrAC15=mean(Data.IgrAC15.data(:));
out.Temperature=mean(Data.Temperature.data(:));
clf;plot(out.R);
Center=Fit_Line(1,'X',CenterRange);
Left=Fit_Line(0,'X',LeftRange);
Right=Fit_Line(0,'X',RightRange);
out.RhoXX=[Center.a]*5e-6*Scaling;
out.Lmr=h_./(2*qe_^2*sqrt(pi*abs(Vbg_to_n(out.BG,0.575))))./(out.RhoXX);
out.Rleft =-(Left.AVR-Center.Func(LEdge));
out.Rright=Right.AVR-Center.Func(REdge);
end