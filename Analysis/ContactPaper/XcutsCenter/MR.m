  %% 	Sharvin : X cuts as function of Magnetic field [0:0.25:2,2.5,3,3.5,4,6] through the center for electron\holes
    %BG=-8.5,9.9, (+-) theoretical half collimation
   
    ScanNameList={'Xcenter_Magnet_Holes','Xcenter_Magnet_Electrons'};
    figureindex = 201;
    ZeroFieldScans=[2,23];
    for j=1:numel(ScanNameList)
      figure(45);
        subplot(2,2,j);
        ScanName=ScanNameList{j};
        Vec=[];
        UnPackScan(Scans.Xcenter_BG{ZeroFieldScans(j)})
        Beta_Sum=0.51;
        R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
        plot(R,'-');hold on
        for i=1:numel(Scans.(ScanName))
            
            UnPackScan(Scans.(ScanName){i})
            Beta_Sum=0.51;
            R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
            plot(R,'-');hold on
            
            
        end

       [dRdX,Sharvin_Right,Sharvin_Left,Vsd]=AnalyzeX_Cut(RangeMid,RangeRight,RangeLeft,InterpolationPoints);
        figure(figureindex+1) ;
        subplot(3,2,j);grid on
        [Xo,Yo]=SortXY([0,SuperAxis.(ScanName).W_Rc],1e6*dRdX*5e-6);
        plot(Xo,Yo,'o--','linewidth',1);
        n = Vbg_to_n(Info.Channels.BG,0.7)/1e15;
        TXYL(sprintf('$Rho _{xx}$ as function of W/Rc %0.2f  $*10^{12}cm^{-2}$',n),'$W/Rc$','$\rho_{xx}$ [Ohm]')
   figure(45);
    close(gcf);
    end

   
    for i = 1:4
LoadScan(946.01+i-1);
Beta_HS15_Hset = BreakIData(Beta_HS15_Hset);
BetaSum =0.51;
R = Beta_HS15_Hset/BetaSum.*AVS15./mean(IgrAC15.data(:));


R = CenterIDataAxis(R,[1],1e6)
figure(813); clf
plot_with_color(R,1);

 [dRdX,Sharvin_Right,Sharvin_Left,Vsd]=AnalyzeX_Cut(RangeMid,RangeRight,RangeLeft,InterpolationPoints);
 n=abs(Vbg_to_n(Info.Channels.BG,0.7)); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    
    r_c=p_F./qe_./B;
    
    W_Rc=5e-6./r_c
   figure(figureindex+1)
    subplot(3,2,i+2)
 plot(W_Rc,dRdX*5e-6*1e6,'o--','LineWidth',1)
 n = Vbg_to_n(Info.Channels.BG,0.7)/10^15;
 TXYL(sprintf('$Rho _{xx}$ as function of W/Rc %0.2f  $*10^{12}cm^{-2}$',n),'$W/Rc$','$\rho_{xx}$ [Ohm]')
 RR(i,:)=(dRdX+fliplr(dRdX))/2*5e-6*1e6;
 end   
 figure(813);
 close(gcf);