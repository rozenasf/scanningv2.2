function Length=FormatLength(Format)
        Length=0;
        for i=1:numel(Format)
            switch(Format(i))
                case 'B'
                    Length=Length+1;
                case 'f'
                    Length=Length+4;
                case 'L'
                    Length=Length+4;
                otherwise
                    error('Unknown format');
            end
        end
    end