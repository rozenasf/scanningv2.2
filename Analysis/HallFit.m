function out=HallFit(x,a)

% sigma=3.5e-6;
% A=140;Shift=1e-4;
X=linspace(5e-6,-19.4e-6,100);Y=X*0;Y(find((X>-9.2e-6) .*( X<-5.2e-6)))=X(find((X>-9.2e-6) .*( X<-5.2e-6)))+7.2e-6;
O=a(1)*conv(Y,exp(-(X+7.2e-6).^2/(2*a(3).^2)),'same')-a(2);
out=interp1(X,O,x);
end