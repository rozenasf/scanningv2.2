function [ out ] = MergeStruct( in , in2 )
%this may scramble the order
    out=in;
    names=fieldnames(in2);
    for i=1:numel(names)
        out.(names{i})=in2.(names{i});
    end
end

