T=tic;
while(1)
    try
        %if(toc(T)>3600);LoadScan();figure(1);clf;plot(Time.data(:),1./Beta_Sum.data(:));TelegramSend(1);figure(2);clf;plot(Time.data(:),Temperature.data(:));TelegramSend(2);T=tic;end
        if(~isempty(TelegramGet()) || toc(T)>360000)
            T=tic;
            LoadScan();
            figure(1);clf
            subplot(2,2,1);
            plot(Time.data(:),1./Beta_Sum.data(:));
            subplot(2,2,2);
            plot(Time.data(:),Temperature.data(:))
            subplot(2,2,3);
            plot(Time.data(:),Hset.data(:))
            subplot(2,2,4);
            plot(Time.data(:),IgrAC15.data(:))
            TelegramSend(1);
        end
    end
    pause(20);
end
%%
while(1)
    try
        %if(toc(T)>3600);LoadScan();figure(1);clf;plot(Time.data(:),1./Beta_Sum.data(:));TelegramSend(1);figure(2);clf;plot(Time.data(:),Temperature.data(:));TelegramSend(2);T=tic;end
        if(~isempty(TelegramGet()))
            InitialScan=1462;
            BetaSumList=[];
            for i=InitialScan:GetMaxSuperScanNumber()
                for j=1:GetMaxSuperScanNumber(i)
                    LoadScan(i+j/100);
                    BetaSumList(end+1)=Info.Scan.UserData.Initial_Beta_Sum;
                end
            end
            figure(123);clf;plot(1./BetaSumList,'o--');
            TelegramSend(123);
        end
    end
    pause(20);
end
%%
while(1)
    try
        %if(toc(T)>3600);LoadScan();figure(1);clf;plot(Time.data(:),1./Beta_Sum.data(:));TelegramSend(1);figure(2);clf;plot(Time.data(:),Temperature.data(:));TelegramSend(2);T=tic;end
        if(~isempty(TelegramGet()))
            InitialScan=1542;
            BetaSumList=[];
            figure(124);clf
            for i=InitialScan:GetMaxSuperScanNumber()
                for j=1:GetMaxSuperScanNumber(i)
                    LoadScan(i+j/100);
                    BetaSumList(end+1)=Info.Scan.UserData.Initial_Beta_Sum;
                    figure(124);hold on
            subplot(2,2,1);hold on;plot(Beta_HS15_Hset);colormap(jet);ChangePlotColor(@jet)
            subplot(2,2,2);hold on;plot(Hset);colormap(jet);ChangePlotColor(@jet)
            subplot(2,2,3);hold on;plot(FeedbackAmp);colormap(jet);ChangePlotColor(@jet)
            subplot(2,2,4);hold on;plot(IgrAC15);colormap(jet);ChangePlotColor(@jet)
                end
                
            end
            
            figure(123);clf;plot(1./BetaSumList,'o--');
            TelegramSend(123);
            TelegramSend(124);
        end
    end
    pause(20);
end
%% Raw Data
InitialScan=1537;
BetaSumList=[];
figure(124);clf
for i=InitialScan:GetMaxSuperScanNumber()
    for j=1:GetMaxSuperScanNumber(i)
        LoadScan(i+j/100);
        B1=Beta_HS15_Hset;
        plot(B1,'linewidth',3);hold on
    end
    
end
ChangePlotColor(@jet);
% legend({'W/Rc=0.25','W/Rc=0.5','W/Rc=0.75','W/Rc=1','W/Rc=1.25','W/Rc=1.5','W/Rc=1.75','W/Rc=2'})
%%
while(1)
    try
        if(~isempty(TelegramGet()))
% for i=InitialScan:GetMaxSuperScanNumber();
%      for j=1:GetMaxSuperScanNumber(i)
          LoadScan(1547+GetMaxSuperScanNumber(1547)/100);
          figure(1);clf;plot(Time.data(:),1./Beta_Sum.data(:));TelegramSend(1);
%          if(i==InitialScan)
%             B=Beta_HS15_Hset;    
%          else
%             B=B{j}+Beta_HS15_Hset;
%          end
%          plot(Beta_HS15_Hset);hold on
         
%      end
        end
% end
    end
    pause(30);
end
%%
clf
SlopeDiff=22e3;
for i=1:numel(B)
   plot(B{i} - B{i}.axes{1}.data*SlopeDiff);hold on
end
%%
InitialScan=1538;
BetaSumList=[];
figure(124);clf
for i=InitialScan:InitialScan
    for j=1:(GetMaxSuperScanNumber(i)-2)/2
        LoadScan(i+(j*2+1)/100);
        B1=Beta_HS15_Hset;
        LoadScan(i+(j*2+2)/100);
        B2=Beta_HS15_Hset;
        figure(124);hold on;
        %plot((-B1+B2)./(-Info.Scan.UserData.WRc),'linewidth',3);
        plot((-B1+B2),'linewidth',3);
    end
    
end
ChangePlotColor(@jet);
legend({'W/Rc=0.25','W/Rc=0.5','W/Rc=0.75','W/Rc=1','W/Rc=1.25','W/Rc=1.5','W/Rc=1.75','W/Rc=2'})
%             figure(123);clf;plot(1./BetaSumList,'o--');
%             TelegramSend(123);
%             TelegramSend(124);
        %%
        InitialScan=1536;
        BetaSumList=[];
        figure(124);clf
        Line={};MaxDummy=10;
        Append=[];Ref=[];
        LoadScan(InitialScan+1/100);Ref=Beta_HS15_Hset;
        for i=InitialScan:InitialScan
            for j=1:(GetMaxSuperScanNumber(i)-1)/2
                LoadScan(i+(j*2)/100);
                B1=Beta_HS15_Hset;
                LoadScan(i+(j*2+1)/100);
                B2=Beta_HS15_Hset;B2.data=(B2.data)
                figure(124);hold on;
                %plot((-B1+B2)./(-Info.Scan.UserData.WRc),'linewidth',3);
%                  Line{j}=-B2-Ref;
%                  Line{j}=-B2+B1-2*Ref;
                 Line{j}=B2;
%                Line{j}=B1;
                plot(Line{j},'linewidth',3);
                Append=[Append;Line{j}.data];
            end
            
        end
        
        %%
%         Fit_Line(1,'X',[-15.0e-6,-7e-6]);
Append=[];SS=5.7e3;
        clf
        for j=1:numel(Line)
            plot(Line{j}.axes{1}.data,Line{j}.data-SS*(Line{j}.axes{1}.data),'linewidth',3);hold on
          %  plot(Line{j}.axes{1}.data,-Line{j}.data+6e3*(Line{j}.axes{1}.data),'linewidth',3);hold on
             Append=[Append;Line{j}.data-SS*(Line{j}.axes{1}.data)];
        end
%         ylim([-5e-3,5e-3])
        ChangePlotColor(@jet);
%         legend({'W/Rc=0.25','W/Rc=0.5','W/Rc=0.75','W/Rc=1','W/Rc=1.25','W/Rc=1.5','W/Rc=1.75','W/Rc=2'})
        %%
        clf
        Append(Append>0.07)=nan;
        Append(Append<0.02)=nan;
        surf(Append);
        
         colormap(jet(128));
%          caxis(-[5e-3,10e-3])
%         colormap(jet)
%%
clf
StartVec=datevec(Info.Scan.Runtime.Now);
StartSeconds=StartVec(4)*3600+StartVec(5)*60+StartVec(6);
% Time_Abs=(Time.data(:)+StartSeconds)/3600;
Time_Abs=(Time.data(:)/86400+Info.Scan.Runtime.Now);
Temperature_smooth=smooth(Temperature.data(:),100);
InvBetaSum_Abs=smooth(1./Beta_Sum.data(:),100);
subplot(2,1,1)
plot(Time_Abs,Temperature_smooth);
datetick
grid
TXYL(' ','Hour','Temperature')
subplot(2,1,2)
plot(Time_Abs,InvBetaSum_Abs);
datetick
grid
TXYL(' ','Hour','Inverse beta sum')
%%
T=tic
while(1)
        try
   if(~isempty(TelegramGet()) || (toc(T)>3600))
        
        figure(1);clf;subplot(2,2,1);List=[];
      LoadScan([],L,'NoScale');MaxSuper=Info.Flags.SuperScan;
      ListP=[];
      for i=1:MaxSuper
          LoadScan(Info.ScanNumber+i/100,L,'NoScale');
           subplot(2,2,1);hold on
           plot(Beta_HS15_Hset.mean(2))
           List=[List;1./Info.Scan.UserData.Initial_Beta_Sum];
           ListP=[ListP;FeedbackAmp.data(~isnan(FeedbackAmp.data))];
      end
      subplot(2,2,2);plot(List,'o')
      subplot(2,2,3);plot(ListP,'o');
      TelegramSend(1);
      if(toc(T)>3600);T=tic;end
   end
  
    end
    pause(20);
end

%%

while(1)
    try
        if(~isempty(TelegramGet()))
            
            LoadScan([],L,'NoScale');
            figure(1);clf;
            subplot(3,2,1);plot_with_color(Temperature.slice_index(3,Info.Scan.Runtime.Position(3)),1,@jet);
            subplot(3,2,2);plot_with_color(ZsCapY.slice_index(3,Info.Scan.Runtime.Position(3)),1,@jet);
            subplot(3,2,3);plot_with_color(IgrAC15.slice_index(3,Info.Scan.Runtime.Position(3)),1,@jet);
            subplot(3,2,4);plot_with_color(Beta_Sum.slice_index(3,Info.Scan.Runtime.Position(3)),1,@jet);
            subplot(3,2,5);plot_with_color(Idc.slice_index(3,Info.Scan.Runtime.Position(3)),1,@jet);
            subplot(3,2,6);plot_with_color(WorkPoint.slice_index(3,Info.Scan.Runtime.Position(3)),1,@jet);
            
            TelegramSend(1);
            
        end
        
    end
    pause(20);
end
%%
while(1)
    try
        if(~isempty(TelegramGet()))
 LoadScan([],L,'NoScale');
            figure(1);clf;
            subplot(2,2,1);plot(Time.data(:),Temperature.data(:));TXYL('Temperature','Time',' ');
            subplot(2,2,2);plot(Time.data(:),InverseBetaSum.data(:));TXYL('InverseBetaSum','Time',' ');
            subplot(2,2,3);plot(Time.data(:),ZsCapY.data(:));TXYL('ZsCapY','Time',' ');
            subplot(2,2,4);plot(Time.data(:),Rgr15.data(:));TXYL('Rgr15','Time',' ');
            TelegramSend(1);
             end
        
    end
    pause(30);
end
%%
clf
LoadScan(1241);Data=Rgr15;%Data=Rgr15;
TempLeg={};
for i=1:6
    plot(Data.slice_index(2,11).slice_index(2,i),'.--');hold on
    TempLeg{end+1}=sprintf('Temp=%0.2f',Temperature.slice_index(2,11).data(1,i));

end
hold on
LoadScan(1242);Data=Rgr15;
for i=1:5
    plot(Data.slice_index(2,11).slice_index(2,i),'.--');hold on
    TempLeg{end+1}=sprintf('Temp=%0.2f',Temperature.slice_index(2,11).data(1,i));

end
legend(TempLeg)
ChangePlotColor(@(~)flipud(jet(11)));
%%
clf
LoadScan(1241);Data=Vg_loc;%Data=Rgr15;
TempLeg={};
for i=1:6
    plot(Data.slice_index(1,21).slice_index(2,i),'.--');hold on
    TempLeg{end+1}=sprintf('Temp=%0.2f',Temperature.slice_index(2,11).data(1,i));

end
hold on
LoadScan(1242);Data=Vg_loc;
for i=1:5
    plot(Data.slice_index(1,21).slice_index(2,i),'.--');hold on
    TempLeg{end+1}=sprintf('Temp=%0.2f',Temperature.slice_index(2,11).data(1,i));

end
legend(TempLeg)
ChangePlotColor(@(~)flipud(jet(11)));
%%
Wrc=4;BG=9.9;
qe_=1.6022e-19;
                    hbar_=1.0546e-34;

                        r_c=5e-6./Wrc;
                        n=abs(5.946e14*(BG-0.575));
                        p_F=hbar_*sqrt(pi*n);
                        B=p_F./qe_./r_c;
                        Magnet_I=B./120e-3*10