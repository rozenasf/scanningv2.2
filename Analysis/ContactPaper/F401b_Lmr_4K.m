%% Lmr as function of BG:
List=405:411;
W_RcList=[4,-4,2,-2,1,-1,0];
Phi={};
for i=1:numel(List)
    LoadScan(List(i),L,'NoScale')
    Phi{i}=(Beta_HS15_Hset-Beta_HS2_Hset)./(2*(Beta_HS15_Hset+Beta_HS2_Hset))+0.5;
end
%%

Symmetrized={};
Symmetrized{1}=Phi{7};
Symmetrized{2}=(Phi{5}+Phi{6})/2;
Symmetrized{3}=(Phi{4}+Phi{3})/2;
Symmetrized{4}=(Phi{2}+Phi{1})/2;
%%
RhoXX=[];
Slopes=[];
for i=1:4
    Symmetrized{i}.axes{1}.data=Symmetrized{i}.axes{1}.data*0.8385;
set(gca, 'ColorOrder', hsv(21), 'NextPlot', 'replacechildren');
plot(Symmetrized{i},'linewidth',1);
RhoXX(i,:)=Fit_Line(1,'X',[-2.5e-5,-1.6e-5],'a')*Info.Channels.AVS15./IgrAC15.mean(1).data;
end
%% 1/RhoXX
clf
for i=1:4
% plot(RampBG,h_./(8*qe_^2*sqrt(pi*abs(Vbg_to_n(RampBG',0.7))))./(RhoXX(1,:)*5e-6)','o-','linewidth',3)
plot(RampBG',1./(RhoXX(i,:)*5e-6)','o-','linewidth',3)
hold on
end
hold off
TXYL('$ $scans 405:411,$\frac{1}{\rho_{xx}}$','$ $BG [V]','$\frac{1}{\rho_{xx}} [\frac{1}{Ohm}]$',...
    '$\\frac{w}{R_c}=%d$',{[0,1,2,4]})
% title('scans 405:411,$l_{mr}=\frac{h}{2e^2\rho_{xx}\sqrt{\pi n}}$','Interpreter','latex','fontsize',24)
set(gca,'FontSize',16)
grid
%%
for i=1:4
% plot(RampBG,h_./(8*qe_^2*sqrt(pi*abs(Vbg_to_n(RampBG',0.7))))./(RhoXX(1,:)*5e-6)','o-','linewidth',3)
plot(RampBG',1e6*h_./(2*qe_^2*sqrt(pi*abs(Vbg_to_n(RampBG',0.7))))./(RhoXX(i,:)*5e-6)','o-','linewidth',3)
hold on
end
hold off
TXYL('$ $ scans 405:411,$l_{mr}=\frac{h}{2e^2\rho_{xx}\sqrt{\pi n}}$','$ $ BG [V]','$l_{mr} [\mu m]$',...
    '$\\frac{w}{R_c}=%d$',{[0,1,2,4]},'location','north')
% title('scans 405:411,$l_{mr}=\frac{h}{2e^2\rho_{xx}\sqrt{\pi n}}$','Interpreter','latex','fontsize',24)
set(gca,'FontSize',16)
grid
return
%%
clf
Lmr_W=1e6*h_./(2*qe_^2*sqrt(pi*abs(Vbg_to_n(RampBG',0.7))))./(RhoXX(4,:)*5e-6)'/5;
plot(RampBG',Lmr_W,'o-','linewidth',3)
%Fit_Line(8,'X',[-10,0.3]);
BGXX=linspace(-9.9,0.3,100);
clf;plot(BGXX,interp1(RampBG,Lmr_W,BGXX));Fit_Line(7,'X',[-10,0.3]);
%
clf
plot(RampBG,Lmr_W,'o--','linewidth',3);hold on
plot(BGXX,Fit.Func(BGXX),'linewidth',3);
%
xlim([-10,0.3])
poly=[5.69814893e-06,1.96910484e-04,2.67801986e-03,1.77549882e-02,5.48655391e-02,2.84913971e-02,-5.50675966e-01,5.11791320e-01];
hold on
plot(BGXX,polyval(poly,BGXX),'b')
 sprintf('%0.8e,',Fit.fit')