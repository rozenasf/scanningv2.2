function [outP,outN]=Symmetrize(out,name)
        NewAxis=unique([out.(name)]);
        NewAxis=NewAxis(ismember(NewAxis,-NewAxis));
        NewAxis=NewAxis(NewAxis>=0);
        NewAxis=sort(NewAxis);
        IndexP=[];IndexN=[];
        for i=1:numel(NewAxis)
            IndexP=[IndexP,find([out.(name)]==NewAxis(i))];
            IndexN=[IndexN,find([out.(name)]==-NewAxis(i))];
        end
       outP=out(IndexP);outN=out(IndexN);
end