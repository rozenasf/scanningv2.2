function out=Document(varargin)
global Info;
out=nan;
if(~isfield(Info.Scan,'Document'));Info.Scan.Document=[];end
if(Info.Flags.RunningScan)
    PositionIndex=Info.Scan.Runtime.PositionIndex;
else
    PositionIndex=0;
end
for i=1:numel(varargin)
    if(~isfield(Info.Scan.Document,varargin{i}))
        Info.Scan.Document.(varargin{i})=[];
    end
    Info.Scan.Document.(varargin{i})=[Info.Scan.Document.(varargin{i});...
        toc(Info.Scan.Runtime.Tic),PositionIndex,GetFromMemory(varargin{i})];
end
end