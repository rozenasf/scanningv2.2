function BatchPlot(Fig,Title,N,M,Fun,varargin)
    figure(Fig);
    clf;
    for k=1:numel(varargin)
           subplot(N,M,k);
           Fun(varargin{k});
    end
    suptitle(Title);
end