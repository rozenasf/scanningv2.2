classdef Magnet_Driver < handle
    properties
        Tic
        Timer
        I_Channel_Name
        V_Channel_Name
    end
    
    methods
        %datetick
        %datestr(737119.88295)
        function obj=Magnet_Driver(I_Channel_Name,V_Channel_Name)
            obj.I_Channel_Name=I_Channel_Name;
            obj.V_Channel_Name=V_Channel_Name;
            obj.Tic=tic;
            obj.Timer=timer('TimerFcn',@(x,y)obj.GetTimer(),'Period', 10*1,'ExecutionMode', 'fixedSpacing');
            start(obj.Timer);
        end

        %-----
        function out=Get(obj,Channel)
            global Info S
            out=nan;
            switch Channel
                case 'Magnet_V'
                    out=S.Get(obj.V_Channel_Name);
                    %out=Info.Drivers.(obj.V_Driver_Name).Get(Channel); 
                    obj.Tic=tic;
            otherwise
                error('Get only Magnet_V @ Magnet');
            end
        end
        function Set(obj,Channel,Value)
            global Info S
            switch Channel
                case 'Magnet_I'
                    stop(obj.Timer)
                    RampChannel(obj.I_Channel_Name,-Value,1,0.1);
                    %Info.Drivers.(obj.I_Driver_Name).Set(Channel,-Value);
                    pause(0.5);
                    S.Get('Magnet_V');
                    start(obj.Timer)
                otherwise
                    error('Set only Magnet_I @ Magnet');
            end
        end
        function Zero(obj)
            global S
            S.Set('Magnet_I',0);
        end
        function GetTimer(obj)
            global S;
            S.Get('Magnet_V');
            obj.Tic=tic;
        end
       function out=Toc(obj)
            out=toc(obj.Tic);
        end
        function delete(obj)
            try stop(obj.Timer);delete(obj.Timer);end
        end
    end
end