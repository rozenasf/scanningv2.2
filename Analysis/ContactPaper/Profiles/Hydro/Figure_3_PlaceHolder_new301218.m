figure(3);clf;suptitle('Figure 3 : WARNING, some of the titles are incorrect!')
Lmr_BG_Fit=[-9.38362e-05,-4.20564e-03,-6.39272e-02,-4.72690e-01,-3.20175e+00,2.54587e+00];
AsFuncitonOfKf=0;
%%
% %Fake 4/3 scaling, check! Fake /13 amplitude, normelize by classical Hall
% F412_curveture_2D_Lmr_1_5_WRc_1_6_Low_Long_Scan
% figure(503)
% subplot(2,3,1)
% 
% surf(Export.Figure3.Panel1.Curve1.X,Export.Figure3.Panel1.Curve1.Y/4*3,Export.Figure3.Panel1.Curve1.Data.data/13);
%     colormap(jet);
%      TXYL('T = 7K','X','Ey')
%   Ax=gca;
%  Ax.DataAspectRatio(2)=4;
%  Ax.DataAspectRatio(1)=Ax.DataAspectRatio(2);
% Ax.View=[-60,12.5];
%  zlim([0.0,1]);
% xlim([-1.5,1.5]);ylim([-3,3]);
%     TXYL('T = 6K','X','Ey')
%         shading interp
% Light=lightangle(45,45)
%%
% check! Fake /3.5 amplitude, normelize by classical Hall
 F608_High_quality_2D_75K_BG_N5_WRc_1_6;
  figure(3);subplot(2,3,2);cla;PasteFigure();
  
 %%
%  figure(503)
% subplot(2,3,2)
% 
% surf(Export.Figure3.Panel2.Curve1.X-mean(Export.Figure3.Panel2.Curve1.X),Export.Figure3.Panel2.Curve1.Y,Export.Figure3.Panel2.Curve1.Data/3);
%     colormap(jet);
% %      TXYL('T = 75K','X','Ey')
%   Ax=gca;
%  Ax.DataAspectRatio(2)=4;
%  Ax.DataAspectRatio(1)=Ax.DataAspectRatio(2);
% Ax.View=[-60,12.5];
% zlim([0.0,1.2]);xlim([-1.5,1.5]);ylim([-3,3]);
% 
%     TXYL('T = 75K','X','Ey')
%     shading interp
%  Light=lightangle(45,45)
 %%
 figure(3);subplot(2,3,3);cla;grid
%     %Curveture as function of W/Rc @ Lmr=1.5W
%     %At 75K
F607_profiles_75K_BG_N5_WRc_Vgraphene_10;PasteFigure(3,2,3,3);
%     errorbar(0,-1,0,'m.-','markersize',20,'linewidth',2);hold on
%     
%     %At 33K
     %Currently from F404_Curveture_WRc_BG_N2, exitation of Vsd=15mV
%      F332=Export.Figure3.Panel3.Curve2;
%      errorbar(F332(1,5:end),F332(2,5:end),F332(3,5:end),'r.-','markersize',20,'linewidth',2)
%     %At 4K
%     %Currently from F409_Curveture_WRc_BG_N2_Low, exitation of Vsd=7.5mV
%     F331=Export.Figure3.Panel3.Curve1;
%     errorbar(F331(1,:),F331(2,:),F331(3,:),'b.-','markersize',20,'linewidth',2);
%     
%     xlim([0,6.5]);ylim([0,4.5])
%     TXYL('Ey Curveture @ Lmr=1.5W','W/Rc','Curveture','T=75K','T=33K','T=6K','location','northwest')
%     Ax=gca;Ax.FontSize=12
%     grid
 %%
% subplot(2,3,4)
%     %1D at 4K no curveture , Lmr=1.5W
%     % Now from F405_Curveture_Multi_Side_Bridge_WRc_1_6_Lmr_1_5W_Low with
%     % Vsd=7.5mV
%     F341=Export.Figure3.Panel4.Curve1;
%     plot(F341(1,:)/5,smooth(F341(2,:)+fliplr(F341(2,:)),3)/2,'.--','markersize',8);
%     out1=Fit_Line(2,'X',[-1.5,1.5]/5);
%     hold on
%     plot([-0.5,-0.5],[0,3],'r--')
%     plot([0.5,0.5],[0,3],'r--')
%     Curveture=-[out1.a]./[out1.Y_0]*(1)^2;
%     ylim([0,4]);xlim([-0.57,0.57])
%     
%     TXYL(sprintf('Curveture %0.2f',Curveture),'X','Ey','T=6K','location','south')
%     Ax=gca;Ax.FontSize=12
%     %
% subplot(2,3,5)
%     %1D at 75K yes curveture , Lmr=1.5W
%     % Now from F406_Curveture_Multi_Side_Bridge_WRc_1_6_Lmr_1_5W_High with
%     % Vsd=25mV, short scan
% 
%     F351=Export.Figure3.Panel5.Curve1;
%     plot(F351(1,:)/5,smooth(F351(2,:)+fliplr(F351(2,:)),3)/2,'.--','markersize',8);
%     out5=Fit_Line(2,'X',[-1.5,1.5]/5);
%     hold on
%     plot([-0.5,-0.5],[0,3],'r--')
%     plot([0.5,0.5],[0,3],'r--')
%     Curveture=-[out5.a]./[out5.Y_0]*(1)^2;
%     ylim([0,4]);xlim([-0.57,0.57])
%     
%     TXYL(sprintf('Curveture %0.2f',Curveture),'X','Ey','T=33K','location','south')
%     Ax=gca;Ax.FontSize=12
%     %
%%
 subplot(2,3,6);cla;grid
%     %Curveture as function of Ef [K] @ W/Rc=1.6
%     %At 4K
%      plot(0,-3,'b.-','markersize',20,'linewidth',2);hold on
%     %At 33K
         % from F403_Curveture_BG_WRc_1_6, Vsd=15mV
 %        F362=Export.Figure3.Panel6.Curve2;
 %        BG=F362(1,:);
 %        Lmr=polyval(Lmr_BG_Fit,BG)
 %        plot(Lmr/5,F362(2,:),'r.-','markersize',20,'linewidth',2);hold on
 %        SimulationFit=[4.15528e-01,-1.77357e+00,1.56021e+00];
 %        plot(Lmr/5,polyval(SimulationFit,Lmr/5))
 %        grid;ylim([-1.5,3]);xlim([0,4])
% % TXYL('Curveture -a/c*W^2 @ W/Rc=1.6', 'BG','Curveture')
%     %At 75K
F604_profiles_75K_BG_WRc_1_6_Vgraphene_7_5;PasteFigure(3,2,3,6);
%     plot(0,-3,'m.-','markersize',20,'linewidth',2);hold on
%     TXYL('Ey Curveture @ W/Rc=1.6','Lmr/W','Curveture','T=75K','T=33K','T=6K')
%     Ax=gca;Ax.FontSize=12
%    
%%
% subplot(2,3,6)
% errorbar(Export.Figure3.Panel3.Curve1(1,:),Export.Figure3.Panel3.Curve1(2,:),Export.Figure3.Panel3.Curve1(3,:),'o--','markersize',8,'linewidth',2)
% 
