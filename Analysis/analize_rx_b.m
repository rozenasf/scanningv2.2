Vdirac=-4; %BG voltage in Dirac point
% V_BG=-2:0.5:3;
V_BG=2;
W_eff=3.5*um_; %effective channel width
I_magnet=linspace(0,10,21);
B=I_magnet*0.11/10; %field in Tesla
r_c=zeros(1,length(B));


for i=1:length(B)
    n=Vbg_to_n(V_BG,Vdirac); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    r_c(i)=p_F/qe_/B(i);
end

figure(100);
% plot(I_magnet,r_c/um_,I_magnet,ones(size(I_magnet))*W_eff/um_,'k:')
% xlabel('I [A]');
plot(B*1e3,r_c/um_,B*1e3,ones(size(B))*W_eff/um_,'k:')
xlabel('B [mT]')
ylabel('rc [um]')

%%
V=0;
kf=sqrt(pi*abs(Vbg_to_n(V,-4)));
Rho=42;
Sigma=1/Rho;
l=Sigma*h_/(2*qe_.^2)/kf

%2*qe_^2/h_*kf*3*um_
%1./(2*qe_^2/h_*kf*3*um_)