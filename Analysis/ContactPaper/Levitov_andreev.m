
% while(1)
% TelegramGetLoop();
set(gca, 'ColorOrder', hsv(19), 'NextPlot', 'replacechildren');
LoadScan(392); figure(1);
Center=2.05e-5;
Beta_Sum=358.492e-3;
% try
V=Beta_HS15_Hset./Beta_Sum.*AVS15;%./IgrAC15
VMatrix1=[];VMatrix2=[];
for i=2:size(V.data,2)/2
    LineIData1=(-V.slice_index(2,2*i-1));
    LineIData2=(-V.slice_index(2,2*i));
    %LineIData.data=LineIData.data+fliplr(LineIData.data);
    %plot(LineIData.axes{1}.data*1e6+20.5433,(LineIData.data)*1e6,'linewidth',3)
    hold on
    VMatrix1=[LineIData1.data;VMatrix1];
    VMatrix2=[LineIData2.data;VMatrix2];
end
hold off
TXYL('Scan 392, BG=-8.5, Hall Voltage - W/R_c = [0.5:0.25:5]','X [\mum]','V [\muV]')
xlim([-10,10])

set(gca,'FontSize',16)
%%
X=LineIData1.axes{1}.data*1e6+20.5433;
plot(X,transpose(VMatrix1-fliplr(VMatrix2)));
Data=(VMatrix1-fliplr(VMatrix2));
Fit_Line(1,'X',[-6.2,5.5]);
%%
clf
DataY=[];
DataX=[];
Range=[10:42];
for i=1:numel(Fit)
    DataY(i,:)=Data(i,:)-Fit(i).Func(X);
    DataX(i,:)=X;
%     plot(Fit(i).X,i*0e-5+Fit(i).Y-Fit(i).Func(Fit(i).X))
%     hold on
end
% hold off
% surf(DataY(:,Range)*1e4)
 surf(X(Range),fliplr(0.5:0.25:5),DataY(:,Range)*1e4)
 
 caxis([-2,2])
 view([30,60])
 ylabel('W/R_c')
 xlabel('X [um]')
 Ax=gca;
 Ax.ZAxis.Visible='off';
%  grid off;
 Ax.Color='none';
 colormap(jet)
%   Li=light