classdef Lockin_Driver < handle
    properties
        Freq
        Vac
        Iac
        Communication
    end
    methods
        function obj=Lockin_Driver(address)
           obj.Freq=0;
           obj.Vac=0;
           obj.Iac=0;
           obj.Communication=gpib('adlink',address(1),address(2));
           fopen(obj.Communication);
           fprintf(obj.Communication, 'AOFF 2');
           disp('Connection to Lock-in established!');
        end
        function [Value,Text]=GetFromMemory(obj,Channel)
            Text=[];Value=0;
            switch Channel(1)
                case 'V'
                    Value=obj.Vac;
                case 'I'
                    Value=obj.Iac;
                case 'F'
                    Value=obj.Freq;
            end
        end
        %-----
        function out=Get(obj,Channel)
            switch Channel(1)
                case 'I'
                    %fprintf(obj.Communication, 'AOFF 2');
                    %pause(0.01);
                    fprintf(obj.Communication, 'OUTP? 1');
                    val=fscanf(obj.Communication);
                    obj.Iac = str2double(val);
                    out=obj.Iac;
                case 'V'
                    out=obj.Vac;
                case 'F'
                    out=obj.Freq;
            end                    
        end
        function Set(obj,Channel,Value)
            switch Channel(1)
                case 'V'
                    obj.Vac = Value;
                    fprintf(obj.Communication, 'SLVL %f',Value);
                case 'F'
                    obj.Freq=Value;
                    fprintf(obj.Communication, 'FREQ %f',Value);
            end
        end
        function delete(obj)
            fclose(obj.Communication);
            try delete(obj);end
            disp('Lockin disconnected!');
        end
    end
end