function out=PlotBeta(Fig,varargin)
global Info S;
varargin=RegExpChannels(varargin);
%varargin=InitiateNewFunction(varargin);clf
out=1;
figureN(Fig);clf
%SP=OptimizeSP(numel(varargin)-1);
SP=OptimizeSP(numel(varargin));
for j=1:numel(varargin)
    subplot(SP(1),SP(2),j);
    idgr=Info.Scan.idgr.(varargin{j});
    %idgr=idgr.slice_index(1,Info.Scan.Runtime.Position(1));
    idgr.plot;
end
drawnow;
end