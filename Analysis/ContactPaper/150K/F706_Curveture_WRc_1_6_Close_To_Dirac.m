%1313 is the BG=-5, 1315 is the BG=-9.9 perliminary
%new set starts at : 1331 up to : ?
%------------------------------------%
%   RUN F601 to get the ExportLmr!   %
%------------------------------------%
while(1)
   try
       if(~isempty(TelegramGet()))
           
%%         
figure(6021);clf
Data=[];
ScaleY=1.46; % from scan 1638 see notebook 150K->approach and initial calibration of Y

Curvature_150K_BG=struct('BG',[],'Curvature',[],'CurvatureSTD',[],'WRc',[]);
BaseScanList={};

LastValid=(GetMaxSuperScanNumber()-1);
Ank=[1877:4:1877+150]; % check that it catches all relevant scans
IDX=find(LastValid>=Ank);
for i=1:(numel(IDX)-1)
    BaseScanList{end+1}=[Ank(i):(Ank(i+1)-1)];
end
 BaseScanList{end+1}=[Ank(numel(IDX)):LastValid];
out={};
 Beta_Sum_List=[];
for l=1:numel(BaseScanList)
   EyMasked=[];k=0;
   Curvature=[];
for  BaseScan=BaseScanList{l}
    k=k+1;
RSmooth={};R={};
figure(1);clf;figure(2);clf

for i=1:4
    LoadScan(BaseScan+0.01*i,L,'NoScale')
    BetaSum = 0.46;
    R{i} = CenterIDataAxis(Beta_HS15_Hset./BetaSum .*AVS15./IgrAC15,[1],1e6);
    figure(1);hold on
    plot(R{i},'o')
    %hold on
    %plot(R{i}.mean(2),'linewidth',3)
    %hold off
    RSmooth{i} = smooth(R{i}.data,3);
    Beta_Sum_List(end+1)=Info.Scan.UserData.Initial_Beta_Sum;
end
M=jet(4);ChangePlotColor(@(~)M(ones(5,1)*[1:4],:))
Y=(R{1}.axes{1}.data(1:end-1)+R{1}.axes{1}.data(2:end))/2;
Y=Y* ScaleY+0.1; %From lior analysis in Heatup 75K -> scaling XY and psf see notebook
%
Boundaries=[-0.0,0.0];ErfPar=4;
R_Mask=(1+erf(ErfPar*(Y+Boundaries(2))));
L_Mask=(1-erf(ErfPar*(Y+Boundaries(1))));
Total_Mask=R_Mask+L_Mask;
Ey1=-(diff(RSmooth{1}-RSmooth{2}));
Ey2=-(diff(RSmooth{3}-RSmooth{4}));
figure(3);clf
    plot(Y,Ey1,'o--');
hold on
 plot(Y,Ey2,'o--');
 plot(Y,R_Mask);plot(Y,L_Mask);
hold off
figure(4);clf
EyMasked(:,k)=(Ey1.*R_Mask'+Ey2.*L_Mask')./Total_Mask';
%plot(Y,(EyMasked(:,k)+flipud(EyMasked(:,k)))/2,'o--');
plot(Y,EyMasked(:,k),'o--');
 
 Fit_Line(2,'X',[-1.25,1.25]);
     if([Fit.Y_0]*1.4>0)
%     ylim([0,[Fit.Y_0]*1.4])
    ylim([0,20])
    else
%         ylim([[Fit.Y_0]*1.4,0])
        ylim([0,20])
    end
%  ylim([-30,30])
Curvature(k) = -[Fit.a]./[Fit.Y_0]*5^2;
%ylim([0,[Fit.Y_0]*1.4])
TXYL(sprintf('curvature=%0.1f',Curvature(k)),'Y [um]','Hall R [ARB]');
end
CurvatureSTD=std(Curvature)/sqrt(numel(Curvature));

%%
figure(6021);subplot(4,4,l);
CurList=[];
%for i=1:size(EyMasked,2)
Data(l,:)=mean(EyMasked,2);
    plot(Y,Data(l,:),'o');
    out{l}=Fit_Line(2,'X',[-1.25,1.25])
    Curvature = -[out{l}.a]./[out{l}.Y_0]*5^2;
    CurList(end+1)=Curvature;
    if([Fit.Y_0]*1.4>0)
        ylim([0,20])
%     ylim([0,[Fit.Y_0]*1.4])
    else
%         ylim([[Fit.Y_0]*1.4,0])
        ylim([-20,0])
    end
    TXYL(sprintf('cur=%0.2f [%0.2f]@BG=%0.1f[V]@WRc=%0.2f',Curvature,CurvatureSTD,Info.Channels.BG,-Info.Scan.UserData.WRc),'Y [um]','Hall R [ARB]');
    xlim([-3,3])
    P1=patch([-3,-2.5,-2.5,-3],[0,0,1,1]*max([Fit.Y_0]*1.4),'k');P1.FaceAlpha=0.5;P1.EdgeAlpha=0;
    P2=patch([3,2.5,2.5,3],[0,0,1,1]*max([Fit.Y_0]*1.4),'k');P2.FaceAlpha=0.5;P2.EdgeAlpha=0;
    %end
    Curvature_150K_BG.WRc(end+1)=Info.Scan.UserData.WRc;
    Curvature_150K_BG.Curvature(end+1)=Curvature;
    Curvature_150K_BG.CurvatureSTD(end+1)=CurvatureSTD;
    Curvature_150K_BG.BG(end+1)=Info.Channels.BG;
end
figure(12);
plot(1./Beta_Sum_List)
figure(13);clf
[~,IDX]=sort(Curvature_150K_BG.BG);
Curvature_150K_BG.WRc=Curvature_150K_BG.WRc(IDX);
Curvature_150K_BG.BG=Curvature_150K_BG.BG(IDX);
Curvature_150K_BG.Curvature=Curvature_150K_BG.Curvature(IDX);
Curvature_150K_BG.CurvatureSTD=Curvature_150K_BG.CurvatureSTD(IDX);
errorbar(Curvature_150K_BG.BG,Curvature_150K_BG.Curvature,Curvature_150K_BG.CurvatureSTD,'-','linewidth',3)
%%
TelegramSend({6021,12,13})
end
end
pause(30);
end
%% Build the full 4K-75K-150K curvature as function of BG
%Based on F411
% Scans [1014.01:0.01:1014.04], [1015.01:0.01:1015.04]
% [[1:0.25:3,3.5:0.5:6],[1:0.25:3],[1:0.25:2.5],[1:0.25:2]];
figure(2);clf
Y={};Traces={};
%ScanList=1017:1083; %SUM=7.5mV and BG=-2 as function of W/Rc
ScanList={};
ScanList{1}=[1105:1136,1173:1235]; % LowExitation dinamic
ScanList{2}=1406:1460; % High Temperature 75K
Curvature_6K_BG=struct('BG',[],'Curvature',[],'CurvatureSTD',[]);
Curvature_75K_BG=struct('BG',[],'Curvature',[],'CurvatureSTD',[]);
Output={};
figure(4113);clf
for ScanExitationVelueIndex=2:-1:1
Range=[-1.28,1.28];
Curveture=[];CurvetureSTD=[];
BGList=[];BGListFull=[];BetaSumList=[];

for i=1:numel(ScanList{ScanExitationVelueIndex})
    LoadScan(ScanList{ScanExitationVelueIndex}(i)+0.01,L,'NoScale');BGListFull(i)=Info.Scan.UserData.BG;
    BetaSumList(i)=1./Info.Scan.UserData.Initial_Beta_Sum;
end
BGList_unique=uniquetol(BGListFull,1e-3);
for i=1:numel(BGList_unique)
    figure(4111);clf
        index=find( abs(BGList_unique(i)-BGListFull)<1e-3 );
        
        CurvetureSTDList=[];
        for j=1:numel(index)
            CurvetureSTDList(j)=GetCurvetureFromDSB4(ScanList{ScanExitationVelueIndex}(index(j)),Range);
        end
        [Curveture(i),WRcList(i),BG]=GetCurvetureFromDSB4(ScanList{ScanExitationVelueIndex}(index),Range);
        %BGList(i)=BG;
        if(numel(index)>1)
        CurvetureSTD(i)=std(CurvetureSTDList)./sqrt(numel(index)-1);
        else
            CurvetureSTD(i)=nan;
        end
        
end
    %
figure(4113);hold on
% WRcList_unique=unique(WRcList);Curveture_AVR=[];
Lmr_BG_Fit=[-9.38362e-05,-4.20564e-03,-6.39272e-02,-4.72690e-01,-3.20175e+00,2.54587e+00];
Lmr=polyval(Lmr_BG_Fit,BGList_unique);
%errorbar(Lmr/5,Curveture,CurvetureSTD,'.','markersize',20,'linewidth',2);
errorbar(BGList_unique,Curveture,CurvetureSTD,'.','markersize',20,'linewidth',2);
Output{ScanExitationVelueIndex}.BG=BGList_unique;
Output{ScanExitationVelueIndex}.Curveture=Curveture;
Output{ScanExitationVelueIndex}.CurvetureSTD=CurvetureSTD;
% TXYL('curveture in Ey','lMR/W','curveture')
TXYL('curveture in Ey','BG','curveture')
Export.Figure3.Panel3.Curve1=[BGList;Curveture;CurvetureSTD];
%1016 is the first one
ylim([-2,4]);grid

if(ScanExitationVelueIndex==1)
    Curvature_6K_BG.Curvature=Curveture;
    Curvature_6K_BG.CurvatureSTD=CurvetureSTD;
    Curvature_6K_BG.BG=BGList_unique;
else
    Curvature_75K_BG.Curvature=Curveture;
    Curvature_75K_BG.CurvatureSTD=CurvetureSTD;
    Curvature_75K_BG.BG=BGList_unique;
end
end
legend({'75K','High','Low Constant','Low dinamic'},'location','northwest')
return
%% Plot as function of BG
figure(7041)
clf;
errorbar(Curvature_6K_BG.BG,Curvature_6K_BG.Curvature,Curvature_6K_BG.CurvatureSTD,'.','markersize',20,'linewidth',3);
hold on
errorbar(Curvature_75K_BG.BG,Curvature_75K_BG.Curvature,Curvature_75K_BG.CurvatureSTD,'.','markersize',20,'linewidth',3);
errorbar(Curvature_150K_BG.BG,Curvature_150K_BG.Curvature,Curvature_150K_BG.CurvatureSTD,'.','markersize',20,'linewidth',3);
hold off
legend({'6K','75K','150K'},'location','northwest');
Ax=gca;Ax.FontSize=16;
TXYL('Curvature','BG [V]','Curvature');
grid
ylim([0,4])
%% Plot as function of Kf

figure(7047)

clf;
n=abs(Vbg_to_n(Curvature_6K_BG.BG,0.575));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=6.5;lee=-(Ef./T^2)./log(abs(Ef)/T);
errorbar(Kf,Curvature_6K_BG.Curvature,Curvature_6K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
hold on
n=abs(Vbg_to_n(Curvature_75K_BG.BG,0.575));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=75;lee=-(Ef./T^2)./log(abs(Ef)/T);
errorbar(Kf,Curvature_75K_BG.Curvature,Curvature_75K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
n=abs(Vbg_to_n(Curvature_150K_BG.BG,0.310));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=150;lee=-(Ef./T^2)./log(abs(Ef)/T);
errorbar(Kf,Curvature_150K_BG.Curvature,Curvature_150K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
hold off
legend({'6K','75K','150K'},'location','northeast');
Ax=gca;Ax.FontSize=16;
TXYL('Curvature','Kf','Curvature');
grid
ylim([0,4])

%% Plot as function of Lmr
figure(7042)
Poly_6=[5.69814893e-06,1.96910484e-04,2.67801986e-03,1.77549882e-02,5.48655391e-02,2.84913971e-02,-5.50675966e-01,5.11791320e-01];
Poly_75=[-1.05305119e-06,-4.38744380e-05,-7.57166633e-04,-7.14722794e-03,-4.19879822e-02,-1.71069158e-01,-4.94630562e-01,4.27898584e-01];
Poly_150=[-4.84322732e-06,-1.97676055e-04,-3.22668645e-03,-2.72593291e-02,-1.29873537e-01,-3.45025503e-01,2.60044220e-01];
clf;
errorbar(polyval(Poly_6,Curvature_6K_BG.BG),Curvature_6K_BG.Curvature,Curvature_6K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
hold on
errorbar(polyval(Poly_75,Curvature_75K_BG.BG),Curvature_75K_BG.Curvature,Curvature_75K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
errorbar(polyval(Poly_150,Curvature_150K_BG.BG),Curvature_150K_BG.Curvature,Curvature_150K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
hold off
legend({'6K','75K','150K'},'location','northeast');
Ax=gca;Ax.FontSize=16;
TXYL('Curvature','Lmr/W','Curvature');
grid
ylim([0,4])

%% Plot as function of Theoretical lee

figure(7043)

clf;
n=abs(Vbg_to_n(Curvature_6K_BG.BG,0.575));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=6.5;lee=-(Ef./T^2)./log(abs(Ef)/T);
errorbar(lee,Curvature_6K_BG.Curvature,Curvature_6K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
hold on
n=abs(Vbg_to_n(Curvature_75K_BG.BG,0.575));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=75;lee=-(Ef./T^2)./log(abs(Ef)/T);
errorbar(lee,Curvature_75K_BG.Curvature,Curvature_75K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
n=abs(Vbg_to_n(Curvature_150K_BG.BG,0.310));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=150;lee=-(Ef./T^2)./log(abs(Ef)/T);
errorbar(lee,Curvature_150K_BG.Curvature,Curvature_150K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
hold off
legend({'6K','75K','150K'},'location','northeast');
Ax=gca;Ax.FontSize=16;
TXYL('Curvature','lee','Curvature');
grid
ylim([0,4])

%% Plot as function of Dnu

figure(7044)

clf;
n=abs(Vbg_to_n(Curvature_6K_BG.BG,0.575));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=6.5;lee=-(Ef./T^2)./log(abs(Ef)/T);Lmr=polyval(Poly_6,Curvature_6K_BG.BG);Dnu=sqrt(lee.*Lmr)
errorbar(Dnu,Curvature_6K_BG.Curvature,Curvature_6K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
hold on
n=abs(Vbg_to_n(Curvature_75K_BG.BG,0.575));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=75;lee=-(Ef./T^2)./log(abs(Ef)/T);Lmr=polyval(Poly_75,Curvature_75K_BG.BG);Dnu=sqrt(lee.*Lmr)
errorbar(Dnu,Curvature_75K_BG.Curvature,Curvature_75K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
n=abs(Vbg_to_n(Curvature_150K_BG.BG,0.310));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=150;lee=-(Ef./T^2)./log(abs(Ef)/T);Lmr=polyval(Poly_150,Curvature_150K_BG.BG);Dnu=sqrt(lee.*Lmr)
errorbar(Dnu,Curvature_150K_BG.Curvature,Curvature_150K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
hold off
legend({'6K','75K','150K'},'location','northeast');
Ax=gca;Ax.FontSize=16;
TXYL('Curvature','Dnu','Curvature');
grid
ylim([0,4])
%% Plot as function of Theoretical lee

figure(7045)

clf;
n=abs(Vbg_to_n(Curvature_6K_BG.BG,0.575));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=6.5;lee=-(Ef./T^2)./log(abs(Ef)/T);
errorbar(lee,Curvature_6K_BG.Curvature,Curvature_6K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
hold on
n=abs(Vbg_to_n(Curvature_75K_BG.BG,0.575));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=75;lee=-(Ef./T^2)./log(abs(Ef)/T);
errorbar(lee,Curvature_75K_BG.Curvature,Curvature_75K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
n=abs(Vbg_to_n(Curvature_150K_BG.BG,0.310));Kf=sqrt(pi*n);Ef=Vf_*hbar_*Kf/qe_;T=150;lee=-(Ef./T^2)./log(abs(Ef)/T);
errorbar(lee,Curvature_150K_BG.Curvature,Curvature_150K_BG.CurvatureSTD,'--','markersize',20,'linewidth',3);
hold off
legend({'6K','75K','150K'},'location','northeast');
Ax=gca;Ax.FontSize=16;
TXYL('Curvature','lee','Curvature');
grid
ylim([0,4])
%%





BG2LMR=[-1.05305119e-06,-4.38744380e-05,-7.57166633e-04,-7.14722794e-03,-4.19879822e-02,-1.71069158e-01,-4.94630562e-01,4.27898584e-01];
Colors='rgb';
figure(60211);clf
out={};
k=0;BGLL=[-9.9,-5,-1.6];
for i=[1,2,4]
k=k+1;
   subplot(3,2,k*2-1);
   fit=polyfit(Y/5,Data(i,:),2);MAX=polyval(fit,-fit(2)/(2*fit(1)));
   plot(Y/5,Data(i,:)./MAX,Colors(k),'linewidth',3);hold on
   patch([-0.5,-0.5,-0.6,-0.6],[0,1.05,1.05,0],'k','FaceAlpha',0.3)
    patch(-[-0.5,-0.5,-0.6,-0.6],[0,1.05,1.05,0],'k','FaceAlpha',0.3)
out{i}=Fit_Line(2,'X',[-1.25,1.25]/5);
ylim([0,1.05])
XX=linspace(-1.5,1.5,100);

   plot(XX,out{i}(1).Func(XX),'k--','linewidth',2);
   ylim([0,1.05]);xlim([-1,1])
   grid('minor')

TXYL(sprintf('$k_F=%0.0f \\ [1/\\mu m]$, curvature: %0.1f , Lmr/W: %0.2f ',-sqrt(pi*abs(Vbg_to_n(BGLL(k),0.6)))/1e6,(out{i}(1).a./out{i}(1).dY_0),polyval(BG2LMR,BGLL(k))),'$Y/W$','$Ey [arb]$');
end
subplot(2,2,2);cla
for i=1:5
    IDX=find(ExportLmr{i}.BG<0.3);
    plot(-sqrt(pi*abs(Vbg_to_n(ExportLmr{i}.BG(IDX),0.6)))/1e6,ExportLmr{i}.Lmr(IDX)/5e-6,'o-','linewidth',3);hold on
end
% xlim([-10,0.2])
ChangePlotColor
BGListCurvatures=[-9.9,-5,-2,-1.6];
k=0;
for i=[1,2,4]
    k=k+1;
plot(-sqrt(pi*abs(Vbg_to_n(BGListCurvatures(i)*[1,1],0.6)))/1e6,[0,1.5],[Colors(k),'-'],'linewidth',5)
end
TXYL('normalized mean free path @ W/R_c','$k_F [1/\mu m]$','$\frac{l_{MR}}{W} [\mu m]$','$W/R_c=%d$',{[0,1,2,4,6]},'location','southwest')
hLegend = findobj(gcf, 'Type', 'Legend');hLegend.FontSize=8;
subplot(2,2,4)
k=0;
for i=[1,2,6]
    k=k+1;
    IDX=find(abs(ExportMR{i}.WRc)~=4);
    if(i==1);
        plot(ExportMR{i}.WRc(IDX),ExportMR{i}.Lmr(IDX)./5e-6,[Colors(k),'.'],'linewidth',3,'markersize',30);hold on
    else
        plot(ExportMR{i}.WRc(IDX),ExportMR{i}.Lmr(IDX)./5e-6,[Colors(k),'.'],'linewidth',3,'markersize',20);hold on
    end
end
% TXYL('normalized mean free path @ k_F','$W/Rc$','$\frac{l_{MR}}{W} [\mu m]$','$k_F=%d \\ [1/\\mu m]$',{[-140,-102,-64]},'location','northwest')
TXYL('normalized mean free path @ k_F','$W/Rc$','$\frac{l_{MR}}{W} [\mu m]$')
% ylim([0.5,1.5])

hLegend = findobj(gcf, 'Type', 'Legend');hLegend.FontSize=8;