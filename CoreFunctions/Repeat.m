function out=Repeat(n,in)
    out=repmat(in,1,n);
end