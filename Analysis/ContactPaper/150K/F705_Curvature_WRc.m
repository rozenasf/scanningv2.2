%%         
figure(602);clf
Data=[];
ScaleY=1.46; % from scan 1638 see notebook 150K->approach and initial calibration of Y

Curvature_150K_BG=struct('BG',[],'Curvature',[],'CurvatureSTD',[],'WRc',[]);
BaseScanList={};

WRcToDo=[1:0.25:3,3.5:0.5:9];
NForWRcToDo=min([round(10./WRcToDo);8+WRcToDo*0]);

 Ank=[1807+cumsum([0,NForWRcToDo])];
% Ank=[1727:10:1807];
%  BaseScanList{1}=1667:1686; %1644 bad bridge. 1647:1648 BG=-5, W/Rc=1.6
%  BaseScanList{2}=1647:1666; %1644 bad bridge. 1647:1648 BG=-5, W/Rc=1.6
%  BaseScanList{3}=1687:1706;
%  BaseScanList{4}=1707:1726;
 
LastValid=(GetMaxSuperScanNumber()-1);
IDX=find(LastValid>=Ank);
for i=1:(numel(IDX)-1)
    BaseScanList{end+1}=[Ank(i):(Ank(i+1)-1)];
end
% BaseScanList{end+1}=[Ank(numel(IDX)):LastValid];
out={};
 Beta_Sum_List=[];
for l=1:numel(BaseScanList)
   EyMasked=[];k=0;
   Curvature=[];
for  BaseScan=BaseScanList{l}
    k=k+1;
RSmooth={};R={};
figure(1);clf;figure(2);clf

for i=1:4
    LoadScan(BaseScan+0.01*i,L,'NoScale')
    BetaSum = 0.46;
    R{i} = CenterIDataAxis(Beta_HS15_Hset./BetaSum .*AVS15./IgrAC15,[1],1e6);
    figure(1);hold on
    plot(R{i},'o')
    %hold on
    %plot(R{i}.mean(2),'linewidth',3)
    %hold off
    RSmooth{i} = smooth(R{i}.data,3);
    Beta_Sum_List(end+1)=Info.Scan.UserData.Initial_Beta_Sum;
end
M=jet(4);ChangePlotColor(@(~)M(ones(5,1)*[1:4],:))
Y=(R{1}.axes{1}.data(1:end-1)+R{1}.axes{1}.data(2:end))/2;
Y=Y* ScaleY+0.1; %From lior analysis in Heatup 75K -> scaling XY and psf see notebook
%
Boundaries=[-0.0,0.0];ErfPar=4;
R_Mask=(1+erf(ErfPar*(Y+Boundaries(2))));
L_Mask=(1-erf(ErfPar*(Y+Boundaries(1))));
Total_Mask=R_Mask+L_Mask;
Ey1=-(diff(RSmooth{1}-RSmooth{2}));
Ey2=-(diff(RSmooth{3}-RSmooth{4}));
figure(3);clf
    plot(Y,Ey1,'o--');
hold on
 plot(Y,Ey2,'o--');
 plot(Y,R_Mask);plot(Y,L_Mask);
hold off
figure(4);clf
EyMasked(:,k)=(Ey1.*R_Mask'+Ey2.*L_Mask')./Total_Mask';
%plot(Y,(EyMasked(:,k)+flipud(EyMasked(:,k)))/2,'o--');
plot(Y,EyMasked(:,k),'o--');
 ylim([0,50])
 Fit_Line(2,'X',[-1.25,1.25]);
Curvature(k) = -[Fit.a]./[Fit.Y_0]*5^2;
%ylim([0,[Fit.Y_0]*1.2])
TXYL(sprintf('curvature=%0.1f',Curvature(k)),'Y [um]','Hall R [ARB]');
end
CurvatureSTD=std(Curvature)/sqrt(numel(Curvature));

%%
figure(602);subplot(5,5,l);
CurList=[];
%for i=1:size(EyMasked,2)
Data(l,:)=mean(EyMasked,2);
    plot(Y,Data(l,:),'o');
    out{l}=Fit_Line(2,'X',[-1.25,1.25])
    Curvature = -[out{l}.a]./[out{l}.Y_0]*5^2;
    CurList(end+1)=Curvature;
    ylim([0,[out{l}.Y_0]*1.2])
    TXYL(sprintf('cur=%0.2f [%0.2f]@BG=%0.1f[V]@WRc=%0.2f',Curvature,CurvatureSTD,Info.Channels.BG,-Info.Scan.UserData.WRc),'Y [um]','Hall R [ARB]');
    xlim([-3,3])
    P1=patch([-3,-2.5,-2.5,-3],[0,0,1,1]*max([Fit.Y_0]*1.2),'k');P1.FaceAlpha=0.5;P1.EdgeAlpha=0;
    P2=patch([3,2.5,2.5,3],[0,0,1,1]*max([Fit.Y_0]*1.2),'k');P2.FaceAlpha=0.5;P2.EdgeAlpha=0;
    %end
    Curvature_150K_BG.WRc(end+1)=Info.Scan.UserData.WRc;
    Curvature_150K_BG.Curvature(end+1)=Curvature;
    Curvature_150K_BG.CurvatureSTD(end+1)=CurvatureSTD;
    Curvature_150K_BG.BG(end+1)=Info.Channels.BG;
end
figure(12);
plot(1./Beta_Sum_List)
figure(14);
[~,IDX]=sort(Curvature_150K_BG.BG);
Curvature_150K_BG.WRc=Curvature_150K_BG.WRc(IDX);
Curvature_150K_BG.BG=Curvature_150K_BG.BG(IDX);
Curvature_150K_BG.Curvature=Curvature_150K_BG.Curvature(IDX);
Curvature_150K_BG.CurvatureSTD=Curvature_150K_BG.CurvatureSTD(IDX);
errorbar(-Curvature_150K_BG.WRc,Curvature_150K_BG.Curvature,Curvature_150K_BG.CurvatureSTD,'.','linewidth',3,'markersize',20)