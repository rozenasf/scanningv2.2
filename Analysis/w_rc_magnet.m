%%
-8.5,-3.78, -1.0705
Bridge2, Bridge6, Bridge9
%%
WRc_List=[1.6];
Wrc = WRc_List;
r_c=5e-6./Wrc;
n=abs(Vbg_to_n(-2,0.7)); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    B=p_F./qe_./r_c;
    W_Rc=5e-6./r_c;
    Current=B./120e-3*10;
CC=[Current;-Current];CC=CC(:)';
sprintf('%0.3f,',CC)
% sprintf('%0.3f,',sort([0,CC(:)']))
%%
n=abs(Vbg_to_n(9.9,0.7)); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    
    r_c=p_F./qe_./B
    %%
    Wrc=4;BG=BGList;
    r_c=5e-6./Wrc;
    n=abs(5.946e14*(BG-0.7));
    p_F=hbar_*sqrt(pi*n);
    B=p_F./qe_./r_c;
    Current=B./120e-3*10
    sprintf('%0.3f,',Current)
%     CC=[Current;-Current];CC(:)';
%     sprintf('%0.3f,',sort([0,CC(:)']))

    %%
       B = [0    0.0152   -0.0152    0.0389   -0.0389    0.0597   -0.0597];
    n=abs(Vbg_to_n(-8.9,0.5)); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    r_c=p_F./qe_./B;
     W_Rc=9e-6./r_c
     %%
     Wrc = [1.8];
r_c=5e-6./Wrc
n=abs(Vbg_to_n(-8.5,0.7)); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    
    B=p_F./qe_./r_c
    
    W_Rc=5e-6./r_c
    Current=B./120e-3*10