function out=Raster(Start,End,NumberOfPoints)
    if(mod(NumberOfPoints,2)==0);error('Raster should have an odd number of points!');end
    Raster0=linspace(Start,End,NumberOfPoints);
    out=[Raster0(1:2:end),Raster0(end-1:-2:1)];
end