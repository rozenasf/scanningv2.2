figure(403);clf
 
Shift=23;
for i=1:23
    UnPackScan(Scans.XY_Center_BG_Wrc{i});Beta_Sum=0.5;R=Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15;
    R1=R.mean(1);
    UnPackScan(Scans.XY_Center_BG_Wrc{i+Shift});Beta_Sum=0.5;R=Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15;
    R2=R.mean(1);
    R1.data=smooth(R1.data,1);
    R2.data=smooth(R2.data,1);
    RHall=diff(R1-R2);
    RHall=RHall*sign(mean(RHall));
    
    plot(CenterIDataAxis(RHall,1,1e6),'o','linewidth',3)
    hold on
end
ChangePlotColor(jet)
Fit_Line(2,'X',[-1.5,1.5])
clf
Curv=-[Fit.a]./[Fit.dY_0]*(5)^2;
plot(BGList,Curv,'.','markersize',25);grid;ylim([-1.5,3]);xlim([-9.8,9.9])
TXYL('Curveture -a/c*W^2 @ W/Rc=1.6', 'BG','Curveture')
Export.Figure3.Panel6.Curve2=[BGList;Curv];