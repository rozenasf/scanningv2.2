%%

%% retract if one wants to 
M_Dock();
%% Go to initial X Y
tic
%need to ask about the scan no
scan_no=Info.ScanNumber+2;%why +2? +1 because this is the last one, and another +1 for the Coarse
%changes
X_points(scan_no)=4000e-6;
Y_points(scan_no)=2500e-6;
S.Set('Zc',4000e-6);
S.Set('Xc',X_points(scan_no),'Yc',Y_points(scan_no))
%changes
Vs=0.017;
Vgold=0.2;
S.Set('Vg',2,'Vs',Vs,'TL',Vgold,'TR',Vgold,'TM',Vgold);
Info.Range.Zc=[2500e-6,4590e-6];
%% Z Approach course
Info.Range.Beta_Sum=[nan,1/250];

Info.Scan=[];
Info.Scan.Description=['Zc Approach'];
Info.Scan.WaitTimes=[0,0.5];
Info.Scan.Axis=BuildStruct(  ...
    'Vg',linspace(1.7,2.1,3), ...
    'Zc',linspace(Info.Channels.Zr,4480e-6,13) ... %4580
    );
Info.Scan.Get=...
    {{                                            ...
    'TuneSignals',3,...
    @()Plot1D(1,'Idc','Hset','Hbg','Hm','Cond'),...
    },{
    'Save',...
    'Beta',...
    @()PlotBeta(2,'Beta_.+'),...
    @()PlotBetaFit(3,'Beta_.+_.+'),...
    @()PlotApproach(4 ,'Beta_Sum')...
    }};
S.RunScan();
DocSend({Info.Scan.Axis,Info.Scan.Description,Info.Scan.SaveName})

%% Z approach Fine
Info.Range.Beta_Sum=[nan,1/100];

Info.Scan=[];
Info.Scan.Description=['Zc Approach'];
Info.Scan.WaitTimes=[0,0.5];
Info.Scan.Axis=BuildStruct(  ...
    'Vg',linspace(1.7,2.1,5), ...
    'Zc',linspace(Info.Channels.Zr,4580e-6,11) ... %4580
    );
Info.Scan.Get=...
    {{                                            ...
    'TuneSignals',3,...
    @()Plot1D(1,'Idc','Hset','Hbg','Hm','Cond'),...
    },{
    'Save',...
    'Beta',...
    @()PlotBeta(2,'Beta_.+'),...
    @()PlotBetaFit(3,'Beta_.+_.+'),...
    @()PlotApproach(4 ,'Beta_Sum')...
    }};
S.RunScan();
DocSend({Info.Scan.Axis,Info.Scan.Description,Info.Scan.SaveName})

pz{scan_no}=Info.Scan.idgr.Beta_Sum.fit('poly1');
%% Z retract
S.Set('Zc',4000e-6);
%% retract
M_Dock();
%% analysis

for scan_n_temp=number1:2:number2
    z_0(scan_n_temp)=-pz{scan_n_temp}.p2/pz{scan_n_temp}.p1;
end
p_x=polyfit(X_points(number1:2:number2),z_0(number1:2:number2),1);
figure;plot(X_points(number1:2:number2),z_0(number1:2:number2),'o',X_points(number1:2:number2),polyval(p_x,X_points(number1:2:number2)),'-');
xlabel('x');
ylabel('z_0');
p_y=polyfit(Y_points(number1:2:number2),z_0(number1:2:number2),1);
figure;plot(Y_points(number1:2:number2),z_0(number1:2:number2),'o',Y_points(number1:2:number2),polyval(p_y,Y_points(number1:2:number2)),'-');
xlabel('y');
ylabel('z_0');




