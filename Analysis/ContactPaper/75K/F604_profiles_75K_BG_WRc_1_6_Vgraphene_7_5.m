% Scans [1014.01:0.01:1014.04], [1015.01:0.01:1015.04]
% [[1:0.25:3,3.5:0.5:6],[1:0.25:3],[1:0.25:2.5],[1:0.25:2]];

if(~exist('AsFuncitonOfKf'));AsFuncitonOfKf=0;end
figure(604);clf
Y={};Traces={};
%ScanList=1017:1083; %1017:1083 LONG OVERNIGHT LOW EXITATION SUM=7.5mV
%ScanList=1105:1136; % Low Voltage
% ScanList=1162:1172;
%  ScanList=[1400:1403]; 
 ScanList=[1406:1460]; %as function of BG 7.5mV on graphene

%%
BGList=[-9.9,-8.9,-6.9,-5.0,-3.4,-2.7,-2.0,-1.4,-0.9,-0.4,0.0];
Range=[-1.25,1.25];
CurvetureSTDList=[];
for j=1:numel(ScanList)
CurvetureSTDList(j)=GetCurvetureFromDSB4(ScanList(j),Range,1.22);
end
%%
Curveture={};
for i=1:11
   Curveture{i}=[]; 
end

for j=1:numel(ScanList)
    Curveture{mod(j-1,11)+1}(end+1)=CurvetureSTDList(j);
end
CurvetureMean=[];Curveturestd=[];
for i=1:11
   CurvetureMean(i)=mean(Curveture{i});
   Curveturestd(i)=std(Curveture{i});
end
if(~AsFuncitonOfKf)
  
  errorbar(BGList,CurvetureMean,Curveturestd*0+mean(Curveturestd/sqrt(4)),'.--','linewidth',2,'markersize',25);
  TXYL('Curveture @ W/Rc=1.6','BG [V]','Curveture');Ax=gca;Ax.FontSize=16;
else
    errorbar(sqrt(-pi*Vbg_to_n(BGList,0.7)),CurvetureMean,Curveturestd*0+mean(Curveturestd/sqrt(4)),'.--','linewidth',2,'markersize',25);
    TXYL('Curveture @ W/Rc=1.6','Kf [1/m]','Curveture');Ax=gca;Ax.FontSize=16;
end

ylim([0,4]);

grid;

% CopyFigure();
return
%%
% j=1:5;
clf
XX={};YY={};
 for j=1:5
%      subplot(2,3,j)
    [~,~,~,XX{j},YY{j}]=GetCurvetureFromDSB4(ScanList(j)+[0:4]*11,Range,1.22);
%     ylim([0,8])
 end
 %%
 clf
 MaxAt=[4.0328    4.2766    4.9375    5.8751    7.1631];
 for j=1:5
     YYY=smooth(YY{j},1)./MaxAt(j);%*sqrt(abs(Vbg_to_n(BGList(j),0.56)))
     plot(XX{j},YYY,'-','linewidth',3);hold on
 end
%    ylim([0,8]);Fit_Line(2,'X',[-1.25,1.25])
 ChangePlotColor(@jet);
 %%
figure(15);clf
X=linspace(-6,6,100);
CurvList=[1.07,2,2.4];
Gauss=exp(-X.^2*2.8);out={};
for i=1:numel(CurvList)
    subplot(3,1,i)
   C= 1./(CurvList(i)./25);
   Parab=(-X.^2+C)./C;
   Parab(abs(X)>2.5)=0;
   plot(X/5,conv(Parab,Gauss,'same'),'linewidth',3);hold on
   out{i}=Fit_Line(2,'X',[-1.5,1.5]/5);
   %(out{i}.Func(X)>=0)
   plot(X,out{i}.Func(X),'-','linewidth',2);
   xlim(out{1}.X_0)
   patch([-0.5,-0.5,-0.6,-0.6],[0,14,14,0],'k','FaceAlpha',0.3)
    patch(-[-0.5,-0.5,-0.6,-0.6],[0,14,14,0],'k','FaceAlpha',0.3)
    ylim([0,14])
end
% ChangePlotColor



% ylim([0,1])
%%


clf
plot(X,exp(-X.^2*2.8)-0.5);
 Fit_Line(@(x)exp(-x.^2));
 diff([Fit.X_0])
%% Hall Voltage
AVS2List=[];
AVS15List=[];
IgrAC15List=[];
for i=1:11
    LoadScan(ScanList(j));
    for j=1:4
        LoadScan(ScanList(i)+0.01*j);
        AVS2List(i,j)=Info.Channels.AVS2;
        AVS15List(i,j)=Info.Channels.AVS15;
        IgrAC15List(i,j)=mean(IgrAC15.data(:));
    end
end
%%
clf
n=abs(Vbg_to_n(BGList,0.7));
plot(1./sqrt(pi*n),-mean(AVS2List(:,[1,4])-AVS2List(:,[2,3]),2)./mean(IgrAC15List,2),'o');
%
WRc_List=[1.6];
Wrc = WRc_List;
r_c=5e-6./Wrc;
    p_F=hbar_*sqrt(pi*n);
    B=p_F./qe_./r_c
   RHall=B./(n*qe_);
   hold on
   Fit_Line(1)
   plot(1./sqrt(pi*n),RHall)
xlim([0,3e-8]);ylim([0,120]);
TXYL('Hall Resistance from DSB calibration','1/Kf [1/m]','RHall [ohm]','Data','linearFit','Theory','location','east')
%%
plot(1./sqrt(pi*n),-(mean(AVS2List(:,[1,4])-AVS2List(:,[2,3]),2)./mean(IgrAC15List,2))./RHall','o');
TXYL('Ratio between measured Hall to expected hall','1/Kf','Ratio')
%%
Curveture=[];CurvetureSTD=[];
BGList=[];BGListFull=[];BetaSumList=[];
for i=1:numel(ScanList)
    LoadScan(ScanList(i)+0.01);BGListFull(i)=Info.Scan.UserData.BG;
    BetaSumList(i)=1./Info.Scan.UserData.Initial_Beta_Sum;
end
BGList_unique=uniquetol(BGListFull,1e-3);
for i=1:numel(BGList_unique)
        index=find( abs(BGList_unique(i)-BGListFull)<1e-3 );
        
        CurvetureSTDList=[];
        for j=1:numel(index)
            CurvetureSTDList(j)=GetCurvetureFromDSB4(ScanList(index(j)),Range);
        end
        [Curveture(i),WRcList(i),BG]=GetCurvetureFromDSB4(ScanList(index),Range);
        %BGList(i)=BG;
        if(numel(index)>1)
        CurvetureSTD(i)=std(CurvetureSTDList)./sqrt(numel(index)-1);
        else
            CurvetureSTD(i)=nan;
        end
        
end
    
figure(4112);clf
% WRcList_unique=unique(WRcList);Curveture_AVR=[];
Lmr_BG_Fit=[-9.38362e-05,-4.20564e-03,-6.39272e-02,-4.72690e-01,-3.20175e+00,2.54587e+00];
Lmr=polyval(Lmr_BG_Fit,BGList_unique);
errorbar(Lmr/5,Curveture,CurvetureSTD,'.','markersize',20,'linewidth',2);
TXYL('curveture in Ey','lMR/W','curveture')
Export.Figure3.Panel3.Curve1=[BGList;Curveture;CurvetureSTD];
%1016 is the first one
ylim([-2,4]);grid