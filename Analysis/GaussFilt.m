function out=GaussFilt(obj,Sigma)
if(floor(Sigma/2)==Sigma/2);error('Sigma must be odd');end
[a,b]=meshgrid(linspace(-3,3,Sigma),linspace(-3,3,Sigma));

out=obj.map(@(x)conv2(x,exp(-a.^2-b.^2)/sum(sum(exp(-a.^2-b.^2))),'same'));
end