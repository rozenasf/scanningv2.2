classdef Keithley237_Driver < handle
    properties
        Vdc
        Idc
        Compliance
        Range_S
        Range_M
        Communication
    end
    %M3
    methods
        function obj=Keithley237_Driver(address)
            obj.Communication=gpib('adlink',address(1),address(2));
            fopen(obj.Communication);
            disp('Connection to Keithly237 established');
        end
        function Init(obj)
           obj.Compliance=1e-3;
           fprintf(obj.Communication,'L,%f,0X',obj.Compliance);
           obj.Range_M=9;
           fprintf(obj.Communication,'L,%dX',obj.Range_M);
           obj.Range_S=2;
           fprintf(obj.Communication,'B,%d,0X',obj.Range_S);
        end
        function [Value,Text]=GetFromMemory(obj,Channel)
            Text=[];Value=0;
            switch Channel
                case 'Vk'
                    Value=obj.Vdc;
                case 'Ik'
                    Value=obj.Idc;
            end
        end
        function Set(obj,Channel,Value)
            switch Channel
                case 'Vk'
                fprintf(obj.Communication, 'B%f,,0X', Value);
                obj.Vdc=Value;
                case 'Compliance'
                fprintf(obj.Communication,'L%f,X',Value);
                obj.Compliance=Value;
                case 'Range_M'
                fprintf(obj.Communication,'L,%dX',Value);
                obj.Range_M=Value;
                case 'Range_S'
                fprintf(obj.Communication,'B,%d,0X',Value);
                obj.Range_S=Value;
            end    
        end
        function out=Get(obj,Channel)
          fprintf(obj.Communication,'G5,0,0X'); 
          kth=fscanf(obj.Communication,'%s');
          line_pos=find(kth==','); 
          if length(line_pos) >= 2
            measure=kth(line_pos(1)+1:line_pos(2)-1);
            mstate=sscanf(measure,'%5s,',1);
          end
            obj.Idc=sscanf(measure,[mstate '%e'],1);
            out=obj.Idc;
        end
        function delete(obj)
            disp('Removing Keithley 237');
            fclose(obj.Communication);
            delete(obj.Communication);
        end
    end
end