function out=InRange(Value,Lim1,Lim2)
    if(Value>real(Lim2) || Value<real(Lim1))
       out=0; 
    else
       out=1; 
    end
end