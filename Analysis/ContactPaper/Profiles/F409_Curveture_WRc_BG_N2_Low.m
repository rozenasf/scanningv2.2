% Scans [1014.01:0.01:1014.04], [1015.01:0.01:1015.04]
% [[1:0.25:3,3.5:0.5:6],[1:0.25:3],[1:0.25:2.5],[1:0.25:2]];
figure(4091);clf;
Y={};Traces={};
ScanList=1017:1083; %1017:1083 LONG OVERNIGHT LOW EXITATION SUM=7.5mV
% ScanList=1102
Range=[-1.28,1.28];
Curveture=[];CurvetureSTD=[];
WRcList=[];WRcListFull=[];
for i=1:numel(ScanList)
    LoadScan(ScanList(i)+0.01);WRcListFull(i)=Info.Scan.UserData.WRc;
end
WRcList_unique=uniquetol(WRcListFull,1e-3);
for i=1:numel(WRcList_unique)
        index=find( abs(WRcList_unique(i)-WRcListFull)<1e-3 );
        
        CurvetureSTDList=[];
        for j=1:numel(index)
            CurvetureSTDList(j)=GetCurvetureFromDSB4(ScanList(index(j)),Range);
        end
        [Curveture(i),WRcList(i),BG]=GetCurvetureFromDSB4(ScanList(index),Range);
        if(numel(index)>1)
        CurvetureSTD(i)=std(CurvetureSTDList)./sqrt(numel(index)-1)/2;
        else
            CurvetureSTD(i)=nan;
        end
        
end
    
figure(4092);clf
% WRcList_unique=unique(WRcList);Curveture_AVR=[];

errorbar(WRcList,Curveture,CurvetureSTD,'.-','markersize',20,'linewidth',2)
Export.Figure3.Panel3.Curve1=[WRcList;Curveture;CurvetureSTD];
%1016 is the first one
ylim([0,6]);grid