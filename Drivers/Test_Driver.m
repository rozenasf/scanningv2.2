classdef Test_Driver < handle
    properties
        a
        b
    end
    
    methods
        function obj=Test_Driver(ini)
            obj.a=ini;
            obj.b=ini*2;
        end

        function out=GetFromMemory(obj,Channel)
            out=obj.(Channel);
        end
        function out=Get(obj,Channel)
            out=obj.(Channel)*2;
        end
        function Set(obj,Channel,in)
            obj.(Channel)=in;
        end
    end
end