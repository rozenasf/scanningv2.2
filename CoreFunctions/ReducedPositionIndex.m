function PositionIndex=ReducedPositionIndex(Dim)
    global Info
    PositionIndex=Info.Scan.Runtime.PositionIndex;
    for i=1:Dim
       PositionIndex=1+(PositionIndex-Info.Scan.Runtime.Position(i))/Info.Scan.Runtime.Position(i);
    end
end