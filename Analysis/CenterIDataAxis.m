function IData=CenterIDataAxis(IData,Axis,Scaling)
if(~exist('Scaling'));Scaling=1;end
for i=1:numel(Axis)
    IData.axes{i}.data=(IData.axes{i}.data-mean(IData.axes{i}.data))*Scaling;
end
end