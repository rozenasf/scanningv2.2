function Driver=GetDriver(Channel)
    global Info
    Driver=RecursiveIsField(Info,'Drivers',GetChannelType(Channel));
end