 [idgr,std_idgr]=LoadScan([2809],L); %2403 - positive BG
% [idgr,std_idgr]=LoadScan([2405],L); %2405 - negative BG

figure(1)
a_centre = zeros(numel(RampBG),1);
a_left = zeros(numel(RampBG),1);
a_right = zeros(numel(RampBG),1);

betasum = 0.5
for i=1:numel(RampBG)
    subplot(5,4,i);
    
    R=Beta_HS15_Hset.slice_index(2,i)/betasum.*AVS15.slice_index(2,i)./IgrAC15.slice_index(2,i);R.name='R';
    %V=Beta_HS15_Hset/0.4*Info.Channels.AVS15_e;V.name='V';
    plot(R);
    
%     out=[];
%     xlim([-11,-10]*1e-6);
%     out{1}=Fit_Line(1);
%     a_left(i) = out{1}.a;
%     xlim([-10,-7]*1e-6);out{2}=Fit_Line(1);
%     a_centre(i) = out{2}.a;
%     xlim([-7,-3.5]*1e-6);out{3}=Fit_Line(1);
%     a_right(i) = out{3}.a;
%     xlim([-12.75,-5]*1e-6);
%     grid

    out=[];
    xlim([-15,-10.5]*1e-6);
    out{1}=Fit_Line(1);
    a_left(i) = out{1}.a;
    xlim([-10.5,-8.75]*1e-6);out{2}=Fit_Line(1);
    a_centre(i) = out{2}.a;
    xlim([-8.75,-5]*1e-6);out{3}=Fit_Line(1);
    a_right(i) = out{3}.a;
    xlim([-15,-5]*1e-6);
    grid
    gray = [0 0 0];
    Ax=gca;
    Ylim=ylim;
    P=patch(gca,[-9,-6,-6,-9]*1e-6,[Ylim(1),Ylim(1),Ylim(2),Ylim(2)],gray,'FaceAlpha',0.1,'LineStyle','none');
    
    
    
    Ax.Children=[Ax.Children(2:end);Ax.Children(1)];
    
   title(sprintf('BG = %0.2f,dR/dx [Ohm/um]=[%0.2f,%0.2f,%0.2f]',RampBG(i), out{1}.a/1e6,out{2}.a/1e6,out{3}.a/1e6))
end
AddTitle

%% plot the slopes
figure(2);
subplot(1,3,2)
plot(RampBG,a_centre); title('centre');xlabel('BG');ylabel('dR/dx (ohm/um)')
subplot(1,3,1)
plot(RampBG,a_left);title('left');xlabel('BG');ylabel('dR/dx (ohm/um)')

subplot(1,3,3)
plot(RampBG,a_right);title('right');xlabel('BG');ylabel('dR/dx (ohm/um)')
AddTitle;
%%
Wl_eff=5; %9 from GDS but have crack
Wr_eff=2;


figure(3);
plot(RampBG,a_left./a_right,RampBG,ones(size(RampBG))*Wr_eff/Wl_eff,'k:'); title('l-r rario');xlabel('BG');
legend('data',[num2str(Wr_eff) '/' num2str(Wl_eff)])

v_dirac=-2.5;
RhoXX_left=a_left*Wl_eff;
RhoXX_right=a_right*Wr_eff;

n= Vbg_to_n(RampBG, v_dirac);
k_F_vec = sqrt(pi*abs(n));


l_mfp_left=1./RhoXX_left'./k_F_vec/2/qe_^2*h_/um_;
l_mfp_right=1./RhoXX_right'./k_F_vec/2/qe_^2*h_/um_;
%%
figure(4)
subplot(121)
plot(RampBG,l_mfp_left/1e-6);xlabel('BG');ylabel('l_{mfp} left [um]')
subplot(122)
plot(RampBG,l_mfp_right/1e-6);xlabel('BG');ylabel('l_{mfp} right [um]')

Box_width=2e-6;

R_sharvin_exp = (a_centre-a_left)*Box_width;
%R_sharvin_exp = (a_centre)*Box_width;

G_sharvin_th_wr = 4*qe_^2/h_*k_F_vec*Wr_eff*um_/pi;
G_sharvin_th_wl = 4*qe_^2/h_*k_F_vec*Wl_eff*um_/pi;

%R_sharvin_th = 1./G_sharvin_th_wr;
R_sharvin_th =1/2*( 1./G_sharvin_th_wr - 1./G_sharvin_th_wl);


factor=1;
figure(5);clf;
hold on
plot(RampBG,R_sharvin_th,'o--');%xlabel('BG');ylabel('R_sharvin_ th')
plot(RampBG,R_sharvin_exp*factor,'o--');%xlabel('BG');ylabel('R_sharvin_ exp')
hold off
xlabel('BG');ylabel('R_{sharvin}');
legend('theory',['exp X' num2str(factor)])