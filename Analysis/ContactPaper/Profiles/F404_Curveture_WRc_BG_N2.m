figure(4041);clf
 Range=1.5;
WRcList=SuperAxis.Curveture_WRc.W_Rc;
WRc_List=[0,[0.25:0.25:6],[0.25:0.25:4],[0.25:0.25:3],[0.25:0.25:1.5],[0.25:0.25:1]];
RHall={};
for i=1:numel(WRcList);
    UnPackScan(Scans.Curveture_WRc{2*i-1});Beta_Sum=0.5;R=Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15;
    R1=R.mean(2);
    UnPackScan(Scans.Curveture_WRc{2*i});Beta_Sum=0.5;R=Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15;
    R2=R.mean(2);
    R1.data=smooth(R1.data,3);
    R2.data=smooth(R2.data,3);
    RHall{i}=diff(R1-R2);
    RHall{i}=RHall{i}*sign(mean(RHall{i}));
    RHall{i}=CenterIDataAxis(RHall{i},1,1e6);
    RHall{i}.axes{1}.data=RHall{i}.axes{1}.data-0.25;

end;
%     plot(RHall,'-','linewidth',3);
%     hold on
% return
WRc_unique=unique(WRcList);
CurvSTD=[];CurvMEAN=[];
for i=1:numel(WRc_unique)
%    STD{i}=0;
    Index=find(WRc_unique(i)==WRc_List);
    figure(4042);clf
    plot(RHall{Index(1)});hold on
    Mean{i}=RHall{Index(1)};
    for j=2:numel(Index)
        plot(RHall{Index(j)});
        Mean{i}=Mean{i}+RHall{Index(j)};
    end
    Mean{i}=Mean{i}./numel(Index);
    Fit_Line(2,'X',[-Range,Range])
    CurvSTD(i)=std(-[Fit.a]./[Fit.dY_0]*(5)^2)/2;
    CurvMEAN(i)=mean(-[Fit.a]./[Fit.dY_0]*(5)^2);
    figure(4041);
    plot(Mean{i},'o');hold on
end
ChangePlotColor(jet)
Fit_Line(2,'X',[-Range,Range])
%
clf
Curv=-[Fit.a]./[Fit.dY_0]*(5)^2;
errorbar(WRc_unique,Curv,CurvSTD,'.','markersize',25);
grid;ylim([0,4.5]);xlim([0,6])
TXYL('Curveture -a/c*W^2 @ BG=-2', 'W_Rc','Curveture')
Export.Figure3.Panel3.Curve2=[WRc_unique;Curv;CurvSTD];