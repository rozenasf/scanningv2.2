function GetSaveName()
global Info S L;
MainPath=L.MainPath;
%if(L.Bind)
    MainPath=[MainPath,L.File{1},'\'];
%end
if(Info.Flags.InScan)
    
    if(Info.Flags.SuperScan == 0)
        try
        Info.Scan.SaveName=[MainPath,sprintf('Scan_%d_%s.mat',Info.ScanNumber,Info.Scan.StartDateTime)];
        catch
        Info.Scan.SaveName=[MainPath,sprintf('Scan_%s.mat',datestr(now,'yyyy_mm_dd_HH_MM_SS'))];
        end
    elseif(Info.Flags.SuperScan > 0)
        try
        Info.Scan.SaveName=[MainPath,sprintf('Scan_%d_%d_%s.mat',Info.ScanNumber,Info.Flags.SuperScan,Info.Scan.StartDateTime)];
         catch
        Info.Scan.SaveName=[MainPath,sprintf('Scan_%s.mat',datestr(now,'yyyy_mm_dd_HH_MM_SS'))];
        end
    end
        
else
    Info.Scan.SaveName=[MainPath,sprintf('Scan_%s.mat',datestr(now,'yyyy_mm_dd_HH_MM_SS'))];
end
end