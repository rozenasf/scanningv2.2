classdef Config_Driver < handle
    properties
        Conf
        Active_Channels
    end
    
    methods
        function obj=Config_Driver(Active_Channels)
           obj.Active_Channels = Active_Channels; 
        end

        %-----
        function out=Get(obj,Channel)
            global S
             if (~isfield(obj.Conf,Channel));error('no such configuration in memory');end
                 
            for i = 1:numel(obj.Active_Channels)
              if(isnan(obj.Conf.(Channel).(obj.Active_Channels{i})));error('nan');end
               S.Set(obj.Active_Channels{i},obj.Conf.(Channel).(obj.Active_Channels{i}));
            end
            out=nan;
        end
        function Set(obj,Channel,Value)
            for i=1:numel(obj.Active_Channels)
                obj.Conf.(Channel).(obj.Active_Channels{i})=GetFromMemory(obj.Active_Channels{i});
            end
        end
       
    end
end