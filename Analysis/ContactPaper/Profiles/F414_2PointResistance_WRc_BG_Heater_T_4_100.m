%%
% while(1)
%     try
BGList=[-9.90,-8.50,-7.22,-6.00,-4.85,-3.78,-2.79,-1.88,-1.07,-0.37,0.20,0.60,0.80,1.20,1.77,2.47,3.28,4.19,5.18,6.25,7.40,8.62,9.90];
figure(3);clf;
Curves={};

       for BGindex=1:11
           k=1;
      subplot(3,4,BGindex);

LoadScan(1241);Data=Rgr15-220;%Data=Rgr15;
TempLeg={};TempVal=[];HeaterVal=[];
for i=1:6
    plot(Data.slice_index(2,BGindex).slice_index(2,i),'-','linewidth',2);hold on
    Curves{k,BGindex}=Data.slice_index(2,BGindex).slice_index(2,i);k=k+1;
    TempVal(end+1)=Temperature.slice_index(2,BGindex).data(1,i);
    HeaterVal(end+1)=Info.Scan.Axis.Heater_Ramp(i);
    TempLeg{end+1}=sprintf('Temp=%0.2f',Temperature.slice_index(2,BGindex).data(1,i));

end

LoadScan(1242);Data=Rgr15-220;
for i=1:1
    plot(Data.slice_index(2,BGindex).slice_index(2,i),'-','linewidth',2);hold on
    Curves{k,BGindex}=Data.slice_index(2,BGindex).slice_index(2,i);k=k+1;
    TempVal(end+1)=Temperature.slice_index(2,BGindex).data(1,i);
    HeaterVal(end+1)=Info.Scan.Axis.Heater_Ramp(i);
    TempLeg{end+1}=sprintf('Temp=%0.2f',Temperature.slice_index(2,BGindex).data(1,i));

end
LoadScan(1257);Data=Rgr15-220;%Data=Rgr15;

for i=1:16
    plot(Data.slice_index(2,BGindex).slice_index(2,i),'-','linewidth',2);hold on
    Curves{k,BGindex}=Data.slice_index(2,BGindex).slice_index(2,i);k=k+1;
    TempVal(end+1)=Temperature.slice_index(2,BGindex).data(1,i);
    HeaterVal(end+1)=Info.Scan.Axis.Heater_Ramp(i);
    TempLeg{end+1}=sprintf('Temp=%0.2f',Temperature.slice_index(2,BGindex).data(1,i));

end

LoadScan(1258);Data=Rgr15-220;%Data=Rgr15;

for i=1:5
    plot(Data.slice_index(2,BGindex).slice_index(2,i),'-','linewidth',2);hold on
    Curves{k,BGindex}=Data.slice_index(2,BGindex).slice_index(2,i);k=k+1;
    TempVal(end+1)=Temperature.slice_index(2,BGindex).data(1,i);
    HeaterVal(end+1)=Info.Scan.Axis.Heater_Ramp(i);
    TempLeg{end+1}=sprintf('Temp=%0.2f',Temperature.slice_index(2,BGindex).data(1,i));

end
 %legend(TempLeg)
ChangePlotColor(@(~)flipud(hsv(28)));
title(sprintf('BG=%0.2f',BGList(12-BGindex)))
%     end
%     pause(30);
% end
       end
       Cell={};
       for j=1:size(Curves,2)
        Cell{j}=splice(Curves(:,j),iaxis('Temperature',TempVal));
       end
       Data=splice(Cell,iaxis('BG',fliplr(BGList(1:11))));Data.name='TwoPointResistance';
       %%
       figure(6);clf;plot(HeaterVal,TempVal,'o');TXYL('Heater calibration','Heater e[V]','Temperature [k]');Fit_Line(2)
      
%      Data.slice_index(3,11).slice_index(1,21).plot('-o')
     %%
     BGIndex=0;
     %%
     figure(6);clf
     BGIndex=BGIndex+1;
     for i=1:28
         plot(Data.slice_index(3,BGIndex).slice_index(2,i)./Data.slice_index(3,BGIndex).slice_index(2,i).slice_index(1,21));
         hold on
     end
     ChangePlotColor(@jet)
     %%
     Slope=1/10;
     X=linspace(-10,10,1000);
Y=ContactTrapz(X,Slope);
     clf
     plot(X,Y)
     %%
     clf
     Data.slice_index(3,1).plot;
     Slope=Fit_Line(1,'X',[-6,-4]);
     Slopes=[Slope.a];
     Y={};X=Data.slice_index(3,1).axes{1}.data;
     for i=1:numel(Slopes)
        Y{i}=ContactTrapz(X,-Slopes(i));
%         numel(Slopes)-i+1
        hold on;
     end
     clf
     for i=1:numel(Slopes)
        hold on;plot(X,Y{i});
        plot(X,Data.slice_index(3,1).slice_index(2,i).data)
     end
     clf
     for i=1:numel(Slopes)
        hold on;plot(X,Data.slice_index(3,1).slice_index(2,i).data-Y{i});
%         plot(X,Data.slice_index(3,1).slice_index(2,i).data)
     end
     %ylim([50,150])
     ChangePlotColor(@jet)
     