function Temp=Legend(Text,List,Location)
Temp={};
for u=1:numel(List);
    Temp{end+1}=sprintf(Text,List(u));
end
if(exists('Location'))
    legend(Temp,'Location',Location);
else
    legend(Temp);
end
end