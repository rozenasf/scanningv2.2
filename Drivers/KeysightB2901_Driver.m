classdef KeysightB2901_Driver < handle
    properties
        Communication
        Compliance_I
        Range_V
        RAW
    end
    methods
        function obj=KeysightB2901_Driver(TCPIP,Port)   %t = tcpip('169.254.5.2', 5025)
            
            obj.Communication=tcpip(TCPIP,Port);
                
            fopen(obj.Communication);
            %mode change to V
            fprintf(obj.Communication,':SOUR:FUNC:MODE VOLT')
            %manual ranges initialization
            fprintf(obj.Communication,'SENS:CURR:RANG:AUTO ON');
            fprintf(obj.Communication,'SOUR:VOLT:RANG:AUTO OFF');
            
            %compliance and range check
            fprintf(obj.Communication,'SENS:CURR:PROT:LEV?\n')
            obj.Compliance_I = str2num(fgets(obj.Communication));
            fprintf(obj.Communication,'SOUR:VOLT:RANG?\n');
            obj.Range_V = str2num(fgets(obj.Communication));
            disp('Connection to KeysightB2901A Successful!');
            fprintf('ComplianceI is set to %e\n',obj.Compliance_I);
            fprintf('Range_V is set to %e\n',obj.Range_V);
        end
        
        
     
        function out=Get(obj,Channel)
             
            flushinput(obj.Communication);
            names=regexp(Channel,'_','split');
%             SplitRaw=regexp(obj.RAW,',','split');
            switch names{end}
%             switch Channel
                case 'I'
                     fprintf(obj.Communication,':MEAS:CURR?\n');
                     out=str2double(fgets(obj.Communication));
                case 'V'
                    fprintf(obj.Communication,':MEAS:VOLT?\n');
                    out=str2double(fgets(obj.Communication));
                otherwise
                    error('Keysight 2901 channel name must end with _I/V');
            end
        end
%             
%             flushinput(obj.Communication);
% %             fprintf(obj.Communication,'READ?\n');
%             fprintf(obj.Communication,':MEAS:CURR?\n');
%            
%             obj.RAW=fgets(obj.Communication);
%             
%             names=regexp(Channel,'_','split');
%             SplitRaw=regexp(obj.RAW,',','split');
%             switch names{end}
%                 case 'I'
%                     out=str2double(SplitRaw{2});
%                 case 'V'
%                     out=str2double(SplitRaw{1});
%                 otherwise
%                     error('Keysight 2901 channel name must end with _I/V');
%             end
%         end
        
        function Set(obj,Channel,Value)
             names=regexp(Channel,'_','split');
%             SplitRaw=regexp(obj.RAW,',','split');
            switch names{end}
%             switch Channel
                case 'V'
                    if(Value<=obj.Range_V)
                        fprintf(obj.Communication,'SOUR:VOLT:LEV %E\n',Value);
                        fprintf(obj.Communication,':MEAS:VOLT?\n');
                        vout=str2double(fgets(obj.Communication));
                        if (vout < Value)
                            error('Compliance in current reached'); 
                        end
                    else
                      sprintf('Keysight2901 voltage cant be above range! %e  >  %e',Value,obj.Range_V)
                    end
  
                 case 'ComplianceI'
                      fprintf(obj.Communication,'SENS:CURR:PROT %E\n',Value);
                      fprintf(obj.Communication,'SENS:CURR:PROT:LEV?\n');
                      complianceI = fgets(obj.Communication);
                      if(Value == str2num(complianceI))
                          obj.Compliance_I = str2num(complianceI);
                          fprintf('ComplianceI set OK %e\n',obj.Compliance_I);
                      else
                          error('Keysight compliance I not set');
                      end
                case 'RangeV'
                    fprintf(obj.Communication,':MEAS:VOLT?\n');
                    vout=str2double(fgets(obj.Communication));
                    if (Value <= vout)
                        error('Vout is greater than the range. Please reduce Vout and then change range of V');
                    end
                    
                    fprintf(obj.Communication,'SOUR:VOLT:RANG %E\n',Value);
                    fprintf(obj.Communication,'SOUR:VOLT:RANG?\n');
                    RangeV = fgets(obj.Communication);
                      if(Value <= str2num(RangeV))
                          obj.Range_V = str2num(RangeV);
                          fprintf('Range_V set OK %e\n',obj.Range_V);
                      else
                          error('Keysight Range_V not set');
                      end
                otherwise
                    error('Set command must be V or ComplianceI or RangeV')
            end
        end
        
        
        
%             obj.Tic=tic;
%         end
%          function set.Range_V(obj,Value)
%             if(obj.ActiveSet)
%                 Input1=obj.Query(sprintf('SOUR:VOLT:RANG %E\n SOUR:VOLT:RANG? \n',Value));
%                 Input2=obj.Query(sprintf('SENS:VOLT:RANG %E\n SENS:VOLT:RANG? \n',Value));
%                 if(Input1==Input2)
%                     obj.Range_V=Input1;
%                     fprintf('Range_V is set to %e\n',obj.Range_V);
%                 else
%                     error('Problem Keithley Range!');
%                 end
%             else
%                 obj.Range_V=Value;
%                 
%             end
%         end
%        function out=Toc(obj)
%             out=toc(obj.Tic);
%         end
%         function set.SourceMode(obj,Value)
%             if(obj.ActiveSet)
%                 fprintf(obj.Communication,'SOUR:FUNC?\n');
%                 Input=fgets(obj.Communication);
%                 obj.SourceMode=Input(1);
%                 fprintf('SourceMode is %s\n',obj.SourceMode);
%             else
%                 obj.SourceMode=Value;
%             end
%         end


%         function set.Compliance_I(obj,Value)
%             if(obj.ActiveSet)
%                 fprintf(obj.Communication,'SENS:CURR:PROT %E\n',Value);
%                 Input=obj.Query('SENS:CURR:PROT:LEV?\n');
%                 if(Input==Value)
%                     obj.Compliance_I=Input;
%                     fprintf('Compliance_I set OK %e\n',obj.Compliance_I);
%                 else
%                     error(sprintf('Problem setting Compliance_I! %e != %e',str2num(obj.RAW),Value));
%                 end
%             else
%                 obj.Compliance_I=Value;
%             end
%         end
%         function set.Compliance_V(obj,Value)
%             if(obj.ActiveSet)
%                 fprintf(obj.Communication,'SENS:VOLT:PROT %E\n',Value);
%                 Input=obj.Query('SENS:VOLT:PROT:LEV?\n');
%                 if(Input==Value)
%                     obj.Compliance_V=Input;
%                     fprintf('Compliance_V set OK %e\n',obj.Compliance_V);
%                 else
%                     error(sprintf('Problem setting Compliance_V! %e != %e',str2num(obj.RAW),Value));
%                 end
%             else
%                 obj.Compliance_V=Value;
%             end
%         end
%         function Input=Query(obj,Command)
%             fprintf(obj.Communication,Command);
%             obj.RAW=fgets(obj.Communication);
%             Input=str2num(obj.RAW);
%         end
%         function set.Range_I(obj,Value)
%             if(obj.ActiveSet)
%                 Input1=obj.Query(sprintf('SOUR:CURR:RANG %E\n SOUR:CURR:RANG? \n',Value));
%                 Input2=obj.Query(sprintf('SENS:CURR:RANG %E\n SENS:CURR:RANG? \n',Value));
%                 if(Input1==Input2)
%                     obj.Range_I=Input1;
%                     fprintf('Range_I is set to %e\n',obj.Range_I);
%                 else
%                     Input1
%                     Input2
%                     error('Problem Keithley Range!');
%                 end
%             else
%                 obj.Range_I=Value;
%             end
%             
%         end
       

%         function RAW=GetRaw(obj)
%             flushinput(obj.Communication);
%             fprintf(obj.Communication,'READ?\n');
%             RAW=fgets(obj.Communication);
%             obj.RAW=RAW;
%         end
        
        function delete(obj)
            disp('DELETING KeysightB2901A!');
            fclose(obj.Communication);
            try delete(obj);end
        end
        
        end
end