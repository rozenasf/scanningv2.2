function FigureNumbers=GetActiveFiguresNumbers()
    handles=findall(0,'type','figure');
    FigureNumbers=sort([handles(:).Number]);
end