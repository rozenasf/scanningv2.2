function [opcode,out]=QueryArduino(Communication,opcode,SendData,Sendformat,Reciveformat)
    function Length=FormatLength(Format)
        Length=0;
        for i=1:numel(Format)
            switch(Format(i))
                case 'B'
                    Length=Length+1;
                case 'f'
                    Length=Length+4;
                case 'L'
                    Length=Length+4;
                otherwise
                    error('Unknown format');
            end
        end
    end
    function [BinnaryOutput,Length]=Data2Format(Format,Data)
        Length=FormatLength(Format);
        Message=zeros(1,Length);
        Location=1;
        for i=1:numel(Format)
            switch(Format(i))
                case 'B'
                    Message(Location)=Data(i);Location=Location+1;
                case 'f'
                    Message(Location:(Location+3))=typecast(single(Data(i)),'uint8');Location=Location+4;
                case 'L'
                    Message(Location:(Location+3))=typecast(int32(Data(i)),'uint8');Location=Location+4;
                otherwise
                    error('Unknown format');
            end
        end
        BinnaryOutput=sprintf('%02X',Message);
    end

    function Data=Format2Data(Format,BinnaryInput)
        Length=FormatLength(Format);
        Data=zeros(1,numel(Format));
        Location=1;
        for i=1:numel(Format)
            switch(Format(i))
                case 'B'
                    Data(i)=BinnaryInput(Location);Location=Location+1;
                case 'f'
                    Data(i)=typecast(uint8([BinnaryInput(Location:(Location+3))]),'single');Location=Location+4;
                case 'L'
                    Data(i)=typecast(uint8([BinnaryInput(Location:(Location+3))]),'int32');Location=Location+4;
                otherwise
                    error('Unknown format');
            end
        end
    end
    [BinnaryOutput,MessageLength]=Data2Format(['B',Sendformat],[opcode*1,SendData]);
    ExpectedLength=FormatLength(['B',Reciveformat]);
    fwrite(Communication,[char(255), char(MessageLength+32), char(ExpectedLength+32), BinnaryOutput ,char(13)]);
    Recived=fread(Communication,ExpectedLength*2);
    Readout=sscanf(char(Recived'),'%02X')';
    Data=Format2Data(['B',Reciveformat],Readout);
    opcode=char(Data(1));
    out=Data(2:end);
% Readout=[90    52   219    33    63]
% disp('hi');
%      Message=['g'];
%      ExpectedLength=5;
%      fwrite(f,[char(255), char(length(Message)+32), char(ExpectedLength+32), sprintf('%02x',Message*1),char(13)]);
%      out=fread(f,10);
%      Readout=sscanf(char(out'),'%02x')';
%      char(Readout(1));
end