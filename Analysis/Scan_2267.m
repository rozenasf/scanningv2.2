%% Create Figures
%%
figure(1);clf
ColorIndex=1;
ColorList=jet(13);
suptitle(sprintf('Scan %d',Info.ScanNumber));
i=1;
Panel{i}=subplot(2,3,i);i=i+1;
grid;xlabel('B');ylabel('Resistance [Ohm]');hold on

Panel{i}=subplot(2,3,i);i=i+1;
grid;xlabel('B');ylabel('l [um]');hold on

Panel{i}=subplot(2,3,i);i=i+1;
grid;xlabel('B');ylabel('Conductivity [1/Ohm]');hold on

Panel{i}=subplot(2,3,i);i=i+1;
xticks([-4,-3,-2,-1,-0.5,0,0.5,1,2,3,4]);grid
xlabel('W/Rc');ylabel('Resistance [Ohm]');xlim([-7,7]);hold on

Panel{i}=subplot(2,3,i);i=i+1;
xticks([-4,-3,-2,-1,-0.5,0,0.5,1,2,3,4]);grid
xlabel('W/Rc');ylabel('l [um]');xlim([-7,7]);hold on

Panel{i}=subplot(2,3,i);i=i+1;
xticks([-4,-3,-2,-1,-0.5,0,0.5,1,2,3,4]);grid
xlabel('W/Rc');ylabel('Conductivity [1/Ohm]');xlim([-7,7]);hold on
%% Load
s = whos;List={s.class};for i=1:numel(List); if(strcmp(List{i},'idata'));clear(s(i).name);end;end %Kill all old idata objects

LoadScan([],L); %AND 2304 at +Vg


Bridge=(Info.Dac.DispExs.VS15.Frequency==Info.Dac.DispExs.VS2.Frequency);

% Parameters
Beta_Sum=0.3;

VSenseGain=1e3;RSense=120;W=5*um_;
v_dirac=-1; %approximate for now
Magnet_Constant=1/10*120e-3;

B=Magnet_I*Magnet_Constant;
if(~IsAxis('BG'));BG=Info.Channels.BG;end
n= Vbg_to_n(BG, v_dirac);
k_F_vec = sqrt(pi*abs(n));
%Rc=r_cyclo(n, B).*sign(B);Rc(isnan(Rc))=inf;

% Calculate helper channels
I=IgrAC15        ;                                                               I.name='I';
if(~Bridge)
    Phi=0.5+0.5*(Beta_HS15_Hset-Beta_HS2_Hset)./(Beta_HS15_Hset+Beta_HS2_Hset);  Phi.name='Phi';
    R=Rgr15;                                                                     R.name='R';
else
    Phi=Beta_HS15_Hset/Beta_Sum.*AVS15_e./(AVS15_e-AVS2_e)-AVS2_e./(AVS15_e-AVS2_e); Phi.name='Phi';
    R = (AVS15_e - AVS2_e)./I;                                                     R.name='R';
end
%Phi=Phi.roi(1,-3e-5,-2.5e-5);
Symmetrize(Phi,'S',2);Symmetrize(Phi,'A',2);
Symmetrize(R,'S',2);Symmetrize(R,'A',2);
I=mean(I,1);
Rgr = R-RSense;                                                                  Rgr.name='Rgr';

dPhi_dX  =SliceSlope(Phi.roi(1,-29e-6,-25e-6));
dPhi_dX_S=SliceSlope(Phi_S.roi(1,-29e-6,-25e-6));
dPhi_dX_A=SliceSlope(Phi_A.roi(1,-29e-6,-25e-6));

RhoXX  =dPhi_dX  .*mean(R,1)  .*W;                                                try RhoXX.name='RhoXX';end
RhoXX_S=dPhi_dX_S.*mean(R_S,1).*W;                                                try RhoXX_S.name='RhoXX_S';end
RhoXX_A=dPhi_dX_A.*mean(R_S,1).*W;                                                try RhoXX_A.name='RhoXX_A';end

k_F_mat=repmat(k_F_vec,size(RhoXX,1),1);
l_mfp=1./RhoXX_S./k_F_mat/2/qe_^2*h_/um_;                                        l_mfp.name='l_mfp';
%l_mfp=1./RhoXX./k_F_mat/2/qe_^2*h_/um_;   

%%

%ColorList=ColorList(12,:)
for i=1:numel(BG)
    Rc=r_cyclo(n(i), B).*sign(B);Rc(isnan(Rc))=inf;W_Rc=W./Rc;
    j=1;
    plot(Panel{j},B,RhoXX_S.data(:,i),'LineStyle','-');j=j+1;
    plot(Panel{j},B,l_mfp.data(:,i),'LineStyle','-');j=j+1;
    plot(Panel{j},B,1./RhoXX_S.data(:,i),'LineStyle','-');j=j+1;
    plot(Panel{j},W_Rc,RhoXX_S.data(:,i),'LineStyle','-');j=j+1;
    plot(Panel{j},W_Rc,l_mfp.data(:,i),'LineStyle','-');j=j+1;
    plot(Panel{j},W_Rc,1./RhoXX_S.data(:,i),'LineStyle','-');j=j+1;
end


% TelegramSend({1});
return
%% Change natural axes
OverideAxis('Magnet_I','B [T]',B);
%or:
% Phi.axes{i_of_B}.data=...

%% Plot

%%
figure(5);
plot_with_color(Phi,1)
% Fit_Line(1,'all')

% R=slice_index(map_vectors(Rgr15,@(x)mean(x),1),1,1);R.name='R';
PhiR=Phi.*Rgr15;
PhiR.name='PhiR';

DPhi_DXs=SliceSlope(Phi);

% dPhidX=[Fit.a, zeros(1,N_B-length([Fit.a]))];

RhoXX=DPhi_DXs.*mean(Rgr15,1)*W; RhoXX.name='RhoXX';
RhoXX.data=(fliplr(RhoXX.data)+RhoXX.data)/2;
l_mfp=1./RhoXX/k_F_vec/2/qe_^2*h_/um_;
l_mfp.name='l_mfp[um]';

figure(24);
subplot(211);l_mfp.plot;
subplot(212);RhoXX.plot;

return;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
for i=1:size(Phi.data,2)
   F=polyfit(Phi.axes{1}.data',Phi.data(:,i),1);
   DPhi_DXs.data(i)=F(1);
end
DPhi_DXs.data=0.5*(DPhi_DXs.data+fliplr(DPhi_DXs.data))
%%

figure(1)
plot(mean(Rgr,1))
AddTitle('Density: 4.4417e+11 1/cm^2')
set(gca,'XTick',[-120:30:120]*1e-3)
grid
%%
RhoXX=DPhi_DXs*Vsd*W./I1;
Idata('RhoXX',RhoXX);
figure(2)
plot(RhoXX)
AddTitle('Density: 4.4417e+11 1/cm^2')
set(gca,'XTick',[-120:30:120]*1e-3)
grid
%%
RhoXX=DPhi_DXs*Vsd*W./I1;
Idata('RhoXX',RhoXX,'W/Rc',W./Rc);
figure(3)
plot(RhoXX)
AddTitle('Density: 4.4417e+11 1/cm^2')
set(gca,'XTick',[-8,-6,-4,-2,-1,0,1,2,4,6,8])
grid
%%
figure(4)
Idata('l_tr_um',1e6*( h_./(2*qe_^2*k_F_vec.*RhoXX))); % in um
plot(l_tr_um)
AddTitle('Density: 4.4417e+11 1/cm^2')
set(gca,'XTick',[-8,-6,-4,-2,-1,0,1,2,4,6,8])
grid