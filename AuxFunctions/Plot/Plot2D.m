    function out=Plot2D(Fig,varargin)
        global Info S;
        varargin=unique(RegExpChannels(varargin));
        out=0;
        figureN(Fig);clf
        SP=OptimizeSP(numel(varargin));
        if(isa(varargin{1},'function_handle'))
            SP=OptimizeSP(numel(varargin)-1);
            f=varargin{1};
            names=varargin(2:end);
        else
            SP=OptimizeSP(numel(varargin));
            names=varargin;
            f=[];
        end
        
        for j=1:numel(names)
            subplot(SP(1),SP(2),j);
            idgr=Info.Scan.idgr.(names{j});
            if(~isempty(f))
                idgr=f(idgr);
            end
%             if(sum(strcmp(names{j},{'HBG','HGraphete'})))
%                 idgr=-idgr;
%             end
            if(idgr.dim==1);continue;end
            idgr.pcolor;%set(gca,'Xdir','reverse')
            %axis equal
            colormap(jet(128));
            %N=3;data=(idgr.data(:));data=data(~isnan(data));data=sort(data);%caxis([min(data(N:end-N)),max(data(N:end-N))]);
            if(isfield(Info.Scan.Axis, 'Xs') && isfield(Info.Scan.Axis, 'Ys'))
                axis equal
            end
        end
        
        drawnow;
    end