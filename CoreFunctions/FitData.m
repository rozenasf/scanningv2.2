function [Slope,SlopeError]=FitData(Chan1,Chan2,Dim,Position)
    global Info
    a=Info.Scan.idgr.(Chan1).data;
    b=Info.Scan.idgr.(Chan2).data;
    if(size(a,1)==1);a=a';b=b';end
    IdxVec=size(a,Dim);IdxVec=1:IdxVec;
    Position=num2cell(Position);
    Position{Dim}=IdxVec;
    a=a(Position{:});a=a(:);a=a(~isnan(a));
    b=b(Position{:});b=b(:);b=b(~isnan(b));
    [fit,bint]=polyfit(a,b,1);[~,d]=polyval(fit,a,bint);
    Slope=fit(1);SlopeError=(mean(d)./(max(a)-min(a)));
end