function out=SliceValue(out,varargin)
    for i=1:numel(varargin)/2
       out=out([out.(varargin{2*i-1})]==varargin{2*i});
    end
end