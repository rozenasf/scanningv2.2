function out=Xs_M_Goto(Target)
out=nan;
    Step=1e-6;
    CurrentLocation=GetFromMemory('Xs');
    number=ceil(abs(CurrentLocation-Target)/Step)+1;
    FastScan({'Xs',linspace(CurrentLocation,Target,number)},{'WorkPoint','Temperature','TuneSignals'})
end