%% DSB profiles 2D scan
   
MagnetList=[1.247,-1.247];
CalibrationList=[1,2];

CalibrationLetter='UD';    
MagnetLetter='PN';
for k=1:20
    S.Set('Dummy',k);
    Info.Flags.SuperScan = 1;
for i=1:numel(CalibrationList)
    for j=1:numel(MagnetList)
        
        
        S.Set('Magnet_I',MagnetList(j));
        
Info.Scan=[];
Info.Scan.Description=['XY Magnet Scanner Feedback at BG = -2 W/Rc=1.6 3umX excitation 7.5mV'];
Info.Scan.UserData.Magnet_I=MagnetList(j);
Info.Scan.UserData.CalibrationPoint=CalibrationLetter(i);
Info.Scan.UserData.BG=-2;

 Info.Scan.WaitTimes=[0.2 0.5];
 
Info.Scan.End=BuildStruct( ...
     'Magnet_I',0 ...
     );
 
 %'Xs_Ramp',linspace(-18.75e-6,-23.75e-6,26),
 % 'Ys_Ramp',linspace(-21.9e-6,-28.05e-6,31) ...
 
Info.Scan.Axis=BuildStruct( ...
        'Ys_Ramp',linspace(-21e-6,-28e-6,29), ...
        'Xs_Ramp', linspace(-20.25e-6,-23.25e-6,15) ...
     );

Info.Scan.Get=...
    {{                                            ...
     'Feedback',...
     {'TuneSignals',...
     'Beta'},7,...
     'Temperature','Time',...
     @()Plot1D(1,'Idc','Hset','Hbg','HS2','HS15','HRest','Rgr15'),...
     @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint')...
    },{  ...
    'Save',...
    }};


Info.Scan.Analysis={...
    @()Plot2D(3,'Beta_.+','WorkPoint','Hset','Idc','Rgr15'),...
    ...@()Plot1D_with_color(4,'Beta_.+','WorkPoint','Hset')...
    };


Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();
% S.Get('Regular');
% S.Get('Feedback','TuneSignals','Beta');
% Info.Scan.UserData.Initial_Beta_Sum=GetFromMemory('Beta_Sum');

S.Get(['Bridge',CalibrationLetter(i),MagnetLetter(j)]);
S.Get('Feedback','TuneSignals','Beta');

S.RunScan();

S.Set('Xs_Ramp',-20.25e-6);
S.Set('Ys_Ramp',-21e-6);
S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));
S.Set('WorkPoint',0);

S.Get('Feedback','TuneSignals','Beta');
    end
end
S.Set('Magnet_I',0);
Info.Flags.SuperScan = 0;
end
