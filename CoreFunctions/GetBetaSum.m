function Beta_Sum=GetBetaSum()
            global S
            S.Set('TempConf');
            S.Get('Regular');
            S.Get('Feedback','TuneSignals','Beta');
            Beta_Sum=GetFromMemory('Beta_Sum');
            pause(2);
            S.Get('TempConf');
            %S.Get('Feedback','TuneSignals','Beta');
end