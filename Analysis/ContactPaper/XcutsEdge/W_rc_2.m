%% X cuts close to the edge as functin of BG at W/Rc = 2;
figure(303);clf
ScanNameList={'XEdge_BGlist_wrc_P2','XEdge_BGlist_wrc_N2'};

for i=1:22
        UnPackScan(Scans.XEdge_BGlist_wrc_P2{i+1})
        Beta_Sum = 0.51;
        R1 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
        UnPackScan(Scans.XEdge_BGlist_wrc_N2{i});Beta_Sum=0.51;
        R2 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
        IData=R1-R2;
        plot(IData.roi(1,-6.5,6.5),'-','linewidth',2,'MarkerSize',10)
%         n = Vbg_to_n(Info.Channels.BG,0.7);
        hold on
end
legend(num2str(BGList'),'Location','northeast')
TXYL('Magnetic focusing at W/rc = 2 as function of BG','Xs (um)','Hall Resistance (ohm)')
    
    
%     legend(num2str(SuperAxis.XEdge_BGlist_Holes_wrc.BG'),'Location','northeast')
%     TXYL(['W/Rc=',num2str(SuperAxis.XEdge_BGlist_Holes_wrc.W_Rc(j))],'X [um]','\Delta R')
% end
% suptitle(sprintf('Hole side'));
