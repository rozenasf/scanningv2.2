function out=InitiateNewFunction(patterns)
%irrelevent old function, dont use...

    global Info;
    if(isempty(Info.Scan.Runtime.RelevantGetProperties{Info.Scan.Runtime.RelevantGetIndex}))
    %Set figure number
    Info.Scan.Runtime.RelevantGetProperties{Info.Scan.Runtime.RelevantGetIndex}.FigureNumber=min(setdiff(1:100,GetActiveFiguresNumbers()));
    %parse input parameters
    Info.Scan.Runtime.RelevantGetProperties{Info.Scan.Runtime.RelevantGetIndex}.Parameters=MatchRegularExpression(fieldnames(Info.Scan.idgr),patterns);
    end
    figureN(Info.Scan.Runtime.RelevantGetProperties{Info.Scan.Runtime.RelevantGetIndex}.FigureNumber);
    out=Info.Scan.Runtime.RelevantGetProperties{Info.Scan.Runtime.RelevantGetIndex}.Parameters;
end