function [dRdX,Sarvin_Right,Sarvin_Left,Vsd]=AnalyzeX_Cut(RangeMid,RangeRight,RangeLeft,Extrapulate)
                Fit_Mid=Fit_Line(1,'X',RangeMid);
                dRdX=[Fit_Mid.a];
                if(nargin>1)
                    Fit_Right=Fit_Line(0,'X',RangeRight);
                    Fit_Left =Fit_Line(0,'X',RangeLeft);
                    Sarvin_Right =   ExtrapulateFit(Fit_Right,Extrapulate(2))-ExtrapulateFit(Fit_Mid,Extrapulate(2));
                    Sarvin_Left  = -(ExtrapulateFit(Fit_Left,Extrapulate(1))-ExtrapulateFit(Fit_Mid,Extrapulate(1)));
                    Vsd=[Fit_Right.a]-[Fit_Left.a];
                end
end