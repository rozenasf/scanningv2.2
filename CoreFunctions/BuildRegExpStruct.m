function out = BuildRegExpStruct( varargin )
out=struct();
    for i=1:(numel(varargin)/2)
        out=AddChannelRegExpToStruct(out,varargin{2*i-1},varargin{2*i});
    end
end

