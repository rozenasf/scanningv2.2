 %% X cuts close to the edge as function of W/Rc for focusing
    %BG=-8.5,9.9,-3.78,5.181
    
    ScanNameList={'XEdge_Magnet_Holes','XEdge_Magnet_Holes_0_5','XEdge_Magnet_Electrons_0_5','XEdge_Magnet_Electrons'};
    for j=1:numel(ScanNameList)
        R=[];
        figure(47)
        subplot(2,4,j);
        ScanName=ScanNameList{j};
        for i=1:numel(Scans.(ScanName))-2
            UnPackScan(Scans.(ScanName){i})
            Beta_Sum=0.51;
            
            R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
            plot(R,'-');hold on
        end
        ChangePlotColor(@(n)jet(28))
        TXYL(sprintf('Edge cuts, Density = %0.2e',Vbg_to_n(SuperAxis.(ScanName).BG,0.7)/1e4),'$X$ (Scaled) $[\mu m]$','$R [Ohm]$')
        %             [dRdX,Sharvin_Right,Sharvin_Left,Vsd]=AnalyzeX_Cut(RangeMid,RangeRight,RangeLeft,InterpolationPoints);
        figure(301);
        subplot(1,4,j);hold on
        %             [Xo,Yo1]=SortXY(Vbg_to_n(SuperAxis.(ScanName).W_Rc(1:end-2),0.7)/1e4,Sharvin_Right);
        %             [Xo,Yo2]=SortXY(Vbg_to_n(SuperAxis.(ScanName).W_Rc(1:end-2),0.7)/1e4,Sharvin_Left);
        %             plot(Xo,Yo1,'.');plot(Xo,Yo2,'.');
        R={};
        for i=1:numel(Scans.(ScanName))-2
            UnPackScan(Scans.(ScanName){i})
            Beta_Sum=0.51;
            if(numel(find(W_Rc_Bare==abs(SuperAxis.(ScanName).W_Rc(i))))>0)
                R{find(W_Rc_Bare==abs(SuperAxis.(ScanName).W_Rc(i)))}{sign(SuperAxis.(ScanName).W_Rc(i))/2+1.5} = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
            end
        end
        for i=1:numel(R)
            Sym=R{i}{2}-R{i}{1};Sym.data=Sym.data+fliplr(Sym.data);
            plot(Sym,'.-','linewidth',1)
        end
        
        ChangePlotColor(@(a)hsv(13));
%       TXYL(sprintf('Symmetrized',Vbg_to_n(SuperAxis.(ScanName).BG,0.7)/1e4),'$X$ (Scaled) $[\mu m]$','$\Delta R [Ohm]$')
            TXYL(sprintf('Density = %0.2e',Vbg_to_n(SuperAxis.(ScanName).BG,0.7)/1e4),'$X$ (Scaled) $[\mu m]$','$R [Ohm]$')
grid on;
    end
    suptitle('Hall Resistance showing Magnetic Focusing');
    figure(47)
    close(gcf);