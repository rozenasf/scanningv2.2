function [Xo,Yo]=SortXY(X,Y)
    [Xo,IDX]=sort(X);
    Yo=Y(IDX);
end