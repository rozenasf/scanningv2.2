           %% Up Location
           ScanNumber=2063;
ScanList=ScanNumber+(0.01:0.01:GetMaxSuperScanNumber(ScanNumber)/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[]
BGList=nan(21,1);
for k=1:numel(ScanList)
   
    LoadScan(ScanList(k));
    Bridge_Calibration_SmallChange.data=(Bridge_Calibration_SmallChange.data-fliplr(Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Bridge_Calibration_SmallChange.axes{1}.data,'o');
%     plot( Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Bridge_Calibration_SmallChange.axes{1}.data;
    n=-(Vbg_to_n(Info.Scan.UserData.BG,0.284));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    
%     plot(WRc_List,RHallTheory)
    %
%     clf
    Measured=Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
%     plot(WRc_List,Measured.data./Theory,'.')
    RatioData=Measured.data./Theory;
 %    RatioData=Measured.data;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];Number=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    for i=1:numel(UWRc)
        List(i)=mean(   RatioData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(RatioData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    BGList(k)=Info.Scan.UserData.BG;
    WRcListBare=linspace(-6,6,28);
Data2D(k,:)=List;
Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,List,abs(STDList./sqrt(Number)),'o')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3,3.8])
figure(10);
pcolor(UWRc,BGList(1:size(Data2D,1)),(Data2D));TXYL('Up side hall effect','W/Rc','BG[V]')
% caxis([0.68,0.79]);
colormap(jet(128))
colorbar
shading flat
FullData{1}=struct('WRc',UWRc,'BG',BGList(1:size(Data2D,1)),'Data',(Data2D));
 % TelegramSend({7,8});
% clf
% imagesc(Data2D)
%        end
%    end
%    pause(30);
% end
           %% Up Location
           ScanNumber=2064;
ScanList=ScanNumber+(0.01:0.01:GetMaxSuperScanNumber(ScanNumber)/100);
figure(7);hold on
Data2D=[];Data2DI=[];
BetaSumVector=[]
BGList=nan(21,1);
for k=1:numel(ScanList)
   
    LoadScan(ScanList(k));
    Bridge_Calibration_SmallChange.data=(Bridge_Calibration_SmallChange.data-fliplr(Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Bridge_Calibration_SmallChange.axes{1}.data,'o');
%     plot( Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Bridge_Calibration_SmallChange.axes{1}.data;
    n=-(Vbg_to_n(Info.Scan.UserData.BG,0.284));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    
%     plot(WRc_List,RHallTheory)
    %
%     clf
    Measured=Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
%     plot(WRc_List,Measured.data./Theory,'.')
    RatioData=Measured.data./Theory;
%     RatioData=Measured.data;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];Number=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    for i=1:numel(UWRc)
        List(i)=mean(   RatioData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(RatioData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    BGList(k)=Info.Scan.UserData.BG;
    WRcListBare=UWRc;


Data2D(k,:)=List;
Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,List,abs(STDList./sqrt(Number)),'o')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3,3.8])
figure(10);
pcolor(UWRc,BGList(1:size(Data2D,1)),abs(Data2D));TXYL('Up side hall effect','W/Rc','BG[V]')
% caxis([0.68,0.79]);
colormap(jet(128))
colorbar
shading flat
 % TelegramSend({7,8});
% clf
% imagesc(Data2D)
%        end
%    end
%    pause(30);
% end
FullData{2}=struct('WRc',UWRc,'BG',BGList(1:size(Data2D,1)),'Data',(Data2D));
%%
           ScanNumber=2065;
ScanList=ScanNumber+(0.01:0.01:GetMaxSuperScanNumber(ScanNumber)/100);
figure(7);hold on
Data2D=[];Data2DI=[];
BetaSumVector=[]
BGList=nan(21,1);
for k=1:numel(ScanList)
   
    LoadScan(ScanList(k));
    Bridge_Calibration_SmallChange.data=(Bridge_Calibration_SmallChange.data-fliplr(Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Bridge_Calibration_SmallChange.axes{1}.data,'o');
%     plot( Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Bridge_Calibration_SmallChange.axes{1}.data;
    n=-(Vbg_to_n(Info.Scan.UserData.BG,0.284));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    
%     plot(WRc_List,RHallTheory)
    %
%     clf
    Measured=Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
%     plot(WRc_List,Measured.data./Theory,'.')
    RatioData=Measured.data./Theory;
%     RatioData=Measured.data;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];Number=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    for i=1:numel(UWRc)
        List(i)=mean(   RatioData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(RatioData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    BGList(k)=Info.Scan.UserData.BG;
    WRcListBare=UWRc;


Data2D(k,:)=List;
Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,List,abs(STDList./sqrt(Number)),'o')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3,3.8])
figure(10);
pcolor(UWRc,BGList(1:size(Data2D,1)),abs(Data2D));TXYL('Up side hall effect','W/Rc','BG[V]')
% caxis([0.68,0.79]);
colormap(jet(128))
colorbar
shading flat
 % TelegramSend({7,8});
% clf
% imagesc(Data2D)
%        end
%    end
%    pause(30);
% end
FullData{3}=struct('WRc',UWRc,'BG',BGList(1:size(Data2D,1)),'Data',(Data2D));
%%

[WRc,IDX]=sort([FullData{1}.WRc,FullData{2}.WRc]);
% size(FullData{1}.Data)
Data=[FullData{1}.Data,FullData{2}.Data];

Data=Data(:,IDX);
% pcolor(WRc,BG,Data(:,IDX));
[BG,IDX2]=sort([FullData{1}.BG;FullData{3}.BG]);
Data=[Data;FullData{3}.Data];
Data=Data(IDX2,:);

surf(WRc,BG,Data)
% caxis([0.5,0.8])
% zlim([0.45,0.9])
% shading flat
colormap(jet)
TXYL('Hall resistance, measured at the top-center of the channel','W/Rc','BG[V]')
%%
