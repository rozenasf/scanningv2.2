function units(system,mode)
% UNITS(SYSTEM,MODE) - define unit system including relevand dimentions and constants
% system: mks, cgs, au
% mode: check 

global units_flag_

if ((nargin>1)&~isempty(units_flag_)), return; end
units_flag_=1;

%mass
global gm_ kg_ me_;
%length
global a0_ angst_ km_ m_ cm_ mm_ um_ nm_ in_;
%time
global sec_ msec_ usec_ nsec_ min_ hour_;
%energy
global erg_ joule_ Ryd_ mRyd_ eV_ meV_ ueV_ keV_ MeV_;
%Pressure
global torr_ mtorr_ Pa_ GPa_ atm_ bar_ kbar_ Mbar_;
%Power
global W_ mW_ uW_ kW_ MW_;
%Force
global dyn_ newton_;
%Volatge
global coul_ Volt_ mVolt_ uVolt_ nVolt_;
%Current
global A_ mA_ uA_ nA_ pA_;
%Resistance
global Ohm_ mOhm_ kOhm_ MOhm_ GOhm_;
%Magnetic Field
global  Tesla_ Gauss_;
%Frequency
global Hz_ kHz_ MHz_ GHz_ THz_;
%Temprature
global K_ mK_;

%Voltage
global V_ mV_ uV_;
%Current
global A_ mA_ uA_ nA_ pA_;
%Resistance
global Ohm_ kOhm_ MOhm_ GOhm_;
%Capacitance
global F_ mF_ uF_ nF_ pF_ fF_ aF_;
%Inductance
global H_ mH_;
%Magnetic Field
global Tesla_ Gauss_;
%Capacitance
global F_ mF_ uF_ nF_ pF_ fF_ aF_;
%Inductance
global Henry_ mHenry_;

%Others
global mole_;

%Physical constants
global h_ hbar_ c_ qe_ me_ mp_ mn_ N0_ Kb_ Rgas_;
global a0_ e0_ eta0_ sig_ G_ ue_ up_ ub_ alpha_;
global Vf_; % Fermi velocity of graphene/NTs

%2DEG constants
global me_GaAs_ mlh_GaAs_ mhh_GaAs_ e_GaAs_ DOSe_ DOSlh_ DOShh_;

% Common units
K_=1;
mole_=1;
coul_=1;

switch system
case{'CGS','cgs'},
%if (strcmp(system,'CGS') | strcmp(system,'cgs')),
  cm_=1;
  gm_=1;
  sec_=1;
case{'MKS','mks'},
%elseif (strcmp(system,'MKS') | strcmp(system,'mks') ),
  m_=1;
  kg_=1;
  sec_=1;
  cm_=1e-2*m_;
  gm_=1e-3*kg_;
case{'AU','au'},
%elseif (strcmp(system,'AU') | strcmp(system,'au')),
  a0_=1;
  Ryd_=1;
  me_=1/2;
  gm_=1/9.1093897e-28*me_;
  cm_=1e8/0.529177249*a0_;
  hbar_=1;
  sec_=1.0545726e-27*gm_*cm_^2/hbar_;
%else
otherwise  
   disp('Incorrect units system');
   return
end   

% Mass
kg_=1e3*gm_;

% Length
angst_=1e-8*cm_;
m_=1e2*cm_;
km_=1e3*m_;
mm_=1e-3*m_;
um_=1e-6*m_;
nm_=1e-9*m_;
in_=2.54*cm_;

% Time
msec_=1e-3*sec_;
usec_=1e-6*sec_;
nsec_=1e-9*sec_;
min_=60*sec_;
hour_=60*min_;

% Temprature
mK_=1e-3*K_;

% Energy
erg_=gm_*(cm_/sec_)^2;
joule_=kg_*(m_/sec_)^2;

% Force
dyn_=gm_*cm_/sec_^2;
newton_=kg_*m_/sec_^2;

% Pressure
Pa_=newton_/m_^2;
GPa_=1e9*Pa_;
atm_=1.01325e5*Pa_;
bar_=1e5*Pa_;
kbar_=1e3*bar_;
Mbar_=1e6*bar_;
torr_=133.322*Pa_;
mtorr_=1e-3*torr_;

%Power
W_=joule_/sec_;
mW_=1e-3*W_;
uW_=1e-6*W_;
kW_=1e3*W_;
MW_=1e6*W_;

% Frequancy
Hz_=1/sec_;
kHz_=1e3*Hz_;
MHz_=1e6*Hz_;
GHz_=1e9*Hz_;
THz_=1e12*Hz_;

%Volatge
V_=kg_*m_^2/(sec_^2*coul_);
mV_=1e-3*V_;
uV_=1e-6*V_;
nV_=1e-9*V_;
%Current
A_=coul_/sec_;
mA_=1e-3*A_;
uA_=1e-6*A_;
nA_=1e-9*A_;
pA_=1e-12*A_;
%Resistance
Ohm_=V_/A_;
mOhm_=1e-3*Ohm_;
kOhm_=1e3*Ohm_;
MOhm_=1e6*Ohm_;
GOhm_=1e9*Ohm_;
%Magnetic Field
Tesla_=kg_/(sec_*coul_);
Gauss=1e-4*Tesla_;
%Capacitance
F_=coul_/V_;
mF_=1e-3*F_;
uF_=1e-6*F_;
nF_=1e-9*F_;
pF_=1e-12*F_;
fF_=1e-15*F_;
aF_=1e-18*F_;
%Inductance
H_=V_*sec_/A_;
mH_=1e-3*H_;

% Physical Constants
h_=6.6260755e-34*joule_/sec_;
hbar_=h_/(2*pi);
c_=2.99792458e8*m_/sec_;
qe_=1.60217733e-19*coul_;
me_=9.1093897e-31*kg_;              % Electron rest mass
mp_=1.6726231e-27*kg_;              % Proton rest mass
mn_=1.6749286e-27*kg_;              % Neutron rest mass
N0_=6.0221367e23*1/mole_;           % Avogadro number
Kb_=1.380658e-23*joule_/K_;         % Boltzmann constant
Rgas_=N0_*Kb_;                      % Universal gas constant
a0_=0.529177249*angst_;             % Bohr radius
e0_=8.854187817e-12*F_/m_;          % Permittivity of vacuum
eta0_=4*pi*1e-7*newton_/A_^2;       % Permeability of vacuum
sig_=5.67051e-8*W_/(m_^2*K_^4);     % Stefan-Boltzmann constant
G_=6.67259e-11*m_^2/(kg_*sec_^2);   % Gravitational constant
ue_=928.47701e-26*joule_/Tesla_;    % electron magnetic moment
up_=1.41060761e-26*joule_/Tesla_;   % Proton magnetic moment
ub_=927.40154e-26*joule_/Tesla_;    % Bohr magneton
alpha_=7.29735308e-13;              % Fine structure constant
Vf_=8e5*m_/sec_;

%Derived energies
eV_=qe_*V_;
meV_=1e-3*eV_;
ueV_=1e-6*eV_;
keV_=1e3*eV_;
MeV_=1e6*eV_;
Ryd_=hbar_^2/(2*me_*a0_^2);
mRyd_=1e-3*Ryd_;

%2DEG constants
me_GaAs_=0.067*me_;                 % Ligth holes in GaAs
mlh_GaAs_=0.38*me_;                 % Ligth holes in GaAs
mhh_GaAs_=0.60*me_;                 % Heavy holes in GaAs
e_GaAs_=13.1*e0_;							% dialectric constant of GaAs	

DOSe_=me_GaAs_/(pi*hbar_^2);			% Electronic DOS
DOSlh_=mlh_GaAs_/(pi*hbar_^2);		% Light Holes DOS
DOShh_=mhh_GaAs_/(pi*hbar_^2);		% Heavy Holes DOS

