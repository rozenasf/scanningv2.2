function M_Zero
global Info S
ToZero={'Heater_Ramp','Vs_Ramp','Vg_Ramp','BG','WorkPoint','Vall'};
for i=1:numel(ToZero)
    S.Set(ToZero{i},0);
    pause(0.02);
end
% Info.Drivers.FeedBack.Zero();
end
