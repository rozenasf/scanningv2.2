
ScanList= 1291:1310;
out=[];
for i=ScanList;
    for j=1:GetMaxSuperScanNumber(i)
        out=[out,AnalyzeRhoXXSharvinNoBridge(i+j/100)];
    end
end
%%
BGPlot=[-9.9,-5,-1.6,3,5,7.5,9.9];
%%
plot([out.BG],[out.Lmr],'o')
%%
out2=SliceValue(out,'BG',-1.6);
plot([out2.WRc],[out2.Lmr],'o')
%%
clf
for Index=1:numel(BGPlot);
R_sharvin_th = h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(BGPlot(Index),0.6)))/5.0e-6;
out2 =SliceValue(out,'BG',BGPlot(Index));
[outP,outN]=Symmetrize(out2,'WRc');
WRc=[outP.WRc];SymContact=([outP.Rleft]+[outP.Rright]+[outN.Rleft]+[outN.Rright])/4;
plot([-fliplr(WRc),WRc],[fliplr(SymContact),SymContact]./R_sharvin_th,'o')
hold on
end
ChangePlotColor(@jet)
%%
clf
WRcPlot=[0];
for Index=1:numel(WRcPlot);
    out2 =SliceValue(out,'WRc',WRcPlot(Index));
R_sharvin_th = h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n([out2.BG],0.6)))/5.0e-6;
Contact=([out2.Rleft]+[out2.Rright])/2;
plot([out2.BG],Contact./R_sharvin_th,'o')
hold on
end
% ChangePlotColor(@jet)