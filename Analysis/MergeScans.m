

List=fieldnames(id{ToMerge(1)});
for i=1:numel(List)
    if(strcmp(class(id{ToMerge(1)}.(List{i})),'idata'))
        Channel=List{i};
        Dim=0;
        for j=1:numel(id{ToMerge(1)}.(Channel).axes)
            if(strcmp(id{ToMerge(1)}.(Channel).axes{j}.name,AxesName))
               Dim=j;
               break; 
            end
        end
        if(Dim==0);continue;end
        Data=[];Ax=[];
        try
            for j=1:numel(ToMerge)
                Data=cat(Dim,Data,id{ToMerge(j)}.(Channel).data);
                Ax=[Ax,id{ToMerge(j)}.(Channel).axes{Dim}.data];
            end
            eval([Channel,'.data=Data;']);
            eval([Channel,'.axes{Dim}.data=Ax;']);
        end
    end
end 