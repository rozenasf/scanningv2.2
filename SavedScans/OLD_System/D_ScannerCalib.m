%% Run a Y-Scan to center of pad
D_XCourseScanFast('Gate2',520e-3,'Xc',linspace(S.Get('Xr'),3650e-6,20),'Vsd',30e-3);
%% Scan lengthily for pad edges, X Direction
D_XCourseScanFast('Gate2',520e-3,'Xc',linspace(S.Get('Xr'),3300e-6,60),'Vsd',30e-3);
%% Run back to original point, pad center
D_XCourseScanFast('Gate2',520e-3,'Xc',linspace(S.Get('Xr'),3500e-6,60),'Vsd',30e-3);
%% Activate scanners
S.Set('Xs0', 50e-6);
%% Scan again
D_YCourseScanFast('Gate2',520e-3,'Yc',linspace(S.Get('Yr'),3800e-6,60),'Vsd',30e-3);
