
%%

GDS.Design_GDS.Design=ExtractLayers(gds_load(['C:/Users/ilani.group/Documents/scanningv2.2/UIFunctions/Sample5x3.gds']),'MainSymbol',[0,1,2,3]);
GDS.Design_GDS.Matrix=sprintf('matrix(%e,%e,%e,%e,%e,%e)',[1,0,0,1,4100.5,2240]*1e-6);
%%

clf
pcolor(Beta_HS2_Hset)
hold on
PlotGDS(applyTransform(Info.Design_GDS.Design,Info.Design_GDS.Matrix));
hold off
%%
figure(1);

k=1;
for J=[479,475,465,473,471,467,477,469,481]

IdgrS{k}=id{J}.Beta_HC6_Hset+id{J+1}.Beta_HC6_Hset;
IdgrA{k}=id{J}.Beta_HC6_Hset-id{J+1}.Beta_HC6_Hset;
k=k+1;
end

%
N=3;M=3;
BGList=[0.5,0.8,0.9,0.95,1,1.05,1.1,1.2,1.5];
for j=1:9
    subplot(N,M,j)
    
    pcolor(Idata(IdgrA{j}.data(:,2:end),fliplr(Xs0),Ys0(2:end)))
    hold on
    PlotGDSOutline(applyTransform(Design_GDS,sprintf('matrix(%e,%e,%e,%e,%e,%e)',matrix)));
    hold off
    title(['BG=',num2str(BGList(j))]);
end
colormap(jet(128));
suptitle('Antisymmetrized B(-10[A] -> 10[A])') 
%%