function OK=CheckStartingTolerance(Channel,Value)
global Info S
OK=1;
if(isfield(Info.InitialTolerance,Channel) )
    if(isnumeric(Info.InitialTolerance.(Channel)))
        if(abs(Info.Channels.(Channel)-Value)>Info.InitialTolerance.(Channel))
            disp(sprintf('out of Initial tolerance %s=%e -> %e',Channel,Info.SetChannels.(Channel),Value))
            OK=0;return;
        end
    else
        S.Get(Info.InitialTolerance.(Channel){1} ); %the channel to check is in the form  {'Xr',10e-6}
        if(abs(Info.GetChannels.( Info.InitialTolerance.(Channel){1} )-Value)>Info.InitialTolerance.(Channel){2})
            fprintf('out of Initial tolerance %s = %e , %s -> %e\n',Info.InitialTolerance.(Channel){1},Info.GetChannels.( Info.InitialTolerance.(Channel){1} ),Channel,Value)
            OK=0;return;
        end
    end
    %Info.Scan.Runtime.HadBeenSetInScan.(Channel)=1;
end
end