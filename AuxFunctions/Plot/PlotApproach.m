    function out=PlotApproach(Fig ,Var)
        out=0;
        global Info
        figure(Fig)
        %idgr=Info.Scan.idgr.(Var).slice_index(1,Info.Scan.Runtime.MaxPosition(1));
        idgr=Info.Scan.idgr.(Var);
        %idgr_err=Info.Scan.idgr.Beta_Sum_Err.slice_index(1,Info.Scan.Runtime.MaxPosition(1));
%        for j=2:numel(Info.Scan.Runtime.Position) %was 3
%            idgr=idgr.slice_index(2,Info.Scan.Runtime.Position(j));
            %idgr_err=idgr_err.slice_index(2,Info.Scan.Runtime.Position(j));
%        end
        %errorbar(1./idgr,idgr_err./(idgr.^2));
        plot(1./idgr);
        hold on
        Zlist=Info.Scan.Axis.(Info.Scan.Runtime.AxisNames{end});
%         if(isfield(Info.Range,'Beta'))
%             plot(Zlist,Zlist*0+1./Info.Range.Beta(2),'r');
%         end
        y=(1./idgr.data);
        x=Zlist;
        x=x(~isnan(y));
        y=y(~isnan(y));
        %x=sort(x); %temporary fix - ilanit
        if(numel(x)>1)
            fit=polyfit(x,y,1);Crash=roots(fit);
            plot([min(x),Crash],polyval(fit,[min(x),Crash]),'m');
            title(['Crash at:', SprintfA(Crash)])
        end
         hold off
    end