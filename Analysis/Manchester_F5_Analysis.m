%% 40mV and BG=1 Xs-Magnet
BatchPlot(1,'Scan 434,436',3,2,@(x)plot_with_color(x,1),...
    id{434}.Beta_HC1_Hset,id{434}.Beta_HC6_Hset,...
    id{436}.Beta_HC1_Hset,id{436}.Beta_HC6_Hset,...
    id{434}.Beta_HC1_Hset-id{436}.Beta_HC1_Hset,...
    id{434}.Beta_HC6_Hset-id{436}.Beta_HC6_Hset)
%% 20mV @ BG = 1.2V Xs-Magnet
BatchPlot(1,'Scan 438,440',3,2,@(x)plot_with_color(x,1),...
    id{438}.Beta_HC1_Hset,id{438}.Beta_HC6_Hset,...
    id{440}.Beta_HC1_Hset,id{440}.Beta_HC6_Hset,...
    id{438}.Beta_HC1_Hset-id{440}.Beta_HC1_Hset,...
    id{438}.Beta_HC6_Hset-id{440}.Beta_HC6_Hset)
%% 20mV @ Xs = -20, BG-Magnet
BatchPlot(1,'Scan 442,443',3,2,@(x)plot_with_color(x,1),...
    id{442}.Beta_HC1_Hset,id{442}.Beta_HC6_Hset,...
    id{443}.Beta_HC1_Hset,id{443}.Beta_HC6_Hset,...
    id{442}.Beta_HC1_Hset-id{443}.Beta_HC1_Hset,...
    id{442}.Beta_HC6_Hset-id{443}.Beta_HC6_Hset)
%% 20mV @ Xs = -20, BG-Magnet - higher resulution
BatchPlot(1,'Scan 444,445',3,2,@(x)plot_with_color(x,1),...
    id{444}.Beta_HC1_Hset,id{444}.Beta_HC6_Hset,...
    id{445}.Beta_HC1_Hset,id{445}.Beta_HC6_Hset,...
    id{444}.Beta_HC1_Hset-id{445}.Beta_HC1_Hset,...
    id{444}.Beta_HC6_Hset-id{445}.Beta_HC6_Hset)
%% 20mV @ BG=1.2, Magnet=10
BatchPlot(5,'Scan 455 2D + magnet = 10 @ BG=1.2V',2,2,@(x)pcolor(x),...
    id{455}.Beta_HC6_Hset.slice_index(3,1),id{455}.Beta_HC1_Hset.slice_index(3,1),...
    id{455}.WorkPoint.slice_index(3,1),id{455}.Beta_Sum.slice_index(3,1)...
    )
colormap(jet(128));
%% 20mV @ BG=1.2, Magnet=10
BatchPlot(6,'Scan 457 2D + magnet =0 @ BG=1.2V',2,2,@(x)pcolor(x),...
    id{457}.Beta_HC6_Hset,id{457}.Beta_HC1_Hset,...
    id{457}.WorkPoint,id{457}.Beta_Sum...
    )
colormap(jet(128));
%% Magnet +-10 @BG=1.2
pcolor(-(id{458}.Beta_HC6_Hset-id{455}.Beta_HC6_Hset.slice_index(3,1)));colormap(jet)
%pcolor(-(id{458}.Beta_HC6_Hset-id{455}.Beta_HC6_Hset.slice_index(3,1))./(id{458}.Beta_HC6_Hset+id{455}.Beta_HC6_Hset.slice_index(3,1)));colormap(jet)
%% Magnet +-10 @BG=0.9
pcolor(id{463}.Beta_HC6_Hset-id{464}.Beta_HC6_Hset);colormap(jet)
%pcolor((id{463}.Beta_HC6_Hset-id{464}.Beta_HC6_Hset)./(id{463}.Beta_HC6_Hset+id{464}.Beta_HC6_Hset));colormap(jet)
%%
%%
Design_GDS=ExtractLayers(gds_load(['Set_imaging_device01_.gds']),'MainSymbol',[1,3]);
%%
matrix=[-1,0,0,-1,110.5,-123.7]*1e-6;
clf
pcolor(Idata(IdgrS{1}.data(:,2:end),fliplr(Xs0),Ys0(2:end)))
hold on
PlotGDSOutline(applyTransform(Design_GDS,sprintf('matrix(%e,%e,%e,%e,%e,%e)',matrix)));
hold off
%%
%BatchLoad([465:482],L)
Channel='Magnet_V';

Symmetrize=0;
Sigma=1;
Contour=0;

figure(1);

k=1;
for J=[479,475,465,473,471,467,477,469,481]

IdgrS{k}=id{J}.(Channel)+id{J+1}.(Channel);
IdgrA{k}=id{J}.(Channel)-id{J+1}.(Channel);
k=k+1;
end

%
N=3;M=3;
BGList=[0.5,0.8,0.9,0.95,1,1.05,1.1,1.2,1.5];
for j=1:9
    subplot(N,M,j)
    if(Contour)
        if(Symmetrize)
            %contour(GaussFilt
            contour(GaussFilt(Idata(IdgrS{j}.data(:,1:end),fliplr(Xs0),Ys0(1:end)),Sigma),20);
        else
            contour(GaussFilt(Idata(IdgrA{j}.data(:,1:end),fliplr(Xs0),Ys0(1:end)),Sigma),20);
        end
    else
        if(Symmetrize)
            %contour(GaussFilt
            %pcolor(GaussFilt(Idata(IdgrS{j}.data(:,1:end),fliplr(Xs0),Ys0(1:end)),Sigma));
            pcolor(GaussFilt(Idata(IdgrS{j}.data(:,1:end),fliplr(Xs0),Ys0(1:end)),Sigma));
        else
            pcolor(GaussFilt(Idata(IdgrA{j}.data(:,1:end),fliplr(Xs0),Ys0(1:end)),Sigma));
        end
    end
    hold on
    PlotGDSOutline(applyTransform(Design_GDS,sprintf('matrix(%e,%e,%e,%e,%e,%e)',matrix)));
    hold off
        xlim([-2.6,-1.6]*1e-5);
    ylim([-1.6,-1]*1e-5);
    %caxis([-0.02,0.02])
    title(['BG=',num2str(BGList(j))]);
end
colormap(jet(128));
if(Symmetrize)
    suptitle(['Symmetrized B(-10[A] -> 10[A]), Smooth = ',num2str(Sigma)]) 
else
    suptitle(['Antisymmetrized B(-10[A] -> 10[A]), Smooth = ',num2str(Sigma)]) 
end
%% retracting hight analysis:
figure(12);
plot( (Beta_HC3_Hset-Beta_HC6_Hset)./Beta_Sum)
%
Fit_Line(@(x)tanh(x),'all');
figure(13);
plot(1./Beta_Sum.map_vectors(@(x)mean(x),1).data(1,1:numel([Fit.a])),1./[Fit.a],'o');
out=Fit_Line(1);
xlabel('1/BetaSum');ylabel('width');
figure(14);
plot(Zs0(1:numel([Fit.a])),1./[Fit.a],'o');
out2=Fit_Line(1);
xlabel('Zs0');ylabel('width');
%TelegramSend('Done')
%% Heating up:
ScanNumbers=[493:498];
BatchLoad(ScanNumbers,L)
%%
Now0=id{493}.Now;
figure(14)
T=[];Beta_Sum=[];ZsCapY=[];WorkPoint=[];Beta_HC3=[];
for J=ScanNumbers
    T=[T,(86400*(id{J}.Now-Now0)+id{J}.Time.data(:)')/60];
    Beta_Sum=[Beta_Sum,id{J}.Beta_Sum.data(:)'];
    ZsCapY=[ZsCapY,id{J}.ZsCapY.data(:)'];
    WorkPoint=[WorkPoint,id{J}.WorkPoint.data(:)'];
    Beta_HC3=[Beta_HC3,id{J}.Beta_HC3_Hset.data(:)'];
end
subplot(1,3,1)
plot(T,Beta_Sum);xlabel('Minutes');ylabel('Beta_Sum');
subplot(1,3,2)
plot(T,ZsCapY);xlabel('Minutes');ylabel('ZsCapY');
subplot(1,3,3)
plot(T,WorkPoint);xlabel('Minutes');ylabel('WorkPoint');
%%
figure(16)
R1=[];
Time=[];
for J=ScanNumbers
    IDX=find(~isnan(id{493}.R1.data(1,:)));
    R1=[R1,id{J}.R1.data(:,IDX)];
    Time=[Time,id{J}.Time.data(1,IDX)]/60;
end
plot_with_color(Idata(R1,BG,'Minutes',Time),1)
%%
LoadScan(498,L);
figure(15);
plot(Time.data(:)/60,ZsCapY.data(:))
xlim([5,max(Time.data(:))]/60)
xlabel('Minutes');ylabel('ZsCapY');
Fit_Line(@(x)exp(x));
fprintf('RC time is : %0.2f minutes\n', -(1./Fit.a))
%%
Idata('Phi',0.5+0.5*(Beta_HS14_Hset-Beta_HS6_Hset)./(Beta_HS14_Hset+Beta_HS6_Hset));
figure(1);clf
plot_with_color(Phi,1)
Fit_Line(@(x,a)a(3)*tanh(a(1)*x+a(2))+a(4)+a(5)*x,[3.07e5,6.18,0.473,0.489,6000],'all')
allfit=[Fit.fit];
Idata('R[Ohm]',slice_index(map_vectors(Rgr,@(x)mean(x),1),1,1));
Idata('Slope[Phi/um]',allfit(5,:)*60e-6.*R.data,BG);
Idata('Rhoxx[Ohm/Squere]',Slope.*R*4e-6);
figure(2);clf
BatchPlot(2,Info.Scan.Description,2,2,@(x)plot(x),Rhoxx,Slope,R)

%% Rxx vs B, BG
% i_posB=547:557;
% i_negB=558:568;

% i_posB=580:580;
i_posB=557;

N=length(i_posB);

LoadScan(i_posB,L);
% LoadScan(i_negB,L);
%
figure(11);clf;
figure(12);clf;
figure(22);clf;
figure(13);clf;
figure(14);clf;
figure(15);clf;


% Ex14=1/Info.Scaling.Ggr14;
edgeX=[-8 0]*1e-6;
Len=60e-6;
W_eff=4*um_; %effective channel width

i_s=i_posB;
% i_s=i_negB;
dPhidX=[];
N_B=length(Info.Scan.idgr.Beta_HS14_Hset.axes{2});
N_x=length(Info.Scan.idgr.Beta_HS14_Hset.axes{1});


n1=1;
n2=1;

n=Vbg_to_n(Info.Scan.Start.BG,-4); %units of m-2
k_F=sqrt(pi*n);

%Idata('Magnet_I',Magnet_I*0.11/10,Magnet_I);
% B=Magnet_I*0.11/10;B.name='B [T]';
%  idgr.Beta_Hbg_Hset.axes{2}.name='B [T]';

for i=1:N
    LoadScan(i_s(i),L);
    
%     id{i_s(i)}.Beta_Hbg_Hset.axes{2}.data=id{i_s(i)}.Beta_Hbg_Hset.axes{2}.data*0.11/10;
%     id{i_s(i)}.Beta_Hbg_Hset.axes{2}.name='B [T]';
    idgr.Beta_HS14_Hset.axes{2}.data=idgr.Beta_HS14_Hset.axes{2}.data*0.11/10;
    idgr.Beta_HS14_Hset.axes{2}.name='B [T]';
    B=idgr.Beta_Hbg_Hset.axes{2};

   idgr.Beta_HS14_Hset.axes{2}.data= r_cyclo(n, B.data)/W_eff; 
   idgr.Beta_HS14_Hset.axes{2}.name='R_c/W';

    figure(11);
    subplot(n1,n2,i);
    plot_with_color(Beta_HS14_Hset,1)
    
%     Idata('Phi',0.5+0.5*(id{i_s(i)}.Beta_HS14_Hset-id{i_s(i)}.Beta_HS6_Hset)./(id{i_s(i)}.Beta_HS14_Hset+id{i_s(i)}.Beta_HS6_Hset));
  %     Idata('R',slice_index(map_vectors(id{i_s(i)}.Rgr,@(x)mean(x),1),1,1));

%     Idata('PhiR',Phi.data.*repmat(R.data,N_x,1),Xs0,Magnet_I);
    
    Phi=0.5+0.5*(Beta_HS14_Hset-Beta_HS6_Hset)./(Beta_HS14_Hset+Beta_HS6_Hset);
    Phi.name='Phi';

    idgr.R=slice_index(map_vectors(Rgr,@(x)mean(x),1),1,1);
    idgr.R.name='R';
    
    PhiR=Phi.*repmat(R.data,N_x,1);
    PhiR.name='PhiR';
    
    figure(22);
    subplot(2*n1,n2,2*i-1);
    plot_with_color(Phi,1)
    subplot(2*n1,n2,2*i);
    plot_with_color(PhiR,1)

    
    figure(12);
    subplot(n1,n2,i);
    plot_with_color(Phi,1)
    xlim(edgeX)
    Fit_Line(1,'all');
    dPhidX=[Fit.a, zeros(1,N_B-length([Fit.a]))];
        
        RXX=dPhidX*Len.*R; RXX.name='RXX';        
        RhoXX=dPhidX.*R*W_eff; RhoXX.name='RhoXX';

    figure(13);
    subplot(n1,n2,i);RhoXX.plot;
    
    figure(14);
    subplot(n1,n2,i);R.plot;
    
    Idata('l_mfp[um]',1./RhoXX/k_F/2/qe_^2*h_/um_);
    figure(15);
    subplot(n1,n2,i);l_mfp.plot;

    figure(20);
    subplot(211);l_mfp.plot;
    subplot(212);RhoXX.plot;
%     
end

%%
for i=1:N

figure(12);
    subplot(3,4,i);
    plot_with_color(id{i_negB(i)}.Beta_HS14_Hset,1)
end

%% Thoretical point where R_c=W
Vdirac=-4.47; %BG voltage in Dirac point
% V_BG=-2:0.5:3;
V_BG=7;
W_eff=4*um_; %effective channel width
I_magnet=linspace(0,10,21);
B=I_magnet*0.12/10; %field in Tesla
r_c=zeros(1,length(B));


for i=1:length(B)
    n=Vbg_to_n(V_BG,Vdirac); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    r_c(i)=p_F/qe_/B(i);
end

figure(100);
% plot(I_magnet,r_c/um_,I_magnet,ones(size(I_magnet))*W_eff/um_,'k:')
% xlabel('I [A]');
plot(B*1e3,r_c/um_,B*1e3,ones(size(B))*W_eff/um_,'k:')
xlabel('B [mT]')
ylabel('rc [um]')
title(['BG=' num2str(V_BG)])
legend('r_C','channel width W')

%% Phi, Rhoxx, l mean free path and more
% ind=557;
% ind=583;
% ind=704;
% ind=705;
ind=706;

LoadScan([],L);

edgeX=[-8 0]*1e-6;
% edgeX=[-36 -27.5]*1e-6;

Len=60e-6;
W_eff=4*um_; %effective channel width
n=Vbg_to_n(Info.Scan.Start.BG,-4); %units of m-2
k_F=sqrt(pi*n);


Beta_HS14_Hset.axes{2}.data=Beta_HS14_Hset.axes{2}.data*0.11/10;
Beta_HS14_Hset.axes{2}.name='B [T]';
B=Beta_HS14_Hset.axes{2};
N_B=length(Info.Scan.idgr.Beta_HS14_Hset.axes{2});
N_x=length(Info.Scan.idgr.Beta_HS14_Hset.axes{1});


 idgr.Beta_HS14_Hset.axes{2}.data= W_eff./r_cyclo(n, B.data); 
 idgr.Beta_HS14_Hset.axes{2}.name='W/R_c';

Phi=0.5+0.5*(Beta_HS14_Hset-Beta_HS6_Hset)./(Beta_HS14_Hset+Beta_HS6_Hset);
Phi.name='Phi';

Rgr=0*Phi+Rgr;
Rgr.name='Rgr';
%
R=slice_index(map_vectors(Rgr,@(x)mean(x),1),1,1);
R.name='R';
PhiR=Phi.*repmat(R.data,N_x,1);
PhiR.name='PhiR';
    

%
figure(20);
subplot(121);
plot_with_color(Phi,1)
subplot(122);
plot_with_color(PhiR,1)
%
figure(21);
subplot(121)
plot(R);
subplot(122);
plot_with_color(Rgr,1)

figure(22);
plot_with_color(Phi,1)
xlim(edgeX)
Fit_Line(1,'all');
dPhidX=[Fit.a, zeros(1,N_B-length([Fit.a]))];


% RXX=dPhidX*Len.*R; RXX.name='RXX';        
RhoXX=dPhidX.*R*W_eff; RhoXX.name='RhoXX';
l_mfp=1./RhoXX/k_F/2/qe_^2*h_/um_;
l_mfp.name='l_mfp[um]';

figure(24);
subplot(211);l_mfp.plot;
subplot(212);RhoXX.plot;


%%
inds=[626 629 632:638 ];

bb=[];
xx=[0:-5:-40]*1e-6;
 
 
for i=1:length(inds)
    LoadScan(inds(i),L)
    Channel=(Beta_HS14_Hset+Beta_HS6_Hset)./Beta_Sum;
%         Channel=(Beta_HS14_Hset+Beta_HS6_Hset);

    
    figure(10);plot(Channel)
    Fit_Line(@(x)tanh(x))
    
    bb=[bb Fit.fit(1)];
end

figure(5);clf;plot(xx,1./bb,'o');xlabel('Xs0');ylabel('1/b [um]')
% Fit_Line(1)
