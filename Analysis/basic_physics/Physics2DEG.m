function P=Physics2DEG()
    
    P.Quantum_Capacitance=@(n)Quantum_Capacitance(n);
    P.Geometrical_Capacitance=@(d)Geometrical_Capacitance(d);
    P.Kf=@(n)Kf(n);P.Fermi_Wavelength=@(n)Fermi_Wavelength(n);
    P.Thomas_Fermi_Length=@(n)Thomas_Fermi_Length(n);
    %%
global gm_ kg_ me_;
global a0_ angst_ km_ m_ cm_ mm_ um_ nm_;
global sec_ msec_ usec_ nsec_ min_ hour_;
global erg_ joule_ Ryd_ mRyd_ eV_ meV_ ueV_ keV_ MeV_;
global torr_ mtorr_ Pa_ GPa_ atm_ bar_ kbar_ Mbar_;
global W_ mW_ uW_ kW_ MW_;
global dyn_ newton_;
global coul_ Volt_ mVolt_ uVolt_ nVolt_;
global A_ mA_ uA_ nA_ pA_;
global Ohm_ mOhm_ kOhm_ MOhm_ GOhm_;
global  Tesla_ Gauss_;
global Hz_ kHz_ MHz_ GHz_ THz_;
global K_ mK_;
global V_ mV_ uV_;
global A_ mA_ uA_ nA_ pA_;
global Ohm_ kOhm_ MOhm_ GOhm_;
global F_ mF_ uF_ nF_ pF_ fF_ aF_;
global H_ mH_;
global Tesla_ Gauss_;
global F_ mF_ uF_ nF_ pF_ fF_ aF_;
global Henry_ mHenry_;
global mole_;
global h_ hbar_ c_ qe_ me_ mp_ mn_ N0_ Kb_ Rgas_;
global a0_ e0_ eta0_ sig_ G_ ue_ up_ ub_ alpha_;
global Vf_;
global me_GaAs_ mlh_GaAs_ mhh_GaAs_ e_GaAs_ DOSe_ DOSlh_ DOShh_;
    %%
    
    %NT energy scale 0.5[meV*um] (in shahal paper it is 5-20. why?)
    function [Value,Units]=Quantum_Capacitance(n);Value=2/pi*(qe_^2*sqrt(pi*n))/(hbar_*Vf_*pi);Units='F/m^2';end
    function [Value,Units]=Geometrical_Capacitance(d);Value=e0_/d;Units='F/m^2';end
    function [Value,Units]=Kf(n);Value=sqrt(pi*n);Units='1/m';end
    function [Value,Units]=Fermi_Wavelength(n);Value=2*pi/sqrt(pi*n);Units='m';end
%     function [Value,Units]=Thomas_Fermi_Length(n);Value=pi*e0_*hbar_*Vf_/(qe_^2*sqrt(pi*n));Units='m';end
   % function [Value,Units]=Thomas_Fermi_Length(n);Value=2*pi/ (2*qe_*sqrt(sqrt(pi*n)/(e0_*h_*Vf_)));Units='m';end
function [Value,Units]=Thomas_Fermi_Length(n);Value=2/sqrt(pi*n);Units='m';end
end