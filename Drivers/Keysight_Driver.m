classdef Keysight_Driver < handle
    properties
        V
        Range
        Communication
    end
    methods
        function obj=Keysight_Driver()
            DutAddr = 'USB0::0x2A8D::0x1301::MY57206080::0::INSTR'; 
            myDmm = visa('agilent',DutAddr);
            set(myDmm,'EOSMode','read&write');
            set(myDmm,'EOSCharCode','LF') ;
            obj.Communication=myDmm;
            fopen(obj.Communication);
            disp('Connection to Keysight established');
        end
        function Init(obj)
              fprintf(obj.Communication,'CONF:VOLT:DC 1.0, 0.0001');
              fprintf(obj.Communication,'VOLT:DC:NPLC 1');
        end
%         function [Value,Text]=GetFromMemory(obj,Channel)
%             Text=[];Value=0;
%             switch Channel
%                 case 'V'
%                     Value=obj.V;
%             end
%         end
% was commented out by Asaf R
        function out=Get(obj,Channel)
        fprintf(obj.Communication,'READ?');
        res=str2double(fscanf(obj.Communication));
            obj.V=res;
            out=obj.V;
        end
        function delete(obj)
            disp('Removing Keysight');
            fclose(obj.Communication);
            delete(obj.Communication);
        end
    end
end