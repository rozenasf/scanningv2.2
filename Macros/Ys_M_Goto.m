function out=Ys_M_Goto(Target)
out=nan;
    Step=2e-6;
    CurrentLocation=GetFromMemory('Ys');
    number=ceil(abs(CurrentLocation-Target)/Step)+1;
    FastScan({'Ys',linspace(CurrentLocation,Target,number)},{'WorkPoint','Temperature','TuneSignals'})
end