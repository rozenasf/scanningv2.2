function CalculateScalingFromDac()
global Info;
ExitationsNames=fieldnames(Info.Dac.DispExs);TuneNames=fieldnames(Info.Dac.Tone_Signals);
for i=1:numel(TuneNames)
    ExitationNumber=Info.Dac.Tone_Signals.(TuneNames{i}).Excitation+1;
    if(Info.Dac.Tone_Signals.(TuneNames{i}).Harm==0)
        Info.Scaling.(TuneNames{i})=1/sqrt(2);
    else
        Info.Scaling.(TuneNames{i})=1/Info.Dac.DispExs.(ExitationsNames{ExitationNumber}).Amp...
            *((Info.Dac.DispExs.(ExitationsNames{ExitationNumber}).Phase/(2^16)==0)*2-1); %Asaf Addition for 180 deg phase support
    end
end

%stupid uri adds stuff for the first time
fc=346;

for i=1:numel(TuneNames)
    ExitationNumber=Info.Dac.Tone_Signals.(TuneNames{i}).Excitation+1;
    if (Info.Dac.DispExs.(ExitationsNames{ExitationNumber}).Amp>0.028)
        %error(' please look here!!!!!! this is not checked for channels that have not 0 phase!!!!');
        Transfer_Func=1/(1+1i*Info.Dac.DispExs.(ExitationsNames{ExitationNumber}).Frequency/fc);
        %         Transfer_Func=1;
        %the phase is in binnary! AsafR 220518. fixed
        if(Info.Dac.Tone_Signals.(TuneNames{i}).Component==0)
            Info.Scaling.(TuneNames{i})=Info.Scaling.(TuneNames{i})/real(Transfer_Func*exp(1i*2*pi/2^16*Info.Dac.DispExs.(ExitationsNames{ExitationNumber}).Phase));
        else
            Info.Scaling.(TuneNames{i})=Info.Scaling.(TuneNames{i})/imag(Transfer_Func*exp(1i*2*pi/2^16*Info.Dac.DispExs.(ExitationsNames{ExitationNumber}).Phase));
        end
    end
end
end