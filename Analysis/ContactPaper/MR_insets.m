
%% magneto resistance - analyze all
List=332:336
figure(2);clf;
RhoXX=cell(1,numel(List));SharvinJump=cell(1,numel(List));BGList=[];
W_Rc=cell(numel(List),1);
for i=1:numel(List)
LoadScan(List(i),L)
BGList(i)=Info.Channels.BG;
figure(1)
    n=abs(Vbg_to_n(Info.Channels.BG,0.7)); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    r_c=p_F./qe_./B;
    W_Rc{i}=5e-6./r_c;
    
R=((Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5)*(2.5e-3)./IgrAC15;
XLocation=[-11,-30]*1e-6;
figure(1);clf
plot_with_color(R,1,@jet)
% plot_with_color(((Beta_HS15_Hset-Beta_HS2_Hset)./(2*Beta_Sum)+0.5)*(2.5e-3),1,@jet);
%
[Slope,Extrapulate]=Fit_Line(1,'X',([-3.2,3.2]-20.5)*1e-6,'a','Eval',XLocation);
Top=Fit_Line(1,'X',[XLocation(1),XLocation(1)+2e-6],'AVR');
Bot=Fit_Line(1,'X',[XLocation(2)-2e-6,XLocation(2)],'AVR');

R_sharvin_th = h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(Info.Channels.BG,0.7)))/5.0e-6;

figure(i+1);
subplot(2,1,1)
%plot(B,Top-Extrapulate(1,:),'o--','linewidth',3);  hold on
SharvinJump{i}=0.5*((Top-Extrapulate(1,:))-(Bot-Extrapulate(2,:)));
SharvinJump{i}=0.5*(SharvinJump{i}+fliplr(SharvinJump{i}));
plot(W_Rc{i},SharvinJump{i},'-','linewidth',3);hold on
% plot(W_Rc{i},R_sharvin_th*ones(size(B)),'-','linewidth',3);          hold off
% ylim([0,60]);
title(sprintf('Contact resistance of the 5um channel, scan %d, BG=%0.1f',Info.ScanNumber,Info.Channels.BG));
xlabel('W/Rc');ylabel('R [Ohm]');
Ax=gca;Ax.XTick=[-10:-1,-0.5,0,0.5,1:10];grid
subplot(2,1,2)
 RhoXX{i}=0.5*(Slope+fliplr(Slope))*5e-6;
% RhoXX{i}=Slope*5e-6;
plot(W_Rc{i},RhoXX{i},'-','linewidth',3);
title(sprintf('RhoXX, scan %d, BG=%0.1f',Info.ScanNumber,Info.Channels.BG));
%ylim([0,20]);
xlabel('W/Rc');ylabel('R_{per square} [Ohm]');
Ax=gca;Ax.XTick=[-10:-1,-0.5,0,0.5,1:10];grid

end
%%
 plot(W_Rc{1},1.3*RhoXX{1},'.--','markersize',20,'linewidth',2);
 hold on
plot(W_Rc{3},RhoXX{3},'.--','markersize',20,'linewidth',2);
hold off
%%
figure(1);clf
plot(W_Rc{3},(1.3*RhoXX{1}+RhoXX{3})/2.3,'.--','markersize',20,'linewidth',2);
hold on
plot(WRC*1.4,-R*62,'o');grid