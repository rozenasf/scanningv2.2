%% load GDS
% % gdsload = gds_load('SET_imaging_device01_.gds');
% % st_sample = gdsload.structures.elements;
% % save('manchester1_GDS','st_sample')
% load('manchester1_GDS');
%% Load data
idgr=LoadScan(249)
idgr.Beta_HC1_Hset.axes{1}.data = idgr.Beta_HC1_Hset.axes{1}.data*1e6;
idgr.Beta_HC1_Hset.axes{2}.data = idgr.Beta_HC1_Hset.axes{2}.data*1e6;
idgr.Beta_HC6_Hset.axes{1}.data = idgr.Beta_HC6_Hset.axes{1}.data*1e6;
idgr.Beta_HC6_Hset.axes{2}.data = idgr.Beta_HC6_Hset.axes{2}.data*1e6;
idgr.Beta_HC3_Hset.axes{1}.data = idgr.Beta_HC3_Hset.axes{1}.data*1e6;
idgr.Beta_HC3_Hset.axes{2}.data = idgr.Beta_HC3_Hset.axes{2}.data*1e6;
idgr.Beta_Hbg_Hset.axes{1}.data = idgr.Beta_Hbg_Hset.axes{1}.data*1e6;
idgr.Beta_Hbg_Hset.axes{2}.data = idgr.Beta_Hbg_Hset.axes{2}.data*1e6;
idgr.WorkPoint.axes{1}.data = idgr.WorkPoint.axes{1}.data*1e6;
idgr.WorkPoint.axes{2}.data = idgr.WorkPoint.axes{2}.data*1e6;
%
% ax_all=[-30 0 -30 0];
ax_all=[-30 -15 -20 -5];
shift_all=[-110 125];

figure(100); 
clf;

subplot(231); %title('C1')
pcolor(idgr.Beta_HC1_Hset); colormap jet
hold on; 
overlay_gds(gca, st_sample, 'initial_shift', shift_all, 'rotate_180', true);
hold off
axis(ax_all)

subplot(232); %title('C1')
pcolor(idgr.Beta_HC6_Hset); colormap jet
hold on; 
overlay_gds(gca, st_sample, 'initial_shift', shift_all, 'rotate_180', true);
hold off
axis(ax_all)

subplot(233); %title('C1')
pcolor(idgr.Beta_HC3_Hset); colormap jet
hold on; 
overlay_gds(gca, st_sample, 'initial_shift', shift_all, 'rotate_180', true);
hold off
axis(ax_all)

subplot(234); %title('C1')
pcolor(idgr.Beta_Hbg_Hset); colormap jet
hold on; 
overlay_gds(gca, st_sample, 'initial_shift', shift_all, 'rotate_180', true);
hold off
axis(ax_all)

subplot(235); %title('C1')
pcolor(idgr.WorkPoint); colormap jet
hold on; 
overlay_gds(gca, st_sample, 'initial_shift', shift_all, 'rotate_180', true);
hold off
axis(ax_all)

