W_Rc_List=[1,-1,1.5,-1.5,2,-2,3,-3,4,-4];
for Rc_Index=1:numel(W_Rc_List)
% w/rc = -2
Vec=linspace(-9.2,9.2,22);
BGList=[-9.9,-9.9,sign(Vec).*abs(Vec).^1.5/sqrt(9.2)+0.7];
BGList=BGList(1:13);
Info.Flags.SuperScan=1;

Wrc=W_Rc_List(Rc_Index);

BG=BGList;
Width = 5e-6;qe_=1.6022e-19;hbar_=1.0546e-34;
r_c=Width./Wrc;n = abs(Vbg_to_n(BG,0.7));
p_F=hbar_*sqrt(pi*n);B=p_F./qe_./r_c;
MagnetList=B./120e-3*10;
TelegramSend(sprintf('Started W/Rc=%0.1f',Wrc));

for i=1:numel(BGList)
    
    S.Set('RampBG',BGList(i));
    S.Get('Feedback','TuneSignals','Beta');
    S.Get(['Bridge',num2str(i-(i>1))]);
    
    S.Set('Magnet_I',MagnetList(i));
    
Bound=[-10,-14,-28.5,-32.5]*1e-6;
N=[27,30,27];
Mid=linspace(Bound(2),Bound(3),N(2));Mid=Mid(2:end-1);
XVec=smooth([linspace(Bound(1),Bound(2),N(1)),Mid,linspace(Bound(3),Bound(4),N(3))],9);

Info.Scan=[];
Info.Scan.Description=['X Scanner Feedback'];
Info.Scan.UserData=struct('BG',BGList(i),'W_Rc',Wrc,'Magnet_I',MagnetList(i));
 Info.Scan.WaitTimes=[0.1];
 
 
Info.Scan.Axis=BuildStruct(  ...
    'Xs_Ramp',XVec ...
    );


Info.Scan.Get=...
    {{                                            ...
     'Feedback',...
     {'TuneSignals',...
     'Beta'},4,...
     'Temperature','Time',...
     @()Plot1D(1,'Idc','Hset','Hbg','HS2','HS15','HRest','Rgr15'),...
     @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint'),...
    'Save'...
     }};

S.RunScan();
% 
S.Set('Xs_Ramp',-10.0e-6);
S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));
S.Set('WorkPoint',0)
S.Get('Feedback','TuneSignals','Beta');
end
Info.Flags.SuperScan=0;
end