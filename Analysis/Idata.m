function out=Idata(varargin)
GotData=0;
Marker=0;
Ax={};
k=1;
SetInWorkspace=0;
Data=[];DataName='NoName';
for i=1:numel(varargin)
    Input=varargin{i};
    switch class(Input)
        case 'double'
            if(~Marker)
                Name=inputname(i);
            end
            if(~GotData)
               DataName=Name;
               Data=Input;
               GotData=1;
            else
                Ax{k}=iaxis(Name,Input);k=k+1;
            end
            Marker=0;
        case 'char'
            Marker=1;
            Name=Input;
            if(~GotData)
                SetInWorkspace=1;
            end
        case 'iaxis'
                Ax{k}=Input;k=k+1;
        case 'idata'
            if(~Marker)
                DataName=Input.name;
            else
                DataName=Name;
            end
            Data=Input.data;
            Ax=Input.axes;
            GotData=1;
    end
end
out=idata(DataName,Data,Ax,{0},' ');
IDX=find(DataName=='[');
if(~isempty(IDX));DataName=DataName(1:IDX-1);end
if(SetInWorkspace);assignin('base',DataName,out);end
end