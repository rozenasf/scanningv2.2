function [Slope,Slope_Std]=SliceSlope(In)
if(numel(In.axes)==1)
    Fit=polyfit(In.axes{1}.data,In.data(:)',1);
    Slope=Fit(1);
    YFit=polyval(Fit,In.axes{1}.data);
    Slope_Std=std(In.data'-YFit)/(max(In.axes{1}.data)-min(In.axes{1}.data));
   return 
end
Slope=In.slice_index(1,1)*nan;

Slope_Std=In.slice_index(1,1)*nan;

for i=1:numel(Slope.data)
    Fit=polyfit(In.axes{1}.data,In.data(:,i)',1);
    Slope.data(i)=Fit(1);
    YFit=polyval(Fit,In.axes{1}.data);
    Slope_Std.data(i)=std(In.data(:,i)'-YFit)/(max(In.axes{1}.data)-min(In.axes{1}.data));
end
Slope.name=[In.name,'@ Slope'];
Slope_Std.name=[In.name,'@ Slope_Std'];

end