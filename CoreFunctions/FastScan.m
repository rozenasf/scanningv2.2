function out=FastScan(Set,Get)
    global S;
    out=nan;
    %Check that all values will be in range
    for j=1:numel(Set{2})
        for i=1:numel(Set)/2
            if(~InChannelRange(Set{2*i-1},Set{2*i}(j)))
                error('Value will be out of range!');
            end
        end
    end
    
    %Check that tere will be no jumps
    for i=1:numel(Set)/2
        if(~CheckStartingTolerance(Set{2*i-1},Set{2*i}(1)))
            error('Value will is out of starting tolerance!');
        end
    end
    
    %Run the "Fast Scan"
    for j=1:numel(Set{2})
        for i=1:numel(Set)/2
            S.Set(Set{2*i-1},Set{2*i}(j));
        end
        S.Get(Get);
    end
end
%FastScan({'BG',linspace(0,0.1,5)},{'WorkPoint','Temperature','TuneSignals','Rgr'})