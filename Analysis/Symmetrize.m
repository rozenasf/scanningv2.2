function Out=Symmetrize(In,Type,Dim)

if(~exist('Dim'));Dim=1;end
    Out=In*0;
    if(size(In.data,1)==1)
        Transpose=1;
    else
        Transpose=0;
    end
    
    if(Transpose)
        Data=In.data';
    else
        Data=In.data;
    end
    
    switch Type(1)
        case {'S','s'}
            Out.name=[In.name,'_S'];
            Data2=(Data+flip(Data,Dim))/2;
        case {'A','a'}
            Out.name=[In.name,'_A'];
            Data2=(Data-flip(Data,Dim))/2;
        otherwise
            error('Ask Debarghya whats going on...');
    end
    
    if(Transpose)
        Out.data=Data2';
    else
        Out.data=Data2;
    end
    assignin('base',Out.name,Out);
end