%linspace(-0.15,7.05,10)
figure(1);clf
figure(2);clf
figure(3);clf
for k=912:913
LoadScan(k,L)
%% 
Vsd=10e-3;
Vbg=BG;

Alpha=1;

W=4e-6;
e=1.6e-19;
VDirac=-4.47;
CapacitiveBgFactor=5.946e14;
hbar=6.62e-34/(2*pi);

n=CapacitiveBgFactor*(Vbg-VDirac);
%%
    Idata('Phi',0.5+0.5*(Beta_HS14_Hset-Beta_HS6_Hset)./(Beta_HS14_Hset+Beta_HS6_Hset));
    Idata('I',(Ggr6/VSenseGain/Info.Scaling.Ggr6)/RSense);
    n= Vbg_to_n(BG, v_dirac);
    k_F_vec = sqrt(pi*abs(n));
%%
figure(1);
    I1=mean(I,1);
    Idata('V',Phi*Vsd);
plot_with_color(V,1)
hold on
%%
    DPhi_DXs=I1*0;
    for i=1:size(Phi.data,2)
       F=polyfit(Phi.axes{1}.data',Phi.data(:,i),1);
       DPhi_DXs.data(i)=F(1);
    end
    %%
    RhoXX=DPhi_DXs*Vsd*W./(I1);
   n= Vbg_to_n(BG, v_dirac)
    l=1e6*( h_./(2*qe_^2*k_F_vec.*RhoXX));
%%
figure(2);
 plot(n/1e4,l.data,'o--');xlabel('n [1/cm^2]');ylabel('l [um]');

hold on
%%
figure(3)
plot(n/1e4,1./RhoXX.data,'o--');xlabel('n [1/cm^2]');ylabel('Conductivity [1/Ohm]');
hold on
end
figure(4)
plot(2*pi./sqrt(pi*n)*1e9,l.data,'o--');xlabel('\lambda [nm]');ylabel('l [um]');
hold on
plot([1,1]*200/3,[3,7],'m')
plot([1,1]*200/4,[3,7],'m')
plot([1,1]*200/5,[3,7],'m')
plot([1,1]*200/2,[3,7],'m')
plot([1,1]*200/2.5,[3,7],'g')
plot([1,1]*200/3.5,[3,7],'g')
plot([1,1]*200/4.5,[3,7],'g')
hold off