function CheckSetRangeBeforeScan()
    global Info
    ToCheck={};
    if(isfield(Info.Scan,'Start'));ToCheck=[ToCheck,Info.Scan.Start];end
    if(isfield(Info.Scan,'End'));ToCheck=[ToCheck,Info.Scan.End];end
    if(isfield(Info.Scan,'Axis'));ToCheck=[ToCheck,Info.Scan.Axis];end
    for Index=1:numel(ToCheck)
        names=fieldnames(ToCheck{Index});
        for i=1:numel(names)
            VectorToCheck=ToCheck{Index}.(names{i});
            for j=1:numel(VectorToCheck)
                if(~InChannelRange(names{i},VectorToCheck(j)))
                    error('The scan will set a channel out of range %s = %e\n Range IS: %s',...
                        names{i},VectorToCheck(j),SprintfA(Info.Range.(names{i})));
                end
            end
        end
    end
end