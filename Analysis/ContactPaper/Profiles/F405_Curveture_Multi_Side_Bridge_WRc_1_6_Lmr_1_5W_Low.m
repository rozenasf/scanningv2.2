%Scans [1005.01:0.01:1005.06]
figure(405);clf
R={};
SmoothParameter=1;
for k=0:2
    for i=1:6
        UnPackScan(Scans.XY_bridge_lower_excitation_Full{i+6*k});Beta_Sum=0.51;
        if(k==0)
            R{i} = CenterIDataAxis(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15,[1,2],1e6);
        else
            R{i} = R{i}+CenterIDataAxis(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15,[1,2],1e6);
        end
    end
end
for i=1:6
   R{i}=R{i}/3;
           for j=1:size(R{i},1)
            R{i}.data(j,:)=smooth(R{i}.data(j,:),SmoothParameter);
        end
        for j=1:size(R{i},2)
            R{i}.data(:,j)=smooth(R{i}.data(:,j),SmoothParameter);
        end
end
%

Traces={};

subplot(2,2,1)
Y=R{1}.axes{1}.data ;
Y=(Y(1:end-1)+Y(2:end))/2;
%
for i=1:3
    Traces{i}=-diff(R{2*i-1}.data-R{2*i}.data);
     Traces{i}=mean(Traces{i},2);
     plot(Y,Traces{i},'.');hold on
end
%
ylim([0,3.5])
%
Boundaries=[-0.5,0.5];ErfPar=4;
R_Mask=(1+erf(ErfPar*(Y+Boundaries(2))));
M_Mask=(erf(ErfPar*(Y+Boundaries(2)))-erf(ErfPar*(Y+Boundaries(1))));
L_Mask=(1-erf(ErfPar*(Y+Boundaries(1))));
Total_Mask=R_Mask+M_Mask+L_Mask;
% plot(Y,R_Mask*1.7) %Right bridge
% plot(Y,M_Mask*1.7) %Left bridge
% plot(Y,L_Mask*1.7) %Mid bridge
ylim([0,3.5])
%
subplot(2,2,2)
Profile=((R_Mask'.*(Traces{1})+L_Mask'.*(Traces{3})+M_Mask'.*(Traces{2})))./(Total_Mask');

%plot(Y,(Profile+flipud(Profile))/2,'.','markersize',10)
 plot(Y,(Profile),'.','markersize',10)
Fit_Line(2,'X',[-1.5,1.5]);
Curveture=-[Fit.a]./[Fit.Y_0]*(5)^2
TXYL(sprintf('Ey BG=-2, W/Rc=1.6 DSB -a/c*W^2=%0.2f ',Curveture),'Y','R')
%TXYL('Ey BG=-2, W/Rc=1.6 DSB -a/c*W^2=0.84 ','Y','R')
ylim([0,3.5])
 Export.Figure3.Panel4.Curve1=[Y;Profile'];