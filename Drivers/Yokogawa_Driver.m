classdef Yokogawa_Driver < handle
    properties
        Range
        Communication
    end
    methods
        function obj=Yokogawa_Driver(varargin)
           %vendor,GPIB no, Address (example: 'ni',0,2)
           obj.Communication=gpib(varargin{1},varargin{2},varargin{3});
           fopen(obj.Communication);
           disp('Yokogawa connected!');
        end
        %-----
        function Set(obj,Channel,Value)
                    fprintf(obj.Communication, 'S%f;E',Value);
        end
        function set.Range(obj,R)
                    LR=log10(R);
                    if(LR~=round(LR) || LR<-2 || LR>2)
                       error('range input must devide by 10 and between 0.001 and 100');
                    end
                    fprintf(obj.Communication, 'F1;R%d;E',LR+4);
                    obj.Range=R;
        end
        function delete(obj)
            fclose(obj.Communication);
            try delete(obj);end
            disp('Yokogawa removed');
        end
    end
end