function FigSend(FigNumbers,Title)
global Info L
if(isempty(FigNumbers))
   q=gcf;FigNumbers=q.Number;
end

handles=findall(0,'type','figure');
Numbers=[handles.Number];
%if(~exist('FigNumbers'));FigNumbers=Numbers(1:find(Numbers==999)-1);end
handles(find(ismember(Numbers,FigNumbers)))
savefig(handles(find(ismember(Numbers,FigNumbers))),[pwd,'\',Title],'compact');
end