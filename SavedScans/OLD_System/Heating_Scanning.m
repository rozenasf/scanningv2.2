A=Info.A;B=Info.B;
%% before burning
%Clist=[5,9];
% Info.RelaysState=[0,0,1,1,1,0,1];
% Info=B.SetMatrixLockinRelay(Info,struct('Matrix',[2,1;Clist],'Relay',Info.RelaysState,'Lockin',Info.Lockin.Ground,'Force',1));
Info.Keithley.Force=1;
Info.Keithley.DontUpdateVoltage=0;
Info.Keithley.Compliance=1e-6;
Info.Keithley.Range_M=1e-5;
Info.Keithley.Range_S=10;
Info.Keithley.V=0;
Info.Keithley.Serial=1;
%Info=SetParameters(Info);
Info=Keithley(Info);
DocSend('Prepare for heating');
%%
Data.VIn={};Data.VOut={};Data.IOut={};Data.ROut={};Data.RIn={};Line=1;h=[];hCompliance=[];hRMAX=[];
Data.Vindividual1={};Data.Vindividual1={};

%%
Power=3;
%Info.Keithley.Compliance=30e-3;
%Vlist=linspace(0.111,17.55,100);Pause=0.0;
%AveragingWindow=
Info.Keithley.Compliance=Cur;
%Vlist=linspace(6.511,35.55,100);
Pause=0.0;

%RMAX=17.5*1.02; %in ohm, 4  probe
%RMAX=24; %in ohm, 4 probe
%RMAX=36; %in ohm, 4 probe
RMAX=Res; %in ohm, 4 probe
fprintf(Info.Keithley.Serial,'SENS:CURR:PROT %E\n',Info.Keithley.Compliance);
figure(2);

%--%
subplot(2,3,1)
for i=1:Line-1
    plot(Data.IOut{i},Data.VOut{i});hold on
end
h{1}=plot(1,1,'linewidth',5);hold on
hCompliance{1}=plot(1,1,'--');
xlabel('Currnet(A)');ylabel('Voltage mes(V)');title(['4 probe IV diff']);
set(gca,'fontsize',14)
hold off

%--%
subplot(2,3,2)
for i=1:Line-1
    plot(Data.IOut{i},Data.ROut{i});hold on
end
h{2}=plot(1,1,'linewidth',5);hold on
hCompliance{2}=plot(1,1,'--');
hRMAX{1}=plot(1,1,'--r','linewidth',3);
xlabel('Currnet(A)');ylabel('4 - Resistance(Ohm)');title(['4 probe Resistance']);
set(gca,'fontsize',14)
hold off
%--%

%--%
subplot(2,3,3)
for i=1:Line-1
    plot(Data.IOut{i},Data.VIn{i});hold on
end
h{3}=plot(1,1,'linewidth',5);hold on
hCompliance{3}=plot(1,1,'--');
xlabel('Currnet(A)');ylabel('Voltage In(V)');title(['2 probe IV']);
set(gca,'fontsize',14)
hold off

%--%
subplot(2,3,4)
for i=1:Line-1
    plot(Data.IOut{i},Data.RIn{i});hold on
end
h{4}=plot(1,1,'linewidth',5);hold on
hCompliance{4}=plot(1,1,'--');
xlabel('Currnet(A)');ylabel('2 - Resistance(Ohm)');title(['2 probe Resistance']);
set(gca,'fontsize',14)
hold off
%--%

%--%

subplot(2,3,5)
for i=1:Line-1
    plot(Data.IOut{i}.^Power,Data.ROut{i});hold on
end
h{5}=plot(1,1,'linewidth',5);hold on
hCompliance{5}=plot(1,1,'--');
hRMAX{2}=plot(1,1,'--r','linewidth',3);
xlabel(['Currnet(A) Power ',num2str(Power)]);ylabel(['4 - Resistance(Ohm .^)']);title(['4 probe Resistance power of ',num2str(Power)]);
set(gca,'fontsize',14)
hold off
%--%

subplot(2,3,6)
for i=1:Line-1
    plot(Data.IOut{i},Data.Vindividual2{i});hold on
    plot(Data.IOut{i},Data.Vindividual1{i});hold on
end
h{6}=plot(1,1,'linewidth',5);hold on
h{7}=plot(1,1,'linewidth',5);hold on
xlabel('Currnet(A)');ylabel('Voltage mes(V)');title(['4 probe IV absulute']);
set(gca,'fontsize',14)
hold off
%--%

Data.VIn{Line}=[];Data.VOut{Line}=[];Data.IOut{Line}=[];Data.ROut{Line}=[];Data.RIn{Line}=[];
Data.Vindividual1{Line}=[];Data.Vindividual2{Line}=[];

[V,I,R,Vindividual]=GetVI(Vlist(1),Info.Keithley.Serial,Info);
pause(2);
for i=1:numel(Vlist)
    
    [V,I,R,Vindividual]=GetVI(Vlist(i),Info.Keithley.Serial,Info,1);
    Data.VOut{Line}(end+1)=V;
    Data.IOut{Line}(end+1)=I;
    Data.ROut{Line}(end+1)=R;
    Data.RIn{Line}(end+1)=Vlist(i)/I;
    Data.VIn{Line}(end+1)=Vlist(i);
    Data.Vindividual1{Line}(end+1)=Vindividual(1);
    Data.Vindividual2{Line}(end+1)=Vindividual(2);
    
    h{1}.XData =Data.IOut{Line};
    h{1}.YData =Data.VOut{Line};
    
    h{2}.XData =Data.IOut{Line};
    h{2}.YData =Data.ROut{Line};
    
    h{3}.XData =Data.IOut{Line};
    h{3}.YData =Data.VIn{Line};
    
    h{4}.XData =Data.IOut{Line};
    h{4}.YData =Data.RIn{Line};
    
    h{5}.XData =Data.IOut{Line}.^Power;
    h{5}.YData =Data.ROut{Line};
    
    h{6}.XData =Data.IOut{Line};
    h{6}.YData =Data.Vindividual1{Line};
    
    h{7}.XData =Data.IOut{Line};
    h{7}.YData =Data.Vindividual2{Line};
    
    [V,I,R,Vindividual]
    for j=1:4
        hCompliance{j}.XData=[Info.Keithley.Compliance,Info.Keithley.Compliance];
        hCompliance{j}.YData=[min(Data.VOut{Line}),max(Data.VOut{Line})];
    end
    hCompliance{5}.XData=[Info.Keithley.Compliance,Info.Keithley.Compliance].^Power;
    hCompliance{5}.YData=[min(Data.VOut{Line}),max(Data.VOut{Line})];
    hRMAX{1}.XData=[min(Data.IOut{Line}),max(Data.IOut{Line})];
    hRMAX{1}.YData=[RMAX,RMAX];
    hRMAX{2}.XData=[min(Data.IOut{Line}),max(Data.IOut{Line})].^Power;
    hRMAX{2}.YData=[RMAX,RMAX];
    for j=[1,3,4]
        subplot(2,3,j);
        ylim([min(h{j}.YData),1.05*max(h{j}.YData)])
    end
    for j=[2,5]
        subplot(2,3,j);
        ylim([min(h{j}.YData),1.05*RMAX]);
    end
    if(max(abs(Data.ROut{Line}))>=RMAX)
        disp('max R reached');
        break;
    end
    
    pause(Pause)
    
end
[V,I,R,Vindividual]=GetVI(0,Info.Keithley.Serial,Info);
Line=Line+1;

%save([Info.Doc.File{1},'_heating.mat'],'Data')

DocSend('Heating contacts:');DocSend(2)
%% before mating
Info.Keithley.Force=1;
Info.Keithley.DontUpdateVoltage=0;
Info.Keithley.Compliance=0.5e-6;
Info.Keithley.Range_M=1e-7;
Info.Keithley.Range_S=1;
Info.Keithley.V=0;

%Info=SetParameters(Info);
Info=Keithley(Info);
Info=A.Capacitance(Info);
DocSend('Changed to capacitance after heating');