%% Lmr as function of BG:

figure(204);clf
List=405:411;
W_RcList=[4,-4,2,-2,1,-1,0];
Phi={};
for i=1:numel(List)
    LoadScan(List(i))
    Phi{i}=(Beta_HS15_Hset-Beta_HS2_Hset)./(2*(Beta_HS15_Hset+Beta_HS2_Hset))+0.5;
end
%

Symmetrized={};
Symmetrized{1}=Phi{7};
Symmetrized{2}=(Phi{5}+Phi{6})/2;
Symmetrized{3}=(Phi{4}+Phi{3})/2;
Symmetrized{4}=(Phi{2}+Phi{1})/2;
%
Slopes=[];RhoXX=[];
for i=1:4
set(gca, 'ColorOrder', hsv(21), 'NextPlot', 'replacechildren');
plot(Symmetrized{i},'linewidth',1);
RhoXX(i,:)=Fit_Line(1,'X',[-2.5e-5,-1.6e-5],'a')*Info.Channels.AVS15./IgrAC15.mean(1).data;
end
% 1/RhoXX
for i=1:4
% plot(RampBG,h_./(8*qe_^2*sqrt(pi*abs(Vbg_to_n(RampBG',0.7))))./(RhoXX(1,:)*5e-6)','o-','linewidth',3)
plot(Vbg_to_n(RampBG',0.7)*1e-4,1./(RhoXX(i,:)*5e-6)','o-','linewidth',3)
hold on
end
hold off
TXYL('scans 405:411,$\frac{1}{\rho_{xx}}$','n [$\frac{1}{cm}$]','$\frac{1}{\rho_{xx}} [\frac{1}{Ohm}]$',...
    '$\\frac{w}{R_c}=%d$',{[0,1,2,4]})
% title('scans 405:411,$l_{mr}=\frac{h}{2e^2\rho_{xx}\sqrt{\pi n}}$','Interpreter','latex','fontsize',24)
set(gca,'FontSize',24)
xlim(Vbg_to_n([-9.8,9.2],0.7)*1e-4);
% Lmr:
for i=1:4
% plot(RampBG,h_./(8*qe_^2*sqrt(pi*abs(Vbg_to_n(RampBG',0.7))))./(RhoXX(1,:)*5e-6)','o-','linewidth',3)
plot(RampBG',1e6*h_./(2*qe_^2*sqrt(pi*abs(Vbg_to_n(RampBG',0.7))))./(RhoXX(i,:)*5e-6)','o-','linewidth',3)
hold on
end
hold off
TXYL('$ $scans 405:411,$l_{mr}=\frac{h}{2e^2\rho_{xx}\sqrt{\pi n}}$','$ $BG [V]','$l_{mr} [\mu m]$',...
    '$\\frac{w}{R_c}=%d$',{[0,1,2,4]})
% title('scans 405:411,$l_{mr}=\frac{h}{2e^2\rho_{xx}\sqrt{\pi n}}$','Interpreter','latex','fontsize',24)
set(gca,'FontSize',24)

% xlim(Vbg_to_n([-9.8,9.2],0.7)*1e-4);
