
for k=1:2
figure(9+k);clf;colormap(jet)
ScanNameList={'XY_BG_Electrons','XY_BG_Holes'};
W_RcList=[1,2,4,6];
Beta_Sum = 0.51;
for J=1:k+2
    try
     subplot(2,2,J);cla
    ScanName=ScanNameList{k};
    UnPackScan(Scans.(ScanName){J*2});Beta_Sum=0.51;
    R1 = CenterIDataAxis(BreakIData(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15),[1,2],1e6);
    UnPackScan(Scans.(ScanName){J*2+1});Beta_Sum=0.51;
    R2 = CenterIDataAxis(BreakIData(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15),[1,2],1e6);
    Data=R1-R2;Data=Data.roi(1,-7.0,7.0);Data=Data.roi(2,-2.5,2.5)
    for i=1:size(Data,1)
       Data.data(i,:)=smooth( Data.data(i,:),3);
    end
    for i=1:size(Data,2)
       Data.data(:,i)=smooth( Data.data(:,i),3);
    end
Sign=sign(mean(mean(diff(Data.data'),1),1));
    surf(Sign.*diff(Data.data'));
TXYL(['W/Rc=',num2str(W_RcList(J))],'X[um]','Y[um]')
Ax=gca;Ax.DataAspectRatio(1)=1;Ax.DataAspectRatio(2)=Ax.DataAspectRatio(1);
    end
    suptitle(ScanNameList{k}(7:end))
end
end