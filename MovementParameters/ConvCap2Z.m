function out=ConvCap2Z()
error('please do not use this function. use the new driver instead, ConvCap2Z_Driver');
% global Info S;
% out=1;
% FitDataY=Info.Drivers.ZsCapFitData.FitDataY;
% FitDataX=Info.Drivers.ZsCapFitData.FitDataX;
% if(Info.Flags.RunningScan)
%     S.Set( 'ZsCapXum' ,interp1(FitDataX(:,2),FitDataX(:,1),GetFromMemory('ZsCapX')));
%     S.Set( 'ZsCapYum' ,interp1(FitDataY(:,2),FitDataY(:,1),GetFromMemory('ZsCapY')));
% else
%     if(~IsIdgr('ZsCapXum'));S.CreateScanOutput('ZsCapXum');end
%     if(~IsIdgr('ZsCapYum'));S.CreateScanOutput('ZsCapYum');end
%     Info.Scan.idgr.ZsCapXum.data=interp1(FitDataX(:,2),FitDataX(:,1),Info.Scan.idgr.ZsCapX.data);
%     Info.Scan.idgr.ZsCapYum.data=interp1(FitDataY(:,2),FitDataY(:,1),Info.Scan.idgr.ZsCapY.data);
% end
end