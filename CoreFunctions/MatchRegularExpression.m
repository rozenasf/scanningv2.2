function out=MatchRegularExpression(in,patterns)
%    ToMeasure={'(?!.*Err)Beta_.*','Idc.'};
%fieldnames(Info.Scan.Data)
    out=[];
    if(~iscell(patterns));patterns={patterns};end
    for i=1:numel(patterns)
    pattern=patterns{i};
        a=regexp(in,['^',pattern,'$'],'match');out=[out,a{:}];
    end
end