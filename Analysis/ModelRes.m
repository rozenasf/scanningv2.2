function out=ModelRes(x,a)
out=x*0;
for i=1:numel(x)
if(x(i)>=a(1))
    out(i)=a(2).*x(i)+a(3);
else
    out(i)=a(4).*(x(i)-a(1))+a(2).*a(1)+a(3);
end
end
end