%
LoadScan([2372],L)
Pairs=[2,3;4,5;6,7];
figure(1);clf
for j=1:3
    subplot(2,2,j);
Beta_Sum=0.307
Phi2=Beta_HS15_Hset/Beta_Sum.*AVS15_e./(AVS15_e-AVS2_e)-AVS2_e./(AVS15_e-AVS2_e); Phi.name='Phi';
Phi=Phi2.slice_index_range(1,[1:48]);
Phi_A=(Phi.slice_index(3,Pairs(j,1))-Phi.slice_index(3,Pairs(j,2)))/2;
Phi_S=(Phi.slice_index(3,Pairs(j,1))+Phi.slice_index(3,Pairs(j,2)))/2;
%Phi_A=Phi_A.slice_index_range(1,[2:51]);
%Phi_S=Phi_S.slice_index_range(1,[2:51]);
%Phi_S=Phi_S.roi_index(1,[2,51]);
%Phi_S=Phi.slice_index(3,2);
N=1;
[X,Y]=meshgrid(linspace(-3,3,N),linspace(-3,3,N));
contour(Phi_A,10,'linewidth',5);hold on%
%caxis([min(min(Phi_A.data)),max(max(Phi_A.data))])
contour(Phi_S,30,'linewidth',3);hold on
caxis([min(min(Phi_S.data)),max(max(Phi_S.data))])

colormap(jet)
%hold off
axis equal
title(sprintf('Phi @ Magnet=+-%d mT , Scan 2367',Magnet_I(Pairs(j,1))*120/10)) 
AddGDS(GDS,[1,0,0,1,-3,-4.3]*1e-6,0); %for movving from negative to positive X

%AddTitle
end
%TelegramSend(999)
title('Potential and streamlines, BG=8V, 120mT');
%%
subplot(2,2,4)

%caxis([min(min(Phi_A.data)),max(max(Phi_A.data))])
contour(Phi.slice_index(3,1),30,'linewidth',3);
title(sprintf('Phi @ Magnet=+-%d mT , Scan 2367',0)) 
AddGDS(GDS,[1,0,0,1,-3,-4.3]*1e-6,0); %for movving from negative to positive X

%%
figure(2);clf
Phi.slice_index(3,1).pcolor;colormap(jet)
%caxis([0.7602,0.8286]);
axis equal
AddGDS(GDS,[1,0,0,1,-3,-4.3]*1e-6,0);
title(sprintf('Phi @ Magnet = 0, Scan 2366',Magnet_I(Pairs(j,1))*120/10))