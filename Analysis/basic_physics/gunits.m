% GUNITS make all units defined by the units program global variable in the matlab enviorment so
% you dont have to define them one by one

%mass
global gm_ kg_ me_;
%length
global a0_ angst_ km_ m_ cm_ mm_ um_ nm_;
%time
global sec_ msec_ usec_ nsec_ min_ hour_;
%energy
global erg_ joule_ Ryd_ mRyd_ eV_ meV_ ueV_ keV_ MeV_;
%Pressure
global torr_ mtorr_ Pa_ GPa_ atm_ bar_ kbar_ Mbar_;
%Power
global W_ mW_ uW_ kW_ MW_;
%Force
global dyn_ newton_;
%Volatge
global coul_ Volt_ mVolt_ uVolt_ nVolt_;
%Current
global A_ mA_ uA_ nA_ pA_;
%Resistance
global Ohm_ mOhm_ kOhm_ MOhm_ GOhm_;
%Magnetic Field
global  Tesla_ Gauss_;
%Frequency
global Hz_ kHz_ MHz_ GHz_ THz_;
%Temprature
global K_ mK_;

%Voltage
global V_ mV_ uV_;
%Current
global A_ mA_ uA_ nA_ pA_;
%Resistance
global Ohm_ kOhm_ MOhm_ GOhm_;
%Capacitance
global F_ mF_ uF_ nF_ pF_ fF_ aF_;
%Inductance
global H_ mH_;
%Magnetic Field
global Tesla_ Gauss_;
%Capacitance
global F_ mF_ uF_ nF_ pF_ fF_ aF_;
%Inductance
global Henry_ mHenry_;

%Others
global mole_;

%Physical constants
global h_ hbar_ c_ qe_ me_ mp_ mn_ N0_ Kb_ Rgas_;
global a0_ e0_ eta0_ sig_ G_ ue_ up_ ub_ alpha_;
global Vf_;

%2DEG constants
global me_GaAs_ mlh_GaAs_ mhh_GaAs_ e_GaAs_ DOSe_ DOSlh_ DOShh_;

