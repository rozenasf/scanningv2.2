function output=FilterScanDescription(id,input)
    output=[];
    for i=1:numel(id)
       if((~isempty(id{i})) && isfield(id{i},'Description') && ~isempty(regexp(id{i}.Description,input)))
           disp(id{i}.Description);
          output=[output,i]; 
       end
    end
end