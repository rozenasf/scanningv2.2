classdef Equations_Driver < handle
    methods
        function out=Get(~,Channel)
            global Info S ;
            RSence=120;
            %out=Info.Equations.(Channel)();
            switch Channel
                case 'InverseBetaSum'
                    out=1/GetFromMemory('Beta_Sum');
                case 'IgrAC15'
                    out=-GetFromMemory('Ggr15')/1e3/Info.Scaling.Ggr15/RSence;
                case 'IgrAC2'
                    out=-GetFromMemory('Ggr2')/1e3/Info.Scaling.Ggr2/RSence;
                case 'Rgr15'
                    if(Info.Dac.DispExs.VS2.Frequency==Info.Dac.DispExs.VS15.Frequency)
                        out=(-1/Info.Scaling.Ggr2+1/Info.Scaling.Ggr15)/Info.Channels.IgrAC15-RSence;
                    else
                        out=abs(Info.Dac.DispExs.VS15.Amp/Info.Channels.IgrAC15)-120;
                    end
                case 'Rgr2'
                    if(Info.Dac.DispExs.VS2.Frequency==Info.Dac.DispExs.VS15.Frequency)
                        out=(-1/Info.Scaling.Ggr2+1/Info.Scaling.Ggr15)/Info.Channels.IgrAC2-RSence;
                    else
                        out=abs(Info.Dac.DispExs.VS2.Amp/Info.Channels.IgrAC2)-120;
                    end
                case 'UnScaled_Beta'
                    out=GetFromScan('Beta_HS15_Hset',Info.Scan.Runtime.Position)/Info.Scaling.HS15;
                case 'RhoXX'
                    Beta_Sum=Info.Parameters.Beta_Sum;W=Info.Parameters.W;
                    Slope=FitData('Xs','UnScaled_Beta',1,Info.Scan.Runtime.Position);
                    AverageI=mean(Info.Scan.idgr.IgrAC15.data);
                    out=Slope/Beta_Sum/AverageI(ReducedPositionIndex(1))*W;
                case 'n'
                    VDirac=Info.Parameters.VDirac;
                    CapacitiveBgFactor=5.946e14;
                    out=CapacitiveBgFactor*(GetFromMemory('BG')-VDirac);
                case 'l'
                    h_=6.6261e-34;
                    qe_=1.6022e-19;
                    k_F_vec = sqrt(pi*abs(GetFromMemory('n')));
                    out=( h_./(2*qe_^2*k_F_vec.*GetFromMemory('RhoXX')));
                case 'Write1'
                    out=1;
                    fprintf('1(%d)(%d,%d) ',Info.Scan.Runtime.GetDim,Info.Scan.Runtime.DimEnd,Info.Scan.Runtime.DimStart)
                case 'Write2'
                    out=1;
                    fprintf('2(%d)(%d,%d) ',Info.Scan.Runtime.GetDim,Info.Scan.Runtime.DimEnd,Info.Scan.Runtime.DimStart)
                case 'Write3'
                    out=1;
                    fprintf('3(%d)(%d,%d) ',Info.Scan.Runtime.GetDim,Info.Scan.Runtime.DimEnd,Info.Scan.Runtime.DimStart)
                case 'Write4'
                    out=1;
                    fprintf('4(%d)(%d,%d) ',Info.Scan.Runtime.GetDim,Info.Scan.Runtime.DimEnd,Info.Scan.Runtime.DimStart)
                case 'Bridge_Calibration'
%                     InitialGuess=[0.5,6];
                    %InitialGuess=[0.21,4.9];
                    %                     InitialGuess=[0.25,4];
                    if(GetFromMemory('FVS15')==GetFromMemory('FVS2'))
                        InitialGuess=[0.25,4];
                        Span=diff(InitialGuess);
                        BridgeCalibratedAtRatio=mean(InitialGuess);
                        
                        for k=1:7 %was 6
                            Vec=linspace(min([BridgeCalibratedAtRatio+Span/2,InitialGuess(2)]),max([BridgeCalibratedAtRatio-Span/2,InitialGuess(1)]),6);
                            List=[];
                            for i=1:numel(Vec)
                                Ratio=Vec(i);
                                S.Set('Ratio',Ratio);
                                
                                S.Get({'Feedback','TuneSignals','Beta'});
                                List=[List;Ratio,GetFromMemory('Beta_HS15_Hset')];
                                disp(List(end,:));
                            end
                            fit=polyfit(List(:,1),List(:,2),1);
                            BridgeCalibratedAtRatio=roots(fit)
                            if(BridgeCalibratedAtRatio>max(InitialGuess) || BridgeCalibratedAtRatio<min(InitialGuess))
                                TelegramSend('Cant Calibrate');error('Wrong initial gauss!');
                            end
                            Span=Span/2;
                        end
                        
                        Ratio=BridgeCalibratedAtRatio;
                        S.Set('Ratio',Ratio);
                        S.Get({'Feedback','TuneSignals','Beta'});
                        
                        out=BridgeCalibratedAtRatio;
                    else
                        out=nan;
                        disp('bridge will not calibrate it FVS15 and FVS2 are not equal');
                    end
case 'Weight_Bridge_Calibration'
%                     InitialGuess=[0.5,6];
                    %InitialGuess=[0.21,4.9];
                    %                     InitialGuess=[0.25,4];
                    if(GetFromMemory('FVS15')==GetFromMemory('FVS2'))
                        InitialGuess=[0.1,0.9];
                        Span=diff(InitialGuess);
                        BridgeCalibratedAtWeight=mean(InitialGuess);

                        for k=1:2 %was 6
                            Vec=linspace(min([BridgeCalibratedAtWeight+Span,InitialGuess(2)]),max([BridgeCalibratedAtWeight-Span,InitialGuess(1)]),6);
                            List=[];
                            for i=1:numel(Vec)
                                S.Set('Weight',Vec(i));
                                
                                S.Get({'Feedback','TuneSignals','Beta'});
                                List=[List;Vec(i),GetFromMemory('Beta_HS15_Hset')];
                                fprintf('%0.5e,',List(end,:));fprintf('\n')
                            end
                            BridgeCalibratedAtWeight=roots(polyfit(List(:,1),List(:,2).*List(:,1),1));
                            if(BridgeCalibratedAtWeight>max(InitialGuess) || BridgeCalibratedAtWeight<min(InitialGuess))
                                TelegramSend('Cant Calibrate');error('Wrong initial gauss!');
                            end
                            Span=Span/4;
                        end
                        
                        out=BridgeCalibratedAtWeight;
                        S.Set('Weight',out);

                    else
                        out=nan;
                        disp('bridge will not calibrate it FVS15 and FVS2 are not equal');
                    end      
                    
                case 'Weight_Bridge_Calibration_SmallChange'

                    if(GetFromMemory('FVS15')==GetFromMemory('FVS2'))
                        %based on second stage of Weight bridge calibration
                        %span=0.2 (which is 0.8/4)
                        InitialGuess=[max([GetFromMemory('Weight')-0.1,0.1]),min([GetFromMemory('Weight')+0.1,0.9])];
                        Span=diff(InitialGuess);
                        BridgeCalibratedAtWeight=mean(InitialGuess);

                        for k=1:1
                            Vec=linspace(min([BridgeCalibratedAtWeight+Span,InitialGuess(2)]),max([BridgeCalibratedAtWeight-Span,InitialGuess(1)]),6);
                            List=[];
                            for i=1:numel(Vec)
                                S.Set('Weight',Vec(i));
                                
                                S.Get({'Feedback','TuneSignals','Beta'});
                                List=[List;Vec(i),GetFromMemory('Beta_HS15_Hset')];
                                fprintf('%0.5e,',List(end,:));fprintf('\n')
                            end
                            BridgeCalibratedAtWeight=roots(polyfit(List(:,1),List(:,2).*List(:,1),1));
                            if(BridgeCalibratedAtWeight>max(InitialGuess) || BridgeCalibratedAtWeight<min(InitialGuess))
                                TelegramSend('Cant Calibrate');error('Wrong initial gauss!');
                            end
                            Span=Span/4;
                        end
                        
                        out=BridgeCalibratedAtWeight;
                        S.Set('Weight',out);

                    else
                        out=nan;
                        disp('bridge will not calibrate it FVS15 and FVS2 are not equal');
                    end       
                    
                    
case 'Bridge_Calibration_SmallChange'
%                     InitialGuess=[0.5,6];
                    %InitialGuess=[0.21,4.9];
%                     InitialGuess=[0.25,4];
if(GetFromMemory('FVS15')==GetFromMemory('FVS2'))
 InitialGuess=GetFromMemory('Ratio').*[0.75,1.3];
                    Span=diff(InitialGuess);
                    BridgeCalibratedAtRatio=mean(InitialGuess);
                    
                    for k=1:1 %was 6
                        Vec=linspace(min([BridgeCalibratedAtRatio+Span/2,InitialGuess(2)]),max([BridgeCalibratedAtRatio-Span/2,InitialGuess(1)]),12);
                        List=[];
                        S.Get('Feedback');
                        for i=1:numel(Vec)
                            Ratio=Vec(i);
                            S.Set('Ratio',Ratio);
                            
                            S.Get({'TuneSignals','Beta'});
                            List=[List;Ratio,GetFromMemory('Beta_HS15_Hset')];
                            disp(List(end,:));
                        end
                        
                        RatioVoltage=(List(:,1)-1)./(List(:,1)+1);
                        fit=polyfit(RatioVoltage,List(:,2),1);
                        VoltageRoot=roots(fit);
                        BridgeCalibratedAtRatio=(VoltageRoot+1)./(1-VoltageRoot)
                       
                        if(BridgeCalibratedAtRatio>max(InitialGuess) || BridgeCalibratedAtRatio<min(InitialGuess))
                            error('Wrong initial gauss!');
                        end
                        Span=Span/2;
                    end
                    
                    Ratio=BridgeCalibratedAtRatio;
                    S.Set('Ratio',Ratio);
                    %S.Get({'Feedback','TuneSignals','Beta'});
                   
                    out=VoltageRoot;
else
    out=nan;
    disp('bridge will not calibrate it FVS15 and FVS2 are not equal');
end
                case 'ShiftToZero'
                    MaxShift=0.4e-3;
                    BetaSum=0.4;
%                     if(Info.Flags.RunningScan)
                        V=GetFromMemory('Beta_HS15_Hset')/BetaSum*GetFromMemory('AVS15');
%                     else
%                          V=GetFromMemory('Beta_HS15_Hset')/BetaSum*GetFromMemory('AVS15');
%                     end
                    CurrentShift=(GetFromMemory('AVS15')+GetFromMemory('AVS2'))/2;
                    if(abs(V)>MaxShift);error(['from ShiftToZero check MaxShift, V=',num2str(V)]);end
                    %V
                    out=V;
                    S.Set('Shift',CurrentShift-V);
                case 'SetToSumTarget'
                    out=GetFromMemory('SumTarget');
                    S.Set('Sum',out);
                case 'CenterWorkPoint'
                    ShiftFromValue=(GetFromMemory('WorkPoint')-GetFromMemory('FeedbackShift'));
                    if(abs(ShiftFromValue)<0.2)
                    S.Set('Vg_Ramp',GetFromMemory('Vg')+ShiftFromValue);
                    S.Set('WorkPoint',0);
                    else
                        error('ShiftFromValue is larger then 0.2 in CenterWorkPoint. the value: %e',ShiftFromValue);
                    end
                    S.Get('Feedback');
                    out=ShiftFromValue;
                case 'GetBridgeBetaSum'
                    pause(2);
                    S.Get('Feedback','TuneSignals','Beta');
                    S.Set('TempConf')
                    S.Get('BridgeBetaSum');
                    k=10;out=0;
                    for i=1:k
                        S.Get('Feedback','TuneSignals','Beta');
                        out=out+GetFromMemory('Beta_Sum');
                    end
                    out=1./(out/k);
                    S.Get('TempConf')
                    S.Get('Feedback','TuneSignals','Beta');
                case 'KeepWRc'
                    Wrc=GetFromMemory('WRc');
                    BG=GetFromMemory('BG');
                    qe_=1.6022e-19;
                    hbar_=1.0546e-34;
                    if(Wrc~=0)
                        r_c=5e-6./Wrc;
                        n=abs(5.946e14*(BG-0.284));
                        p_F=hbar_*sqrt(pi*n);
                        B=p_F./qe_./r_c;
                        Magnet_I=B./120e-3*10;
                    else
                        Magnet_I=0;
                    end
                    if(abs(Magnet_I)<10)
                        S.Set('Magnet_I',Magnet_I);
                    else
                        error(sprintf('Magnet_I = %0.2f',Magnet_I));
                    end
                    out=Magnet_I;
                    pause(0.5);
                case 'StartKeepingFeedback'
                    S.Set('KeepFeedback',1);
                    out=1;
                case 'StopKeepingFeedback'
                    S.Set('KeepFeedback',0);
                    out=0;
                case 'Doc_WorkPoint'
                    out=GetFromMemory('WorkPoint');
                    
                case 'TypeSpecificGet'
                    Sum=GetFromMemory('Sum');
                    
                    out=GetFromMemory('MeasurementType');
                    
                    switch (out)
                        case 1 %Regular
                            S.Get('Regular');
                            S.Set('Sum',Sum);
                            S.Set('AVRest',5e-3);
                            S.Get({{'Feedback','TuneSignals','Beta'},4});
                        case 2 %Bridge
                            S.Get('Bridge');
                            S.Set('Sum',Sum);
                            S.Set('AVRest',0.1e-3);
                            S.Get({{'Feedback','TuneSignals','Beta'},4});
                            S.Set('AVRest',5e-3);
                        case 3 %Dynamical bridge
                            S.Get('BridgeDynamic');
                            S.Set('Sum',Sum);
                            S.Set('AVRest',0.1e-3);
                            S.Get('Feedback');
                            S.Get('Weight_Bridge_Calibration_SmallChange');
                            S.Set('AVRest',5e-3);
                    end
                    
                    
                otherwise
                    error('unknown Equation channel (see Equations_Driver)');
            end
            
        end
        function Set(~,Channel,Value)
            global Info S;
            switch Channel
                case 'Ratio'
                    if(Value<=0);error('Ratio must be grater then 0');end
                    InitialValue1=GetFromMemory('AVS2');
                    InitialValue2=GetFromMemory('AVS15');
                    Sum=abs(InitialValue1)+abs(InitialValue2);
                    S.Set('AVS2' ,sign(InitialValue1)*Sum/(1+1/Value));
                    S.Set('AVS15',sign(InitialValue2)*Sum/(1+1/Value)/Value);
                case 'Sum'
                    if(Value<=0);error('Sum must be grater then 0');end
                    InitialValue1=GetFromMemory('AVS2');
                    InitialValue2=GetFromMemory('AVS15');
                    InitialSum=abs(InitialValue1)+abs(InitialValue2);
                    
                    S.Set('AVS2' ,InitialValue1*Value/InitialSum);
                    S.Set('AVS15',InitialValue2*Value/InitialSum);
                case 'Weight'
                    %Weight 1 is fully AVS15, Weight 0 is fully AVS2. AVS2
                    %is negative.
                    if(GetFromMemory('FVS2')~=GetFromMemory('FVS15'));error('Bridge functions work only in brdge mode!');end
                    if(Value<=0 || Value>=1);error('Ratio must be grater then 0');end
                    InitialValue1=GetFromMemory('AVS2');
                    InitialValue2=GetFromMemory('AVS15');
                    Sum=abs(InitialValue1)+abs(InitialValue2);
                    S.Set('AVS2' ,-Sum*(1-Value));
                    S.Set('AVS15',Sum*Value);
                case 'Common'
                    S.Set('AVS2' ,Value);
                    S.Set('AVS15',Value);
                case 'Shift'
                    
                    InitialValue1=GetFromMemory('AVS2');
                    InitialValue2=GetFromMemory('AVS15');
%                     InitialSum=abs(InitialValue1)+abs(InitialValue2);
%                     
%                     S.Set('AVS2' ,-InitialSum/2+Value);
%                     S.Set('AVS15',InitialSum/2+Value);
                    S.Set('AVS2' ,InitialValue1+Value);
                    S.Set('AVS15' ,InitialValue2+Value);
                case 'Xs_feedback'
                    MinStep=2e-6;
                    CurrentLocation=GetFromMemory('Xs');
                    if(abs(Value-CurrentLocation)<MinStep)
                        S.Set('Xs',Value);
                        S.Get({@()AvoidAreas({'WorkPoint'})});
                    else
    
                        number=ceil(abs(CurrentLocation-Value)/MinStep)+1;
                        FastScan({'Xs',linspace(CurrentLocation,Value,number)},{@()AvoidAreas({'WorkPoint'})})
                    end
                case 'RampBG'
                    MaxJump=0.6;
                    N=ceil(max([abs(Value-GetFromMemory('BG'))/MaxJump,1]));
                    FastScan({'BG',linspace(GetFromMemory('BG'),Value,N)},{'Feedback','CenterWorkPoint','Beta'});%,'TuneSignals'
                    %S.Get('TuneSignals');
                case 'RampYs'
                    if(~InChannelRange('Ys',Value));error('out of range RampYs');end
                    MaxJump=0.2e-6;
                    N=ceil(max([abs(Value-GetFromMemory('Ys'))/MaxJump,1]));
                    Vec=linspace(GetFromMemory('Ys'),Value,N);
                    for i=1:numel(Vec)
                        S.Set('Ys_Ramp',Vec(i));
                        pause(0.25);
                        S.Get('Feedback','TuneSignals','Beta','Temperature');
                        S.Get('CenterWorkPoint');
                    end
                case 'RampXs'
                    if(~InChannelRange('Xs',Value));error('out of range RampXs');end
                    MaxJump=0.5e-6;
                    N=ceil(max([abs(Value-GetFromMemory('Xs'))/MaxJump,1]));
                    Vec=linspace(GetFromMemory('Xs'),Value,N);
                    for i=1:numel(Vec)
                        S.Set('Xs_Ramp',Vec(i));
                        pause(0.25);
                        S.Get('Feedback','TuneSignals','Beta','Temperature');
                        S.Get('CenterWorkPoint');
                    end
                case 'SetGrapheneVsd'
                    if(GetFromMemory('FVS15')~=GetFromMemory('FVS2'));error('SetGrapheneVsd works only on bridge!');end;
                    Sum_For_R_Estimate=7.5e-3;
                    Sum_Overide_Target=20e-3;
                    Target_Sum_On_Graphene=Value;
                    S.Set('Sum',Sum_For_R_Estimate);
                    S.Get('Feedback','TuneSignals','Beta');
                    Rgr = Sum_For_R_Estimate/Info.Channels.IgrAC15 - 340;
                    Rtot = Sum_For_R_Estimate./Info.Channels.IgrAC15 ;
                    Vtarget = Target_Sum_On_Graphene;%Low is 4.3, High is 8
                    SetSum = abs(Vtarget.* Rtot./Rgr) ;
                    if(SetSum>Sum_Overide_Target)
                        SetSum=Sum_Overide_Target;
                        disp('SetGrapheneVsd set Vsd higher than 20mV!. setting to 20mV');
                    end
                    S.Set('Sum',SetSum);disp(SetSum);
                     S.Get('Feedback','TuneSignals','Beta');
                case 'DebugSet1'
                    fprintf('Set1 ');
                case 'DebugSet2'
                    fprintf('Set2 ');
                case 'DebugSet3'
                    fprintf('Set3 ');
                case 'Xs_Skip'
                      Xv=1e-6*[-22,-23.5,-24.5,-25.5,-26.5,-27,-30,-30,-22];
                      Yv=1e-6*[-2,-2.4,-2.8,-3.2,-3.6,-3.8,-3.8,-2,-2];
                      Ys=GetFromMemory('Ys');
                    if( inpolygon(Value,Ys,Xv,Yv) )
                         S.Set('SkipVariable',1);
                         fprintf('Xs=%0.2f [um],Ys=%0.2f [um]\n',Value*1e6,Ys*1e6);
                    else
                        S.Set('SkipVariable',0);
                        S.Set('Xs_Ramp',Value);
                    end
            end            
        end
        
    end
end
%32768