function idgr2=OrderIdgr(idgr)
    idgr2=idgr*1;
    for i=1:numel(idgr2.axes)
        [I,L]=sort(idgr2.axes{i}.data);
        idgr2.axes{i}.data=I;
        Vector=1:numel(idgr2.axes);
        Vector(1)=i;Vector(i)=1;
        Data=permute(idgr2.data,Vector);
        S=size(Data);
        L
        Data=reshape(Data(L,:),S);
        Data=ipermute(idgr2.data,Vector);
        idgr2.data=Data;
    end
    %[I,L]=sort(idgr.Beta_Hm_Hset.axes{1}.data);
end
%sort(idgr.Beta_Hbg_Hset.axes{1}.data
%idgr.Beta_Hbg_Hset.slice_index_range(1,))
%idgr.Beta_Hbg_Hset.slice_index_range(1,L).pcolor;colormap(jet)