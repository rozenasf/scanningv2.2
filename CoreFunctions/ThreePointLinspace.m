function out=ThreePointLinspace(a,b,c,N)
    out=[linspace(a,b,N/2),linspace(b,c,N),linspace(c,a,N/2)];
end