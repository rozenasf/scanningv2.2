function TelegramActive()
global Info;
 try
            e=TelegramGet();
            if(~isempty(e))
                if(isnumeric(e))
                    TelegramSend(e)
                else
                    switch e
                        case '?'
                            TelegramSend(sprintf('%d percent',round(Info.Scan.Runtime.PositionIndex/prod(Info.Scan.Runtime.MaxPosition)*100)))
                    end
                end
            end
        end
end