function Idata2=BreakIData(Idata1)
if(isa(Idata1,'idata'))
NewAxes=cell(1,numel(Idata1.axes));
for i=1:numel(Idata1.axes)
    NewAxes{i}=iaxis(Idata1.axes{i}.name,Idata1.axes{i}.data);
end
    Idata2=idata(Idata1.name,Idata1.data,NewAxes,{0},'');
else
    Idata2=Idata1;
end
end