
figure(412);
ScanList=1153:1160;clf%1154
Profile={};SmoothIndex=3;
BetaSumList=[];
for j=1:numel(ScanList)
    R={};RH={};
for i=1:4

   Data=LoadScan(ScanList(j)+0.01*i,L,'NoScale');Beta_Sum=Info.Scan.UserData.Initial_Beta_Sum;
    R{i} = (Data.Beta_HS15_Hset./Beta_Sum .*Data.AVS15./Data.IgrAC15);
    BetaSumList(end+1)=Info.Scan.UserData.Initial_Beta_Sum;
end
for i=1:4
    R{i}.axes{1}.data=R{i}.axes{1}.data*0.8075; %Yhe Y
    R{i}.axes{2}.data=R{i}.axes{2}.data*0.8385; %The X
end
EY={};
for i=1:2
    RH{i}=R{2*i-1}-R{2*i};
    EY{i}=diff(RH{i})/1e6;
%    plot(EY);hold on 
end

Boundaries=[-0.5,0.5];ErfPar=4;
Y=repmat(EY{1}.axes{1}.data',1,15);
Y=(Y-mean(mean(Y)))*1e6;
R_Mask=(1+erf(ErfPar*(Y+Boundaries(2))));
L_Mask=(1-erf(ErfPar*(Y+Boundaries(1))));
Total_Mask=R_Mask+L_Mask;
Profile{j}=((R_Mask.*EY{1}+L_Mask.*EY{2}))./(Total_Mask);
for i=1:size(Profile{j},1)
    Profile{j}.data(i,:)=smooth(Profile{j}.data(i,:),SmoothIndex);
end
for i=1:size(Profile{j},2)
    Profile{j}.data(:,i)=smooth(Profile{j}.data(:,i),SmoothIndex);
end
end
TotalProfile=Profile{1};
for i=2:numel(Profile)
 TotalProfile=TotalProfile+Profile{i};
end
figure(412);
TotalProfile=TotalProfile./numel(Profile);
TotalProfile.axes{1}.data=1e6*(TotalProfile.axes{1}.data-mean(TotalProfile.axes{1}.data));
TotalProfile.axes{2}.data=1e6*(TotalProfile.axes{2}.data-mean(TotalProfile.axes{2}.data));

surf(TotalProfile.axes{2}.data,TotalProfile.axes{1}.data,TotalProfile.data/18) %18 is ARBITRARY
 
zlim([0.0,1.1]);xlim([-1.8,1.8]);ylim([-3.5,3.5]);
colormap(jet)

Ax=gca;
Ax.DataAspectRatio(2)=3;
Ax.DataAspectRatio(1)=Ax.DataAspectRatio(2);
 Export.Figure3.Panel1.Curve1.X= 1e6*(TotalProfile.axes{2}.data-mean(TotalProfile.axes{2}.data));
 Export.Figure3.Panel1.Curve1.Y=1e6*(TotalProfile.axes{1}.data-mean(TotalProfile.axes{1}.data));
%  Export.Figure3.Panel1.Curve1.Data=[Profile,Profile,Profile];
Export.Figure3.Panel1.Curve1.Data=TotalProfile;
view([-3,2,1.5])
title('F412');