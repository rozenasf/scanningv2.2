    function out=PlotErrorBar(Fig,varargin )
        out=0;global Info;figureN(Fig);
        in=RegExpChannels(varargin);
        for i=1:numel(in)
            idgr=Info.Scan.idgr.(in{i}).slice_index(1,Info.Scan.Runtime.MaxPosition(1));
            %idgr_err=Info.Scan.idgr.(in{2*i}).slice_index(1,Info.Scan.Runtime.MaxPosition(1));
            for j=3:numel(Info.Scan.Runtime.Position)
                idgr=idgr.slice_index(2,Info.Scan.Runtime.Position(j));
                %idgr_err=idgr_err.slice_index(2,Info.Scan.Runtime.Position(j));
            end
            errorbar(real(idgr),imag(idgr));
            hold on
        end
        Zlist=Info.Scan.Axis.(Info.Scan.Runtime.AxisNames{2});
        if(isfield(Info.Range,'Beta'))
        plot(Zlist,Zlist*0+Info.Range.Beta(2),'r');
        end
        hold off
        %legend(in(1:2:end));
        Xr=num2str(Info.GetChannels.Xr*1e6);
        Yr=num2str(Info.GetChannels.Yr*1e6);
        Zr=num2str(Info.GetChannels.Zr*1e6);
        title(['Initial location Xr:',Xr,'um Yr:',Yr,'um Zr:',Zr,'um']);
    end