
global L
W=4e-6;
e=1.6e-19;
VDirac=-4.47;
CapacitiveBgFactor=5.946e14;
hbar=6.62e-34/(2*pi);

BetaSum=0.37;W=4e-6;
LoadScan([2141],L);PlotScan
figure(3);
plot_with_color(UnScaled_Beta,1);
Fit_Line(1,'all');
Slope=([Fit.a]);
BG=BG_With_Ratio_Const_Sum(1:numel(Slope));
n=CapacitiveBgFactor*(BG-VDirac);
k_F_vec = sqrt(pi*abs(n));
n=n/1e4;
Idata('dV_dX',Slope/BetaSum,n);
Idata('RhoXX',dV_dX./IgrAC14.mean(1).data(1:numel(Slope))*W)
Idata('Sigma',1./RhoXX)
Idata('Bridge_Ratio',Const_Sum_Ratio.slice_index(1,1).data(1:numel(Slope)),n);

Idata('l',1e6*( h_./(2*qe_^2*k_F_vec.*RhoXX.data)),n)


h=iplot({RhoXX,Sigma,Bridge_Ratio,l},4);
h(1,1).YLim=[0,100]
AddTitle
%  subplot(2,2,1)
%  plot(RhoXX)
%   subplot(2,2,2)
%  plot(Sigma)
%    subplot(2,2,3)
%  plot(Bridge_Ratio)
% subplot(2,2,4)
%  plot(l)