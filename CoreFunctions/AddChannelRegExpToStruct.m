function Out=AddChannelRegExpToStruct(In,Channels,Value)
    Out=In;
    NewNames=RegExpChannels(Channels);
    for i=1:numel(NewNames)
       Out.( NewNames{i} )=Value;
    end
end