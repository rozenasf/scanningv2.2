figure(23)
clf
LoadScan(2084);
RealBetaSum=Beta_HS2_Hset+Beta_HRest_Hset+Beta_Hbg_Hset;
plot(Beta_Hbg_Hset./RealBetaSum,'o')
hold on
LoadScan(2085);
RealBetaSum=Beta_HS2_Hset+Beta_HRest_Hset+Beta_Hbg_Hset;
plot(Beta_Hbg_Hset./RealBetaSum,'o')
% xlim([0.05,0.5]);
% ylim([0,1.5e5])
%%
LoadScan(2086);clf
plot(Beta_Hbg_Hset,'o')
Fit_Line(1,'X',[-1,-0.1]*1e-3);clf
X_0=[Fit.X_0];
plot(Beta_Hbg_Hset.axes{2}.data(1:numel(X_0)),X_0.^2)
% plot(Beta_Hbg_Hset,'o')
%%
figure(26);clf
LoadScan(2087);clf
plot(Beta_Hbg_Hset,'o')
Fit_Line(1,'X',[-0.45,0.3]*1e-3);
X_0=[Fit.X_0];

Signal=((-X_0/25e-3).^-1+1).^-1;

plot(Beta_Hbg_Hset.axes{2}.data(1:numel(X_0)),1./Signal.^2,'o')
hold on
LoadScan(2084);
RealBetaSum=Beta_HS2_Hset+Beta_HRest_Hset+Beta_Hbg_Hset;
plot(1./(Beta_Hbg_Hset./RealBetaSum).^2,'o')
hold on
LoadScan(2085);
RealBetaSum=Beta_HS2_Hset+Beta_HRest_Hset+Beta_Hbg_Hset;
plot(1./(Beta_Hbg_Hset./RealBetaSum).^2,'o')
% xlim([0,0.7])
% ylim([0,160])