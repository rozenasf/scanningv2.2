function out=FlatenCell(in)
    a=[char(in)';repmat(',',1,size(char(q)',2))];
    out=strtrim(a(:)');
    out(out==32)='';
    out=out(1:end-1);