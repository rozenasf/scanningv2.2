classdef Helium_Driver < handle
    properties
        Comunication
        Tic
        Timer
        File
        Data
        EndLevel
    end
    
    methods
        %datetick
        %datestr(737119.88295)
        function obj=Helium_Driver(ComPort)
            if(isnumeric(ComPort))
                obj.Comunication=serial(['COM',num2str(ComPort)],'BaudRate',9600);
            else
                obj.Comunication=serial(ComPort,'BaudRate',9600);
            end
            if(exist('C:/Log/HeliumLevel.dat'))
                obj.Data=load('C:/Log/HeliumLevel.dat');
            else
                obj.Data=[];
            end
            obj.File=fopen('C:/Log/HeliumLevel.dat','a+');
            obj.Tic=tic;
            obj.Timer=timer('TimerFcn',@(x,y)obj.GetTimer(),'Period', 60*15,'ExecutionMode', 'fixedSpacing');
            fopen(obj.Comunication);
            start(obj.Timer);
            obj.EndLevel=-10;
        end

        %-----
        function out=Get(obj,Channel)
            out=nan;
            if(strcmp(Channel,'HeliumLevel'))
                fprintf(obj.Comunication,'MEAS?\n');
                fgetl(obj.Comunication);
                Input=fgetl(obj.Comunication);
                Input=regexp(Input,' ','split');
                out=str2double(Input{1});
                N=now;
                fprintf(obj.File,'%2.1f,%0.5f\r\n',out,N);
                obj.Data=[obj.Data;out,N];
                obj.Tic=tic;
            end
        end
        function GetTimer(obj)
            global S
            S.Get('HeliumLevel');
        end
        function Plot(obj,span)
            if(~exist('span'));span=size(obj.Data,1);end
            
            plot(obj.Data(end-span+1:end,2),obj.Data(end-span+1:end,1),'.');
            hold on
            IDX=find(abs(obj.Data(end,1)-obj.Data(:,1))>6);IDXE=IDX(end);
            fit=polyfit(obj.Data(IDXE:end,2),obj.Data(IDXE:end,1),1);
            fit2=fit;
            if(fit2(1)<0)
                fit2(end)=fit2(end)-obj.EndLevel;
            else
                fit2(end)=fit2(end)-55;
                %fit2(end)=fit2(end)-obj.StartLevel;
            end
            EndTime=roots(fit2);
            plot(linspace(obj.Data(IDXE,2),EndTime,10),polyval(fit,linspace(obj.Data(IDXE,2),EndTime,10),'m'))
            datetick('x','ddd')
            Delta=EndTime-now;
            D=floor(Delta);
            H=floor((Delta-D)*24);
            M=floor((Delta-D)*24*60-H*60);
            S=floor((Delta-D)*24*60*60-H*60*60-M*60);
            
            title(sprintf('Helium Level,Time Left:%d %d:%d:%d, End Date:%s',D,H,M,S,datestr(EndTime)))
            hold off
        end
        function out=Toc(obj)
            out=toc(obj.Tic);
        end
        function delete(obj)
            try fclose(obj.Comunication);delete(obj.Comunication);end
            try stop(obj.Timer);delete(obj.Timer);end
            try fclose(obj.File);delete(obj.File);end
        end
    end
end