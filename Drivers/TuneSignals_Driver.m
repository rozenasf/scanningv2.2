classdef TuneSignals_Driver < handle
    methods
        function Init(~)
            global Info
            UpadateDacInformation();
            Info.cr.get('Sample');
            if(isfield(Info.Scan,'NoiseTolerance'))
                LowNoise=Info.cr.get('LowNoise');
                MaxInteration=3;Interation=1;
                while((abs(LowNoise)>Info.Scan.NoiseTolerance) && (Interation<=MaxInteration))
                    Interation=Interation+1;
                    Info.cr.get('Sample');
                    pause(1e-3);
                    LowNoise=Info.cr.get('LowNoise');
                    pause(1e-3);
                end
                if(Interation>MaxInteration);fprintf('Max iteration reached TuneSignals\n');end
            end
        end
        function out=Get(~,Channel)
            global Info;
            out=Info.cr.get(Channel);
        end
    end
    
end