%% BG=-8.5
ScanNumber=2071;
ScanList=ScanNumber+(0.01:0.01:41/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[]
XList=nan(21,1);
for k=1:numel(ScanList)
    
    LoadScan(ScanList(k));
    Weight_Bridge_Calibration_SmallChange.data=(Weight_Bridge_Calibration_SmallChange.data-fliplr(Weight_Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Weight_Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Weight_Bridge_Calibration_SmallChange.axes{1}.data,'o');
    %     plot( Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Weight_Bridge_Calibration_SmallChange.axes{1}.data;
    n=-(Vbg_to_n(Info.Scan.UserData.BG,0.284));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    
    %     plot(WRc_List,RHallTheory)
    %
    %     clf
    Measured=Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
    %     plot(WRc_List,Measured.data./Theory,'.')
    %RatioData=Measured.data;
    WeightData=Measured.data./Theory;
    %    RatioData=Measured.data;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];Number=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    for i=1:numel(UWRc)
        List(i)=mean(   WeightData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(WeightData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    XList(k)=Info.Scan.UserData.Location;
    WRcListBare=linspace(-6,6,28);
    Data2D(k,:)=List;
    Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,List,abs(STDList./sqrt(Number)),'-')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3,3.8])
figure(10);
pcolor(UWRc,XList(1:size(Data2D,1)),(Data2D));
TXYL('Up side hall effect','W/Rc','X[m]')
% caxis([0.68,0.79]);
colormap(jet(128))
colorbar
shading flat
FullData{1}=struct('WRc',UWRc,'BG',XList(1:size(Data2D,1)),'Data',Data2D);
% TelegramSend({7,8});
% clf
% imagesc(Data2D)
%        end
%    end
%    pause(30);
% end

%% BG=-8.5 plotted differently
ScanNumber=2071;
ScanList=ScanNumber+(0.01:0.01:41/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[]
XList=nan(21,1);
for k=1:numel(ScanList)
    
    LoadScan(ScanList(k));
%     Weight_Bridge_Calibration_SmallChange.data=(Weight_Bridge_Calibration_SmallChange.data-fliplr(Weight_Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Weight_Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Weight_Bridge_Calibration_SmallChange.axes{1}.data,'o');
    %     plot( Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Weight_Bridge_Calibration_SmallChange.axes{1}.data;
    n=-(Vbg_to_n(Info.Scan.UserData.BG,0.284));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);

    %Measured=Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Measured=Weight_Bridge_Calibration_SmallChange* Info.Scan.UserData.Sum;
    Theory=RHallTheory;

    WeightData=Measured.data;

    UWRc=unique(WRc_List);
    List=[];
    STDList=[];Number=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    for i=1:numel(UWRc)
        List(i)=mean(   WeightData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(WeightData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    XList(k)=Info.Scan.UserData.Location;
    WRcListBare=linspace(-6,6,28);
    Data2D(k,:)=List;
    Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,List,abs(STDList./sqrt(Number)),'-')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3,3.8])
figure(10);
pcolor(UWRc,XList(1:size(Data2D,1)),(Data2D));
TXYL('Up side hall effect','W/Rc','X[m]')
% caxis([0.68,0.79]);
colormap(jet(128))
colorbar
shading flat
FullData{1}=struct('WRc',UWRc,'BG',XList(1:size(Data2D,1)),'Data',Data2D);
%%
Data3D=Data2D(9:end-6,:);
surf(Data3D+8e-5*repmat([1:size(Data3D,2)],[size(Data3D,1),1]));
%% BG=9.9
ScanNumber=2072;
ScanList=ScanNumber+(0.01:0.01:GetMaxSuperScanNumber(ScanNumber)/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[];S15List=[];S2List=[];
XList=nan(21,1);
for k=1:numel(ScanList)
    
    LoadScan(ScanList(k));
    Weight_Bridge_Calibration_SmallChange.data=(Weight_Bridge_Calibration_SmallChange.data-fliplr(Weight_Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Weight_Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Weight_Bridge_Calibration_SmallChange.axes{1}.data,'o');
    %     plot( Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Weight_Bridge_Calibration_SmallChange.axes{1}.data;
    n=-(Vbg_to_n(Info.Scan.UserData.BG,0.284));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    
    %     plot(WRc_List,RHallTheory)
    %
    %     clf
    Measured=Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
    %     plot(WRc_List,Measured.data./Theory,'.')
    %RatioData=Measured.data;
    WeightData=Measured.data./Theory;
    %    RatioData=Measured.data;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];Number=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    S15List(k)=Info.Scan.UserData.Beta_HS15_Hset;
    S2List(k)=Info.Scan.UserData.Beta_HS2_Hset;
    for i=1:numel(UWRc)
        List(i)=mean(   WeightData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(WeightData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    XList(k)=Info.Scan.UserData.Location;
    WRcListBare=linspace(-6,6,28);
    Data2D(k,:)=List;
    Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,List,abs(STDList./sqrt(Number)),'-')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3,3.8])
figure(10);
pcolor(UWRc,XList(1:size(Data2D,1)),(Data2D));
TXYL('Up side hall effect','W/Rc','X[m]')
% caxis([0.68,0.79]);
colormap(jet(128))
colorbar
shading flat
FullData{1}=struct('WRc',UWRc,'BG',XList(1:size(Data2D,1)),'Data',Data2D);
% TelegramSend({7,8});
% clf
% imagesc(Data2D)
%        end
%    end
%    pause(30);
% end

%% BG=-5
ScanNumber=2074;
ScanList=ScanNumber+(0.01:0.01:41/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[];S15List=[];S2List=[];
XList=nan(21,1);
for k=1:numel(ScanList)
    
    LoadScan(ScanList(k));
    Weight_Bridge_Calibration_SmallChange.data=(Weight_Bridge_Calibration_SmallChange.data-fliplr(Weight_Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Weight_Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Weight_Bridge_Calibration_SmallChange.axes{1}.data,'o');
    %     plot( Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Weight_Bridge_Calibration_SmallChange.axes{1}.data;
    n=-(Vbg_to_n(Info.Scan.UserData.BG,0.284));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    
    %     plot(WRc_List,RHallTheory)
    %
    %     clf
    Measured=Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
    %     plot(WRc_List,Measured.data./Theory,'.')
    %RatioData=Measured.data;
    WeightData=Measured.data./Theory;
    %    RatioData=Measured.data;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];Number=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    S15List(k)=Info.Scan.UserData.Beta_HS15_Hset;
    S2List(k)=Info.Scan.UserData.Beta_HS2_Hset;
    for i=1:numel(UWRc)
        List(i)=mean(   WeightData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(WeightData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    XList(k)=Info.Scan.UserData.Location;
    WRcListBare=linspace(-6,6,28);
    Data2D(k,:)=List;
    Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,List,abs(STDList./sqrt(Number)),'-')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3,3.8])
figure(10);
pcolor(UWRc,XList(1:size(Data2D,1)),(Data2D));
TXYL('Up side hall effect','W/Rc','X[m]')
% caxis([0.68,0.79]);
colormap(jet(128))
colorbar
shading flat
FullData{1}=struct('WRc',UWRc,'BG',XList(1:size(Data2D,1)),'Data',Data2D);
% TelegramSend({7,8});
% clf
% imagesc(Data2D)
%        end
%    end
%    pause(30);
% end

%% BG=-1.6
ScanNumber=2075;
ScanList=ScanNumber+(0.01:0.01:GetMaxSuperScanNumber(ScanNumber)/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[];S15List=[];S2List=[];
XList=nan(21,1);
for k=1:numel(ScanList)
    
    LoadScan(ScanList(k));
    Weight_Bridge_Calibration_SmallChange.data=(Weight_Bridge_Calibration_SmallChange.data-fliplr(Weight_Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Weight_Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Weight_Bridge_Calibration_SmallChange.axes{1}.data,'o');
    %     plot( Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Weight_Bridge_Calibration_SmallChange.axes{1}.data;
    n=-(Vbg_to_n(Info.Scan.UserData.BG,0.284));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    
    %     plot(WRc_List,RHallTheory)
    %
    %     clf
    Measured=Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
    %     plot(WRc_List,Measured.data./Theory,'.')
    %RatioData=Measured.data;
    WeightData=Measured.data./Theory;
    %    RatioData=Measured.data;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];Number=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    S15List(k)=Info.Scan.UserData.Beta_HS15_Hset;
    S2List(k)=Info.Scan.UserData.Beta_HS2_Hset;
    for i=1:numel(UWRc)
        List(i)=mean(   WeightData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(WeightData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    XList(k)=Info.Scan.UserData.Location;
    WRcListBare=linspace(-6,6,28);
    Data2D(k,:)=List;
    Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,List,abs(STDList./sqrt(Number)),'-')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3,3.8])
figure(10);
pcolor(UWRc,XList(1:size(Data2D,1)),(Data2D));
TXYL('Up side hall effect','W/Rc','X[m]')
% caxis([0.68,0.79]);
colormap(jet(128))
colorbar
shading flat
FullData{1}=struct('WRc',UWRc,'BG',XList(1:size(Data2D,1)),'Data',Data2D);
% TelegramSend({7,8});
% clf
% imagesc(Data2D)
%        end
%    end
%    pause(30);
% end
%% BG=2
ScanNumber=2077;
ScanList=ScanNumber+(0.01:0.01:GetMaxSuperScanNumber(ScanNumber)/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[];S15List=[];S2List=[];
XList=nan(21,1);
for k=1:numel(ScanList)
    
    LoadScan(ScanList(k));
    Weight_Bridge_Calibration_SmallChange.data=(Weight_Bridge_Calibration_SmallChange.data-fliplr(Weight_Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Weight_Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Weight_Bridge_Calibration_SmallChange.axes{1}.data,'o');
    %     plot( Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Weight_Bridge_Calibration_SmallChange.axes{1}.data;
    n=-(Vbg_to_n(Info.Scan.UserData.BG,0.284));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    
    %     plot(WRc_List,RHallTheory)
    %
    %     clf
    Measured=Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
    %     plot(WRc_List,Measured.data./Theory,'.')
    %RatioData=Measured.data;
    WeightData=Measured.data./Theory;
    %    RatioData=Measured.data;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];Number=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    S15List(k)=Info.Scan.UserData.Beta_HS15_Hset;
    S2List(k)=Info.Scan.UserData.Beta_HS2_Hset;
    for i=1:numel(UWRc)
        List(i)=mean(   WeightData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(WeightData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    XList(k)=Info.Scan.UserData.Location;
    WRcListBare=linspace(-6,6,28);
    Data2D(k,:)=List;
    Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,List,abs(STDList./sqrt(Number)),'-')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3,3.8])
figure(10);
pcolor(UWRc,XList(1:size(Data2D,1)),(Data2D));
TXYL('Up side hall effect','W/Rc','X[m]')
% caxis([0.68,0.79]);
colormap(jet(128))
colorbar
shading flat
FullData{1}=struct('WRc',UWRc,'BG',XList(1:size(Data2D,1)),'Data',Data2D);
%%
%% BG=-9.9 super high quality - many points
ScanNumber=2079;
ScanList=ScanNumber+(0.01:0.01:41/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[];ListNL=[];
XList=nan(21,1);
for k=1:numel(ScanList)
    
    LoadScan(ScanList(k));
    Weight_Bridge_Calibration_SmallChange.data=(Weight_Bridge_Calibration_SmallChange.data-fliplr(Weight_Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Weight_Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Weight_Bridge_Calibration_SmallChange.axes{1}.data,'o');
    %     plot( Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Weight_Bridge_Calibration_SmallChange.axes{1}.data;
    n=-(Vbg_to_n(Info.Scan.UserData.BG,0.284));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*abs(n));
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    
    %     plot(WRc_List,RHallTheory)
    %
    %     clf
    Measured=Weight_Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
    %     plot(WRc_List,Measured.data./Theory,'.')
    %RatioData=Measured.data;
    WeightData=Measured.data./Theory;
    
    %    RatioData=Measured.data;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];Number=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    for i=1:numel(UWRc)
        List(i)=mean(   WeightData(WRc_List==UWRc(i))  ) ;
        ListNL(i)=mean(   NLSet.data(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(WeightData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    XList(k)=Info.Scan.UserData.Location;
    WRcListBare=linspace(-6,6,28);
    Data2D(k,:)=List;
    ListNL2D(k,:)=ListNL;
    Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,List,abs(STDList./sqrt(Number)),'-')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3,3.8])
figure(10);
pcolor(UWRc,XList(1:size(Data2D,1)),(Data2D));
TXYL('Up side hall effect','W/Rc','X[m]')
% caxis([0.68,0.79]);
colormap(jet(128))
colorbar
shading flat
FullData{1}=struct('WRc',UWRc,'BG',XList(1:size(Data2D,1)),'Data',Data2D);
% TelegramSend({7,8});
% clf
% imagesc(Data2D)
%        end
%    end
%    pause(30);
% end
%%
WRcListBare=linspace(-6,6,29*2-1)
Factor=3;
MaxN=2;
N=min([MaxN+0*WRcListBare;ceil(abs(Factor./WRcListBare))]);
WRcList=[];
for i=1:numel(WRcListBare)
    for j=1:N(i)
        WRcList=[WRcList, WRcListBare(i)];
    end
end
%%

surf(UWRc,XList(1:size(Data2D,1)),DDD);
zlim([-50,50]);caxis([-50,50])