function out=IsAxis(name)
    global Info
    out=sum(ismember(fieldnames(Info.Scan.Axis),name));
end