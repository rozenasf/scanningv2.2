T=@(t,kfd)exp(-pi*kfd*sin(t).^2);
kfdList=linspace(0.5,1.5,10);FWHMList=[];
clf

for i=1:numel(kfdList)
t=linspace(-pi/2,pi/2,100);plot(t,T(t,kfdList(i))-0.5);Fit_Line(@(x)exp(-x.^2));
FWHMList(i)=diff(Fit.X_0)/pi*180;
end
Kfd_Data=interp1(FWHMList,kfdList,CollimationList);
BGList=[];
for i=2:8
   BGList(i-1)=Scans.XEdge_BGlist_Holes_wrc{j,i}.Info.Channels.BG ;
end
Kf=SqrtSign(pi*Vbg_to_n(BGList,0.7));

%%
figure(11);clf
plot(-Kf,Kfd_Data,'o--','linewidth',3);out=Fit_Line(1,'X',[1.1,1.5]*1e8);
TXYL('$k_Fd$ collimation parameters as funciton of $k_F$','$k_F$','$k_Fd$');
legend(sprintf('Slope=%0.1f [nm]',out.a*1e9),'location','northwest');
Ax=gca;Ax.FontSize=16;
%%
figure(12);clf
plot(BGList,LmrList,'o--','linewidth',3)
TXYL('$lmr_{collimation} as function of BG$','$BG$','$lmr_{collimation}$');
Ax=gca;Ax.FontSize=16;