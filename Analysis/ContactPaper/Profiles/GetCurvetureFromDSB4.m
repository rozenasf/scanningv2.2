function [Curveture,WRc,BG,XX,YY]=GetCurvetureFromDSB4(ScanNumberList,Range,ScaleY,SymmetrizationPoint)
% 
% ScaleY 0.833 for 4K, 1.22 for 75K

global Info L
if(~exist('Range'));Range=[-1.25,1.25];end
if(~exist('SymmetrizationPoint'));SymmetrizationPoint=nan;end

% Scans [1014.01:0.01:1014.04], [1015.01:0.01:1015.04]

Y={};Traces={};
%1016 is the first one
for j=1:numel(ScanNumberList)
    ScanNumber=ScanNumberList(j);
R={};
SmoothParameter=1;
for i=1:4
    Data=LoadScan(ScanNumber+0.01*i,L,'NoScale');
%     Beta_Sum=Info.Scan.UserData.Initial_Beta_Sum;
    Beta_Sum=0.5;
    R{i} = (Data.Beta_HS15_Hset./Beta_Sum .*Data.AVS15./Data.IgrAC15);
    R{i}=R{i}.mean(2);
    % figure(4092);plot(R{i});hold on
%     R{i}.data=smooth(R{i}.data,SmoothParameter);
end
if(~exist('ScaleY'));
   if(Info.Channels.Temperature>70 &&  Info.Channels.Temperature<80)
       ScaleY=1.2207;
   elseif(Info.Channels.Temperature<10)
       ScaleY=0.8075;
%        ScaleY=1/1.2677;
   else
       error('Weird Temperature!') 
   end
end
for i=1:2
    YT=R{2*i}.axes{1}.data;
    YT=(YT(1:end-1)+YT(2:end))/2;
     
    Traces{end+1}=-diff(R{2*i-1}.data-R{2*i}.data);
%     figure(4092);plot(Traces{i});hold on
    Y{end+1}=1e6*(YT);
       
end
end
%


%  DataX=[Y{:}];DataY=[Traces{:}];
% XList=unique(DataX);DataYUnique=[];
% for i=1:numel(XList)
%    index=find( abs(XList(i)-DataX)<1e-3 );
%    DataYUnique(i)=mean(DataY(index));
% end

% figure(409);clf
YTotal=[Y{:}];TracesTotal=[Traces{:}];
% YUnique=uniquetol(YTotal,1e-3);
% TraceUnique=[];
% for i=1:numel(YUnique)
%     index=find(abs(YUnique(i)-YTotal)<1e-3);
%     TraceUnique(i)=mean(TracesTotal(index));
% end

YUnique=YTotal;TraceUnique=TracesTotal;
  %plot([Y{:}],[Traces{:}],'o');
%   plot([YUnique],smooth(([TraceUnique]+fliplr([TraceUnique]))/2,1),'o');

%plot( YUnique-mean(YUnique),[TraceUnique+fliplr(TraceUnique)]/2,'o')
XX=(YUnique-mean(YUnique)) * ScaleY;YY=TraceUnique;
if(isnan(SymmetrizationPoint));
    plot( XX,TraceUnique,'o')
else
    plot( [XX,2*SymmetrizationPoint-XX],[TraceUnique,TraceUnique],'o')
end

% plot(XList,(DataYUnique),'o')
Fit=Fit_Line(2,'X',Range);
Curveture=-[Fit.a]./[Fit.Y_0]*(5)^2
TXYL(sprintf('Ey BG=-2, W/Rc=1.6 DSB -a/c*W^2=%0.2f ',Curveture),'Y','R')
%  ylim([0,5])
BG=Info.Scan.UserData.BG;
WRc=abs(Info.Scan.UserData.WRc);
end