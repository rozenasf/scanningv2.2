%%
idgr412=LoadScan(412,L);
Magnet_I_412=Magnet_I;
idgr413=LoadScan(413,L);

idgr417=LoadScan(417,L);
Magnet_I_417=Magnet_I;
idgr418=LoadScan(418,L);

idgr428=LoadScan(428,L);
Magnet_I_428=Magnet_I;
idgr430=LoadScan(430,L);

idgr434=LoadScan(434,L);
Magnet_I_434=Magnet_I;
idgr436=LoadScan(436,L);
%%

DC1=Idata('DC1',idgr412.Beta_HC1_Hset.data-idgr413.Beta_HC1_Hset.data,Xs0,Magnet_I_412);
DC6=Idata('DC6',idgr412.Beta_HC6_Hset.data-idgr413.Beta_HC6_Hset.data,Xs0,Magnet_I_412);
figure(3);
subplot(3,2,1)
plot_with_color(idgr412.Beta_HC1_Hset,1)
subplot(3,2,2)
plot_with_color(idgr412.Beta_HC6_Hset,1)
subplot(3,2,3)
plot_with_color(idgr413.Beta_HC1_Hset,1)
subplot(3,2,4)
plot_with_color(idgr413.Beta_HC6_Hset,1)
subplot(3,2,5)
plot_with_color(DC1,1)
subplot(3,2,6)
plot_with_color(DC6,1)
suptitle(['\fontsize{16} Scan 412,413']);
%%
DC1=Idata('DC1',idgr417.Beta_HC1_Hset.data-idgr418.Beta_HC1_Hset.data,Xs0,Magnet_I_417);
DC6=Idata('DC6',idgr417.Beta_HC6_Hset.data-idgr418.Beta_HC6_Hset.data,Xs0,Magnet_I_417);
figure(4);
subplot(3,2,1)
plot_with_color(idgr417.Beta_HC1_Hset,1)
subplot(3,2,2)
plot_with_color(idgr417.Beta_HC6_Hset,1)
subplot(3,2,3)
plot_with_color(idgr418.Beta_HC1_Hset,1)
subplot(3,2,4)
plot_with_color(idgr418.Beta_HC6_Hset,1)
subplot(3,2,5)
plot_with_color(DC1,1)
subplot(3,2,6)
plot_with_color(DC6,1)
suptitle(['\fontsize{16} Scan 417,418']);
%%
DC1=Idata('DC1',idgr428.Beta_HC1_Hset.data-idgr430.Beta_HC1_Hset.data,Xs0,Magnet_I_428);
DC6=Idata('DC6',idgr428.Beta_HC6_Hset.data-idgr430.Beta_HC6_Hset.data,Xs0,Magnet_I_428);
figure(5);
subplot(3,2,1)
plot_with_color(idgr428.Beta_HC1_Hset,1)
subplot(3,2,2)
plot_with_color(idgr428.Beta_HC6_Hset,1)
subplot(3,2,3)
plot_with_color(idgr430.Beta_HC1_Hset,1)
subplot(3,2,4)
plot_with_color(idgr430.Beta_HC6_Hset,1)
subplot(3,2,5)
plot_with_color(DC1,1)
subplot(3,2,6)
plot_with_color(DC6,1)
suptitle(['\fontsize{16} Scan 428,430']);

%%
DC1=Idata('DC1',idgr434.Beta_HC1_Hset.data-idgr436.Beta_HC1_Hset.data,Xs0,Magnet_I_434);
DC6=Idata('DC6',idgr434.Beta_HC6_Hset.data-idgr436.Beta_HC6_Hset.data,Xs0,Magnet_I_434);
figure(5);
subplot(3,2,1)
plot_with_color(idgr434.Beta_HC1_Hset,1)
subplot(3,2,2)
plot_with_color(idgr434.Beta_HC6_Hset,1)
subplot(3,2,3)
plot_with_color(idgr436.Beta_HC1_Hset,1)
subplot(3,2,4)
plot_with_color(idgr436.Beta_HC6_Hset,1)
subplot(3,2,5)
plot_with_color(DC1,1)
subplot(3,2,6)
plot_with_color(DC6,1)
suptitle(['\fontsize{16} Scan 434,436']);
