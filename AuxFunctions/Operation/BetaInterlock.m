    function out=BetaInterlock(BetaNames,varargin)
        global Info S;out=0;Value=0;
        for j=1:numel(BetaNames)
            Value=Value+Info.Scan.idgr.(BetaNames{j}).data(Info.Scan.Runtime.PositionIndex);
        end
        if(Value<Info.Range.Beta(1) || Value>Info.Range.Beta(2) )
            if(abs(Info.Scan.idgr.HGraphete.data(Info.Scan.Runtime.PositionIndex))>5e-10)
                disp('Interlock Beta');
                TelegramSend('Interlock Beta');
                disp(['inverse Betasum is ',num2str(1/Value)]);
                TelegramSend(['inverse Betasum is ',num2str(1/Value)]);
                s=input('Do you want to continue scan? (0 - no, 1 - yes)');
                if(s==1);return;end
                
                for i=1:(numel(varargin)/2)
                    S.Set(varargin{i*2-1},varargin{i*2});
                end
                S.Get('Save');
                
                
                %S.GenericInterlock(1);
                %if(Info.Scan.Runtime.InterlockCounter==3)
                Info.Flags.Stop=1;Info.Flags.InScan=0;
                ActiveFigures= GetActiveFigures();
                {S.BuildToSave(1),ActiveFigures{:}}
                DocSend({S.BuildToSave(1),ActiveFigures{:}});
                
                input('INTERLOCK press CTRL-C!');
                %end
            else
                disp('beta interlock bat low HGraphete');
            end
        end
    end