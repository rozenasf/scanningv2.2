function Flexodac_SetAC(varargin)
global S
    for i=1:numel(varargin)
        S.Set(['A_',varargin{i}{1}],varargin{i}{2});
        S.Set(['F_',varargin{i}{1}],varargin{i}{3});
    end
end