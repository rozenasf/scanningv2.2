% Scans [1014.01:0.01:1014.04], [1015.01:0.01:1015.04]
figure(408);clf
Y={};Traces={};
for k=0:2:2
R={};
SmoothParameter=1;
for i=1:4
    if(k==0)
    UnPackScan(Scans.XY_bridge_lower_excitation_new_standard{i});Beta_Sum=0.51;
    else
        UnPackScan(Scans.XY_bridge_lower_excitation_new_standard2{i});Beta_Sum=0.51;
    end
    R{i} = (Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15);
    R{i}=R{i}.mean(2);
end
for i=1:2
    Y{i+k}=R{2*i}.axes{1}.data;
    Y{i+k}=(Y{i+k}(1:end-1)+Y{i+k}(2:end))/2;
    Traces{i+k}=-diff(R{2*i-1}.data-R{2*i}.data);
     
    %     Traces{i}=mean(Traces{i},2);
    Y{i+k}=1e6*(Y{i+k}+24.75e-6);
end
end
% plot([Y{:}],[Traces{:}],'o');
DataX=[Y{:}];DataY=[Traces{:}];
XList=unique(DataX);DataYUnique=[];
for i=1:numel(XList)
   index=find( abs(XList(i)-DataX)<1e-6 );
   DataYUnique(i)=mean(DataY(index));
end
plot(XList,0.5*(DataYUnique+fliplr(DataYUnique)),'o')
%
Fit_Line(2,'X',[-1.5,1.5]);
Curveture=-[Fit.a]./[Fit.Y_0]*(5)^2
TXYL(sprintf('Ey BG=-2, W/Rc=1.6 DSB -a/c*W^2=%0.2f ',Curveture),'Y','R')
 ylim([0,5])
