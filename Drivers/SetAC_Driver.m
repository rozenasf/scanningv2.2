classdef SetAC_Driver < handle
    methods
        function Set(~,Channel,Value)
            global Info S;
                Info.cr.set(Channel,Value);
                switch Channel(1)
                    case 'F'
                        Info.Dac.DispExs.(Channel(2:end)).Frequency=Value;
                    case 'A'
                        Info.Dac.DispExs.(Channel(2:end)).Amp=Value;
                end
                CalculateScalingFromDac();
                %S.Set(['A',Channel(2:end),'_e'],1./Info.Scaling.(['H',Channel(3:end)]));
        end
        %will now work like variable on a "GET" Level
        
%           function Value=Get(~,Channel)
%               global Info;
%               Value=Info.cr.get(Channel);
%               switch Channel(1)
%                   case 'F'
%                       Info.Dac.DispExs.(Channel(2:end)).Frequency=Value;
%                   case 'A'
%                       Info.Dac.DispExs.(Channel(2:end)).Amp=Value;
%               end
%               CalculateScalingFromDac();
%           end
          function Value=Get(~,Channel)
              global Info;
              if(Channel(end)~='e');
                  switch Channel(1)
                      case 'F'
                          Value=Info.Dac.DispExs.(Channel(2:end)).Frequency;
                      case 'A'
                          Value=Info.Dac.DispExs.(Channel(2:end)).Amp;
                  end
              else
                  switch Channel(1)
                      case 'A'
                          Value=1./Info.Scaling.(['H',Channel(3:end-2)]);
                  end                  
              end
          end

% 
%         function [out,Text]=GetFromMemory(~,Channel)
%             global Info;
%              switch Channel(1)
%                 case 'F'
%                     out=Info.Dac.DispExs.(Channel(2:end)).Frequency;
%                 case 'A'
%                     out=Info.Dac.DispExs.(Channel(2:end)).Amp;
%              end
%             Text=[];
%         end
    end

end