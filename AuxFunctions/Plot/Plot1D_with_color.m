    function out=Plot1D_with_color(Fig,varargin)
        global Info S;
        varargin=unique(RegExpChannels(varargin));
        out=0;
        figureN(Fig);clf
        SP=OptimizeSP(numel(varargin));
        for j=1:numel(varargin)
            
            subplot(SP(1),SP(2),j);
            idgr=Info.Scan.idgr.(varargin{j});
            
                for i=3:numel(Info.Scan.Runtime.Position)
                    idgr=idgr.slice_index(3,Info.Scan.Runtime.Position(i));
                end
                if(idgr.dim==1)
                    idgr.plot;
                else
                    idgr.plot_with_color(1,@jet);
                end

        end
        drawnow;
    end