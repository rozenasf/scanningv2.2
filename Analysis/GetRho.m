function [Slope,Std,Zero]=GetRho(Data)
if(Data.dim>1)
    Slope=Data.slice_index(1,1)*0;Slope.name='Slope';
    Std=Data.slice_index(1,1)*0;Std.name='Std';
    Zero=Data.slice_index(1,1)*0;Zero.name='Zero';
     SizeRest=size(Data);
    SizeRest=SizeRest(2:end);

    for i=1:prod(SizeRest)
        fit=polyfit(Data.axes{1}.data',Data.data(:,i),1);
        Slope.data(i)=fit(1);
        Zero.data(i)=nan;
        try
        Zero.data(i)=roots(fit);
        end
        Std.data(i)=std(Data.data(:,i)-polyval(fit,Data.axes{1}.data'))./(max(Data.axes{1}.data')-min(Data.axes{1}.data'));
    end
else
    Slope=nan;
    Std=nan;
    Zero=nan;

    
        fit=polyfit(Data.axes{1}.data',Data.data(:),1);
        Slope=fit(1);
        Zero=nan;
        try
        Zero=roots(fit);
        end
        Std=std(Data.data(:)-polyval(fit,Data.axes{1}.data'))./(max(Data.axes{1}.data')-min(Data.axes{1}.data'));
   
end
    %%
    
   
end