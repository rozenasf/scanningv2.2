    function out=Plot( varargin)
    %this is an old function, dont use...
        out=0;global Info;
varargin=RegExpChannels(varargin);
    %varargin=InitiateNewFunction(varargin);
        for i=1:numel(varargin)
            idgr=Info.Scan.idgr.(varargin{i});
            %idgr_err=Info.Scan.idgr.(in{2*i});
            for j=2:numel(Info.Scan.Runtime.Position)
                idgr=idgr.slice_index(2,Info.Scan.Runtime.Position(j));
                %idgr_err=idgr_err.slice_index(2,Info.Scan.Runtime.Position(j));
            end
            plot(idgr);
            hold on
        end
        Zlist=Info.Scan.Axis.(Info.Scan.Runtime.AxisNames{1});
        if(isfield(Info.Range,'Beta'))
        plot(Zlist,Zlist*0+Info.Range.Beta(2),'r');
        end
        hold off
        %legend(in(1:2:end));
        Xr=num2str(Info.GetChannels.Xr*1e6);
        Yr=num2str(Info.GetChannels.Yr*1e6);
        Zr=num2str(Info.GetChannels.Zr*1e6);
        title(['Initial location Xr:',Xr,'um Yr:',Yr,'um Zr:',Zr,'um']);
    end