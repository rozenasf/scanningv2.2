 %% Sharvin: X cuts as function of BG no magnetic field
 BGList=[-9.90,-8.50,-7.22,-6.00,-4.85,-3.78,-2.79,-1.88,-1.07,-0.37,0.20,0.60,0.80,1.20,1.77,2.47,3.28,4.19,5.18,6.25,7.40,8.62,9.90];

    %W/Rc = 1,2,4,6
    figureindex = 200;
    ScanName='Xcenter_BG';IgrVec=[];
    figure(201);clf;subplot(2,2,1);
    for i=1:numel(Scans.(ScanName))
        UnPackScan(Scans.(ScanName){i})
        Beta_Sum=0.51;
        IgrVec(i)=mean(mean(IgrAC15.data));
        R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
        R.axes{1}.data=R.axes{1}.data*0.8385;
        plot(R,'-');hold on
    end
    ChangePlotColor(@jet)
    Fit_Right=Fit_Line(0,'X',RangeRight);Fit_Mid=Fit_Line(1,'X',RangeMid);Fit_Left=Fit_Line(0,'X',RangeLeft);
    Sarvin_Right=ExtrapulateFit(Fit_Right,InterpolationPoints(2))-ExtrapulateFit(Fit_Mid,InterpolationPoints(2));
    Sarvin_Left=-(ExtrapulateFit(Fit_Left,InterpolationPoints(1))-ExtrapulateFit(Fit_Mid,InterpolationPoints(1)));
    R_sharvin_th =h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(BGList,0.7)))/5.0e-6;
    ylim([-200,200]);
     ClearFit
    TXYL('X center cuts','$ $Xs (scaled) $[\mu m]$','$R$ [Ohm]')
    Kf=SqrtSign(pi*Vbg_to_n(BGList,0.56));
    %
    subplot(2,2,2);cla
    RhoXX=[Fit_Mid.a]*5;
    plot(Vbg_to_n(BGList,0.56)/1e4,1e6*h_./(2*qe_^2.*abs(Kf).*RhoXX),'o--','linewidth',1,'markersize',5)
    TXYL('$ $Mean free path $l_{mr}=\frac{h}{2e^2\rho_{xx}\sqrt{\pi n}}$','Density [1/cm^2]','$l_{mr} [\mu m]$')
    %
    subplot(2,2,3);cla;hold on
    plot(Vbg_to_n(BGList,0.56)/1e4,Sarvin_Left,'o--','linewidth',1,'markersize',5);
    plot(Vbg_to_n(BGList,0.56)/1e4,Sarvin_Right,'o--','linewidth',1,'markersize',5);
    plot(Vbg_to_n(BGList,0.56)/1e4,R_sharvin_th,'o--','linewidth',1,'markersize',5);
    ylim([0,280]);grid
    TXYL('Contact Resistance','$ $Density $[1/cm^2]$','$\Delta R$ [Ohm]')
    %
    subplot(2,2,4);cla;hold on
    plot(Kf/1e6,Sarvin_Right./R_sharvin_th,'o--','linewidth',1,'markersize',5);
    hold on
    plot(Kf/1e6,Sarvin_Left./R_sharvin_th,'o--','linewidth',1,'markersize',5);
    ylim([0,8])
    
    %
    Fit_Line(@(x,a)0+4*sqrt(-(x-0.56).*a(1))+a(2),[2.15e-8,30],'X',[-150,-35])
    
    Fit(1).Range=[-150,0];Fit(2).Range=[-150,0];
    grid
    dleft = Fit(1).a * 1000;
    dright = Fit(2).a * 1000;
    TXYL(sprintf('$ $Left/Right $d_{pn}$ = %0.2f/%0.2f nm',dleft,dright),'$ $Kf $[1/\mu m]$','$\Delta R/R_{Quantum}$')