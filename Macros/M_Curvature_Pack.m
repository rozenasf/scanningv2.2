%% Check noise

List=[];
for i=1:20
    S.Get('Feedback','TuneSignals','Beta');
    List(i)=GetFromMemory('Beta_HS15_Hset');
end
std(List)

% 1e-4*

%std=2.45 for Vs=0.64,FeedbackAVR=100
%std=2.28 for Vs=0.64,FeedbackAVR=200
%std=2.58 for Vs=0.64,FeedbackAVR=400

%std=2.45 for Vs=0.64,FeedbackAVR=100
%std=3.82 for Vs=0.62,FeedbackAVR=100
%std=2.30 for Vs=0.60,FeedbackAVR=100

%std=3.29 for Vs=0.64,FeedbackShift=0.014
%std=2.45 for Vs=0.64,FeedbackShift=0.012
%std=2.25 for Vs=0.64,FeedbackShift=0.010
%std=4.15 for Vs=0.64,FeedbackShift=0.008

%% Center bridge profile as function of W/Rc, BG
Wrc=[1:0.25:3,3.5:0.5:6];BG=-2;
Width = 5e-6;qe_=1.6022e-19;hbar_=1.0546e-34;
r_c=Width./Wrc;nn = abs(Vbg_to_n(-2,0.7));
p_F=hbar_*sqrt(pi*nn);B=p_F./qe_./r_c;
Magnet=B./120e-3*10;


XCenter = -21.75e-6;
YCenter = -24.75e-6;

DeltaX = 2e-6;
DeltaY = 2.5e-6;

XRange = [XCenter + DeltaX   /2, XCenter - DeltaX   /2];
YRange = [YCenter + DeltaY   /2, YCenter - DeltaY   /2];

XAxis     = linspace(XRange(1),XRange(2),6);
YAxis     = linspace(YRange(1),YRange(2),6);

%position for the first movement
% S.Set('Xs_Ramp',XRange(2));
% S.Set('Ys_Ramp',YRange(2));
% S.Set('Xs_Ramp',XRange(1));
% S.Set('Ys_Ramp',YRange(1));
for n=1:numel(Magnet)
    MagnetList=[Magnet(n),-Magnet(n)];

    S.Set('Dummy',n);
    Info.Flags.SuperScan = 1;
    for j=1:numel(MagnetList)
        
        S.Set('Magnet_I',MagnetList(j));
        
        Info.Scan=[];
        Info.Scan.Description=['Curveture at W/Rc=1.6 and BG=-2'];
        Info.Scan.UserData=struct('Magnet_I',MagnetList(j),...
            'BG',Info.Channels.BG, 'WRc',Wrc(n)*sign(MagnetList(j)));
        Info.Scan.WaitTimes=[0.3 1];
        
        Info.Scan.End=BuildStruct( ...
            'Magnet_I',0 ...
            );
        
        Info.Scan.Axis=BuildStruct( ...
            'Ys_Ramp', YAxis, ...
            'Xs_Ramp', XAxis ...
            );
        
        Info.Scan.Get=...
            {{                                            ...
            'Feedback',...
            {'TuneSignals',...
            'Beta'},5,...
            'Temperature','Time',...
            @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint')...
            },{  ...
            'Save',...
            }};
        
        pause(2);
        
        Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();
        
        S.RunScan();
        
        S.Set('Xs_Ramp',XRange(1));
        S.Set('Ys_Ramp',YRange(1));
        
        S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));
        S.Set('WorkPoint',0);
        
        S.Get('Feedback','TuneSignals','Beta');
    end

end
%% Center bridge profile at specific W/Rc, BG
Wrc=1.6;BG=-2;
Width = 5e-6;qe_=1.6022e-19;hbar_=1.0546e-34;
r_c=Width./Wrc;nn = abs(Vbg_to_n(-2,0.7));
p_F=hbar_*sqrt(pi*nn);B=p_F./qe_./r_c;
Magnet=B./120e-3*10;
MagnetList=[Magnet,-Magnet];

XCenter = -21.75e-6;
YCenter = -24.75e-6;

DeltaX = 2e-6;
DeltaY = 2e-6; %instead of 2.5

XRange = [XCenter + DeltaX   /2, XCenter - DeltaX   /2];
YRange = [YCenter + DeltaY   /2, YCenter - DeltaY   /2];

XAxis     = linspace(XRange(1),XRange(2),6);
YAxis     = linspace(YRange(1),YRange(2),6);

%position for the first movement
% S.Set('Xs_Ramp',XRange(2));
% S.Set('Ys_Ramp',YRange(2));
% S.Set('Xs_Ramp',XRange(1));
% S.Set('Ys_Ramp',YRange(1));

for i=1:20
    S.Set('Dummy',i);
    Info.Flags.SuperScan = 1;
    for j=1:numel(MagnetList)
        
        S.Set('Magnet_I',MagnetList(j));
        
        Info.Scan=[];
        Info.Scan.Description=['Curveture at W/Rc=1.6 and BG=-2'];
        Info.Scan.UserData=struct('Magnet_I',MagnetList(j),...
            'BG',Info.Channels.BG, 'WRc',Wrc*sign(MagnetList(j)),'Sum',GetFromMemory('Sum'));
        Info.Scan.WaitTimes=[0.3 1];
        
        Info.Scan.End=BuildStruct( ...
            'Magnet_I',0 ...
            );
        
        Info.Scan.Axis=BuildStruct( ...
            'Ys_Ramp', YAxis, ...
            'Xs_Ramp', XAxis ...
            );
        
        Info.Scan.Get=...
            {{                                            ...
            'Feedback',...
            {'TuneSignals',...
            'Beta'},7,...
            'Temperature','Time',...
            @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint')...
            },{  ...
            'Save',...
            }};
        
        pause(2);
        
        Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();
        
        S.RunScan();
        
        S.Set('Xs_Ramp',XRange(1));
        S.Set('Ys_Ramp',YRange(1));
        
        S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));
        S.Set('WorkPoint',0);
        
        S.Get('Feedback','TuneSignals','Beta');
    end
end

%% DSB profiles 2D vs W/Rc  particular BG
% Wrc = [0,[1:0.25:3,3.5:0.5:6],[1:0.25:3],[1:0.25:2]];


Wrc=2;

Width = 5e-6;qe_=1.6022e-19;hbar_=1.0546e-34;
r_c=Width./Wrc;nn = abs(Vbg_to_n(-2,0.7));
p_F=hbar_*sqrt(pi*nn);B=p_F./qe_./r_c;
Magnet=B./120e-3*10;

XCenter = -21.75e-6;
YCenter = -24.75e-6;

for n = 1:numel(Magnet)
    
     S.Set('Magnet_I',Magnet(n));
     S.Set('Xs_Ramp',XCenter);
     S.Set('Ys_Ramp',-21.5e-6);
     S.Set('Vg_Ramp',-0.85); S.Set('WorkPoint',0);
     S.Get('Feedback','TuneSignals','Beta');
     S.Get('Bridge_Calibration')
     S.Set('BridgeUP')
     S.Set('Magnet_I',-Magnet(n));
     S.Get('Feedback','TuneSignals','Beta');
     S.Get('Bridge_Calibration')
     S.Set('BridgeUN')
     
     S.Set('Magnet_I',Magnet(n));
     S.Set('Ys_Ramp',-28e-6);
     S.Set('Vg_Ramp',-0.85); S.Set('WorkPoint',0);
     S.Get('Feedback','TuneSignals','Beta');
     S.Get('Bridge_Calibration')
     S.Set('BridgeDP')
     S.Set('Magnet_I',-Magnet(n));
     S.Get('Feedback','TuneSignals','Beta');
     S.Get('Bridge_Calibration')
     S.Set('BridgeDN')

     
MagnetList=[Magnet(n),-Magnet(n)];     


DeltaY = [6.5e-6,3.5e-6,0.7e-6];
%bridge is on 6.5e-6

DeltaX = 2e-6;

YUp    = [YCenter + DeltaY(2)/2, YCenter - DeltaY(3)/2];
YDown  = [YCenter + DeltaY(3)/2, YCenter - DeltaY(2)/2];
YTotal = [YCenter + DeltaY(1)/2, YCenter - DeltaY(1)/2];
XRange = [XCenter + DeltaX   /2, XCenter - DeltaX   /2];

YAxisUp   = linspace(YUp(1)   ,YUp(2)   ,7);
YAxisDown = linspace(YDown(1) ,YDown(2) ,7);
XAxis     = linspace(XRange(1),XRange(2),6);

CalibrationLetter='UD';
MagnetLetter='PN';

S.Set('Xs_Ramp',XRange(2));
S.Set('Xs_Ramp',XRange(1));
S.Set('Ys_Ramp',YTotal(2));
S.Set('Ys_Ramp',YTotal(1));
S.Set('Ys_Ramp',YUp(1));
S.Set('Vg_Ramp',-0.99);
S.Set('WorkPoint',0);
S.Get('Feedback','TuneSignals','Beta');
  
Info.Flags.SuperScan = 1;


for k=1:1
    S.Set('Dummy',k);
 
    for i=1:numel(CalibrationLetter)
        switch(CalibrationLetter(i))
            case 'U'
                YAxis=YAxisUp;
            case 'D'
                YAxis=YAxisDown;
        end
        for j=1:numel(MagnetList)
            
            S.Set('Magnet_I',MagnetList(j));
            S.Get(['Bridge',CalibrationLetter(i),MagnetLetter(j)]);
            
            
            if(CalibrationLetter(i)=='D')
                S.Set('Ys_Ramp',YDown(1));
            end
            
            Info.Scan=[];
            Info.Scan.Description=['Test if at heigher Sum we get more curveture'];
            Info.Scan.UserData=struct('Magnet_I',MagnetList(j),'CalibrationPoint',CalibrationLetter(i),...
            'BG',Info.Channels.BG, 'WRc',Wrc(n)*sign(MagnetList(j)),'Sum',GetFromMemory('Sum'),'Mark','DSB_Profile1');
            Info.Scan.WaitTimes=[0.2 1];
            
            Info.Scan.End=BuildStruct( ...
                'Magnet_I',0 ...
                );
            
            Info.Scan.Axis=BuildStruct( ...
                'Ys_Ramp', YAxis, ...
                'Xs_Ramp', XAxis ...
                );
            
            Info.Scan.Get=...
                {{                                            ...
                'Feedback',...
                {'TuneSignals',...
                'Beta'},7,...
                'Temperature','Time',...
                @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint')...
                },{  ...
                'Save',...
                }};
            
            pause(2);
            
            Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();
            
            S.RunScan();
            
            S.Set('Xs_Ramp',XRange(1));
            S.Set('Ys_Ramp',YTotal(2));
            S.Set('Ys_Ramp',YTotal(1));
            S.Set('Ys_Ramp',YUp(1));
            
            S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));
            S.Set('WorkPoint',0);
            
            S.Get('Feedback','TuneSignals','Beta');
        end
    end
  
end

  Info.Flags.SuperScan = 0;

end


%% DSB profiles 2D vs BG particular W/Rc - relevant - 271218
% Wrc = [0,[1:0.25:3,3.5:0.5:6],[1:0.25:3],[1:0.25:2]];
Parameters=struct(...
    'WRc',1.6,...
    'BG',{-9.9,-8.9,-6.9,-5.0,-3.4,-2.7,-2.0,-1.4,-0.9,-0.4,0.0},...
    'VsdGraphene',7.5e-3);

VgGraphene = -1.49;
% X -9.75     -> -11.75
% Y -10.55e-6 -> -15.35e-6

% S.Set('Dummy',1);
for nm=1:numel(Parameters)
    Info.Flags.SuperScan=1;
    S.Set('Dummy',GetFromMemory('Dummy')+1);
    
    WRc=Parameters(nm).WRc;
    BG =Parameters(nm).BG;
    VsdGraphene=Parameters(nm).VsdGraphene;
    
    %numbers
        XCenter = -10.75e-6;
        YCenter = -12.95e-6;
        DeltaY = [4.8e-6,2.87e-6,1.23e-6];
        DeltaX = 2e-6;
    
    %end numbers
    YUp    = [YCenter + DeltaY(2)/2, YCenter - DeltaY(3)/2];
    YDown  = [YCenter + DeltaY(3)/2, YCenter - DeltaY(2)/2];
    YTotal = [YCenter + DeltaY(1)/2, YCenter - DeltaY(1)/2];
    XRange = [XCenter + DeltaX   /2, XCenter - DeltaX   /2];
    
    YAxisUp   = linspace(YUp(1)   ,YUp(2)   ,7);
    YAxisDown = linspace(YDown(1) ,YDown(2) ,7);
    XAxis     = linspace(XRange(1),XRange(2),4);
    
    S.Set('Xs_Ramp',XCenter);
    
    S.Set('WRc',0);S.Get('KeepWRc');S.Set('RampBG',BG);
    S.Get('Bridge');S.Set('SetGrapheneVsd',VsdGraphene);
    
    pause(2);
    
    S.Set('WRc',WRc);S.Get('KeepWRc');
    S.Get('Feedback','TuneSignals','Beta');
    S.Get('Bridge_Calibration')
    S.Set('BridgeUP')
    
    S.Set('WRc',-WRc);S.Get('KeepWRc');
    S.Get('Feedback','TuneSignals','Beta');
    S.Get('Bridge_Calibration')
    S.Set('BridgeUN')
    
    VgEtch=GetFromMemory('Vg');
    
    S.Set('WRc',0);S.Get('KeepWRc');S.Get('Regular');
    S.Set('Ys_Ramp',YUp(1));
    
    S.Set('Vg_Ramp',VgGraphene);S.Set('WorkPoint',0);
    S.Get('Feedback','TuneSignals','Beta');
    S.Get('CenterWorkPoint');
    
    S.Get('Feedback','TuneSignals','Beta','Temperature');
    
    Info.Scan=[];Info.Scan.Description=['Y Scanner Feedback - For BetaSum'];Info.Scan.WaitTimes=[0.5];
    Info.Scan.Axis=BuildStruct(  ...
        'Ys_Ramp',[linspace(YUp(1),YDown(2),41)] ...
        );
    Info.Scan.Get=...
        {{   ...
        'Feedback',...
        {'TuneSignals',...
        'Beta'},1,...
        'Temperature','Time',...
        @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint'),...
        'Save'...
        }};
    S.RunScan();
    
    S.Set('Ys_Ramp',YTotal(2));S.Set('Vg_Ramp',VgEtch);S.Set('WorkPoint',0);
    S.Get('Feedback','TuneSignals','Beta','Temperature');
    
    S.Get('BridgeUN');
    
    S.Set('WRc',WRc);S.Get('KeepWRc');
    S.Get('Feedback','TuneSignals','Beta');
    S.Get('Bridge_Calibration')
    S.Set('BridgeDP')
    
    S.Set('WRc',-WRc);S.Get('KeepWRc');
    S.Get('Feedback','TuneSignals','Beta');
    S.Get('Bridge_Calibration')
    S.Set('BridgeDN')
    
    S.Set('WRc',0);S.Get('KeepWRc');
    S.Set('Ys_Ramp',YTotal(1));
    S.Set('Xs_Ramp',XRange(2));
    S.Set('Xs_Ramp',XRange(1));
    S.Set('Ys_Ramp',YUp(1));
    
    S.Set('Vg_Ramp',VgGraphene);S.Set('WorkPoint',0);
    S.Get('Feedback','TuneSignals','Beta');
    S.Get('CenterWorkPoint');
    
    CalibrationLetter='UD';
    MagnetLetter='PN';
    for k=1:4
    for i=1:numel(CalibrationLetter)
        switch(CalibrationLetter(i))
            case 'U'
                YAxis=YAxisUp;
            case 'D'
                YAxis=YAxisDown;
        end
        for j=1:numel(MagnetLetter)
            if(j==1)
                S.Set('WRc',WRc);S.Get('KeepWRc');
            else
                S.Set('WRc',-WRc);S.Get('KeepWRc');
            end
            S.Get(['Bridge',CalibrationLetter(i),MagnetLetter(j)]);
            
            if(CalibrationLetter(i)=='D')
                S.Set('Ys_Ramp',YDown(1));
            end
            if(CalibrationLetter(i)=='U')
                S.Set('Ys_Ramp',YUp(1));
            end
            S.Get('Feedback','TuneSignals','Beta','Temperature');
            Info.Scan=[];
            Info.Scan.Description=['Curveture as function of BG for W/Rc=1.6'];
            Info.Scan.UserData=struct('Magnet_I',GetFromMemory('Magnet_I'),'CalibrationPoint',CalibrationLetter(i),...
                'BG',Info.Channels.BG, 'WRc',GetFromMemory('WRc'),'Sum',GetFromMemory('Sum'),'Mark','DSB_Profile3',...
                'VsdGraphene',VsdGraphene);
            Info.Scan.WaitTimes=[0.2 1];
            
            Info.Scan.End=BuildStruct( ...
                'Magnet_I',0 ...
                );
            
            Info.Scan.Axis=BuildStruct( ...
                'Ys_Ramp', YAxis, ...
                'Xs_Ramp', XAxis ...
                );
            
            Info.Scan.Get=...
                {{                                            ...
                'Feedback',...
                {'TuneSignals',...
                'Beta'},7,...
                'Temperature','Time',...
                @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint')...
                },{  ...
                'Save',...
                }};
            
            pause(2);
            
            S.RunScan();
            S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));
            S.Set('WorkPoint',0);
            S.Set('Xs_Ramp',XRange(1));
            S.Set('Ys_Ramp',YTotal(2));
            S.Set('Ys_Ramp',YTotal(1));
            
            %S.Get('Feedback','TuneSignals','Beta');
        end
    end
    end
    Info.Flags.SuperScan = 0;
    S.Set('Vg_Ramp',VgEtch);S.Set('WorkPoint',0);
    S.Get('Feedback','TuneSignals','Beta','Temperature');
end
%% DSB profiles 2D vs W/Rc  particular BG

MagnetList=[1.247,-1.247];

XCenter = -21.75e-6;
YCenter = -24.75e-6;

DeltaY = [6.5e-6,3.5e-6,0.7e-6];
%bridge is on 6.5e-6

DeltaX = 5e-6;

YUp    = [YCenter + DeltaY(2)/2, YCenter - DeltaY(3)/2];
YDown  = [YCenter + DeltaY(3)/2, YCenter - DeltaY(2)/2];
YTotal = [YCenter + DeltaY(1)/2, YCenter - DeltaY(1)/2];
XRange = [XCenter + DeltaX   /2, XCenter - DeltaX   /2];

YAxisUp   = linspace(YUp(1)   ,YUp(2)   ,7);
YAxisDown = linspace(YDown(1) ,YDown(2) ,7);
XAxis     = linspace(XRange(1),XRange(2),6);

CalibrationLetter='UD';
MagnetLetter='PN';

S.Set('Xs_Ramp',XRange(2));
S.Set('Xs_Ramp',XRange(1));
S.Set('Ys_Ramp',YTotal(2));
S.Set('Ys_Ramp',YTotal(1));
S.Set('Ys_Ramp',YUp(1));

for k=1:4
    S.Set('Dummy',k);
    Info.Flags.SuperScan = 1;
    for i=1:numel(CalibrationLetter)
        switch(CalibrationLetter(i))
            case 'U'
                YAxis=YAxisUp;
            case 'D'
                YAxis=YAxisDown;
        end
        for j=1:numel(MagnetList)
            
            S.Set('Magnet_I',MagnetList(j));
            S.Get(['Bridge',CalibrationLetter(i),MagnetLetter(j)]);
            
            
            if(CalibrationLetter(i)=='D')
                S.Set('Ys_Ramp',YDown(1));
            end
            
            Info.Scan=[];
            Info.Scan.Description=['Play around with'];
            Info.Scan.UserData=struct('Magnet_I',MagnetList(j),'CalibrationPoint',CalibrationLetter(i),'BG',Info.Channels.BG);
            Info.Scan.WaitTimes=[0.2 1];
            
            Info.Scan.End=BuildStruct( ...
                'Magnet_I',0 ...
                );
            
            Info.Scan.Axis=BuildStruct( ...
                'Ys_Ramp', YAxis, ...
                'Xs_Ramp', XAxis ...
                );
            
            Info.Scan.Get=...
                {{                                            ...
                'Feedback',...
                {'TuneSignals',...
                'Beta'},7,...
                'Temperature','Time',...
                @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint')...
                },{  ...
                'Save',...
                }};
            
            pause(2);
            
            Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();
            
            S.RunScan();
            
            S.Set('Xs_Ramp',XRange(1));
            S.Set('Ys_Ramp',YTotal(2));
            S.Set('Ys_Ramp',YTotal(1));
            S.Set('Ys_Ramp',YUp(1));
            
            S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));
            S.Set('WorkPoint',0);
            
            S.Get('Feedback','TuneSignals','Beta');
        end
    end
    Info.Flags.SuperScan = 0;
end
%% Calibrate for 2D at W/Rc=-5
S.Get('Regular');
S.Get('Feedback','TuneSignals','Beta','Temperature');

% S.Set('RampBG',-9.9);
% S.Set('RampBG',-5);
S.Get('Feedback','TuneSignals','Beta','Temperature');

% pause(60);
Letter='U';
S.Set('AVRest',0.1e-3);
S.Get('Bridge');
S.Set('Sum',15e-3);
S.Set('WRc',1.6);
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'P'])
S.Set('WRc',-1.6);
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'N'])

% -6.85e-6,-10.55e-6
S.Set('Ys_Ramp',-10.55e-6);
pause(2);
S.Get('Feedback','TuneSignals','Beta','Temperature');

pause(60)
Letter='D';
S.Set('AVRest',0.1e-3);
S.Get('Bridge');
S.Set('Sum',15e-3);
S.Set('WRc',1.6);
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'P'])
S.Set('WRc',-1.6);
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'N'])

S.Set('Ys_Ramp',-6.85e-6);
S.Set('AVRest',5e-3);
S.Get('Feedback','TuneSignals','Beta','Temperature');
%% DSB profiles 2D scan

% MagnetList=[1.247,-1.247];
WRcList=[1.6,-1.6];

%for fast curvetures
%XAxis=linspace(-9.75e-6,-11.75e-6,4);
%YAxis=linspace(-10.55e-6,-15.35e-6,31); %10.55,15.35
%for High quality 2D:

XAxis=linspace(-6.83e-6,-8.57e-6,15);
YAxis=linspace(-6.85e-6,-10.55e-6,29); 

CalibrationLetter='UD';
MagnetLetter='PN';
for k=3:3
    S.Set('Dummy',k);
    Info.Flags.SuperScan = 1;
    for i=1:numel(CalibrationLetter)
        for j=1:numel(WRcList)
            S.Set('WRc',WRcList(j));
            S.Get('KeepWRc');
            
            %         S.Set('Magnet_I',MagnetList(j));
            
            Info.Scan=[];
            Info.Scan.Description=['XY Magnet Scanner Feedback at BG = -2 W/Rc=1.6 3um after scaling X excitation 15mV'];
            Info.Scan.UserData.Magnet_I=GetFromMemory('Magnet_I');
            Info.Scan.UserData.WRc=GetFromMemory('WRc');
            Info.Scan.UserData.CalibrationPoint=CalibrationLetter(i);
            Info.Scan.UserData.BG=GetFromMemory('BG');
            
            Info.Scan.WaitTimes=[0.2 0.5];
            
            Info.Scan.End=BuildStruct( ...
                'Magnet_I',0 ...
                );
            
            %'Xs_Ramp',linspace(-18.75e-6,-23.75e-6,26),
            % 'Ys_Ramp',linspace(-21.9e-6,-28.05e-6,31) ...
            
            Info.Scan.Axis=BuildStruct( ...
                'Ys_Ramp',YAxis, ...
                'Xs_Ramp', XAxis ...
                );
            
            Info.Scan.Get=...
                {{                                            ...
                'Feedback',...
                {'TuneSignals',...
                'Beta'},4,...
                'Temperature','Time',...
                @()Plot1D(1,'Beta_HRest_Hset','Beta_HS15_Hset','Hset','Rgr15','Beta_Hbg_Hset','WorkPoint')...
                },{  ...
                'Save',...
                }};
            
            
            Info.Scan.Analysis={...
                @()Plot2D(3,'Beta_.+','WorkPoint','Hset','Idc','Rgr15'),...
                ...@()Plot1D_with_color(4,'Beta_.+','WorkPoint','Hset')...
                };
            
    
                           S.Set('AVRest',5e-3);
    Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();
     Info.Scan.UserData.Beta_HRest_Hset=GetFromMemory('Beta_HRest_Hset');
     Info.Scan.UserData.Beta_Hbg_Hset=GetFromMemory('Beta_Hbg_Hset');
     Info.Scan.UserData.Beta_HS15_Hset=GetFromMemory('Beta_HS15_Hset');
     Info.Scan.UserData.Beta_HS2_Hset=GetFromMemory('Beta_HS2_Hset');
     Info.Scan.UserData.Beta_Sum=GetFromMemory('Beta_Sum');
    S.Set('AVRest',0.1e-3);
    
            S.Get(['Bridge',CalibrationLetter(i),MagnetLetter(j)]);
            S.Get('Feedback','TuneSignals','Beta');
            
            S.RunScan();
            
            S.Set('Xs_Ramp',XAxis(1));
            S.Set('Ys_Ramp',YAxis(1));
            S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));
            S.Set('WorkPoint',0);
            
            S.Get('Feedback','TuneSignals','Beta');
        end
    end
    S.Set('Magnet_I',0);
    Info.Flags.SuperScan = 0;
end
%%
% {-9.9,-8.9,-6.9,-5.0,-3.4,-2.7,-2.0,-1.4,-0.9,-0.4,0.0}
% [-9.9,-8.5,-6.8,-5,-3.4,-2.5,-2,-1.6,-1.2,-0.8,-0.4]
% done : -9.9,-5,-2.5,-1.6
% needed : -8.5,-6.8,-3.4,-2,-1.2,-0.8,-0.4
% BGToDo=[-6.8,-3.4,-2,-1.2,-0.8,-0.4];
TelegramSend('Started');pause(60);
% S.Set('RampBG',-9.9);
% BGToDo=[-0.267,-0.133,0.62,0.75,0.88,1.01,1.8];
BGToDo=[5];
for ggg=1:numel(BGToDo)
    if(ggg==3);TelegramSend('Electron side');pause(60);end
S.Get('Regular');
S.Get('Feedback','TuneSignals','Beta','Temperature');

S.Set('RampBG',BGToDo(ggg));
S.Get('Feedback','TuneSignals','Beta','Temperature');

pause(60);
Letter='U';
S.Set('AVRest',0.1e-3);
S.Get('Bridge');
S.Set('Sum',15e-3);
S.Set('WRc',1.6);
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'P'])
S.Set('WRc',-1.6);
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'N'])

% -6.85e-6,-10.55e-6
S.Set('Ys_Ramp',-10.55e-6);
pause(2);
S.Get('Feedback','TuneSignals','Beta','Temperature');

pause(60)
Letter='D';
S.Set('AVRest',0.1e-3);
S.Get('Bridge');
S.Set('Sum',15e-3);
S.Set('WRc',1.6);
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'P'])
S.Set('WRc',-1.6);
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'N'])

S.Set('Ys_Ramp',-6.85e-6);
S.Set('AVRest',5e-3);
S.Get('Feedback','TuneSignals','Beta','Temperature');
%
%   -6.85
%   -10.55
%BG to do -2.5,-1.6
% DSB single trace as specific W/Rc and BG

WRcList=[1.6,-1.6];

YAxis=linspace(-6.85e-6,-10.55e-6,29); 

CalibrationLetter='UD';
MagnetLetter='PN';
for k=1:4
    S.Set('Dummy',k);
    Info.Flags.SuperScan = 1;
    for i=1:numel(CalibrationLetter)
        for j=1:numel(WRcList)
            
            S.Set('WRc',WRcList(j));
            S.Get('KeepWRc');

            Info.Scan=[];
            Info.Scan.Description=['Y Magnet Scanner Feedback at specific BG and specific W/R'];
            Info.Scan.UserData.Magnet_I=GetFromMemory('Magnet_I');
            Info.Scan.UserData.WRc=GetFromMemory('WRc');
            Info.Scan.UserData.CalibrationPoint=CalibrationLetter(i);
            Info.Scan.UserData.BG=GetFromMemory('BG');
            Info.Scan.UserData.Sum=GetFromMemory('Sum');
            Info.Scan.UserData.Temperature=S.Get('Temperature');
    
            Info.Scan.WaitTimes=[0.2];
            
            Info.Scan.End=BuildStruct( ...
                'Magnet_I',0 ...
                );

            Info.Scan.Axis=BuildStruct( ...
                'Ys_Ramp',YAxis ...
                );
            
            Info.Scan.Get=      ...
                {{              ...
                'Feedback',     ...
                {'TuneSignals', ...
                'Beta'},8,      ...
                'Time','Temperature',...
                @()Plot1D(1,'Beta_HRest_Hset','Beta_HS15_Hset','Hset','Rgr15','Beta_Hbg_Hset','WorkPoint'),...
                'Save' ...
                }};
            
               S.Set('AVRest',5e-3);
    Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();
     Info.Scan.UserData.Beta_HRest_Hset=GetFromMemory('Beta_HRest_Hset');
     Info.Scan.UserData.Beta_Hbg_Hset=GetFromMemory('Beta_Hbg_Hset');
     Info.Scan.UserData.Beta_HS15_Hset=GetFromMemory('Beta_HS15_Hset');
     Info.Scan.UserData.Beta_HS2_Hset=GetFromMemory('Beta_HS2_Hset');
     Info.Scan.UserData.Beta_Sum=GetFromMemory('Beta_Sum');
    S.Set('AVRest',0.1e-3);

            S.Get(['Bridge',CalibrationLetter(i),MagnetLetter(j)]);
            S.Get('Feedback','TuneSignals','Beta');
            
            S.RunScan();
            
            S.Set('Ys_Ramp',YAxis(1));
            S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));
            S.Set('WorkPoint',0);
            
            S.Get('Feedback','TuneSignals','Beta');
        end
    end
    S.Set('Magnet_I',0);
    Info.Flags.SuperScan = 0;
end
end
%% As function of W/Rc

WRcToDo=[1:0.25:3,3.5:0.5:9];
NForWRcToDo=min([round(10./WRcToDo);8+WRcToDo*0]);
BGToDo=-5;
for ggg=1:numel(WRcToDo)
S.Get('Regular');
S.Get('Feedback','TuneSignals','Beta','Temperature');

S.Set('RampBG',BGToDo);
S.Get('Feedback','TuneSignals','Beta','Temperature');

pause(60);
Letter='U';
S.Set('AVRest',0.1e-3);
S.Get('Bridge');
S.Set('Sum',15e-3);
S.Set('WRc',WRcToDo(ggg));
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'P'])
S.Set('WRc',-WRcToDo(ggg));
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'N'])

% -6.85e-6,-10.55e-6
S.Set('Ys_Ramp',-10.55e-6);
pause(2);
S.Get('Feedback','TuneSignals','Beta','Temperature');

pause(60)
Letter='D';
S.Set('AVRest',0.1e-3);
S.Get('Bridge');
S.Set('Sum',15e-3);
S.Set('WRc',WRcToDo(ggg));
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'P'])
S.Set('WRc',-WRcToDo(ggg));
S.Get('KeepWRc');
S.Get('Bridge_Calibration')
S.Set(['Bridge',Letter,'N'])

S.Set('Ys_Ramp',-6.85e-6);
S.Set('AVRest',5e-3);
S.Get('Feedback','TuneSignals','Beta','Temperature');
%
%   -6.85
%   -10.55
%BG to do -2.5,-1.6
% DSB single trace as specific W/Rc and BG

WRcList=[WRcToDo(ggg),-WRcToDo(ggg)];

YAxis=linspace(-6.85e-6,-10.55e-6,29); 

CalibrationLetter='UD';
MagnetLetter='PN';
for k=1:NForWRcToDo(ggg)
    S.Set('Dummy',k);
    Info.Flags.SuperScan = 1;
    for i=1:numel(CalibrationLetter)
        for j=1:numel(WRcList)
            
            S.Set('WRc',WRcList(j));
            S.Get('KeepWRc');

            Info.Scan=[];
            Info.Scan.Description=['Y Magnet Scanner Feedback at specific BG and specific W/R'];
            Info.Scan.UserData.Magnet_I=GetFromMemory('Magnet_I');
            Info.Scan.UserData.WRc=GetFromMemory('WRc');
            Info.Scan.UserData.CalibrationPoint=CalibrationLetter(i);
            Info.Scan.UserData.BG=GetFromMemory('BG');
            Info.Scan.UserData.Sum=GetFromMemory('Sum');
            Info.Scan.UserData.Temperature=S.Get('Temperature');
    
            Info.Scan.WaitTimes=[0.2];
            
            Info.Scan.End=BuildStruct( ...
                'Magnet_I',0 ...
                );

            Info.Scan.Axis=BuildStruct( ...
                'Ys_Ramp',YAxis ...
                );
            
            Info.Scan.Get=      ...
                {{              ...
                'Feedback',     ...
                {'TuneSignals', ...
                'Beta'},4,      ...
                'Time','Temperature',...
                @()Plot1D(1,'Beta_HRest_Hset','Beta_HS15_Hset','Hset','Rgr15','Beta_Hbg_Hset','WorkPoint'),...
                'Save' ...
                }};
            
               S.Set('AVRest',5e-3);
    Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();
     Info.Scan.UserData.Beta_HRest_Hset=GetFromMemory('Beta_HRest_Hset');
     Info.Scan.UserData.Beta_Hbg_Hset=GetFromMemory('Beta_Hbg_Hset');
     Info.Scan.UserData.Beta_HS15_Hset=GetFromMemory('Beta_HS15_Hset');
     Info.Scan.UserData.Beta_HS2_Hset=GetFromMemory('Beta_HS2_Hset');
     Info.Scan.UserData.Beta_Sum=GetFromMemory('Beta_Sum');
    S.Set('AVRest',0.1e-3);

            S.Get(['Bridge',CalibrationLetter(i),MagnetLetter(j)]);
            S.Get('Feedback','TuneSignals','Beta');
            
            S.RunScan();
            
            S.Set('Ys_Ramp',YAxis(1));
            S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));
            S.Set('WorkPoint',0);
            
            S.Get('Feedback','TuneSignals','Beta');
        end
    end
    S.Set('Magnet_I',0);
    Info.Flags.SuperScan = 0;
end
end
%% Create A DSB bridge calibration list
BGList=[-9.9,-8.9,-6.9,-5.0,-3.4,-2.7,-2.0,-1.4,-0.9,-0.4,0.0];
WRc=1.6;
CalibrationListBG=[];

S.Set('RampYs',YTotal(2));
S.Set('Ys_Ramp',YTotal(1));
S.Set('Ys_Ramp',YTotal(2));
S.Set('Xs_Ramp',-11.75e-6);
S.Set('Xs_Ramp',-9.75e-6);
S.Set('Xs_Ramp',-10.75e-6);
S.Get('Feedback','TuneSignals','Beta','Temperature');
%
UD='D';

for i=1:numel(BGList)
    S.Set('RampBG',BGList(i));
    S.Set('SetGrapheneVsd',7.5e-3);
    for j=1:2
        S.Set('WRc',WRc*(-1)^(j-1));S.Get('KeepWRc');
        S.Get('Bridge_Calibration');
        CalibrationListBG=[CalibrationListBG,...
           struct('AVS2',GetFromMemory('AVS2'),'AVS15',GetFromMemory('AVS15'),...
            'BG',GetFromMemory('BG'),'WRc',GetFromMemory('WRc'),...
            'Sum',GetFromMemory('AVS15')-GetFromMemory('AVS2'),'UD',UD)];
    end
end


S.Set('Ys_Ramp',YTotal(1));
S.Get('Feedback','TuneSignals','Beta','Temperature');
UD='U';

for i=1:numel(BGList)
    S.Set('RampBG',BGList(i));
    S.Set('SetGrapheneVsd',7.5e-3);
    for j=1:2
        S.Set('WRc',WRc*(-1)^(j-1));S.Get('KeepWRc');
        S.Get('Bridge_Calibration');
        CalibrationListBG=[CalibrationListBG,...
           struct('AVS2',GetFromMemory('AVS2'),'AVS15',GetFromMemory('AVS15'),...
            'BG',GetFromMemory('BG'),'WRc',GetFromMemory('WRc'),...
            'Sum',GetFromMemory('AVS15')-GetFromMemory('AVS2'),'UD',UD)];
    end
end

S.Set('Xs_Ramp',-11.75e-6);
S.Set('Xs_Ramp',-9.75e-6);
S.Set('RampYs',YUp(1));

%% DSB profiles BG list scan - 281218 based on precalibrations


BGList=repmat([-9.9,-8.9,-6.9,-5.0,-3.4,-2.7,-2.0,-1.4,-0.9,-0.4,0.0],1,6); 

WRc=1.6;

% X -9.75     -> -11.75
% Y -10.55e-6 -> -15.35e-6

S.Set('Dummy',1);
for nm=1:numel(BGList)
    Info.Flags.SuperScan=1;
    S.Set('Dummy',GetFromMemory('Dummy')+1);

    %numbers
        XCenter = -10.75e-6;
        YCenter = -12.95e-6;
        DeltaY = [4.8e-6,2.87e-6,1.23e-6];
        DeltaX = 2e-6;
    
    %end numbers
    YUp    = [YCenter + DeltaY(2)/2, YCenter - DeltaY(3)/2];
    YDown  = [YCenter + DeltaY(3)/2, YCenter - DeltaY(2)/2];
    YTotal = [YCenter + DeltaY(1)/2, YCenter - DeltaY(1)/2];
    XRange = [XCenter + DeltaX   /2, XCenter - DeltaX   /2];
    
    YAxisUp   = linspace(YUp(1)   ,YUp(2)   ,7);
    YAxisDown = linspace(YDown(1) ,YDown(2) ,7);
    XAxis     = linspace(XRange(1),XRange(2),4);
    
    CalibrationLetter='UD';
    MagnetLetter='PN';
    for k=1:1
    for i=1:numel(CalibrationLetter)
        switch(CalibrationLetter(i))
            case 'U'
                YAxis=YAxisUp;
            case 'D'
                YAxis=YAxisDown;
        end
        for j=1:numel(MagnetLetter)
            
            S.Set('RampBG', BGList(nm));
           %S.Set('SetGrapheneVsd',7.5e-3);
            
            if(j==1)
                S.Set('WRc',WRc);S.Get('KeepWRc');
            else
                S.Set('WRc',-WRc);S.Get('KeepWRc');
            end
            
            Index=find(([CalibrationListBG.BG]==GetFromMemory('BG')) &...
                ([CalibrationListBG.WRc]==GetFromMemory('WRc')) &...
                [CalibrationListBG.UD]==CalibrationLetter(i));
            
            if(numel(Index)~=1);TelegramSend('Error');error('Somthing is wrong with calibration list');end

            S.Set('AVS2',CalibrationListBG(Index).AVS2);
            S.Set('AVS15',CalibrationListBG(Index).AVS15);
            
            if(CalibrationLetter(i)=='D')
                S.Set('RampYs',YDown(1));
            end
            
            S.Get('Feedback','TuneSignals','Beta','Temperature');
            Info.Scan=[];
            Info.Scan.Description=['Curvatures'];
            Info.Scan.UserData=struct('Magnet_I',GetFromMemory('Magnet_I'),'CalibrationPoint',CalibrationLetter(i),...
                'BG',Info.Channels.BG, 'WRc',GetFromMemory('WRc'),'Sum',GetFromMemory('Sum'),'Mark','DSB_Profile4');
            Info.Scan.WaitTimes=[0.2 1];
            
            Info.Scan.End=BuildStruct( ...
                'Magnet_I',0 ...
                );
            
            Info.Scan.Axis=BuildStruct( ...
                'Ys_Ramp', YAxis, ...
                'Xs_Ramp', XAxis ...
                );
            
            Info.Scan.Get=...
                {{                                            ...
                'Feedback',...
                {'TuneSignals',...
                'Beta'},7,...
                'Temperature','Time',...
                @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint')...
                },{  ...
                'Save',...
                }};
            Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();
            pause(1);
            
            S.RunScan();
            S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));S.Set('WorkPoint',0);
            S.Set('Xs_Ramp',XRange(1));
            S.Set('RampYs',YDown(2));
            S.Set('RampYs',YUp(1));
            S.Get('Feedback','TuneSignals','Beta');
        end
    end
    end
    Info.Flags.SuperScan = 0;
end
%% Create A DSB bridge calibration list for differnt SUM
SumList=[10e-3,15e-3,20e-3];
WRc=1.6;

S.Set('RampYs',YTotal(2));
S.Set('Ys_Ramp',YTotal(1));
S.Set('Ys_Ramp',YTotal(2));
S.Set('Xs_Ramp',-11.75e-6);
S.Set('Xs_Ramp',-9.75e-6);
S.Set('Xs_Ramp',-10.75e-6);
S.Get('Feedback','TuneSignals','Beta','Temperature');

 CalibrationList=[];
UD='D';
for i=1:numel(SumList)
    S.Set('Sum',SumList(i));
    for j=1:2
        S.Set('WRc',WRc*(-1)^(j-1));S.Get('KeepWRc');
        S.Get('Feedback','TuneSignals','Beta','Temperature');
        S.Get('Bridge_Calibration');
        CalibrationList=[CalibrationList,...
            struct('AVS2',GetFromMemory('AVS2'),'AVS15',GetFromMemory('AVS15'),...
            'BG',GetFromMemory('BG'),'WRc',GetFromMemory('WRc'),...
            'Sum',GetFromMemory('AVS15')-GetFromMemory('AVS2'),'UD',UD)];
    end
end

S.Set('Ys_Ramp',YTotal(1));
S.Get('Feedback','TuneSignals','Beta','Temperature');
UD='U';

for i=1:numel(SumList)
    S.Set('Sum',SumList(i));
    for j=1:2
        S.Set('WRc',WRc*(-1)^(j-1));S.Get('KeepWRc');
        S.Get('Feedback','TuneSignals','Beta','Temperature');
        S.Get('Bridge_Calibration');
        CalibrationList=[CalibrationList,...
            struct('AVS2',GetFromMemory('AVS2'),'AVS15',GetFromMemory('AVS15'),...
            'BG',GetFromMemory('BG'),'WRc',GetFromMemory('WRc'),...
            'Sum',GetFromMemory('AVS15')-GetFromMemory('AVS2'),'UD',UD)];
    end
end

S.Set('Xs_Ramp',-11.75e-6);
S.Set('Xs_Ramp',-9.75e-6);
S.Set('RampYs',YUp(1));
%Index=find(([CalibrationList.BG]==-2) & ([CalibrationList.WRc]==-1.6) & [CalibrationList.UD]==1);
%% DSB profiles Different Sum - 281218 based on precalibrations


%BGList=-9.9,-8.9,-6.9,-5.0,-3.4,-2.7,-2.0,-1.4,-0.9,-0.4,0.0;

% SumList=[10e-3,...
%     20e-3,15e-3,10e-3,...
%     20e-3,15e-3,10e-3,15e-3,10e-3...
%     20e-3,15e-3,10e-3,...
%     20e-3,15e-3,10e-3 ...
%     ];
SumList=[10e-3];

WRc=1.6;

% X -9.75     -> -11.75
% Y -10.55e-6 -> -15.35e-6

% S.Set('Dummy',1);
for nm=1:numel(SumList)
    Info.Flags.SuperScan=1;
    S.Set('Dummy',GetFromMemory('Dummy')+1);

    %numbers
        XCenter = -10.75e-6;
        YCenter = -12.95e-6;
        DeltaY = [4.8e-6,2.87e-6,1.23e-6];
        DeltaX = 2e-6;
    
    %end numbers
    YUp    = [YCenter + DeltaY(2)/2, YCenter - DeltaY(3)/2];
    YDown  = [YCenter + DeltaY(3)/2, YCenter - DeltaY(2)/2];
    YTotal = [YCenter + DeltaY(1)/2, YCenter - DeltaY(1)/2];
    XRange = [XCenter + DeltaX   /2, XCenter - DeltaX   /2];
    
    YAxisUp   = linspace(YUp(1)   ,YUp(2)   ,7);
    YAxisDown = linspace(YDown(1) ,YDown(2) ,7);
    XAxis     = linspace(XRange(1),XRange(2),4);
    
    CalibrationLetter='UD';
    MagnetLetter='PN';
    for k=1:1
    for i=1:numel(CalibrationLetter)
        switch(CalibrationLetter(i))
            case 'U'
                YAxis=YAxisUp;
            case 'D'
                YAxis=YAxisDown;
        end
        for j=1:numel(MagnetLetter)
            if(j==1)
                S.Set('WRc',WRc);S.Get('KeepWRc');
            else
                S.Set('WRc',-WRc);S.Get('KeepWRc');
            end
            S.Set('Sum', SumList(nm));
            Index=find(([CalibrationList.BG]==GetFromMemory('BG')) &...
                ([CalibrationList.WRc]==GetFromMemory('WRc')) &...
                [CalibrationList.UD]==CalibrationLetter(i) & ...
                abs([CalibrationList.Sum]-GetFromMemory('Sum'))<1e-6);
            if(numel(Index)~=1);error('Somthing is wrong with calibration list');end
            S.Set('AVS2',CalibrationList(Index).AVS2);
            S.Set('AVS15',CalibrationList(Index).AVS15);
            
            if(CalibrationLetter(i)=='D')
                S.Set('RampYs',YDown(1));
            end
            
            S.Get('Feedback','TuneSignals','Beta','Temperature');
            Info.Scan=[];
            Info.Scan.Description=['Curvatures'];
            Info.Scan.UserData=struct('Magnet_I',GetFromMemory('Magnet_I'),'CalibrationPoint',CalibrationLetter(i),...
                'BG',Info.Channels.BG, 'WRc',GetFromMemory('WRc'),'Sum',GetFromMemory('Sum'),'Mark','DSB_Profile4');
            Info.Scan.WaitTimes=[0.2 1];
            
            Info.Scan.End=BuildStruct( ...
                'Magnet_I',0 ...
                );
            
            Info.Scan.Axis=BuildStruct( ...
                'Ys_Ramp', YAxis, ...
                'Xs_Ramp', XAxis ...
                );
            
            Info.Scan.Get=...
                {{                                            ...
                'Feedback',...
                {'TuneSignals',...
                'Beta'},7,...
                'Temperature','Time',...
                @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint')...
                },{  ...
                'Save',...
                }};
            Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();
            pause(1);
            
            S.RunScan();
            S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));S.Set('WorkPoint',0);
            S.Set('Xs_Ramp',XRange(1));
            S.Set('RampYs',YDown(2));
            S.Set('RampYs',YUp(1));
            S.Get('Feedback','TuneSignals','Beta');
        end
    end
    end
    Info.Flags.SuperScan = 0;
end
%%
%% Create A DSB bridge calibration list W/Rc
BGList=linspace(-9.9,0,30);
WRc=1.6;

CalibrationListBG=[];
YForHall=[-11.3e-6,-14.4e-6]; %mid ~ -13e-6
%11.3, 14.4
%

%14.25
%11.65
% S.Set('Ys_Ramp',YForHall(1));
% S.Set('Ys_Ramp',YForHall(2));
% S.Set('Xs_Ramp',-11.75e-6);
% S.Set('Xs_Ramp',-9.75e-6);
% S.Set('Xs_Ramp',-10.75e-6);
S.Get('Feedback','TuneSignals','Beta','Temperature');
%
UD='D';
S.Set('RampBG',BGList(1));
S.Set('WRc',WRc);S.Get('KeepWRc');
S.Set('SetGrapheneVsd',10e-3);
S.Get('Bridge_Calibration');
for i=1:numel(BGList)
    S.Set('RampBG',BGList(i));
    S.Set('WRc',WRc);S.Get('KeepWRc');
%     S.Set('SetGrapheneVsd',10e-3);
        S.Get('Bridge_Calibration_SmallChange');
        S.Get('Feedback','TuneSignals','Beta','Temperature');
        CalibrationListBG=[CalibrationListBG,...
           struct('AVS2',GetFromMemory('AVS2'),'AVS15',GetFromMemory('AVS15'),...
            'BG',GetFromMemory('BG'),'WRc',GetFromMemory('WRc'),...
            'Sum',GetFromMemory('AVS15')-GetFromMemory('AVS2'),'UD',UD,...
            'IgrAC15',GetFromMemory('IgrAC15'))];
end

%%

S.Set('Ys_Ramp',YForHall(1));
S.Get('Feedback','TuneSignals','Beta','Temperature');
UD='U';
S.Set('RampBG',BGList(1));
S.Set('WRc',WRc);S.Get('KeepWRc');
S.Set('SetGrapheneVsd',10e-3);
S.Get('Bridge_Calibration');
for i=1:numel(BGList)
    S.Set('RampBG',BGList(i));
    S.Set('WRc',WRc);S.Get('KeepWRc');
        S.Get('Bridge_Calibration_SmallChange');
        S.Get('Feedback','TuneSignals','Beta','Temperature');
        CalibrationListBG=[CalibrationListBG,...
           struct('AVS2',GetFromMemory('AVS2'),'AVS15',GetFromMemory('AVS15'),...
            'BG',GetFromMemory('BG'),'WRc',GetFromMemory('WRc'),...
            'Sum',GetFromMemory('AVS15')-GetFromMemory('AVS2'),'UD',UD,...
            'IgrAC15',GetFromMemory('IgrAC15'))];
end


% S.Set('Xs_Ramp',-11.75e-6);
% S.Set('Xs_Ramp',-9.75e-6);
S.Set('RampYs',YForHall(1));

S.Set('WRc',0);S.Get('KeepWRc');
%% DSB profiles WRc list scan - 281218 based on precalibrations

WRcList=[0,[0.25:0.25:6],[0.5:0.25:4],[0.75:0.25:3],[0.25:0.25:1.5],[0.25:0.25:1]];
%60 values

% X -9.75     -> -11.75
% Y -10.55e-6 -> -15.35e-6

S.Set('Dummy',0);
for nm=1:numel(WRcList)
    Info.Flags.SuperScan=1;
    S.Set('Dummy',GetFromMemory('Dummy')+1);

    %numbers
        XCenter = -10.75e-6;
        YCenter = -12.95e-6;
        DeltaY = [4.8e-6,2.87e-6,1.23e-6];
        DeltaX = 2e-6;
    
    %end numbers
    YUp    = [YCenter + DeltaY(2)/2, YCenter - DeltaY(3)/2];
    YDown  = [YCenter + DeltaY(3)/2, YCenter - DeltaY(2)/2];
    YTotal = [YCenter + DeltaY(1)/2, YCenter - DeltaY(1)/2];
    XRange = [XCenter + DeltaX   /2, XCenter - DeltaX   /2];
    
    YAxisUp   = linspace(YUp(1)   ,YUp(2)   ,7);
    YAxisDown = linspace(YDown(1) ,YDown(2) ,7);
    XAxis     = linspace(XRange(1),XRange(2),4);
    
    CalibrationLetter='UD';
    MagnetLetter='PN';
    for k=1:1
    for i=1:numel(CalibrationLetter)
        switch(CalibrationLetter(i))
            case 'U'
                YAxis=YAxisUp;
            case 'D'
                YAxis=YAxisDown;
        end
        for j=1:numel(MagnetLetter)

            if(j==1)
                S.Set('WRc',WRcList(nm));S.Get('KeepWRc');
            else
                S.Set('WRc',-WRcList(nm));S.Get('KeepWRc');
            end
            
            Index=find(([CalibrationListWRc.BG]==GetFromMemory('BG')) &...
                ([CalibrationListWRc.WRc]==GetFromMemory('WRc')) &...
                [CalibrationListWRc.UD]==CalibrationLetter(i));
            
            if(numel(Index)~=1);TelegramSend('Error');error('Somthing is wrong with calibration list');end

            S.Set('AVS2',CalibrationListWRc(Index).AVS2);
            S.Set('AVS15',CalibrationListWRc(Index).AVS15);
            
            if(CalibrationLetter(i)=='D')
                S.Set('RampYs',YDown(1));
            end
            
            S.Get('Feedback','TuneSignals','Beta','Temperature');
            Info.Scan=[];
            Info.Scan.Description=['Curvatures'];
            Info.Scan.UserData=struct('Magnet_I',GetFromMemory('Magnet_I'),'CalibrationPoint',CalibrationLetter(i),...
                'BG',Info.Channels.BG, 'WRc',GetFromMemory('WRc'),'Sum',GetFromMemory('Sum'),'Mark','DSB_Profile4');
            Info.Scan.WaitTimes=[0.2 1];
            
            Info.Scan.End=BuildStruct( ...
                'Magnet_I',0 ...
                );
            
            Info.Scan.Axis=BuildStruct( ...
                'Ys_Ramp', YAxis, ...
                'Xs_Ramp', XAxis ...
                );
            
            Info.Scan.Get=...
                {{                                            ...
                'Feedback',...
                {'TuneSignals',...
                'Beta'},7,...
                'Temperature','Time',...
                @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint')...
                },{  ...
                'Save',...
                }};
            Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();
            pause(1);
            
            S.RunScan();
            S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));S.Set('WorkPoint',0);
            S.Set('Xs_Ramp',XRange(1));
            S.Set('RampYs',YDown(2));
            S.Set('RampYs',YUp(1));
            S.Get('Feedback','TuneSignals','Beta');
        end
    end
    end
    Info.Flags.SuperScan = 0;
end
%% Magnetic focusing - X cut near the (down) edge
% BGList=[-9.90,-8.50,-7.22,-6.00,-4.85,-3.78,-2.79,-1.88,-1.07,-0.37,0.20,0.60,0.80,1.20,1.77,2.47,3.28,4.19,5.18,6.25,7.40,8.62,9.90];
% WRcList=[6,-6,0,1,-1]; %0,1,-1
WRcList= -[0:0.25:5];%WRcList=[WRcList;-WRcList];
%WRcList=[-3];
% WRcList=WRcList(1:end);
for dummy=1:100
Info.Flags.SuperScan=1;
 S.Set('Dummy',dummy);
 for i=1:numel(WRcList)
     S.Set('WRc',WRcList(i));S.Get('KeepWRc');
     pause(2);
     S.Get('Feedback','TuneSignals','Beta','Temperature');
    

    Info.Scan=[];
    Info.Scan.UserData=struct(...
        'Beta_Sum',GetFromMemory('Beta_Sum'),...
        'WRc',GetFromMemory('WRc'),...
        'BG',GetFromMemory('BG')...
        );
    
    Info.Scan.Description=['X cut near the edge for levitov andreev'];
    Info.Scan.WaitTimes=[0.5];

    Info.Scan.Axis=BuildStruct(  ...
            'Xs_Ramp',linspace(-3e-6,-18e-6,41) ... -17.5e-6 -> -3e-6
         );

    Info.Scan.Get=...
        {{          ...
         'Feedback',...
         {'TuneSignals',...
         'Beta'},7,...
         'Temperature','Time','Save',...
         @()Plot1D(1,'Rgr15','Beta_HS15_Hset','Beta_HS2_Hset','Hset','Beta_Sum','WorkPoint')...
        }};
    
Info.Scan.UserData.Initial_Beta_Sum=GetBetaSum();

    S.RunScan();
    S.Set('Vg_Ramp', GetFromMemory('Vg_Initial'));S.Set('WorkPoint',0);
     S.Set('Xs_Ramp',-3e-6);
     S.Get('Feedback','TuneSignals','Beta','Temperature');
%     end
%     S.Set('Magnet_I',0);
%     S.Set('RampBG',0);
 end
 S.Set('WRc',0);S.Get('KeepWRc');
end
 