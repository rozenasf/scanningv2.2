BGList=[-9.90,-8.50,-7.22,-6.00,-4.85,-3.78,-2.79,-1.88,-1.07,-0.37,0.20,0.60,0.80,1.20,1.77,2.47,3.28,4.19,5.18,6.25,7.40,8.62,9.90];
figure(3);clf;
Curves={};

       for BGindex=1:11
           k=1;
      subplot(3,4,BGindex);

LoadScan(1550.01);Data=Rgr15-220;%Data=Rgr15;
TempLeg={};TempVal=[];HeaterVal=[];
for i=1:14
    plot(Data.slice_index(2,BGindex).slice_index(2,i),'-','linewidth',2);hold on
    Curves{k,BGindex}=Data.slice_index(2,BGindex).slice_index(2,i);k=k+1;
    TempVal(end+1)=Temperature.slice_index(2,BGindex).data(1,i);
    HeaterVal(end+1)=Info.Scan.Axis.Heater_Ramp(i);
    TempLeg{end+1}=sprintf('Temp=%0.2f',Temperature.slice_index(2,BGindex).data(1,i));

end
LoadScan(1550.04);Data=Rgr15-220;%Data=Rgr15;
for i=1:11
    plot(Data.slice_index(2,BGindex).slice_index(2,i),'-','linewidth',2);hold on
    Curves{k,BGindex}=Data.slice_index(2,BGindex).slice_index(2,i);k=k+1;
    TempVal(end+1)=Temperature.slice_index(2,BGindex).data(1,i);
    HeaterVal(end+1)=Info.Scan.Axis.Heater_Ramp(i);
    TempLeg{end+1}=sprintf('Temp=%0.2f',Temperature.slice_index(2,BGindex).data(1,i));

end

 %legend(TempLeg)
ChangePlotColor(@(~)flipud(hsv(25)));
title(sprintf('BG=%0.2f',BGList(12-BGindex)))
%     end
%     pause(30);
% end
       end
       Cell={};
       for j=1:size(Curves,2)
        Cell{j}=splice(Curves(:,j),iaxis('Temperature',TempVal));
       end
       Data=splice(Cell,iaxis('BG',fliplr(BGList(1:11))));Data.name='TwoPointResistance';