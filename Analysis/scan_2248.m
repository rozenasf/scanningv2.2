% ind=2249;
ind=2250;


[idgr,std_idgr]=LoadScan([ind],L); 
Idata('Phi',0.5+0.5*(Beta_HS15_Hset-Beta_HS2_Hset)./(Beta_HS15_Hset+Beta_HS2_Hset));

N_B=size(Phi,3);
%    figure(7);pcolor(Phi.*(Rgr15+120));AddTitle;colormap(jet);
figure(8);clf
for i=1:N_B
    subplot(N_B,1,i)
    pcolor(Phi.slice_index(3,i));colormap(jet);
    caxis([0.4 0.65])
%     caxis([0.6,0.8])
%         caxis([0 1])

    % axis equal
    AddGDS(GDS,[1,0,0,1,5,-3.5]*1e-6,0);
end

AddTitle;

%%
v=linspace(-0.04, 0.04,21);
figure(9);clf;
for i=1:N_B
    AS=Phi.slice_index(3,i)-Phi.slice_index(3,(N_B-i+1)); %AS.name='AntySym';
    subplot(3,3,i)
    contour(AS,v);%colormap(jet)
%    caxis([-0.04 0.04])
    AddGDS(GDS,[1,0,0,1,5,-3.5]*1e-6,0);
    xlim([min(Phi.axes{1}.data) max(Phi.axes{1}.data) ])
    ylim([ min(Phi.axes{2}.data) max(Phi.axes{2}.data)])

end

Phi1=Phi;
%% Overlay magnet with Phi data
v1=linspace(0.4, 0.8,41);
v=linspace(-0.05, 0.05,21);

AS=Phi.slice_index(3,1)-Phi.slice_index(3,(N_B));

figure(10);clf
contour(Phi.slice_index(3,round(N_B/2)),v1);
% caxis([0.4 0.8]);
hold on
contour(AS,v); %caxis([-0.04 0.04]);
hold off
axis equal;

