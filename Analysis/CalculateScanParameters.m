%% 
LoadScan([840],L);

Magnet_Constant=1/10*120e-3;
% Beta_Hbg_Hset.axes{2}.data=Beta_Hbg_Hset.axes{2}.data*Magnet_Constant;
% Beta_Hbg_Hset.axes{2}.name='B [T]';
% Beta_Hbg_Hset=Beta_Hbg_Hset*1;

names=fieldnames(Info.Scan.idgr);
for i=1:numel(names)
    switch class(Info.Scan.idgr.(names{i}))
        case 'idata'
        for j=1:numel(Info.Scan.idgr.(names{i}).axes)
           switch Info.Scan.idgr.(names{i}).axes{j}.name
               case 'Magnet_I'
                   Info.Scan.idgr.(names{i}).axes{j}.name='B [T]';
                   B=Info.Scan.idgr.(names{i}).axes{j}.data*Magnet_Constant;
                   Info.Scan.idgr.(names{i}).axes{j}.data=B;
           end
        end
    end
end
%% parameters
VSenseGain=1e3;RSense=120;
W=4*um_;
Vsd=Info.Dac.DispExs.VS14.Amp;
v_dirac=-4.47; %approximate for now
%%
% Beta_Sum_BGFIX=Beta_Hbg_Hset*4.2+Beta_HRest_Hset+Beta_HS14_Hset+Beta_HS6_Hset;
% Phi=0.5+0.5*(Beta_HS14_Hset-Beta_HS6_Hset)./(Beta_HS14_Hset+Beta_HS6_Hset);
% I=(Ggr6/VSenseGain/Info.Scaling.Ggr6)/RSense;
Idata('Beta_Sum_BGFIX',Beta_Hbg_Hset*4.2+Beta_HRest_Hset+Beta_HS14_Hset+Beta_HS6_Hset);
Idata('Phi',0.5+0.5*(Beta_HS14_Hset-Beta_HS6_Hset)./(Beta_HS14_Hset+Beta_HS6_Hset));
Idata('I',(Ggr6/VSenseGain/Info.Scaling.Ggr6)/RSense);
n= Vbg_to_n(Info.Channels.BG, v_dirac);
k_F_vec = sqrt(pi*abs(n));

Rc=r_cyclo(n, B).*sign(B);Rc(isnan(Rc))=inf;

%B=Magnet_I*Magnet_Constant;
%%
I1=mean(I,1);
Idata('V',Phi*Vsd);

%%
DPhi_DXs=I1*0;
for i=1:size(Phi.data,2)
   F=polyfit(Phi.axes{1}.data',Phi.data(:,i),1);
   DPhi_DXs.data(i)=F(1);
end
%%
RhoXX=Dphi_Dx*Vsd*W./I1;
Idata('RhoXX',RhoXX,'W/Rc',W./Rc);
plot(RhoXX)
%%
Idata('l_tr',1e6*( h_./(2*qe_^2*k_F_vec.*RhoXX))); % in um
