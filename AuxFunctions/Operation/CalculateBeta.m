function out=CalculateBeta()
    global Info S;
    I=sqrt(-1);
    BetaChannels=RegExpChannels('Beta_.+');
    Sum=0;
    for Channel=BetaChannels
        BreakText=regexp(Channel,'_','split');BreakText=BreakText{1};
        if(numel(BreakText)==3)
            [Slope,SlopeError]=FitData(BreakText{3},BreakText{2},1,Info.Scan.Runtime.Position);
            %S.Set(Channel{1},Slope+I*SlopeError);
            S.Set(Channel{1},Slope);
            %Sum=Sum+Slope+I*SlopeError;
            Sum=Sum+Slope;
        end
    end
    S.Set('Beta_Sum',Sum);
    out=1;
end