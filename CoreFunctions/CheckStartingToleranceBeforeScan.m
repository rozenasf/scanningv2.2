function CheckStartingToleranceBeforeScan()
global Info
ToCheck={};
Info.Scan.Runtime.HadBeenSetInScan=struct();
    if(isfield(Info.Scan,'Start'));ToCheck=[ToCheck,Info.Scan.Start];end
    if(isfield(Info.Scan,'Axis'));ToCheck=[ToCheck,Info.Scan.Axis];end
for Index=1:numel(ToCheck)
    names=fieldnames(ToCheck{Index});
    for i=1:numel(names)
        VectorToCheck=ToCheck{Index}.(names{i});
        ValueToCheck=VectorToCheck(1); %the first SET
        if(~CheckStartingTolerance(names{i},ValueToCheck))
            error('CheckStartingToleranceBeforeScan in runscan');
        end
    end
end
end
