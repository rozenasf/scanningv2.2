function out=ScanSet(Channel,Axis,Values)
    global Info S
    out=nan;
    S.Set(Channel,Values(Info.Scan.Runtime.Position(Axis)));
end