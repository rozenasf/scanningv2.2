classdef FlexodacAC_Driver < handle
    properties
        AC_Output
    end
    
    methods
        function obj=FlexodacAC_Driver()
            
        end
        function Map(obj,varargin)
            global S
            obj.AC_Output=[];
            S.Set('AC',0);
            for i=1:numel(varargin)
                S.Set(['A_',varargin{i}{1}],varargin{i}{2});
                S.Set(['F_',varargin{i}{1}],varargin{i}{3});
                obj.AC_Output.(varargin{i}{1})=struct('Amplitude',varargin{i}{2},'Frequancy',varargin{i}{3});
            end
        end
        function Frequancy=GetFrequency(obj,Channel)
            Frequancy=obj.AC_Output.(Channel).Frequancy;
        end
        
        %
        %-----
        %         function out=Get(obj,Channel)
        %             %                         out=GetFromMemory(Channel); %Change This!!!
        %                          out=nan;
        %         end
        function Set(obj,Channel,Value)
            global Info
            if(strcmp(Channel,'AC'))
                if(Value)
                    
                    disp('AC on!')
                else
                    
                    disp('AC off!')
                end
            else
                BareChannelName=Channel(3:end);
                switch(Channel(1))
                    case 'A'
                        obj.AC_Output.(BareChannelName).Amplitude=Value;
                    case 'F'
                        obj.AC_Output.(BareChannelName).Frequancy=Value;
                end
                
                Info.Flexodac.F.configure_sine( GetShocketFromName(BareChannelName) ,....
                    obj.AC_Output.(BareChannelName).Amplitude,obj.AC_Output.(BareChannelName).Frequancy,0);
%                 Info.Flexodac.F.configure_sine(,1)
            end
        end
        
    end
end