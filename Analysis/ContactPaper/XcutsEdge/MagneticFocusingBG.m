 %% X cuts close to the edge as functin of BG at different W/Rc
    figure(302);clf
for Extra=1:5
    subplot(2,3,Extra)
j=1+(Extra-1)*2;
for i=2:10
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j,i});Beta_Sum=0.51;
R1 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j+1,i});Beta_Sum=0.51;
R2 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
IData=R1-R2;
plot(IData.roi(1,-6.5,6.5),'.--','linewidth',1,'MarkerSize',10)
n = Vbg_to_n(Info.Channels.BG,0.7);
hold on
end

legend(num2str(SuperAxis.XEdge_BGlist_Holes_wrc.BG'),'Location','northeast')
TXYL(['W/Rc=',num2str(SuperAxis.XEdge_BGlist_Holes_wrc.W_Rc(j))],'X [um]','\Delta R')
end
suptitle(sprintf('Hole side')); 
