function OK=GetTuneSignals(varargin)
global S Info;
UpadateDacInformation();
if(~strcmp(Info.ChannelType.TuneSignals{1},'Sample'));error('Sample must be the first TuneSignals!');end
%    S.Get('Sample'); dont do this, the sample is the first TuneSignal
if(numel(varargin)==0)
    Average=1;
else
    Average=varargin{1};
end
for Av=1:Average
    for j=1:numel(Info.ChannelType.TuneSignals)
        S.Get(Info.ChannelType.TuneSignals{j});
    end
end
OK=1;
end