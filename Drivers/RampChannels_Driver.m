classdef RampChannels_Driver < handle
    properties
        MaxJump
        DefultMaxStep
    end
    %M3
    methods
        function obj=RampChannels_Driver(varargin)
            for i=1:numel(varargin)/2
                obj.MaxJump.(varargin{i*2-1})=varargin{i*2};
            end
            obj.DefultMaxStep=1e-3;
        end
        function Set(obj,Channel,Value)
            global S
                    if(~strcmp(Channel(end-4:end),'_Ramp'))
                        error('Must Be Channel_Ramp');
                    end
                    ChannelName=Channel(1:end-5);
                    if(isfield(obj.MaxJump,ChannelName))
                       diff= obj.MaxJump.(ChannelName);
                    else
                        diff= obj.DefultMaxStep;
                    end
                    InitialValue = GetFromMemory(ChannelName);
                    FinalValue = Value;
                    
                    ListOfValues=InitialValue:diff*sign(FinalValue-InitialValue):FinalValue;
                    ListOfValues=ListOfValues(2:end);
                    if(isempty(ListOfValues) || ListOfValues(end)~=FinalValue)
                        ListOfValues=[ListOfValues,FinalValue];
                    end
                    for i=1:numel(ListOfValues)
                        
                        S.Set(ChannelName,ListOfValues(i));
                        pause(0.1);
                    end
        end  

    end
end