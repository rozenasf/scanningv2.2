function y=TanhFit(x,a,b,c,d)
    %y=d+c.*erf(b.*(x-a));
    y=c*tanh((x-a).*b)+d;
end