
Y={};Traces={};

 ScanList=[1478:1512]; 

%%

Range=[-1.25,1.25];
CurvetureSTDList=[];WRcFullList=[];
for j=1:numel(ScanList)
CurvetureSTDList(j)=GetCurvetureFromDSB4(ScanList(j),Range);
WRcFullList(j)=Info.Channels.WRc;
end
%
WRcVec=unique(WRcFullList);CurvetureMean=[];CurvetureSTD=[];WRcVec=WRcVec(WRcVec~=0);
for i=1:numel(WRcVec)
CurvetureMean(i)=mean(CurvetureSTDList(find(WRcFullList==WRcVec(i))));
CurvetureSTD(i)=std(CurvetureSTDList(find(WRcFullList==WRcVec(i))))./sqrt(sum(WRcFullList==WRcVec(i))-1);
end
errorbar(-WRcVec,CurvetureMean,CurvetureSTD,'.--','linewidth',3,'markersize',40)
TXYL('Curveture as function of W/Rc @ BG=-5V','W/Rc','Curveture');
ylim([0,4.5]);
grid
%%
Curveture={};
for i=1:11
   Curveture{i}=[]; 
end

for j=1:numel(ScanList)
    Curveture{mod(j-1,11)+1}(end+1)=CurvetureSTDList(j);
end
CurvetureMean=[];Curveturestd=[];
for i=1:11
   CurvetureMean(i)=mean(Curveture{i});
   Curveturestd(i)=std(Curveture{i});
end
 errorbar(sqrt(-pi*Vbg_to_n(BGList,0.7)),CurvetureMean,Curveturestd/sqrt(4),'.--','linewidth',3,'markersize',40);
 ylim([0,4]);TXYL('Curveture at 75K','Kf [1/m]','Curveture');Ax=gca;Ax.FontSize=16;
%errorbar(BGList,CurvetureMean,Curveturestd,'.--','linewidth',3,'markersize',40);
%ylim([0,4]);TXYL('Curveture at 75K','BG [V]','Curveture');Ax=gca;Ax.FontSize=16;
%2.2942    2.0203    2.4966
%2.5023    2.1882    2.4935
grid;
CurvetureSTDList
%% Hall Voltage
AVS2List=[];
AVS15List=[];
IgrAC15List=[];
for i=1:11
    LoadScan(ScanList(j));
    for j=1:4
        LoadScan(ScanList(i)+0.01*j);
        AVS2List(i,j)=Info.Channels.AVS2;
        AVS15List(i,j)=Info.Channels.AVS15;
        IgrAC15List(i,j)=mean(IgrAC15.data(:));
    end
end
%%
clf
n=abs(Vbg_to_n(BGList,0.7));
plot(1./sqrt(pi*n),-mean(AVS2List(:,[1,4])-AVS2List(:,[2,3]),2)./mean(IgrAC15List,2),'o');
%
WRc_List=[1.6];
Wrc = WRc_List;
r_c=5e-6./Wrc;
    p_F=hbar_*sqrt(pi*n);
    B=p_F./qe_./r_c
   RHall=B./(n*qe_);
   hold on
   Fit_Line(1)
   plot(1./sqrt(pi*n),RHall)
xlim([0,3e-8]);ylim([0,120]);
TXYL('Hall Resistance from DSB calibration','1/Kf [1/m]','RHall [ohm]','Data','linearFit','Theory','location','east')
%%
plot(1./sqrt(pi*n),-(mean(AVS2List(:,[1,4])-AVS2List(:,[2,3]),2)./mean(IgrAC15List,2))./RHall','o');
TXYL('Ratio between measured Hall to expected hall','1/Kf','Ratio')
%%
Curveture=[];CurvetureSTD=[];
BGList=[];BGListFull=[];BetaSumList=[];
for i=1:numel(ScanList)
    LoadScan(ScanList(i)+0.01);BGListFull(i)=Info.Scan.UserData.BG;
    BetaSumList(i)=1./Info.Scan.UserData.Initial_Beta_Sum;
end
BGList_unique=uniquetol(BGListFull,1e-3);
for i=1:numel(BGList_unique)
        index=find( abs(BGList_unique(i)-BGListFull)<1e-3 );
        
        CurvetureSTDList=[];
        for j=1:numel(index)
            CurvetureSTDList(j)=GetCurvetureFromDSB4(ScanList(index(j)),Range);
        end
        [Curveture(i),WRcList(i),BG]=GetCurvetureFromDSB4(ScanList(index),Range);
        %BGList(i)=BG;
        if(numel(index)>1)
        CurvetureSTD(i)=std(CurvetureSTDList)./sqrt(numel(index)-1);
        else
            CurvetureSTD(i)=nan;
        end
        
end
    
figure(4112);clf
% WRcList_unique=unique(WRcList);Curveture_AVR=[];
Lmr_BG_Fit=[-9.38362e-05,-4.20564e-03,-6.39272e-02,-4.72690e-01,-3.20175e+00,2.54587e+00];
Lmr=polyval(Lmr_BG_Fit,BGList_unique);
errorbar(Lmr/5,Curveture,CurvetureSTD,'.','markersize',20,'linewidth',2);
TXYL('curveture in Ey','lMR/W','curveture')
Export.Figure3.Panel3.Curve1=[BGList;Curveture;CurvetureSTD];
%1016 is the first one
ylim([-2,4]);grid