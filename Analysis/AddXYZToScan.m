function AddXYZToScan()
global Info S
Letters='XYZUVW';
a={};
names=fieldnames(Info.Scan.Axis);
for j=1:numel(names)
   a{j}= Info.Scan.Axis.(names{j});
end
b = cell(1,numel(a));
[b{:}] = ndgrid(a{:});
for j=1:numel(names)
    S.CreateScanOutput(Letters(j));
    Info.Scan.idgr.(Letters(j)).data=b{j};
end
end