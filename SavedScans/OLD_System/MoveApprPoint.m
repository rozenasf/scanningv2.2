global Info S;
%Retract Zum first, then translate to a given point
%----------------------%
Info.Params.MoveApprPoint=struct('ZRetract',200e-6,'Xc',3.85e-3,'Yc',3.6e-3);
%----------------------%
Info.Scan=[];
%Start by retracting Zc
initZ=S.Get('Zr');
placeZ=initZ-Info.Params.MoveApprPoint.ZRetract;
S.Set('Zc',placeZ);
%Translate Y
S.Set('Yc', Info.Params.MoveApprPoint.Yc);
%Translate X
S.Set('Xc', Info.Params.MoveApprPoint.Xc);
%Restore Z value
Info.Params.ZApprCoarse=struct('TargetD',initZ,'Jumps',20e-6,'Vsd',30e-3);
D_ZApprCoarse();

