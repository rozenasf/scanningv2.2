%% Sharvin magnetic dependence
    ScanNameList={'Xcenter_Magnet_Holes','Xcenter_Magnet_Electrons'};
    ZeroFieldScans=[2,23];
    figure(203);clf;
    for j=1:numel(ScanNameList)
        
        figure(46);
        subplot(2,2,j);
        ScanName=ScanNameList{j};
        Vec=[];
        UnPackScan(Scans.Xcenter_BG{ZeroFieldScans(j)})
        Beta_Sum=0.51;
        R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
        plot(R,'-');hold on
        for i=1:numel(Scans.(ScanName))
            
            UnPackScan(Scans.(ScanName){i})
            Beta_Sum=0.51;
            R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
            plot(R,'-');hold on
        end   
       [dRdX,Sharvin_Right,Sharvin_Left,Vsd]=AnalyzeX_Cut(RangeMid,RangeRight,RangeLeft,InterpolationPoints);

        
        %
        figure(203);
        subplot(3,2,j); hold on
        [Xo,Yo1]=SortXY([0,SuperAxis.(ScanName).W_Rc],Sharvin_Right);
        [Xo,Yo2]=SortXY([0,SuperAxis.(ScanName).W_Rc],Sharvin_Left);
        R_sharvin_th =h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(SuperAxis.(ScanName).BG,0.7)))/5.0e-6;
        plot(Xo,(Yo1+fliplr(Yo1))/2./R_sharvin_th,'.--','linewidth',1,'markersize',10);
        plot(Xo,(Yo2+fliplr(Yo2))/2./R_sharvin_th,'.--','linewidth',1,'markersize',10);
        grid on
        n = Vbg_to_n(Info.Channels.BG,0.7)/10^15;
        TXYL(sprintf('Contact Resistance (Symmetrized) %0.2f *10^{12} cm^{-2}',n),'W/Rc','R/R_{Quantum}','Right','Left')
    end
    
    figure(46);
    close(gcf);
    
    
    for i = 1:4
LoadScan(946.01+i-1);
Beta_HS15_Hset = BreakIData(Beta_HS15_Hset);
BetaSum =0.51;
R = Beta_HS15_Hset/BetaSum.*mean(AVS15,1)./mean(IgrAC15,1);


R = CenterIDataAxis(R,[1],1e6)
figure(165); clf
plot_with_color(R,1);

 [dRdX,Sharvin_Right,Sharvin_Left,Vsd]=AnalyzeX_Cut(RangeMid,RangeRight,RangeLeft,InterpolationPoints);
 n=abs(Vbg_to_n(Info.Channels.BG,0.7)); %units of m-2
    p_F=hbar_*sqrt(pi*n);
    
    r_c=p_F./qe_./B;
    
    W_Rc=5e-6./r_c
    
figure(203);
 subplot(3,2,i+2);
 R_sharvin_th =h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(Info.Channels.BG,0.7)))/5.0e-6;
 plot(W_Rc,(Sharvin_Right+fliplr(Sharvin_Right))/2./R_sharvin_th ,W_Rc,(Sharvin_Left+fliplr(Sharvin_Left))/2./R_sharvin_th,'.--','linewidth',1,'markersize',10);
n = Vbg_to_n(Info.Channels.BG,0.7)/10^15;
TXYL(sprintf('Contact Resistance (Symmetrized) %0.2f *10^{12} cm^{-2}',n),'W/Rc','R/R_{Quantum}','Right','Left')
 grid on
%  SR(i,:) =(Sharvin_Right+fliplr(Sharvin_Right))/2./R_sharvin_th;
%  SL(i,:) = (Sharvin_Left+fliplr(Sharvin_Left))/2./R_sharvin_th;
    end
figure(165);
close(gcf);