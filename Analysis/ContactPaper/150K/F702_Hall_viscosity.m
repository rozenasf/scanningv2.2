% while(1)
%    try
%        if(~isempty(TelegramGet()))
           %% Up Location
ScanList=1590+(0.01:0.01:GetMaxSuperScanNumber(1590)/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[]
BGList=nan(23,1);
for k=1:numel(ScanList)
   
    LoadScan(ScanList(k));
    Bridge_Calibration_SmallChange.data=(Bridge_Calibration_SmallChange.data-fliplr(Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Bridge_Calibration_SmallChange.axes{1}.data,'o');
%     plot( Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Bridge_Calibration_SmallChange.axes{1}.data;
    n=abs(Vbg_to_n(Info.Scan.UserData.BG,0.515));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*n);
    B=p_F./qe_./r_c
    RHallTheory=B./(n*qe_);
    
%     plot(WRc_List,RHallTheory)
    %
%     clf
    Measured=Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
%     plot(WRc_List,Measured.data./Theory,'.')
    RatioData=Measured.data./Theory;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    for i=1:numel(UWRc)
        List(i)=mean(   RatioData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(RatioData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    BGList(k)=Info.Scan.UserData.BG;
    WRcListBare=linspace(-6,6,28);
Data2D(k,:)=List;
Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,List,abs(STDList./sqrt(Number)),'o')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3,3.8])
figure(10);
pcolor(UWRc,BGList,abs(Data2D));TXYL('Up side hall effect','W/Rc','BG[V]')
caxis([0.68,0.79]);colormap(jet(128))
colorbar
shading flat
 % TelegramSend({7,8});
% clf
% imagesc(Data2D)
%        end
%    end
%    pause(30);
% end
%% Down location:
tic
CurrentMax=GetMaxSuperScanNumber(1597);
%  while(1)
%     try
%         if(toc>3000)
%            tic; 
%            NewMax=GetMaxSuperScanNumber(1597);
%            if(NewMax==CurrentMax)
%               TelegramSend('Problem with scan progress');
%            end
%            CurrentMax=NewMax;
%         end
%         if(~isempty(TelegramGet()))
           
ScanList=1597+(0.01:0.01:GetMaxSuperScanNumber(1597)/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[];
S15=[];S2=[];Rest=[];
BGList=nan(23,1);
for k=1:numel(ScanList)
   
    LoadScan(ScanList(k));
    Bridge_Calibration_SmallChange.data=(Bridge_Calibration_SmallChange.data-fliplr(Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Bridge_Calibration_SmallChange.axes{1}.data,'o');
%     plot( Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Bridge_Calibration_SmallChange.axes{1}.data;
    n=abs(Vbg_to_n(Info.Scan.UserData.BG,0.515));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*n);
    B=p_F./qe_./r_c;
    RHallTheory=B./(n*qe_);
    
%     plot(WRc_List,RHallTheory)
    %
%     clf
    Measured=Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
%     plot(WRc_List,Measured.data./Theory,'.')
    RatioData=Measured.data./Theory;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    for i=1:numel(UWRc)
        List(i)=mean(   RatioData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(RatioData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    BGList(k)=Info.Scan.UserData.BG;
    Rest(k)=Info.Scan.UserData.Beta_HRest_Hset;
    S15(k)=Info.Scan.UserData.Beta_HS15_Hset;
    S2(k)=Info.Scan.UserData.Beta_HS2_Hset;
    WRcListBare=linspace(-6,6,28);
Data2D(k,:)=List;
Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,(List),abs(STDList./sqrt(Number)),'o')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3.3,3.8])
figure(9);


                        
pcolor(UWRc,BGList,abs(Data2D));TXYL('Down side hall effect','W/Rc','BG[V]')
caxis([0.68,0.79]);colormap(jet(128))
colorbar
shading flat
%% As a function of magnetic field, high quality at high field, BG=-4.85
tic
DiracLocation=0.515;
ScanList=1600+(0.01:0.01:GetMaxSuperScanNumber(1600)/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[];
S15=[];S2=[];Rest=[];
BGList=nan(23,1);
for k=1:numel(ScanList)
   
    LoadScan(ScanList(k));
    Bridge_Calibration_SmallChange.data=(Bridge_Calibration_SmallChange.data-fliplr(Bridge_Calibration_SmallChange.data))/2;

    hold on

    WRc_List=Bridge_Calibration_SmallChange.axes{1}.data;
    n=abs(Vbg_to_n(Info.Scan.UserData.BG,DiracLocation));

    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*n);
    B=p_F./qe_./r_c;
    RHallTheory=B./(n*qe_);

    Measured=Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;

    RatioData=Measured.data./Theory;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];Number=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    for i=1:numel(UWRc)
        List(i)=mean(   RatioData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(RatioData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    BGList(k)=Info.Scan.UserData.BG;
    Rest(k)=Info.Scan.UserData.Beta_HRest_Hset;
    S15(k)=Info.Scan.UserData.Beta_HS15_Hset;
    S2(k)=Info.Scan.UserData.Beta_HS2_Hset;
    WRcListBare=linspace(-6,6,28);
Data2D(k,:)=List;
Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,-(List),abs(STDList./sqrt(Number)),'o')
end
TXYL('Down side @BG=-4.85 High quality at high W/Rc value','W/Rc','measured/Expected')

grid

%% As a function of BG, maximum magnetic field Magnet=+-9.8
tic
DiracLocation=0.515;
            ScanList=1602+(0.01:0.01:GetMaxSuperScanNumber(1602)/100);
            figure(7);clf
            Data2D=[];Data2DI=[];
            BetaSumVector=[];
            S15=[];S2=[];Rest=[];
            BGList=nan(23,1);
            
            LoadScan(ScanList(1));
            Data1=Bridge_Calibration_SmallChange;
            I1=IgrAC15;
            LoadScan(ScanList(2));
            Data2=Bridge_Calibration_SmallChange;
            I2=IgrAC15;
            
            Measured=((Data1-Data2)/2)./((I1+I2)/2) * Info.Scan.UserData.Sum;
            Measured=Measured.data;
            BGList=Bridge_Calibration_SmallChange.axes{1}.data;
            UBG=unique(BGList);
            List=[];
            STDList=[];
            Number=[];
            
            n=abs(Vbg_to_n(UBG,DiracLocation));
B=Info.Scan.UserData.Magnet_I/10*120e-3;
    RHallTheory=B./(n*qe_);
   
            for i=1:numel(UBG)
                List(i)=mean(   Measured(BGList==UBG(i)) ./ RHallTheory(i)  ) ;
                STDList(i)=std(Measured(BGList==UBG(i)) ./ RHallTheory(i)) ;
                Number(i)=sum((BGList==UBG(i)));
            end
  
            errorbar(UBG,List,STDList./sqrt(Number),'o');
         TXYL('Down side @Magnet_I=+-9.8 as function of BG (sensitive to dirac location)','BG','measured/Expected')
grid
%%
Wrc=-8.6;
BG=-4.85;
%                     BG=-9.9;
qe_=1.6022e-19;
hbar_=1.0546e-34;

r_c=5e-6./Wrc;
n=abs(5.946e14*(BG-0.575));
p_F=hbar_*sqrt(pi*n);
B=p_F./qe_./r_c;
Magnet_I=B./120e-3*10

%  TelegramSend({7,8});
%  clf
% imagesc(Data2D)
%         end
%     end
%     pause(30);
%  end

%%
tic
CurrentMax=GetMaxSuperScanNumber(1597);
%  while(1)
%     try
%         if(toc>3000)
%            tic; 
%            NewMax=GetMaxSuperScanNumber(1597);
%            if(NewMax==CurrentMax)
%               TelegramSend('Problem with scan progress');
%            end
%            CurrentMax=NewMax;
%         end
%         if(~isempty(TelegramGet()))
           
ScanList=1597+(0.01:0.01:GetMaxSuperScanNumber(1597)/100);
figure(7);clf
Data2D=[];Data2DI=[];
BetaSumVector=[];
S15=[];S2=[];Rest=[];
BGList=nan(23,1);
for k=1:numel(ScanList)
   
    LoadScan(ScanList(k));
    Bridge_Calibration_SmallChange.data=(Bridge_Calibration_SmallChange.data-fliplr(Bridge_Calibration_SmallChange.data))/2;
    
    % plot( Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum./Bridge_Calibration_SmallChange.axes{1}.data,'o');
%     plot( Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum,'o');
    hold on
    % Fit_Line(1)
    %
    WRc_List=Bridge_Calibration_SmallChange.axes{1}.data;
    n=abs(Vbg_to_n(Info.Scan.UserData.BG,0.515));
    
    % Wrc = WRc_List-0.05;
    r_c=5e-6./WRc_List;
    p_F=hbar_*sqrt(pi*n);
    B=p_F./qe_./r_c;
    RHallTheory=B./(n*qe_);
    
%     plot(WRc_List,RHallTheory)
    %
%     clf
    Measured=Bridge_Calibration_SmallChange./IgrAC15 * Info.Scan.UserData.Sum;
    Theory=RHallTheory;
%     plot(WRc_List,Measured.data./Theory,'.')
    RatioData=Measured.data./Theory;
    UWRc=unique(WRc_List);
    List=[];
    STDList=[];
    BetaSumVector(k)=1./Info.Scan.UserData.Initial_Beta_Sum;
    for i=1:numel(UWRc)
        List(i)=mean(   RatioData(WRc_List==UWRc(i))  ) ;
        STDList(i)=std(RatioData(WRc_List==UWRc(i))) ;
        Number(i)=sum((WRc_List==UWRc(i)));
    end
    BGList(k)=Info.Scan.UserData.BG;
    Rest(k)=Info.Scan.UserData.Beta_HRest_Hset;
    S15(k)=Info.Scan.UserData.Beta_HS15_Hset;
    S2(k)=Info.Scan.UserData.Beta_HS2_Hset;
    WRcListBare=linspace(-6,6,28);
Data2D(k,:)=List;
Data2DI(k,:)=IgrAC15.data;
    errorbar(UWRc,(List),abs(STDList./sqrt(Number)),'o')
end
ChangePlotColor
figure(8);plot(BetaSumVector); ylim([3.3,3.8])
figure(9);
%pcolor(UWRc,BGList,abs(Data2D));TXYL('One side hall effect','W/Rc','BG[V]')
%caxis([0.7,0.76]);colormap(jet(128))
%colorbar
%shading flat

%  TelegramSend({7,8});
 clf
% imagesc(Data2D)
%         end
%     end
%     pause(30);
%  end


%%
Start=0;MinJump=0.075;Max=6;BreakPoint=1;
List=Start:MinJump:BreakPoint;
while(List(end)<Max)
    List(end+1)=List(end)+MinJump*List(end)/BreakPoint;
end
List(end)=Max;
List=[-fliplr(List),List(2:end)];numel(List)

plot((List),'o')
% hist(List,100)
%%
ScanList=[1580:1583];
NoiseLevel=[];
for i=1:numel(ScanList)
      clf
 LoadScan(ScanList(i));
plot( Bridge_Calibration_SmallChange * Info.Scan.UserData.Sum);
  Fit_Line(1);
%  hold on
  NoiseLevel(i)=std(Fit.Y-Fit.Func(Fit.X))
end
%hold on
%LoadScan(1578.01);
%plot( (Bridge_Calibration_SmallChange-1)./(Bridge_Calibration_SmallChange+1) * Info.Scan.UserData.Sum);
 %%
 a=[1.2541   -0.0049

    1.2085   -0.0032

    1.1629   -0.0008

    1.1173    0.0013

    1.0717    0.0036

    1.0261    0.0053];
plot((a(:,1)-1)./(a(:,1)+1),a(:,2));
Fit_Line(1);
NewRatio=(Fit.X_0+1)/(1-Fit.X_0)
%%
plot(a(:,1),a(:,2));Fit_Line(1);
Fit.X_0
%%
List=[];
X=linspace(-2,2,10);
Y=X;plot(X,Y+rand(size(Y))-0.5,'o');
Fit_Line(1);
% List(end+1)=
std(Fit.Y-Fit.Func(Fit.X))

%%
WRcListBare=linspace(-6,6,20);
Factor=5;
MaxN=5;
N=min([MaxN+0*WRcListBare;ceil(abs(Factor./WRcListBare))]);
WRcList=[];
for i=1:numel(WRcListBare)
    for j=1:N(i)
        WRcList=[WRcList, WRcListBare(i)];
    end
end
plot(WRcList,'o')
%%
WRcListBare=linspace(-6,6,28);
Factor=25;
MaxN=25;
N=min([MaxN+0*WRcListBare;ceil(abs(Factor./WRcListBare))]);
WRcList=[];
for i=1:numel(WRcListBare)
    for j=1:N(i)
        WRcList=[WRcList, WRcListBare(i)];
    end
end
numel(WRcList)*10/54
plot(WRcList,'o')

%%

BGList=[-9.90,-8.50,-7.22,-6.00,-4.85,-3.78,-2.79,-1.88,-1.07,-0.37,0.20,0.60,0.80,1.20,1.77,2.47,3.28,4.19,5.18,6.25,7.40,8.62,9.90];

ExtendedBGList=interp1(1:23,BGList,1:0.5:23);
ExtendedBGList(22)=[];
ExtendedBGList
