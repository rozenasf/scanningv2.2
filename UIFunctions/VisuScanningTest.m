%% PathToSource
SourcePath='C:/Users/asafr/Documents/physics_ebeam';
%% Add path
addpath(genpath(SourcePath));
%%
Design_GDS=ExtractLayers(gds_load(['C:\Users\asafr\Desktop\GDSScanningGames\Full_C.gds']),'Wafer',[2,4,39]);
%Design_GDS=applyTransform(Design_GDS,'scale(1e-6,1e-6)yflip()');Design_GDS=applyTransform(Design_GDS,'translate(-460.5958e-6,-18.2575e-6)');clf;PlotGDS(Design_GDS);set(gca,'Xdir','reverse')
Design_GDS=applyTransform(Design_GDS,'scale(1e-6,1e-6)');Design_GDS=applyTransform(Design_GDS,'translate(3.2354e-03,3.9682e-03)');clf;PlotGDS(Design_GDS);set(gca,'Xdir','reverse');set(gca,'Ydir','reverse')
%%
Design_GDS2=applyTransform(Design_GDS,'skewx(45)');
%Design_GDS2=applyTransform(Design_GDS2,'translate(-1000,-1000)');
%Design_GDS2=applyTransform(Design_GDS2,'yflip()');
Design_GDS2=applyTransform(Design_GDS,'skewx(45)');
PlotGDS(Design_GDS2)
%%
PlotGDS(Design_GDS)
%%
clf
PlotGDS(applyTransform(Design_GDS,'matrix(1,0,0,1,1e-3,0)'))