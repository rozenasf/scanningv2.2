  %% 	BG=9.9 -8.5 , W/Rc=0,1,1.5,2,3,4,6
    ScanNameList={'XY_BG_Electrons_one_contacts','XY_BG_Holes_one_contacts'};
    figureindex = 104;
    
    for j=1:numel(ScanNameList)
        ScanName=ScanNameList{j};
        figure(figureindex+j);clf;colormap(jet(16));shading flat
        suptitle(ScanNameList{j}(7:end))
        
        for i=1:numel(Scans.(ScanName))/2+1
            subplot(3,3,i)
            UnPackScan(Scans.(ScanName){2*i-1})
            Beta_Sum=0.51;
            R = CenterIDataAxis(BreakIData(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15),[1,2],1e6);
            WW=WorkPoint+Vg_loc;
            pcolor(R);shading flat;axis equal;
            hold on
            contour(R,20,'LineColor','k');
            TXYL(sprintf('R [ohm], W/Rc=%0.2f',SuperAxis.(ScanName).W_Rc(2*i-1)),'X(scaled) [um]','Y [um]');
            ScaleColorFromIData(R.roi(1,-7,7))
            grid on
            
        end
    end