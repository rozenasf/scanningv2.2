   [idgr,std_idgr]=LoadScan([2399],L); %943
   figure(1)
   V=Beta_HS15_Hset/0.4*Info.Channels.AVS15_e-Info.Channels.AVS2_e./(Info.Channels.AVS15_e-Info.Channels.AVS2_e);V.name='V';
   R=Beta_HS15_Hset/0.4*Info.Channels.AVS15_e./IgrAC15;R.name='R';
   %V=Beta_HS15_Hset/0.4*Info.Channels.AVS15_e;V.name='V';
   plot(R);
   %
   out=[];
    xlim([-14,-9]*1e-6);
    out{1}=Fit_Line(1);
    xlim([-9,-6]*1e-6);out{2}=Fit_Line(1);
    xlim([-6,-2]*1e-6);out{2}=Fit_Line(1);
    xlim([-14,-2]*1e-6);
    grid
    AddTitle
    %
    gray = [0 0 0];
    Ax=gca;
    Ylim=ylim;
    P=patch(gca,[-9,-6,-6,-9]*1e-6,[Ylim(1),Ylim(1),Ylim(2),Ylim(2)],gray,'FaceAlpha',0.1,'LineStyle','none');
    Ax.Children=[Ax.Children(2:end);Ax.Children(1)];
    
  