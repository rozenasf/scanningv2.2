classdef ConvCap2Z_Driver < handle
    properties
        FitDataX
        FitDataY
        Map
    end
    methods
        function obj=ConvCap2Z_Driver()
            load('ConvCap2Z_Data','FitDataX','FitDataY');
            obj.FitDataX=FitDataX;
            obj.FitDataY=FitDataY;
            obj.Map=BuildStruct('ZsCapXum',{'ZsCapX','FitDataX'},'ZsCapYum',{'ZsCapY','FitDataY'});
        end
        function out=Get(obj,Channel)
            out=interp1(obj.(obj.Map.(Channel){2})(:,2),obj.(obj.Map.(Channel){2})(:,1),GetFromMemory(obj.Map.(Channel){1}));
        end
%         function out=GetAll(obj,Channel)
%             global Info S
%             if(~IsIdgr(Channel));S.CreateScanOutput(Channel);end
%             Info.Scan.idgr.(Channel).data=interp1(obj.(obj.Map.(Channel){2})(:,2),obj.(obj.Map.(Channel){2})(:,1),Info.Scan.idgr.(obj.Map.Channel{1}).data);
%             out=Info.Scan.idgr.(Channel);
%         end
    end
end