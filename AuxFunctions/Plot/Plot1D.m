    function out=Plot1D(Fig,varargin)
        global Info S;
        varargin=unique(RegExpChannels(varargin));
        %varargin=InitiateNewFunction(varargin);clf
        out=0;
        figureN(Fig);clf
        %SP=OptimizeSP(numel(varargin)-1);
        SP=OptimizeSP(numel(varargin));
        for j=1:numel(varargin)
            subplot(SP(1),SP(2),j);
            if(isfield(Info.Scan.idgr,varargin{j}))
            idgr=Info.Scan.idgr.(varargin{j});
            if(size(idgr,1)~=1)
                for i=2:numel(Info.Scan.Runtime.Position)
                    idgr=idgr.slice_index(2,Info.Scan.Runtime.Position(i));
                end
            end
            idgr.plot;
            end
        end
        title(['\fontsize{12} Scan ',num2str(Info.ScanNumber)])
        drawnow;
    end