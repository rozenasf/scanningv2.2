
%%
%H:\Measurements\Scanning SET technique\Data\F5_Manchester1
%H:\Measurements\Scanning SET technique\Data\F5_Wafer2_Design6_SET_Platinum
%% added this line just to make sure that the new test system soesnt harm the source control
%%
SetActiveNotebook('F5_ManchesterA');
%% Asaf Version

[idgr,std_idgr]=LoadScan([],L);
while(Info.Flags.RunningScan)
    %%
    try
        [idgr,std_idgr]=LoadScan([],L); %943
        names=fieldnames(Info.Scan.idgr);
        
%               for i=1:numel(names)
%                  if(strcmp(class( Info.Scan.idgr.(names{i})),'idata'))
%                      Info.Scan.idgr.(names{i})=Info.Scan.idgr.(names{i}).roi(2,-29e-6,-21e-6);
%                      
%                      
%                  end
%               end
        Info.Scan.Description
        Info.Scan.Axis
        PlotScan();
      
        %Plot(1,'Idc','Hset','Hbg','HS6','HS14','HRest')
       
%         Idata('Beta_Sum_BGFIX',Beta_Hbg_Hset*4.2+Beta_HRest_Hset+Beta_HS14_Hset+Beta_HS6_Hset);
%         VSenseGain=1e3;RSense=120;
%         Idata('I',(Ggr6/VSenseGain/Info.Scaling.Ggr6)/RSense);
%         Idata('V',Phi*Info.Dac.DispExs.VS14.Amp);
        
        
%         plot(Phi);Fit_Line(1);SlopeError=Fit.Std/(max(Fit.X)-min(Fit.X));
%         title(sprintf('slope = %e, error = %e, SNR = %0.2f', Fit.a,SlopeError,Fit.a/SlopeError));
%         figure(8);plot_with_color(Rgr,2);
         ClipSend()
%          [Info.Channels.BG,Info.Channels.Magnet_I]
    end
    %%
%         drawnow;
%    AddGDS(GDS,[1,0,0,1,-3,-3.8]*1e-6,0);  AddTitle;
%        AddGDS(GDS,[1,0,0,1,-5.4,-5.9]*1e-6,0);AddTitle;
%    
%             Idata('Phi',0.5+0.5*(Beta_HS15_Hset-Beta_HS2_Hset)./(Beta_HS15_Hset+Beta_HS2_Hset));
%          figure(7);pcolor(Phi.*(Rgr15+120));AddTitle;colormap(jet);axis equal

   
%      Idata('Phi',0.5+0.5*(Beta_HS14_Hset-Beta_HS6_Hset)./(Beta_HS14_Hset+Beta_HS6_Hset));
%      figure(7);clf;
%      pcolor(Phi);
%      axis equal
%      caxis([0,1]);
%      colormap(jet(512))
% figure(7);clf;
%      Plot1D_with_color(7,'Beta_HS14_Hset','Beta_HS6_Hset');AddTitle
%      [Beta_HS14_Hset_sym,Beta_HS14_Hset_asym]=sym_asym(Beta_HS14_Hset,2);
%       figure(5); plot_with_color(Beta_HS14_Hset_asym,1);
%      Idata('Phi',0.5+0.5*(Beta_HS14_Hset-Beta_HS6_Hset)./(Beta_HS14_Hset+Beta_HS6_Hset));

% Beta_S=Beta_HS14_Hset+Beta_HS6_Hset+Beta_HRest_Hset+3.8*Beta_Hbg_Hset;
% figure(6);clf;  pcolor(Beta_S)
%     AddGDS(GDS,[1,0,0,1,4.0,-6.9]*1e-6,0);  AddTitle;

% Phi=0.5+0.5*(Beta_HS14_Hset-Beta_HS6_Hset)./(Beta_HS14_Hset+Beta_HS6_Hset);
% Phi.name='Phi';
% % figure(10);plot_with_color(Phi.roi(2,-10e-6,-5e-6),1)
% figure(10);plot_with_color(Phi,1)

% Channel=(Beta_HS14_Hset+Beta_HS6_Hset)./Beta_Sum;
% 
% figure(10);plot(Channel)
% Fit_Line(@(x)tanh(x),'all')
% %
% figure(11)
% afit=[Fit.fit];
% plot(Beta_HS14_Hset.axes{2}.data(1:size(afit,2)),1./afit(1,:),'o')


    %%
   % iplot({Phi,idgr},99)
    %%
pause(10)
end
TelegramSend('Done');
%TelegramSend(3);
%%
figure(12);
plot( (Beta_HC3_Hset-Beta_HC6_Hset)./Beta_Sum)
%%
Fit_Line(@(x)tanh(x),'all');
figure(13);
plot(1./Beta_Sum.map_vectors(@(x)mean(x),1).data(1,1:numel([Fit.a])),1./[Fit.a],'o');
out=Fit_Line(1);
xlabel('1/BetaSum');ylabel('width');
figure(14);
plot(Zs0(1:numel([Fit.a])),1./[Fit.a],'o');
out2=Fit_Line(1);
xlabel('Zs0');ylabel('width');
%TelegramSend('Done')
%%
% figure(10);idgr.Ggr1.pcolor;colormap(jet)
% figure(11);idgr.R1.pcolor;colormap(jet)
 [idgr,std_idgr]=LoadScan([326],L);
Plot1D_with_color(10,'Ggr1','R1','Ggr6','R6')

idgr.Ggr1.data=idgr.Ggr1.data-idgr.Ggr1.slice_index(2,2).data'*ones(21,1)';
idgr.Ggr6.data=idgr.Ggr6.data-idgr.Ggr6.slice_index(2,2).data'*ones(21,1)';
idgr.R1.data=idgr.R1.data-idgr.R1.slice_index(2,2).data'*ones(21,1)';
idgr.R6.data=idgr.R6.data-idgr.R6.slice_index(2,2).data'*ones(21,1)';

Plot1D_with_color(11,'Ggr1','R1','Ggr6','R6')

%%
 [idgr,std_idgr]=LoadScan([336],L);
Plot1D_with_color(10,'R1')
%xlim([1.1 1.4])
%Fit_Line(2,'all');
%figure(12);plot(idgr.R1.axes{2}.data,[Fit.dX_0])

%%
for k=1:10
%%

try [idgr,std_idgr]=LoadScan(54); catch disp('Problem!!');end
% 1D plots
names=[RegExpChannels('Idc'),RegExpChannels('Hset'),RegExpChannels('Hbg'),RegExpChannels('Beta_.+'),RegExpChannels('WorkPoint')]; %
% names=[RegExpChannels('Beta_.+'),'Hset']; %,'WorkPoint',RegExpChannels('Beta_.+')

% names=[RegExpChannels('Idc'),RegExpChannels('Hset'),RegExpChannels('Beta_.+_.+')];

% names=[RegExpChannels('L...+')];
% names=[RegExpChannels('ZsCap.+')];
%if strcmp(idgr.ZsCapX.axes{2}.name,'Zo')
%    idgr.ZsCapX.axes{2}.data=cumsum(idgr.ZsCapX.axes{2}.data);idgr.ZsCapY.axes{2}.data=cumsum(idgr.ZsCapY.axes{2}.data);
%end
if(numel(fieldnames(Info.Scan.Axis))==1)
    %names=[Info.ChannelType.TuneSignals(2:end)];

    Plot1D(8,names{:});
    suptitle(['\fontsize{16} Scan ',num2str(Info.ScanNumber),newline,'\fontsize{10}',Info.Scan.Description])
    %print -clipboard -dmeta;
elseif(numel(fieldnames(Info.Scan.Axis))==2)
    %names=[Info.ChannelType.TuneSignals(2:end)];

    %print -clipboard -dmeta;
    Plot2D(2,names{:});
    
%     Plot2D(Info.ScanNumber+1000,names{:});
    suptitle(['\fontsize{16} Scan ',num2str(Info.ScanNumber),newline,'\fontsize{10}',Info.Scan.Description])
        Plot1D_with_color(1,names{:}); %Info.ScanNumber
    suptitle(['\fontsize{16} Scan ',num2str(Info.ScanNumber),newline,'\fontsize{10}',Info.Scan.Description])
    
         
end

a=fieldnames(Info.Scan.Axis);
if strcmp(a{end},'Zc')||strcmp(a{end},'Zs0')
    PlotApproach(6 ,'Beta_Sum');suptitle(['\fontsize{16} Scan ',num2str(Info.ScanNumber),newline,'\fontsize{10}',Info.Scan.Description])
end

% 2D Plots

ClipSend()

%%
pause(10)
end
%%
for  i=[143,144,145,148,149]
    idgr=LoadScan(i)
PlotApproach(4 ,'Beta_LHgold_LHgate')
hold on
end
hold off

%%
idgr=LoadScan(247)
figure(200);plot(idgr.Beta_HC6_Hset)
stepdata_st = fit_step_width(idgr.Beta_HC6_Hset, [0.35,1e-6,-2e-5,0], 1)
% suptitle(['\fontsize{16} Scan ',num2str(Info.ScanNumber),newline,'\fontsize{10}',Info.Scan.Description])
%% Signal To Noise
figure(3)
q=idgr.LHgate./idgr.LNoise;plot_with_color(q,1,@jet)
figure(4)
q=idgr.LHgate./std_idgr.LHgate;plot_with_color(q,1,@jet)
%%
idgr1=LoadScan(841);
zs1=Info.SetChannels.Zs0
idgr2=LoadScan(830);
zc2=Info.SetChannels.Zc
figure(5);
idgr1.Beta_Sum.axes{1}.data=idgr1.Beta_Sum.axes{1}.data+zs1;
idgr2.Beta_Sum.axes{1}.data=idgr2.Beta_Sum.axes{1}.data+zc2;
plot(idgr2.Beta_Sum);
hold on
plot(idgr1.Beta_Sum);
hold off
%%
idgr1=LoadScan(779); %Yc scan
ys1=Info.SetChannels.Ys0
idgr2=LoadScan(778); %Ys scan
yc2=Info.SetChannels.Yc
fact=1.6;
figure(5);
idgr1.Beta_Hm_Hset.axes{1}.data=idgr1.Beta_Hm_Hset.axes{1}.data+ys1/fact;
idgr2.Beta_Hm_Hset.axes{1}.data=(idgr2.Beta_Hm_Hset.axes{1}.data)/fact+yc2;
plot(idgr2.Beta_Hm_Hset);
hold on
plot(idgr1.Beta_Hm_Hset);
hold off
%%
idgr1=LoadScan(794); %Xc scan 791
xs1=Info.SetChannels.Xs0
idgr2=LoadScan(795); %Xs scan 790
xc2=Info.SetChannels.Xc
fact=1;
figure(5);
idgr1.Beta_Hm_Hset.axes{1}.data=idgr1.Beta_Hm_Hset.axes{1}.data+xs1/fact;
idgr2.Beta_Hm_Hset.axes{1}.data=(idgr2.Beta_Hm_Hset.axes{1}.data)/fact+xc2;
plot(idgr2.Beta_Hm_Hset);
hold on
plot(idgr1.Beta_Hm_Hset);
hold off
%%
%while(1)
ind=286;
[idgr,~]=LoadScan(ind) %247
data=idgr.ZsCapY.data;
data=data(~isnan(data));
figure(ind)
plot(smooth(data,20))
%pause(3)
%end
%%
ind=299;

LoadScan(ind) %247
ConvCap2Z();
a=regexp(fieldnames(Info.Scan.idgr),'^ZsCap.+$','match');
names=[a{:}]
% data=Info.Scan.idgr.ZsCapYum.data;
% names=[RegExpChannels('ZsCap.+')];
Plot1D(Info.ScanNumber,names{:});
%Plot1D(Info.ScanNumber,'ZsCapXum');
suptitle(['\fontsize{16} Scan ',num2str(Info.ScanNumber),newline,'\fontsize{10}',['Dummy with Zs=' num2str(Info.SetChannels.Zs0)]])
% data=data(~isnan(data));
% figure(ind)
% plot(smooth(data,20))

 %%
[idgr,std_idgr]=LoadScan(252);
%%
Vec=linspace(-1.1e-6,11.1e-6,1000);
fit=polyfit(idgr.ZsCapX.axes{1}.data,idgr.ZsCapX.data,6);
plot(idgr.ZsCapX)
hold on
plot(Vec,polyval(fit,Vec),'m')
hold off
%
FitDataX=[Vec',polyval(fit,Vec)'];
%% FitDataX=FitDataX(1:200,:);

Vec=linspace(-1.1e-6,11.1e-6,1000);
fit=polyfit(idgr.ZsCapY.axes{1}.data,idgr.ZsCapY.data,6);
plot(idgr.ZsCapY)
hold on
plot(Vec,polyval(fit,Vec),'m')
hold off
%

%
FitDataY=[Vec',polyval(fit,Vec)'];
%FitDataY=FitDataY(1:200,:)
%%
% % RealZ=interp1(FitData(:,2),FitData(:,1),idgr.ZsCapX.data);
% % RealZFit=interp1(FitData(:,2),FitData(:,1),polyval(fit,idgr.ZsCapX.axes{1}.data));
% % plot(idgr.ZsCapX.axes{1}.data,RealZ-RealZFit,'m')
% %%
% [idgr,std_idgr]=LoadScan();
% figure(2)
% data=idgr.ZsCapX.data;
% data=data(~isnan(data));
% plot(interp1(FitDataX(:,2),FitDataX(:,1),data))
% ylabel('Estimated Z (um)')
% title('creep after full retract - overnight - 6:50 H')
% %%
% figure(1)
% data=idgr.ZsCapY.data;
% data=data(~isnan(data));
% plot(interp1(FitDataY(:,2),FitDataY(:,1),data))
% ylabel('Estimated Z (um)')
% title('creep after full retract - overnight - 6:50 H')
%%
[idgr,std_idgr]=LoadScan(420);
fit={};std_fit={};

for line=1:20

%subplot(2,1,1)
Y=std_idgr.Hset.slice_index(2,line).data;
Y=smooth(Y,5)';
X=std_idgr.Hset.slice_index(2,line).axes{1}.data;
std_fit{line}=polyfit(X,Y,8);
std_idgr.Hset.slice_index(2,line).plot;
hold on
plot(X,polyval(std_fit{line},X),'m')
hold off
%subplot(2,1,2)
Y=idgr.Hset.slice_index(2,line).data;
Y=smooth(Y,5)';
X=idgr.Hset.slice_index(2,line).axes{1}.data;
fit{line}=polyfit(X,Y,8);
idgr.Hset.slice_index(2,line).plot;
hold on
plot(X,polyval(fit{line},X),'m')
hold off
pause(eps)
end
%%
Data=[];
for i=1:20
    Data(:,i)=polyval(fit{i},X)'./polyval(std_fit{i},X)';
end
imagesc(X,idgr.Hset.axes{2}.data,Data');colormap(jet);colorbar
xlabel('Vg');ylabel('Vs');title('Signal to noise by std - left dot')
%%
%uri adding some stupid stuff 
scan_no=423;
idgr=LoadScan(423);
Beta_bg=idgr.Beta_Hbg_Hset.data;
Beta_l=idgr.Beta_Hl_Hset.data;
Beta_r=idgr.Beta_Hr_Hset.data;
Beta_m=idgr.Beta_Hm_Hset.data;
Ydata=idgr.Yc.data(1,:);
figure;plot(Ydata,Beta_bg,Ydata,Beta_l,Ydata,Beta_m,Ydata,Beta_r);
legend('\beta BG','\beta left','\beta middle','\beta right');
xlabel('Y coarse motor pos')
ylabel('\beta vs Hset')
%%
scan_no=[453:461 467:473 477:485];%[453:457 477:485];
BetaSum=zeros(1,length(scan_no));
Scaling=zeros(1,length(scan_no));
Hbg=zeros(1,length(scan_no));
rTf=zeros(1,length(scan_no));
Hbgcorr=ones(1,length(scan_no));
freq=zeros(1,length(scan_no));
fc=346;
for i=1:length(scan_no)
    idgr=LoadScan(scan_no(i));
    Scaling(i)=Info.Scaling.Hbg;
    %BetaSum(i)=mean(idgr.Beta_Sum.data);
    freq(i)=Info.Dac.DispExs.BG.Frequency;
    Hbg(i)=mean(idgr.Hbg.data)/Scaling(i);
    Transfer_Func=1/(1+1i*Info.Dac.DispExs.BG.Frequency/fc); 
    rTf(i)=real(Transfer_Func*exp(1i*Info.Dac.DispExs.BG.Phase));
    Hbgcorr(i)=Hbg(i)/real(Transfer_Func*exp(1i*Info.Dac.DispExs.BG.Phase));
end
figure;
% p=polyfit(freq,log(Hbg),1);
% p
subplot(2,1,1);plot(freq,-Hbgcorr,'or',freq,-Hbg,'ok');%,'o',freq,polyval(p,freq),'-');
legend('Hbg after correction','Hbg before correction')
xlabel('Frequency [Hz]');
ylabel('Hbg','Interpreter','latex');
subplot(2,1,2);plot(freq,rTf,'ob');
xlabel('Frequency [Hz]');
ylabel('Scale Factor');





%% 
idgr=LoadScan(445); 
ExitationsNames=fieldnames(Info.Dac.DispExs);TuneNames=fieldnames(Info.Dac.Tone_Signals);
fc=346;
% a=zeros(numel(TuneNames),length(idgr.Beta_Hbg_Hset.data(:,1)),length(idgr.Beta_Hbg_Hset.data(1,:)));
for i=1:numel(TuneNames)
     ExitationNumber=Info.Dac.Tone_Signals.(TuneNames{i}).Excitation+1;
    if (Info.Dac.DispExs.(ExitationsNames{ExitationNumber}).Amp>0.028)
        Transfer_Func=1/(1+1i*Info.Dac.DispExs.(ExitationsNames{ExitationNumber}).Frequency/fc);
            idgr.(TuneNames{i}).data=(idgr.(TuneNames{i}).data)/real(Transfer_Func*exp(1i*Info.Dac.DispExs.(ExitationsNames{ExitationNumber}).Phase));
    end
end
% pcolor(repmat(idgr.Xc.data(1,:),84,1),idgr.Yc.data,squeeze(a(6,:,:)));shading flat;

figure(445);
subplot(221);idgr.Hm.pcolor; colormap(jet)
subplot(222);idgr.Hr.pcolor; colormap(jet)
subplot(223);idgr.Hl.pcolor; colormap(jet)
subplot(224);idgr.Hbg.pcolor; colormap(jet)

idgr.Beta_Hbg_Hset.data=idgr.Hbg.data./idgr.Hset.data;
idgr.Beta_Hm_Hset.data=idgr.Hm.data./idgr.Hset.data;
idgr.Beta_Hl_Hset.data=idgr.Hl.data./idgr.Hset.data;
idgr.Beta_Hr_Hset.data=idgr.Hr.data./idgr.Hset.data;
idgr.Beta_Sum.data=idgr.Beta_Hbg_Hset.data + idgr.Beta_Hm_Hset.data + idgr.Beta_Hl_Hset.data + idgr.Beta_Hr_Hset.data;


figure(1445);
subplot(321);idgr.Beta_Hbg_Hset.pcolor; colormap(jet)
subplot(322);idgr.Beta_Hm_Hset.pcolor; colormap(jet)
subplot(323);idgr.Beta_Hl_Hset.pcolor; colormap(jet)
subplot(324);idgr.Beta_Hr_Hset.pcolor; colormap(jet)
subplot(325);idgr.Beta_Sum.pcolor; colormap(jet)
%%
% NEED TO Calibrate Using Last Script!
%first lets plot the region of interest:
figure(445);subplot(2,1,1);idgr.Beta_Sum.roi(1,2.05e-3,2.25e-3).roi(2,3.7e-3, 4.25e-3).pcolor; axis([1.85 2.25 3.7 4.3]*10^-3);
subplot(2,1,2);idgr.Beta_Sum.pcolor
%lets trace over The X axis (the second axis) next line realy does
%nothing...
bsum_mean_x=idgr.Beta_Sum.roi(1,2.05e-3,2.25e-3).roi(2,3.7e-3, 4.25e-3).mean(2);
figure(446)
%
y=idgr.Yc.roi(1,2.05e-3,2.25e-3).roi(2,3.7e-3, 4.25e-3).data(:,1);
py=idgr.Beta_Sum.roi(1,2.05e-3,2.25e-3).roi(2,3.7e-3, 4.25e-3).mean(2).fit('poly1');
subplot(2,1,1);plot(idgr.Beta_Sum.roi(1,2.05e-3,2.25e-3).roi(2,3.7e-3, 4.25e-3).mean(2))
hold all;plot(y,py(y));hold off
% Doing the same for the Y axis
subplot(2,1,2);
plot(idgr.Beta_Sum.roi(1,2.05e-3,2.25e-3).roi(2,3.7e-3, 4.25e-3).mean(1));
px=idgr.Beta_Sum.roi(1,2.05e-3,2.25e-3).roi(2,3.7e-3, 4.25e-3).mean(1).fit('poly1');
x=idgr.Xc.roi(1,2.05e-3,2.25e-3).roi(2,3.7e-3, 4.25e-3).data(1,:);
hold all;plot(x,px(x));hold off
%%
idgr=LoadScan(502);
z=idgr.Zc.data(1,:);
pz=idgr.Beta_Sum.fit('poly1');
figure(502)
idgr.Beta_Sum.plot;
hold all;plot(z,pz(z));hold off
%%

thetax=atand(px.p1/pz.p1)
thetay=atand(py.p1/pz.p1)
zcal=(idgr.Beta_Sum.roi(1,2.05e-3,2.25e-3).roi(2,3.7e-3, 4.25e-3).mean(1).data-pz.p2)./pz.p1;
figure(503);plot(x,zcal);

%%
[M,I]=min(Hsetdatadouble(:,3:end),[],1);
p=zeros(2000,3);
for i=3:2000
    p(i,:)= polyfit(Vgdatadouble((I(i-2)-2):(I(i-2)+2),i),Hsetdatadouble((I(i-2)-2):(I(i-2)+2),i),2);
    Vmin1d(i-2)= -p(i,2)/2/p(i,1);
    M(i-2)= polyval(squeeze(p(i,:)),Vmin1d(i-2));
end
Hmin=ones(31,1)*M;
Vmin2d=ones(31,1)*(Vmin1d);
figure;plot(Vgdatadouble(:,3:end)-Vmin2d(),Hsetdatadouble(:,3:end)); 
xlabel('Vg shifted to minimum [V]');
ylabel('Hset');
% figure;plot(Vgdatadouble(:,3:end)-Vmin2d,Hsetdatadouble(:,3:end)./Hmin); 
% xlabel('Vg shifted to minimum [V]');
% ylabel('Hset scaled');

%%




