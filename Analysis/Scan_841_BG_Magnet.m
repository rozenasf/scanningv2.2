LoadScan(841,L)
VSenseGain=1e3;RSense=120;
W=4*um_;
Vsd=Info.Dac.DispExs.VS14.Amp;
v_dirac=-4.47; %approximate for now
Magnet_Constant=1/10*120e-3;
figure(1)
plot_with_color(Idata(Rgr,'n [1/cm^2]',1e-4*Vbg_to_n(BG, v_dirac),'B [T]',Magnet_I*Magnet_Constant),2)
figure(2)
plot_with_color(Idata(Rgr,'n [1/cm^2]',1e-4*Vbg_to_n(BG, v_dirac),'B [T]',Magnet_I*Magnet_Constant),2)
ylim([2.6735    4.8423]*1e3);
figure(3)
plot_with_color(Idata(1./Rgr,'n [1/cm^2]',1e-4*Vbg_to_n(BG, v_dirac),'B [T]',Magnet_I*Magnet_Constant),2)

ClipSend