%% Beta=2.1, Zs=3.075
LoadScan(1638)
DiffData=diff((Beta_HS15_Hset+Beta_HS2_Hset)./Beta_Sum);
DiffData.data=smooth(DiffData.data,5);

suptitle(sprintf('Scan %0.2f',Info.ScanNumber+0.01*Info.Flags.SuperScan))
subplot(1,2,1)
plot(WorkPoint);
out3=Fit_Line(@(x)exp(-x.^2),'X',[-12e-6,-10e-6]);
out4=Fit_Line(4,'X',[-7.2e-6,-6.5e-6]);
ScalingY=5.2e-6/(out4.dX_0-out3.dX_0);
title(sprintf('Assuming peak distance is 5.2um,scale: %0.2f',ScalingY))
 %DiffData.axes{1}.data=DiffData.axes{1}.data*ScalingY;
 subplot(1,2,2)
plot(DiffData);
out1=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-12e-6,-9.5e-6]);
out2=Fit_Line(@(x)sech(-1.76*x).^2,'X',[-8e-6,-6e-6]);
title(sprintf('PSF (including the scaling) Fit 1/a=%0.2e',ScalingY./mean([out1.a,out2.a])))
xlabel('Y not scaled');
ylabel('diff( (S15+S2)/sum)');
mean(1./Beta_Sum.data)
ScalingY
mean([out1.dX_0,out2.dX_0])
%%
figure(28)
clf

peaks=[-1.046058e-05,-7.103963e-06]-1e-8;
Shift=mean(peaks);
peaks=peaks-Shift;
peaks=peaks*1.46*1e6/5;
Bound=[-3,3]/5;

hold on
X=linspace(Bound(1),Bound(2),1000);Y=X*0;Y(X>peaks(1) & X<peaks(2))=1;
plot(X,Y,'linewidth',3)
plot((Beta_Sum.axes{1}.data-Shift)*1.46*1e6/5,(Beta_HS15_Hset.data+Beta_HS2_Hset.data)./Beta_Sum.data*1.022,'-','linewidth',3,'markersize',10);
xlim(Bound)
TXYL('PSF : std=1.0[um]','Y / W ; W=5um','Channel signal');
Ax=gca;Ax.FontSize=16;
sigma=0.5/5*1;
Y2=exp(-X.^2/(2*sigma.^2));
PSFSpread=conv(Y,Y2,'same');PSFSpread=PSFSpread./max(PSFSpread);
plot(X,PSFSpread,'--','linewidth',3)
ylim([0,1])