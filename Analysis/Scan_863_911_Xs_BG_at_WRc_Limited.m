%% 
Vsd=10e-3;
Vbg=linspace(-4,5,10);

Alpha=1;

W=4e-6;
e=1.6e-19;
VDirac=-4.47;
CapacitiveBgFactor=5.946e14;
hbar=6.62e-34/(2*pi);

n=CapacitiveBgFactor*(Vbg-VDirac);
B=hbar*sqrt(pi*n)*Alpha/(e*W);

Magnet_I=B/120e-3*10;

%%
ScanRange=863:911;
%ScanRange=[879,886];
figure(1);
AlphaList=[4,2,1,0,1.5,0.5,3];AlphaList=sort(AlphaList);
BGList=linspace(5,-4,7);
RhoXX=zeros(numel(BGList),numel(AlphaList));
n_Matrix=zeros(numel(BGList),numel(AlphaList));
l_Matrix=zeros(numel(BGList),numel(AlphaList));
for j=ScanRange
%j=851;
    LoadScan(j,L);Info.Scan.Description
    Idata('Beta_Sum_BGFIX',Beta_Hbg_Hset*4.2+Beta_HRest_Hset+Beta_HS14_Hset+Beta_HS6_Hset);
    Idata('Phi',0.5+0.5*(Beta_HS14_Hset-Beta_HS6_Hset)./(Beta_HS14_Hset+Beta_HS6_Hset));
    Idata('I',(Ggr6/VSenseGain/Info.Scaling.Ggr6)/RSense);
    n= Vbg_to_n(Info.Channels.BG, v_dirac);
    k_F_vec = sqrt(pi*abs(n));

    Rc=r_cyclo(n, B).*sign(B);Rc(isnan(Rc))=inf;
    plot(Phi)
    hold on
    I1=mean(I,1);
    Idata('V',Phi*Vsd);

    DPhi_DXs=I1*0;
    for i=1:2
       F=polyfit(Phi.axes{1}.data',(Phi.data(:,i)),1);
       DPhi_DXs.data(i)=F(1);
    end
    DPhi_DXs.data=0.5*(DPhi_DXs.data+fliplr(DPhi_DXs.data));
    RhoXXS=DPhi_DXs*Vsd*W./mean(I1);
    AlphaIdx=find(Info.Scan.Parameters.Alpha==AlphaList);
    BGIdx=find(Info.Scan.Parameters.BG==BGList);
    RhoXX(BGIdx,AlphaIdx)=mean(RhoXXS.data);
    n_Matrix(BGIdx,AlphaIdx)=n;
    l_Matrix(BGIdx,AlphaIdx)=1e6*( h_./(2*qe_^2*k_F_vec.*RhoXX(BGIdx,AlphaIdx)));
    
end

hold off
figure(2);
plot(n_Matrix/1e4,l_Matrix,'o--');xlabel('n [1/cm^2]');ylabel('l [um]');
Temp={};for u=1:numel(AlphaList);Temp{end+1}=sprintf('W/Rc = %0.2f',AlphaList(u));end;legend(Temp,'Location','northwest')
title(['symmetrize 4 point scans ',num2str(ScanRange(1)),' to ',num2str(ScanRange(end))])
figure(3)
plot(n_Matrix/1e4,1./RhoXX,'o--');xlabel('n [1/cm^2]');ylabel('Conductivity [1/Ohm]');
Temp={};for u=1:numel(AlphaList);Temp{end+1}=sprintf('W/Rc = %0.2f',AlphaList(u));end;legend(Temp,'Location','northwest')
title(['symmetrize 4 point scans ',num2str(ScanRange(1)),' to ',num2str(ScanRange(end))])
figure(4)
plot([-fliplr(AlphaList),AlphaList]'*ones(1,7),[flipud(1./RhoXX');1./RhoXX'],'o--');xlabel('W/Rc');ylabel('Conductivity [1/Ohm]');
Temp={};for u=1:numel(AlphaList);Temp{end+1}=sprintf('n [1/cm^2] = %0.2e',n_Matrix(u)/1e4);end;legend(Temp,'Location','north')
title(['symmetrize 4 point scans ',num2str(ScanRange(1)),' to ',num2str(ScanRange(end))])
figure(5)
plot([-fliplr(AlphaList),AlphaList]'*ones(1,7),[flipud(l_Matrix');l_Matrix'],'o--');xlabel('W/Rc');ylabel('l [um]');
Temp={};for u=1:numel(AlphaList);Temp{end+1}=sprintf('n [1/cm^2] = %0.2e',n_Matrix(u)/1e4);end;legend(Temp,'Location','north')
title(['symmetrize 4 point scans ',num2str(ScanRange(1)),' to ',num2str(ScanRange(end))])