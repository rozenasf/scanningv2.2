Info.ActiveDevice='Mid'
%%
CrashSite=[16,10.0]*1e-6;
Vsd=25e-3;Vset=0.55;
VGrapheteList=linspace(0,2.5,20);

%%
Info.Range.Idc=[1e-12,4e-7];
Info.Range.Beta=[0,1/0.2];
Info.Range.Zs0=[0,9e-6];
Info.Range.Xs0=[0e-6,60e-6];
Info.Range.Ys0=[0e-6,60e-6];
%% Go to crash XY location
Q.Scanner_XsTransConductanceAndGate(Vsd,linspace(0.5,0.75,3),CrashSite(1)*ones(1,3))
Q.Scanner_YsTransConductanceAndGate(Vsd,linspace(0.5,0.75,3),CrashSite(2)*ones(1,3))
%% Simple gate dependance
Q.GateDependance(Vsd,linspace(0,1,30))
%% Vset-Graphete 2D scan
Q.GrapheteVsetDependance(Vsd,linspace(0,1,20),linspace(1,-1,20)); %setlist, graphetelist
%% approach
Q.Scanner_HightTransConductanceAndGate(Vsd,linspace(0.4,0.8,8),linspace(S.Get('Zs0'),8.6e-6,20))
%% 2D scan with middle device
Q.Scanner_XsTransConductanceAndGate(Vsd,linspace(0.5,0.75,4),0e-6*ones(1,5))
Q.Scanner_YsTransConductanceAndGate(Vsd,linspace(0.5,0.75,4),0e-6*ones(1,5))
Q.Scanner_XsYsScan(Vsd,Vset,linspace(0e-6,40e-6,30),linspace(0e-6,40e-6,30))
Q.Scanner_XsTransConductanceAndGate(Vsd,linspace(0.5,0.75,4),0*ones(1,5))
Q.Scanner_YsTransConductanceAndGate(Vsd,linspace(0.5,0.75,4),0*ones(1,5))
%% Crash the nanotube!
Q.Crash(Vsd,Vset,VGrapheteList,linspace(S.Get('Zs0'),9.00e-6,50));
%% Retract
Info.Flags.Stop=0;
Q.Scanner_HightTransConductanceAndGate(Vsd,linspace(0.4,0.8,16),linspace(S.Get('Zs0'),0e-6,10))

%% Fast improvise a 1D scan:
Info.Scan=[];
        Range=[0.5,0];
        Info.Scan.Axis=BuildStruct(     ...
            'Vset',VsetList ...
            );
        S.Set('Zero');
        Info.Scan.Start=BuildStruct('Vsd',Vsd,'Vset',0);
        Info.Scan.End=  BuildStruct('Vsd',0,'Vset',0);
        Info.Scan.Get=...
            {{                                                 ...
                Info.ChannelType.TuneSignals{:},                             ...
                @()Plot1D(1,Info.ChannelType.TuneSignals{:}),                ...
                @()InterlockIdc('Vsd',0,'Vset',0),       ...
                @()wait(0.01),...
                'Save'...
            }};
        %S.Set('ArduinoProtection','!D');pause(1);
        %S.Get('ArduinoProtection')
        Info.h=plot(0,0);
        S.RunScan();
        DocSend({S.BuildToSave(1),1});

%% Fast improvise a 2D scan:
        Info.Scan=[];
        % Trans conductance as a function of hight and vset 
        %max beta is the minimum inverse beta allowed
        Info.Scan=[];
        Info.Scan.Axis=BuildStruct(     ...
            'Vset',Vsetlist, ...
            'VGraphete',VGrapheteList ...
            );
        S.Set('Zero');
        Info.Scan.Start=BuildStruct('Vsd',Vsd);
        Info.Scan.End=  BuildStruct('Vsd',0,'Vset',0,'VGraphete',0);
        Info.Scan.Get=...
            {{                                                 ...
                Info.ChannelType.TuneSignals{:},                             ...
                @()Plot1D(1,Info.ChannelType.TuneSignals{:}),                ...
                @()InterlockIdc('Vsd',0,'Vset',0),       ...
                @()DevideIdgr('HBG','Hset'),...         %1./2 -> 2_1
                @()DevideIdgr('HGraphete','Hset'),...   %1./2 -> 2_1
                @()SumBetaIdgr('Hset_HBG','Hset_HGraphete'),...   %didnt check it yet.
                ...%@()BetaInterlock({'Hset_HBG','Hset_HGraphete'},'Vsd',0,'Vset',0),                   ...
                @()wait(0.01)...
            },{...
                @()Plot2D(5,'HBG','HGraphete','Hset_HBG','Hset_HGraphete','Hset','Idc','Beta_Sum'),           ...
                 'Save' ...
            }};
        figure(1);figure(5);
        S.RunScan();
        DocSend({S.BuildToSave(1),1});TelegramSend(5);