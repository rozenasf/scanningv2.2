%%keep plotting rhoXX/l_mfp on graphs
a = numel(BG) %number of BG
figure(52)
clf
for i = 1:6
     plot(B,l_mfp.data(:,i))
     hold on
end
Temp={};for u=1:numel(BG);Temp{end+1}=sprintf('BG = %0.2f',BG(u));end;legend(Temp,'Location','northwest')
%%
a = numel(BG) %number of BG
figure(55)
clf
for i = 8:17
     plot(B,l_mfp.data(:,i))
     hold on
end
Temp={};for u=8:numel(BG);Temp{end+1}=sprintf('BG = %0.2f',BG(u));end;legend(Temp,'Location','northwest')

%%
a = numel(BG) %number of BG
figure(57)
clf
for i = 7:8
     plot(B,l_mfp.data(:,i))
     hold on
end
Temp={};for u=7:9;Temp{end+1}=sprintf('BG = %0.2f',BG(u));end;legend(Temp,'Location','northwest')

%% plot the 2D MR curves together
LoadScan([2316],L);
figure(100);
subplot(221);
contour(Beta_HS2_Hset.slice_index(3,1).roi(2,-2.55e-5,-2.2e-5),linspace(-0.05,0.05,25));colormap(jet);
axis('equal');
subplot(222);
contour(Beta_HS2_Hset.slice_index(3,2).roi(2,-2.55e-5,-2.2e-5),linspace(-0.05,0.05,25));
colormap(jet);axis('equal')
subplot(223);
contour(Beta_HS2_Hset.slice_index(3,3).roi(2,-2.55e-5,-2.2e-5),linspace(-0.05,0.05,25));colormap(jet);axis('equal')

%%plot Phi Rhoxx and l
LoadScan([],L);
figure;
