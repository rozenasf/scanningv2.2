function PlotGDSMatrix()
    global Info;
    %matrix is: [xx,xy,x0;yy,yx,y0];
    try Design=Info.Design.GDS; catch Design=ExtractLayers(gds_load(Info.Design.Path),'Wafer',[2,4,39]);Info.Design.GDS=Design; end
    try Matrix=Info.Design.Matrix; catch Matrix=[1e-6,0,0;0,1e-6,0];Info.Design.Matrix=Matrix; end 
    try Reverse=Info.Design.Reverse; catch Reverse=[0,1];Info.Design.Reverse=Reverse;end
    figureN(20);clf %default
    
    PlotGDS(applyTransform(Design,...
    sprintf('matrix(%e,%e,%e,%e,%e,%e)',...
    Matrix(1),Matrix(2),Matrix(3),Matrix(4),Matrix(5),Matrix(6)...
    )));if(Reverse(1));set(gca,'Xdir','reverse');end;if(Reverse(2));set(gca,'Ydir','reverse');end
    hold on
    Info.Design.Marker=plot(0,0,'*r','markersize',10);
    Info.Design.Range=rectangle('Position',[0,1,0,1]);
    try
        disp(1)
        AxisNames=fieldnames(Info.Scan.Axis);
        Info.Design.ScanAxis={};
        Get=Info.GetChannels;
        for i=1:numel(AxisNames)
            AxisValues=Info.Scan.Axis.(AxisNames{i});
            switch AxisNames{i}
                case 'Xc'
                    Info.Design.ScanAxis{end+1}=plot([min(AxisValues),max(AxisValues)]-Get.Xs0,[1,1]*(Get.Yr-Get.Ys0),'m--','linewidth',3);
                case 'Yc'
                    Info.Design.ScanAxis{end+1}=plot([1,1]*(Get.Xr-Get.Xs0),[min(AxisValues),max(AxisValues)]-Get.Ys0,'m--','linewidth',3);
                case 'Xs0'
                    Info.Design.ScanAxis{end+1}=plot(Get.Xr-[min(AxisValues),max(AxisValues)],[1,1]*(Get.Yr-Get.Ys0),'m--','linewidth',3);
                case 'Ys0'
                    Info.Design.ScanAxis{end+1}=plot([1,1]*(Get.Xr-Get.Xs0),Get.Yr-[min(AxisValues),max(AxisValues)],'m--','linewidth',3);
            end
        end
    end
    hold off
    Info.Design.Title=title('Z=');
    %UpdateMarkerPosition();
end