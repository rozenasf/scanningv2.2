function out=TwoPointLinspace(a,b,N)
    out=[linspace(a,b,N),linspace(b,a,N)];
end