
%Resistance and Temperature for PRT 1000 based on equations in Ekin's book.
%out=pt1000(val,tag), val is the input resistance (ohms) or temperature (K), and out
%is the corresponding temperature or resistance.  tag='R' or 'T' to
%indicate the units of the input val

function out = pt1000(val, tag)
    out = arrayfun(@(x)pt1000_single(x,tag), val);
end

function out = pt1000_single(val,tag)

if tag=='R'
    
    fun =@(T) val-prt(T);
    
    
    %out = arrayfun(@fzero(fun,200),val)
    %out = [num2str(fzero(fun,200)) ' K'];
    
    out = fzero(fun,200);
    


elseif tag=='T'
    %out = [num2str(prt(val)),' Ohms']
    out = prt(val);
  
end
end


function R=prt(T);
  A = 3.9083e-3;
    B = -5.775e-7;
    C=-4.183e-12;
    R0=1000;
    R=R0*(1+A*(T-273.15) + B*(T-273.15).^2 + C*(T-373.15).*(T-273.15).^3);
end
    