function R=XEdgeFromSimulation(Data)
R={};
Extra=1;
SData=Data.Sym('WRc');
AData=Data.ASym('WRc');
for i=1:numel(AData.data)
    V=0.5*(AData.data{i}.IDensity.roi(2,5-Extra,5).mean(2)-AData.data{i}.IDensity.roi(2,0,Extra).mean(2));
     I=SData.data{i}.TotalCurrent;
     R{i}=V./I;
     R{i}.data=smooth(R{i}.data,10);
     R{i}.axes{1}.data=R{i}.axes{1}.data-mean(R{i}.axes{1}.data);
end
end