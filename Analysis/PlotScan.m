function PlotScan(ScanNumber)
global Info L
if(exist('ScanNumber'));LoadScan(ScanNumber,L);end
GetList=[Info.Scan.Get{:}];if(isfield(Info.Scan,'Analysis'));GetList=[Info.Scan.Analysis];end
        for i=1:numel(GetList)
            if(isa(GetList{i},'function_handle') && ~isempty(regexp(func2str(GetList{i}),'Plot','ONCE')))
                f=GetList{i};
                f();
                newline=sprintf('\n');
                suptitle(['\fontsize{16} Scan ',num2str(Info.ScanNumber),newline,'\fontsize{10}',Info.Scan.Description]);
                disp(func2str(GetList{i}));
                drawnow;pause(1e-2);
                
            end
        end
end