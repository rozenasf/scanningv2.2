function [Value,Text]=GetFromMemory(Channel) 
%Search for driver, then:
%takes first from Set, then from Get. This is to protect against scanner
%setting
global Info;
    if(ismember('@',Channel))
        name=regexp(Channel,'\@','split','once');
        DriverName=name{2};
        DriverProperty=name{1};
        Text=[];
        if(isprop(Info.Drivers.(DriverName),DriverProperty))
            Value=Info.Drivers.(DriverName).(DriverProperty);
        elseif(ismethod(Info.Drivers.(DriverName),DriverProperty))
            Value=Info.Drivers.(DriverName).(DriverProperty)();
        else
            Value=nan;
        end
    else
        Driver=GetDriver(Channel);Text=[];
        if(ismethod(Driver,'GetFromMemory')) %works if Driver is null
            [Value,Text]=Driver.GetFromMemory(Channel);
        elseif(isfield(Info.Channels,Channel))
            Value=Info.Channels.(Channel);
        else
            Value=nan;
        end
    end
    if(isempty(Text))
       Text=SprintfA(Value); 
    end
end