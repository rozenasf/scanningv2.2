function out=CalculateBetaSimple(varargin)
global Info S;out=0;
%                                 name=Info.Scan.Runtime.RelevantGet{i};
%                                 if(~isfield(Info.Scan.Data,name))
%                                     S.CreateScanOutput(name);
%                                 end
%                                 S.UpdateData(name,Info.Scan.Runtime.PositionIndex,Value);

% S.CreateScanOutput=@CreateScanOutput;
% S.UpdateData=@UpdateData;

pm=setstr(177);

%varargin=InitiateNewFunction(varargin);
%this is and old function, dont use...
if(~isfield(Info.Scan.idgr,'Beta_Sum'))
    S.CreateScanOutput('Beta_Sum');
    %S.CreateScanOutput('Beta_Sum_Err');
    Value_Sum=[];Value_Error_Sum=[];
end
if(numel(varargin)==0)
    varargin={};
    for j=1:numel(Info.Opt.DeviceSignals)
        varargin=[varargin,Info.Opt.ActiveDevice,Info.Opt.DeviceSignals{j}];
    end
end
for j=1:numel(varargin)/2
    Beta_Name=sprintf('Beta_%s_%s',varargin{2*j-1},varargin{2*j});
    %Beta_Name_Err=sprintf('Beta_%s_%s_Err',varargin{j},varargin{j+1});
    
    
    if(~isfield(Info.Scan.idgr,Beta_Name))
        S.CreateScanOutput(Beta_Name);
        %S.CreateScanOutput(Beta_Name_Err);
    end
    
    
    a=Info.Scan.idgr.(varargin{2*j-1}).data(Info.Scan.Runtime.PositionIndex);
    b=Info.Scan.idgr.(varargin{2*j}).data(Info.Scan.Runtime.PositionIndex);
    out=b./a;
    S.UpdateData(Beta_Name,Info.Scan.Runtime.PositionIndex,out);
    %S.UpdateData(Beta_Name_Err,Info.Scan.Runtime.PositionIndex,0);
    
    V1=Info.Scan.idgr.Beta_Sum.data(Info.Scan.Runtime.PositionIndex);
    %V2=Info.Scan.idgr.Beta_Sum_Err.data(Info.Scan.Runtime.PositionIndex);
    V1(isnan(V1))=0;
    %V2(isnan(V2))=0;
    %         if(~strcmp(varargin{j+1},'HZs'))
    %             S.UpdateData('Beta_Sum',Info.Scan.Runtime.PositionIndex,V1+out);
    %             S.UpdateData('Beta_Sum_Err',Info.Scan.Runtime.PositionIndex,0);
    %         end
    
end
end
