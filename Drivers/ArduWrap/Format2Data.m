function Data=Format2Data(Format,BinnaryInput)
        Length=FormatLength(Format);
        Data=zeros(1,numel(Format));
        Location=1;
        for i=1:numel(Format)
            switch(Format(i))
                case 'B'
                    Data(i)=BinnaryInput(Location);Location=Location+1;
                case 'f'
                    Data(i)=typecast(uint8([BinnaryInput(Location:(Location+3))]),'single');Location=Location+4;
                case 'L'
                    Data(i)=typecast(uint8([BinnaryInput(Location:(Location+3))]),'int32');Location=Location+4;
                otherwise
                    error('Unknown format');
            end
        end
    end