%% 
% The goal of this document is to esimate the pn junction collimation effect 
% by comparing measured collimation with simulation
% 
% Sharvin resistance of a channel (the two sides): $\frac{\pi h}{4e^2 k_F 
% W}$
% 
% for a perfect but rough P-N junciton 
% 
% $$|t(\theta)|=cos^2(\theta)$$
% 
% so the total transmission is $\frac{4e^2}{h}\frac{k_F}{2}$ for a perpenicular 
% collimated beam, or (my interpretetion) a rejection of 0.5 for all particles, 
% hence twice the natural sharvin resistnace. for a smooth P-N junction (or wide 
% P-N with rough edges) the tranmission probability is 
% 
% $$|t(\theta)|=e^{-\pi k_F \cdot d \cdot sin^2\theta}$$
% 
% so that the total transmission is $\frac{2e^2}{\pi h}\sqrt{\frac{k_F}{d}}$, 
% for a directed beam. In term of rejection:
% 
% $$T=\int_{-\pi}^{\pi}{\frac{1}{2\pi}e^{-\pi k_F \cdot d \cdot sin^2\theta}}d\theta\approx\frac{1}{2\pi}\sqrt{\frac{1}{k_Fd}}$$
% 
% when the collimation is large. if it is small, need to solve numerically.

% SharvinBG
%% 
%