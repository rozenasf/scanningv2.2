clf;

PeakList={};PeaksLocations={};

PeaksLocations{1}=[-4.4,0];
PeaksLocations{2}=[-4.8,0];
PeaksLocations{3}=[-5.2,-2.75,0];
PeaksLocations{4}=[-5.76,-4.4,-2.7,-1.2,0];
PeaksLocations{5}=[-6.24,-5.2,-3.7,-2.4,-1.4,0];
for Extra=1:5
    j=1+(Extra-1)*2;
    clf
for i=2:10
%2:10
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j,i});Beta_Sum=0.51;
R1 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j+1,i});Beta_Sum=0.51;
R2 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
IData=R1-R2;
% plot(IData.roi(1,-6.5,6.5),'-','linewidth',3)
IData.data=(IData.data+fliplr(IData.data))/2
plot(IData,'-','linewidth',3)
hold on
end
% PeakList=[];
for i=1:numel(PeaksLocations{Extra})
   out{i}=Fit_Line(2,'X',[PeaksLocations{Extra}(i)-1,PeaksLocations{Extra}(i)+1]) ;
   PeakList{Extra}(i,:)=[out{i}.dY_0];
   
end
for i=1:numel(PeaksLocations{Extra})
%     PeakList{Extra}(i,:)=PeakList{Extra}(i,:)./PeakList{Extra}(end,:);
end
end
%clf
%plot(PeakList','o--')
%%

    figure(5);clf;
%     ScanNameList={'XEdge_Magnet_Holes','XEdge_Magnet_Electrons','XEdge_Magnet_Holes_0_5','XEdge_Magnet_Electrons_0_5'};
ScanNameList={'XEdge_Magnet_Holes','XEdge_Magnet_Electrons'};
Sym={};
    for j=1:numel(ScanNameList)
        R=[];
        figure(6);
        ScanName=ScanNameList{j};
        for i=1:numel(Scans.(ScanName))-2
            UnPackScan(Scans.(ScanName){i})
            Beta_Sum=0.51;
            R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
            plot(R,'-');hold on
        end
        ChangePlotColor(@(n)jet(28))
        TXYL(sprintf('Edge cuts, Density = %0.2e',Vbg_to_n(SuperAxis.(ScanName).BG,0.7)/1e4),'$X$ (Scaled) $[\mu m]$','$R [Ohm]$')
        %             [dRdX,Sharvin_Right,Sharvin_Left,Vsd]=AnalyzeX_Cut(RangeMid,RangeRight,RangeLeft,InterpolationPoints);
        figure(5);hold on
        %             [Xo,Yo1]=SortXY(Vbg_to_n(SuperAxis.(ScanName).W_Rc(1:end-2),0.7)/1e4,Sharvin_Right);
        %             [Xo,Yo2]=SortXY(Vbg_to_n(SuperAxis.(ScanName).W_Rc(1:end-2),0.7)/1e4,Sharvin_Left);
        %             plot(Xo,Yo1,'.');plot(Xo,Yo2,'.');
        R={};
        for i=1:numel(Scans.(ScanName))-2
            UnPackScan(Scans.(ScanName){i})
            Beta_Sum=0.51;
            if(numel(find(W_Rc_Bare==abs(SuperAxis.(ScanName).W_Rc(i))))>0)
                R{find(W_Rc_Bare==abs(SuperAxis.(ScanName).W_Rc(i)))}{sign(SuperAxis.(ScanName).W_Rc(i))/2+1.5} = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
            end
        end
        for i=1:numel(R)
            Sym{i,j}=R{i}{2}-R{i}{1};Sym{i,j}.data=Sym{i,j}.data+fliplr(Sym{i,j}.data);
            
        end
    end
    MeanList=[];
    for i=1:12
        %MeanList(i)=mean(Sym{i,1}.data+Sym{i,2}.data);
        plot( Sym{i,1}+Sym{i,2},'linewidth',3);hold on
    end
    ChangePlotColor(@(a)jet(12));
    %    ChangePlotColor(@(a)hsv(12));
     %   TXYL(sprintf('Symmetrized',Vbg_to_n(SuperAxis.(ScanName).BG,0.7)/1e4),'$X$ (Scaled) $[\mu m]$','$\Delta R [Ohm]$')
    %%
    
    figure(5);clf;
    ScanNameList={'XEdge_Magnet_Holes','XEdge_Magnet_Holes_0_5','XEdge_Magnet_Electrons_0_5','XEdge_Magnet_Electrons'};
    for j=1:numel(ScanNameList)
        R=[];
        subplot(2,4,j);
        ScanName=ScanNameList{j};
        for i=1:numel(Scans.(ScanName))-2
            UnPackScan(Scans.(ScanName){i})
            Beta_Sum=0.51;
            R = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
            plot(R,'-');hold on
        end
        ChangePlotColor(@(n)jet(28))
        TXYL(sprintf('Edge cuts, Density = %0.2e',Vbg_to_n(SuperAxis.(ScanName).BG,0.7)/1e4),'$X$ (Scaled) $[\mu m]$','$R [Ohm]$')
        %             [dRdX,Sharvin_Right,Sharvin_Left,Vsd]=AnalyzeX_Cut(RangeMid,RangeRight,RangeLeft,InterpolationPoints);
        subplot(2,4,j+4);hold on
        %             [Xo,Yo1]=SortXY(Vbg_to_n(SuperAxis.(ScanName).W_Rc(1:end-2),0.7)/1e4,Sharvin_Right);
        %             [Xo,Yo2]=SortXY(Vbg_to_n(SuperAxis.(ScanName).W_Rc(1:end-2),0.7)/1e4,Sharvin_Left);
        %             plot(Xo,Yo1,'.');plot(Xo,Yo2,'.');
        R={};
        for i=1:numel(Scans.(ScanName))-2
            UnPackScan(Scans.(ScanName){i})
            Beta_Sum=0.51;
            if(numel(find(W_Rc_Bare==abs(SuperAxis.(ScanName).W_Rc(i))))>0)
                R{find(W_Rc_Bare==abs(SuperAxis.(ScanName).W_Rc(i)))}{sign(SuperAxis.(ScanName).W_Rc(i))/2+1.5} = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*mean(AVS15)./mean(IgrAC15),[1],1e6);
            end
        end
        for i=1:numel(R)
            Sym=R{i}{2}-R{i}{1};Sym.data=Sym.data+fliplr(Sym.data);
            plot(Sym,'.-','linewidth',1)
        end
        ChangePlotColor(@(a)hsv(13));
        TXYL(sprintf('Symmetrized',Vbg_to_n(SuperAxis.(ScanName).BG,0.7)/1e4),'$X$ (Scaled) $[\mu m]$','$\Delta R [Ohm]$')
    end