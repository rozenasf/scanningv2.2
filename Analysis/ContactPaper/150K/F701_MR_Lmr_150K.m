%%
tic
Th=3.55;

% while(1)
%     try
%         if(~isempty(TelegramGet()) || toc>1800)
%             if(toc>1800);Regular=1;else;Regular=0;end
            figure(1);clf
%             tic
            %        VDirac=0.575;
            VDirac=0.575;
            ScalingX       = 1.6216; %Based on 1556.01 see notebook
            LoadScan;
            FirstScan=1557;LastScan=1575;
            LoadScan(FirstScan+0.01,L,'NoScale');
            Elapsedtime=datetime('now')-datetime(Info.Scan.StartDateTime(9:end),'InputFormat','yyyy_MM_dd_HH_mm_ss');
            
            LoadScan(FirstScan,L,'NoScale');
            ListBG=[];ListBetaSum=[];figure(1);clf;
            TotalBetaSum=[];TotalTemperature=[];TotalIgrAC15=[];TotalHset=[];ListWRc=[];
            k=0;MaxScan=Info.ScanNumber;
            ScanList=[FirstScan:LastScan];
            for j=ScanList
                MaxSuperScan=GetMaxSuperScanNumber(j);
                StartSuperScan=1;
                if(j==max(ScanList));MaxSuperScan=MaxSuperScan-1;end
                if(j==min(ScanList));StartSuperScan=2;end
                
                for i=StartSuperScan:MaxSuperScan
                    try
                        LoadScan(j+i/100,L,'NoScale');
                        if(~strcmp('RhoXX for Lmr',Info.Scan.Description) && ~strcmp('RhoXX for magnetoresistance',Info.Scan.Description));continue;end
                    catch
                        break;
                    end
                    if(~isfield(Info.Scan,'UserData'));continue;end
                    k=k+1;
                    
                    Phi=0.5+0.5*(Beta_HS15_Hset-Beta_HS2_Hset)./Beta_Sum;
                    R=Phi*7.5e-3./IgrAC15.data;
                    ListBG(end+1)=Info.Scan.UserData.BG;
                    ListWRc(end+1)=Info.Scan.UserData.WRc;
                    ListBetaSum(end+1)=Info.Scan.UserData.Beta_Sum;
                    TotalBetaSum=[TotalBetaSum,Beta_Sum.data(:)'];
                    TotalHset=[TotalHset,Hset.data(:)'];
                    TotalIgrAC15=[TotalIgrAC15,IgrAC15.data(:)'];
                    
                    TotalTemperature=[TotalTemperature,Temperature.data(:)'];
                    subplot(2,2,1);plot(R);hold on
                end
            end
            ChangePlotColor(@(~)jet(23))
            %%
            
            LeftTime=Elapsedtime*(161-(k+0.5))/(k+0.5);
            
            TXYL(' ','BG','R')
            Center=Fit_Line(1,'X',[-10.5e-6,-5.3e-6]);
            Left=Fit_Line(0,'X',[-15e-6,-14e-6]);
            Right=Fit_Line(0,'X',[-2e-6,-1e-6]);
            Rleft=[];Rright=[];
            REdge=-3.5e-6;LEdge=-12.8e-6;
            for i=1:numel(Center)
                Rleft(i) =-(Left(i).AVR-Center(i).Func(LEdge));
                Rright(i)=Right(i).AVR-Center(i).Func(REdge);
            end
            RhoXX=[Center.a]*5e-6   / ScalingX;
            
            Lmr=h_./(2*qe_^2*sqrt(pi*abs(Vbg_to_n(ListBG,VDirac))))./(RhoXX);
            subplot(2,2,2);
            plot(ListBG,1./ListBetaSum,'o-');hold on
            plot([-10,10],[3.5,3.5])
            TXYL(' ','BG','InvBetaSum')
            subplot(2,2,3);plot(ListBG,Lmr/5e-6,'o');TXYL(' ','BG','lMR/W')
            subplot(2,2,4);plot(ListBG,1./RhoXX,'o');TXYL(' ','BG','1/RhoXX')
            figure(2);clf;
            subplot(2,2,1);plot(1./TotalBetaSum);hold on ;plot([0,numel(TotalBetaSum)],[3.5,3.5])
            TXYL('invBetaSum','','')
            subplot(2,2,2);plot(TotalTemperature);TXYL('T','','')
            subplot(2,2,3);plot(TotalHset);TXYL('Hset','','')
            subplot(2,2,4);plot(TotalIgrAC15);TXYL('Igr','','')
            
            
            %% For LMR
            figure(3);
            ExportLmr={};ExportMR={};
            clf;subplot(2,2,1)
            WRcPlotList=[0,1,2,2.76,3.27,4,6];
            for i=1:numel(WRcPlotList)
                BGSort={};IndexLMR={};IndexRleft={};IndexRright={};
                FullLMR=[];FullRleft=[];FullRright=[];
                for j=1:2
                    WRc=WRcPlotList(i)*(-1)^(j);
                    Index=find(ListWRc==WRc);
                    [BGSort{j},IDX]=sort(ListBG(Index));
                    IndexLMR{j}=Lmr(Index);IndexLMR{j}=IndexLMR{j}(IDX);
                    IndexRleft{j}=Rleft(Index);IndexRleft{j}=IndexRleft{j}(IDX);
                    IndexRright{j}=Rright(Index);IndexRright{j}=IndexRright{j}(IDX);
                end
                FullBGList=intersect(BGSort{1},BGSort{2});
                for j=1:numel(FullBGList)
                    FullLMR(j)   =mean([IndexLMR{1}(FullBGList(j)==BGSort{1}),IndexLMR{2}(FullBGList(j)==BGSort{2})]);
                    FullRleft(j) =mean([IndexRleft{1}(FullBGList(j)==BGSort{1}),IndexRleft{2}(FullBGList(j)==BGSort{2})]);
                    FullRright(j)=mean([IndexRright{1}(FullBGList(j)==BGSort{1}),IndexRright{2}(FullBGList(j)==BGSort{2})]);
                end
                %figure(777);
                ExportLmr{i}.Lmr=FullLMR;
                ExportLmr{i}.BG=FullBGList;
                ExportLmr{i}.W_Rc=WRcPlotList(i);
                plot(FullBGList,FullLMR/5e-6,'o--','linewidth',3);hold on
                %figure(778);
                % plot(FullBGList,FullRleft,'o--','linewidth',3);hold on
                % plot(FullBGList,FullRright,'o--','linewidth',3);hold on
            end
            %
            ChangePlotColor(@jet)
            TXYL('symmetrized Lmr','BG [V]','Lmr/W','W/Rc=%d',{[0,1,2,4,6]})
            Legend=findobj(gcf, 'Type', 'Legend');Legend(1).Location='southeast';
            %
            subplot(2,2,2)
            WRcPlotList=[0,1,2,4,6];
            for i=1:numel(WRcPlotList)
                BGSort={};IndexLMR={};
                FullLMR=[];
                for j=1:2
                    WRc=WRcPlotList(i)*(-1)^(j);
                    Index=find(ListWRc==WRc);
                    [BGSort{j},IDX]=sort(ListBG(Index));
                    IndexLMR{j}=1./RhoXX(Index);IndexLMR{j}=IndexLMR{j}(IDX);
                    
                end
                FullBGList=intersect(BGSort{1},BGSort{2});
                for j=1:numel(FullBGList)
                    FullLMR(j)=mean([IndexLMR{1}(FullBGList(j)==BGSort{1}),IndexLMR{2}(FullBGList(j)==BGSort{2})]);
                end
                plot(FullBGList,FullLMR,'o--','linewidth',3);hold on
            end
            ChangePlotColor(@jet)
            TXYL('symmetrized 1/RhoXX','BG [V]','1/RhoXX','W/Rc=%d',{[0,1,2,4,6]})
            Legend=findobj(gcf, 'Type', 'Legend');Legend(1).Location='North';
            %   symmetrized version
            subplot(2,2,3)
            BGPlot=[-9.9,-7.5,-5,-2.5,-1.6,3,5,7.5,9.9];
            for i=1:numel(BGPlot)
                Index=find(ListBG==BGPlot(i));
                PlotWRc=ListWRc(Index);[PlotWRc,Idx]=sort(PlotWRc);
                PlotLmr=Lmr(Index);PlotLmr=PlotLmr(Idx);
                
                WRcList=unique(PlotWRc(ismember(PlotWRc,-PlotWRc)));
                WRcList=WRcList(WRcList>=0);LmrList=[];
                for j=1:numel(WRcList)
                    WRc=WRcList(j)*(-1)^k;
                    LmrList(j)=mean([mean(PlotLmr(PlotWRc==(-WRcList(j)))),mean(PlotLmr(PlotWRc==(WRcList(j))))]);
                end
                
                plot([-fliplr(WRcList),WRcList(2:end)],[fliplr(LmrList),LmrList(2:end)]/5e-6,'o','linewidth',3);hold on
                %                   plot(PlotWRc,PlotLmr/5e-6,'o--','linewidth',3);hold on
            end
            
            ChangePlotColor(@jet)
            TXYL('MR','W/Rc','Lmr/W','BG=%0.1f',{[BGPlot]})
            Legend=findobj(gcf, 'Type', 'Legend');Legend(1).Location='southeast';
            %   symmetrized version
            subplot(2,2,4)
            %             BGPlot=[-9.9,-5,-1.6,3,5,7.5,9.9];
            for i=1:numel(BGPlot)
                Index=find(ListBG==BGPlot(i));
                PlotWRc=ListWRc(Index);[PlotWRc,Idx]=sort(PlotWRc);
                PlotLmr=RhoXX(Index);PlotLmr=PlotLmr(Idx);
                
                WRcList=unique(PlotWRc(ismember(PlotWRc,-PlotWRc)));
                WRcList=WRcList(WRcList>=0);LmrList=[];
                for j=1:numel(WRcList)
                    WRc=WRcList(j)*(-1)^k;
                    LmrList(j)=mean([mean(PlotLmr(PlotWRc==(-WRcList(j)))),mean(PlotLmr(PlotWRc==(WRcList(j))))]);
                end
                
                plot([-fliplr(WRcList),WRcList(2:end)],[fliplr(LmrList),LmrList(2:end)],'o','linewidth',3);hold on
                %                   plot(PlotWRc,PlotLmr/5e-6,'o--','linewidth',3);hold on
            end
            
            ChangePlotColor(@jet)
            TXYL('MR','W/Rc','RhoXX','BG=%0.1f',{[BGPlot]})
%        if(~Regular )
%             TelegramSend({1,2,3});
%         elseif(min(1./TotalBetaSum)<Th)
%             Th=Th-0.1;
%             TelegramSend({1,2,3});
%         end
%         end
 
%     end
%     pause(30);
% end
%%
figure(4);clf
WRcPlotList=[4];
for i=1:numel(WRcPlotList)
    BGSort={};IndexLMR={};IndexRleft={};IndexRright={};
    FullLMR=[];FullRleft=[];FullRright=[];
    for j=1:2
        WRc=WRcPlotList(i)*(-1)^(j);
        Index=find(ListWRc==WRc);
        [BGSort{j},IDX]=sort(ListBG(Index));
        IndexLMR{j}=Lmr(Index);IndexLMR{j}=IndexLMR{j}(IDX);
        IndexRleft{j}=Rleft(Index);IndexRleft{j}=IndexRleft{j}(IDX);
        IndexRright{j}=Rright(Index);IndexRright{j}=IndexRright{j}(IDX);
    end
    FullBGList=intersect(BGSort{1},BGSort{2});
    for j=1:numel(FullBGList)
        FullLMR(j)   =mean([IndexLMR{1}(FullBGList(j)==BGSort{1}),IndexLMR{2}(FullBGList(j)==BGSort{2})]);
        FullRleft(j) =mean([IndexRleft{1}(FullBGList(j)==BGSort{1}),IndexRleft{2}(FullBGList(j)==BGSort{2})]);
        FullRright(j)=mean([IndexRright{1}(FullBGList(j)==BGSort{1}),IndexRright{2}(FullBGList(j)==BGSort{2})]);
    end
    %figure(777);
    plot(FullBGList,FullLMR/5e-6,'o--','linewidth',3);hold on
    %figure(778);
    % plot(FullBGList,FullRleft,'o--','linewidth',3);hold on
    % plot(FullBGList,FullRright,'o--','linewidth',3);hold on
end
%
%Fit_Line(8,'X',[-10,0.3]);
BGXX=linspace(-9.9,0.3,100);
clf;plot(BGXX,interp1(FullBGList,FullLMR/5e-6,BGXX));Fit_Line(7,'X',[-10,0.3]);
clf
plot(FullBGList,FullLMR/5e-6,'o--','linewidth',3);hold on
plot(BGXX,Fit.Func(BGXX),'linewidth',3);

xlim([-10,0.3])
poly=[-1.05305119e-06,-4.38744380e-05,-7.57166633e-04,-7.14722794e-03,-4.19879822e-02,-1.71069158e-01,-4.94630562e-01,4.27898584e-01];
% poly_WRc_6=[1.64360558e-06,6.01400983e-05,7.99861526e-04,4.04857659e-03,-4.43483169e-03,-1.35371171e-01,-5.78646532e-01,4.56910421e-01];
hold on
plot(BGXX,polyval(poly,BGXX),'b')
sprintf('%0.8e,',Fit.fit')

%% For  Sharvin
figure(3);
clf;%subplot(2,2,1)
WRcPlotList=[0,1,2,4,6];
for i=1:numel(WRcPlotList)
    BGSort={};IndexLMR={};IndexRleft={};IndexRright={};
    FullLMR=[];FullRleft=[];FullRright=[];
    for j=1:2
        WRc=WRcPlotList(i)*(-1)^(j);
        Index=find(ListWRc==WRc);
        [BGSort{j},IDX]=sort(ListBG(Index));
        IndexLMR{j}=Lmr(Index);IndexLMR{j}=IndexLMR{j}(IDX);
        IndexRleft{j}=Rleft(Index);IndexRleft{j}=IndexRleft{j}(IDX);
        IndexRright{j}=Rright(Index);IndexRright{j}=IndexRright{j}(IDX);
    end
    FullBGList=intersect(BGSort{1},BGSort{2});
    for j=1:numel(FullBGList)
        FullLMR(j)   =mean([IndexLMR{1}(FullBGList(j)==BGSort{1}),IndexLMR{2}(FullBGList(j)==BGSort{2})]);
        FullRleft(j) =mean([IndexRleft{1}(FullBGList(j)==BGSort{1}),IndexRleft{2}(FullBGList(j)==BGSort{2})]);
        FullRright(j)=mean([IndexRright{1}(FullBGList(j)==BGSort{1}),IndexRright{2}(FullBGList(j)==BGSort{2})]);
    end
    
    plot(FullBGList,FullRleft,'o--','linewidth',3);hold on
    plot(FullBGList,FullRright,'o--','linewidth',3);hold on
    
    R_sharvin_th = h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(FullBGList,0.7)))/5.0e-6;
    if(~isempty(FullBGList))
        %plot(sign(FullBGList).*sqrt(abs(FullBGList)),FullRleft./R_sharvin_th,'o--','linewidth',3);hold on
        %plot(sign(FullBGList).*sqrt(abs(FullBGList)),FullRright./R_sharvin_th,'o--','linewidth',3);hold on
    end
end
BGListSharvin=linspace(-9.9,9.9,100);
BGListSharvin=BGListSharvin(abs(BGListSharvin-0.7)>0.5);
R_sharvin_th = h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(BGListSharvin,0.56)))/5.0e-6;
plot(BGListSharvin,R_sharvin_th,'-','linewidth',3);hold on
legend({'W/Rc=0L','W/Rc=0R','W/Rc=1L','W/Rc=1R','W/Rc=2L','W/Rc=2R','W/Rc=4L','W/Rc=4R','W/Rc=6L','W/Rc=6R','Theory'})
ChangePlotColor(@jet);
ylim([0,200])
TXYL('Contact Resistance','BG [V]','R [ohm]')
%%
ChangePlotColor(@jet)
TXYL('symmetrized Lmr','BG [V]','Lmr/W','W/Rc=%d',{[0,1,2,4,6]})
Legend=findobj(gcf, 'Type', 'Legend');Legend(1).Location='southeast';
%
subplot(2,2,2)
WRcPlotList=[0,1,2,4,6];
for i=1:numel(WRcPlotList)
    BGSort={};IndexLMR={};
    FullLMR=[];
    for j=1:2
        WRc=WRcPlotList(i)*(-1)^(j);
        Index=find(ListWRc==WRc);
        [BGSort{j},IDX]=sort(ListBG(Index));
        IndexLMR{j}=1./RhoXX(Index);IndexLMR{j}=IndexLMR{j}(IDX);
        
    end
    FullBGList=intersect(BGSort{1},BGSort{2});
    for j=1:numel(FullBGList)
        FullLMR(j)=mean([IndexLMR{1}(FullBGList(j)==BGSort{1}),IndexLMR{2}(FullBGList(j)==BGSort{2})]);
    end
    plot(FullBGList,FullLMR,'o--','linewidth',3);hold on
end
ChangePlotColor(@jet)
TXYL('symmetrized 1/RhoXX','BG [V]','1/RhoXX','W/Rc=%d',{[0,1,2,4,6]})
Legend=findobj(gcf, 'Type', 'Legend');Legend(1).Location='North';
%   symmetrized version
subplot(2,2,3)
BGPlot=[-9.9,-5,5,9.9,3,-1.6,7.5];
for i=1:numel(BGPlot)
    Index=find(ListBG==BGPlot(i));
    PlotWRc=ListWRc(Index);[PlotWRc,Idx]=sort(PlotWRc);
    PlotLmr=Lmr(Index);PlotLmr=PlotLmr(Idx);
    
    WRcList=unique(PlotWRc(ismember(PlotWRc,-PlotWRc)));
    WRcList=WRcList(WRcList>=0);LmrList=[];
    for j=1:numel(WRcList)
        WRc=WRcList(j)*(-1)^k;
        LmrList(j)=mean([mean(PlotLmr(PlotWRc==(-WRcList(j)))),mean(PlotLmr(PlotWRc==(WRcList(j))))]);
    end
    ExportMR{i}.WRc=[-fliplr(WRcList),WRcList(2:end)];
    ExportMR{i}.Lmr=[fliplr(LmrList),LmrList(2:end)];
    ExportMR{i}.BG=BGPlot(i);
    plot(ExportMR{i}.WRc,ExportMR{i}.Lmr/5e-6,'o','linewidth',3);hold on
    
    %                   plot(PlotWRc,PlotLmr/5e-6,'o--','linewidth',3);hold on
end

ChangePlotColor(@jet)
TXYL('MR','W/Rc','Lmr/W','BG=%0.1f',{[BGPlot]})
Legend=findobj(gcf, 'Type', 'Legend');Legend(1).Location='southeast';
%   symmetrized version
subplot(2,2,4)
BGPlot=[-9.9,-5,5,9.9,3,-1.6,7.5];
for i=1:numel(BGPlot)
    Index=find(ListBG==BGPlot(i));
    PlotWRc=ListWRc(Index);[PlotWRc,Idx]=sort(PlotWRc);
    PlotLmr=RhoXX(Index);PlotLmr=PlotLmr(Idx);
    
    WRcList=unique(PlotWRc(ismember(PlotWRc,-PlotWRc)));
    WRcList=WRcList(WRcList>=0);LmrList=[];
    for j=1:numel(WRcList)
        WRc=WRcList(j)*(-1)^k;
        LmrList(j)=mean([mean(PlotLmr(PlotWRc==(-WRcList(j)))),mean(PlotLmr(PlotWRc==(WRcList(j))))]);
    end
    
    plot([-fliplr(WRcList),WRcList(2:end)],[fliplr(LmrList),LmrList(2:end)]/5e-6,'o','linewidth',3);hold on
    %                   plot(PlotWRc,PlotLmr/5e-6,'o--','linewidth',3);hold on
end

ChangePlotColor(@jet)
TXYL('MR','W/Rc','RhoXX','BG=%0.1f',{[BGPlot]})
return
%%
R_sharvin_th = h_./(8*qe_^2)*sqrt(pi)./sqrt(abs(Vbg_to_n(ListBG,0.7)))/5.0e-6;

%%
clf
BGPlot=[-9.9,-5,-1.6,5,3,9.9,7.5];
for i=1:numel(BGPlot)
    Index=find(ListBG==BGPlot(i));
    PlotWRc=ListWRc(Index);[PlotWRc,Idx]=sort(PlotWRc);
    PlotLmr=Lmr(Index);PlotLmr=PlotLmr(Idx);
    PlotLmr=(PlotLmr+fliplr(PlotLmr))/2;
    plot(PlotWRc,PlotLmr/5e-6,'o--','linewidth',3);hold on
end
ChangePlotColor(@jet)
TXYL('MR','W?Rc [V]','Lmr/W','BG=%0.1f',{[BGPlot]})