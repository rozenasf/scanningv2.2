Scans=103:120;
LoadScan(Scans,L)
ColorSet=jet(100);
%%
figure(1)
Name={'Temperature','ZsCapY'};
for j=1:numel(Name)
    subplot(1,2,j);
TimeZero=id{103}.Now*3600*24;
for i=Scans
    TimeVector=(id{i}.Time.data(:)+id{i}.Now*3600*24-TimeZero)/60;
   plot(TimeVector,id{i}.(Name{j}).data(:));
   hold on
end
hold off
xlabel('Time[Minuts]')
ylabel(Name{j})

end
suptitle(sprintf('Scans %d-%d',Scans(1),Scans(end)));
%%
figure(2)
clf

Name={'Ggr10','Rgr'};
for j=1:numel(Name)
    subplot(1,2,j);
set(gca, 'ColorOrder', ColorSet,'NextPlot','ReplaceChildren');
for i=Scans
   
   plot(BG,id{i}.(Name{j}).data);
   hold on
end
hold off
xlabel('Time[Minuts]')
ylabel(Name{j})
end
suptitle(sprintf('Scans %d-%d',Scans(1),Scans(end)));
%%
figure(3)


Name={'Temperature','ZsCapY'};


for i=Scans

   plot(id{i}.(Name{1}).data(:),id{i}.(Name{2}).data(:));
   hold on
end
hold off
xlabel(Name{1})
ylabel(Name{2})


suptitle(sprintf('Scans %d-%d',Scans(1),Scans(end)));