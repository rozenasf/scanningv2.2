function [idgr,std_idgr]=LoadScan(FileNames,Location,varargin)
global L Info A
Scale=1;
if(numel(varargin)>0)
   for i=1:numel( varargin)
      switch  varargin{i}
          case 'NoScale'
              Scale=0;
              disp('No Scaling!');
      end
   end
end
if(~exist('Location'));Location=L;end
if(~exist('FileNames'));FileNames=[];end


for i=1:max([numel(FileNames),1])
    
    if(iscell(FileNames))
        FileName=FileNames{i};
    elseif(~isempty(FileNames))
        FileName=FileNames(i);
    end
    idgr=nan;
    warning('off', 'MATLAB:dispatcher:UnresolvedFunctionHandle')
    if(~exist('Location'))
        Path=pwd;
    else
        Path=[Location.MainPath,'\',Location.ActiveNotebook];
    end
    if(~exist('FileName') || isempty(FileName))
        %a=dir([L.MainPath,'\',L.ActiveNotebook,'\Scan_*.mat']);
        a=dir([Path,'\Scan_*.mat']);
        [~,idx]=max([a.datenum]);
        FileName=a(idx).name;
    end
    if(isnumeric(FileName))
        if(mod(FileName,1)==0)
        %a=dir([L.MainPath,'\',L.ActiveNotebook,'\Scan_',num2str(FileName),'*']);
        a=dir([Path,'\Scan_',num2str(FileName),'_*']);
        FileName=a.name;
        else
            ScanNo = fix(FileName);
            SuperScanNo = 100*mod(FileName,1);
        a=dir([Path,'\Scan_',num2str(ScanNo),'_',num2str(SuperScanNo),'_*']);   
        FileName=a.name;
        end
        
    end
    FullPath=[Path,'\', FileName];
    a=dir( FullPath);
    Stamp=a.datenum;
    %if(~(isfield(A ,'FullPath') && strcmp(A.FullPath,FullPath) ))
    LoadFile( FullPath);
    if(Scale)
 Info=ScaleChannels(Info);
    end
    A.FullPath=FullPath;
    A.Stamp=Stamp;
    %end
    
    
     %Info.Scan.idgr=FixRaster(Info.Scan.idgr);
%    AddXYZToScan();
    AxisNames=fieldnames(Info.Scan.Axis);
    for i=1:numel(AxisNames)
        assignin('base', AxisNames{i},Info.Scan.Axis.( AxisNames{i}));
        Info.Scan.idgr.(AxisNames{i})=Info.Scan.Axis.( AxisNames{i});
    end
    disp('This load used FixRastet. There is indication that Axes might be wrongly evalueted');
    if(isfield(Info.Scan.Runtime,'Now'));Info.Scan.idgr.Now=Info.Scan.Runtime.Now;end
    Info.Scan.idgr.Description=Info.Scan.Description;
    try idgr=Info.Scan.idgr;end
    try std_idgr=Info.Scan.std_idgr;end
    try disp(Info.ScanNumber);end
    names=fieldnames(idgr);
    for i=1:numel(names)
        assignin('base',names{i},idgr.(names{i}));
    end
    if(1)
    try
%         F=gcf;
%         f=figure(999);f.Visible='off';
%         figure(F);
    end
    end
    assignin('base','idgr',idgr);
%    assignin('base','std_idgr',std_idgr);

if(exist('SuperScanNo'))
    evalin('base',['id{',num2str(Info.ScanNumber),'}{',num2str(SuperScanNo),'}=idgr;']);
else
    evalin('base',['id{',num2str(Info.ScanNumber),'}=idgr;']);
    
end
end
end