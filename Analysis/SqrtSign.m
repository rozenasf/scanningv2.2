function out=SqrtSign(in)
    Minus=find(in<0);
    out=sqrt(abs(in));
    out(Minus)=-out(Minus);
end