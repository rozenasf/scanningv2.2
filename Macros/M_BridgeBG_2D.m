%Y center -7.57e-6. X_calib = -22.5e-6

% BGList=[9.9,1.5,0.5,-0.5,-8.9];
BGList=[-8.9,-2.36,-0.1,2.02,9.9];
1
for i=1:numel(BGList)
    
%S.Set('Bridge')
S.Set('Ys_Ramp',-7.57e-6);
S.Set('Xs_Ramp',-22.5e-6);

S.Get('Regular');
S.Set('RampBG',BGList(i))
S.Get('Feedback','TuneSignals','Beta');
TelegramSend(num2str(GetFromMemory('InverseBetaSum')));
Document('InverseBetaSum','RampBG','Xs','Ys');
S.Get('Bridge');

S.Get('Bridge_Calibration');

S.Set('Xs_Ramp',-27e-6);
S.Set('Xs_Ramp',-16e-6);
S.Set('Ys_Ramp',-14e-6);
S.Set('Ys_Ramp',-3e-6);

S.Get('Feedback','TuneSignals','Beta')
pause(10);

% XY Scanner Feedback
Info.Scan=[];
Info.Scan.Description=['XY Scanner Feedback '];

 Info.Scan.WaitTimes=[0.5 1];
 
Info.Scan.End=BuildStruct( ...
     'Xs_Ramp',-16e-6, ...
     'Ys_Ramp',-3e-6 ...
     );
 
Info.Scan.Axis=BuildStruct(  ...
    'Xs_Ramp',linspace(-16e-6,-27e-6,34), ... 47
    'Ys_Ramp',linspace(-3e-6,-14e-6,37) ... 46
    );
Info.Scan.Get=...
    {{                                            ...
     'Feedback',2,...
     {'TuneSignals',...
     'Beta'},5,...
     'Temperature','Time',...
     @()Plot1D(1,'Idc','Hset','Hbg','HS2','HS15','HRest','Rgr15'),...
     @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint')...
    },{  ...
    'Save',...
    }};
Info.Scan.Analysis={...
    @()Plot2D(3,'Beta_.+','WorkPoint','Hset','Idc','Rgr15'),...
    ...@()Plot1D_with_color(4,'Beta_.+','WorkPoint','Hset')...
    };
S.RunScan();
end
%%
BGList=[0.5];
for i=1:numel(BGList)
    
%S.Set('Bridge')
S.Set('Ys_Ramp',-8.13e-6);
S.Set('Xs_Ramp',-12e-6);
S.Get('Regular');
S.Get('Feedback','TuneSignals','Beta');
TelegramSend(num2str(GetFromMemory('InverseBetaSum')));
S.Get('Bridge');
S.Set('RampBG',BGList(i))

S.Get('Bridge_Calibration');

S.Set('Xs_Ramp',-21e-6);
S.Set('Xs_Ramp',0);
S.Set('Ys_Ramp',-15e-6);
S.Set('Ys_Ramp',-1.5e-6);

S.Get('Feedback','TuneSignals','Beta')
pause(10);

% XY Scanner Feedback
Info.Scan=[];
Info.Scan.Description=['XY Scanner Feedback '];

 Info.Scan.WaitTimes=[0.1 1];
 
Info.Scan.End=BuildStruct( ...
     'Xs_Ramp',-0e-6, ...
     'Ys_Ramp',-1.5e-6 ...
     );
 
Info.Scan.Axis=BuildStruct(  ...
    'Xs_Ramp',linspace(-0e-6,-21e-6,47), ... 71
    'Ys_Ramp',linspace(-1.5e-6,-15e-6,31) ... 46
    );
Info.Scan.Get=...
    {{                                            ...
     'Feedback',...
     {'TuneSignals',...
     'Beta'},5,...
     'Temperature','Time',...
     @()Plot1D(1,'Idc','Hset','Hbg','HS2','HS15','HRest','Rgr15'),...
     @()Plot1D(2,'Beta_HRest_Hset','Beta_HS15_Hset','Beta_HS2_Hset','Beta_Hbg_Hset','Beta_Sum','WorkPoint')...
    },{  ...
    'Save',...
    }};
Info.Scan.Analysis={...
    @()Plot2D(3,'Beta_.+','WorkPoint','Hset','Idc','Rgr15'),...
    ...@()Plot1D_with_color(4,'Beta_.+','WorkPoint','Hset')...
    };
S.RunScan();
end
%%

S.Set('Bridge');
S.Get('Regular');
% S.Set('Ys_Ramp',-1.5e-6);
S.Get('Feedback','TuneSignals','Beta');

Info.Scan=[];
Info.Scan.Description=['Dummy Alive Scan'];

Info.Scan.WaitTimes=[0.01,0.01];
Info.Scan.Axis=BuildStruct(     ...
    'Dummy',1:100, ...
    'Dummy2',1:200 ...
    );
Info.Scan.Get=...
    {{      ...
        {'Feedback','TuneSignals','Beta'},5,'Temperature',...
        'Time', 'Save',...
        @()Plot1D(2,'Idc','Hset','Beta_Sum','Rgr15','Temperature','WorkPoint')...
    },{}};
S.RunScan();