function out=NameToChannel(in)
names={};out={};    
    for i=1:numel(in)
        names=[names,RegExpChannels(in{i})];
    end
    for i=1:numel(names)
        out=[out,evalin('base',names{i})];
    end
end