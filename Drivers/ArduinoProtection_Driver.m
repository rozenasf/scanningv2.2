classdef ArduinoProtection_Driver < handle
    properties
        Comunication
        Tic
        State
        Monitor
        Buffer
        Amplifier
        Threshold
        Averaging
        Input
        InputTime
        LastSent
        PlotObj
        FigureObj
        Timer
        SupressDisp
    end
    
    methods
        function obj=ArduinoProtection_Driver(ComPort)
            if(isnumeric(ComPort))
                obj.Comunication=serial(['COM',num2str(ComPort)],'BaudRate',115200);
            else
                obj.Comunication=serial(ComPort,'BaudRate',115200);
            end
            obj.Comunication.BytesAvailableFcnCount = 1;
            obj.Comunication.BytesAvailableFcnMode = 'terminator';
            obj.Comunication.BytesAvailableFcn = @(x,y)obj.GotData(x,y);
            obj.Tic=tic;
            obj.SupressDisp=0;
            obj.Timer=timer('TimerFcn',@(x,y)obj.CheckAlive(),'Period', 15,'ExecutionMode', 'fixedSpacing');
            fopen(obj.Comunication);
            start(obj.Timer);
        end
        %------
        
        function [Value,Text]=GetFromMemory(obj,Channel)
            Value=obj.State;
            switch Value
                case 0
                    Text='Disabled';
                case 1
                    Text='Armed';
                case 2
                    Text='Pulled!';
            end
        end
        function OK=ScanStart(obj)
            OK=1;
            switch(GetFromMemory('ArduinoState'))
                case 0
                    input('Arduino Protection is disabled. press enter to continue');
                case 1
                    disp('Arduino is Armed! ok');
                case 2
                    disp('Arduino is pulled. Will not start scan!!!');
                    OK=0;
            end
        end
        %-----
        function Send(obj,String)
            obj.LastSent=String;
            switch obj.LastSent
                case {'r','m'}
                    obj.Buffer=[];
            end
            fprintf(obj.Comunication,String);
        end
        function CheckAlive(obj)
            if(toc(obj.Tic)>30)
                obj.SupressDisp=1;
                obj.Send('s');
            end
        end
        function GotData(obj,x,y)
            obj.InputTime=y.Data.AbsTime;
            obj.Tic=tic;
            obj.Input=fgetl(obj.Comunication);
            obj.Input=obj.Input(1:end-1);
            obj.AnalizeInputData();
            if(obj.SupressDisp && ...
                    (strcmp(obj.Input,'0') || strcmp(obj.Input,'1') || strcmp(obj.Input,'2')))
                obj.SupressDisp=0;
            else
                disp(obj.Input);
            end
        end
        function AnalizeInputData(obj)
            global Info
            if(ismember('.',obj.Input) && ismember(obj.Input(1),'0123456789'))
                obj.Buffer=[obj.Buffer,str2double(obj.Input)];
                if(numel(obj.Buffer)==256 && strcmp(obj.LastSent,'r'))
                   SaveBuffer(obj); 
                end
                obj.UpdatePlot();
            else
                
                switch(obj.Input)
                    case 'm on'
                        obj.Monitor=1;
                        obj.Plot;
                    case 'm off'
                        obj.Monitor=0;
                        delete(obj.FigureObj);
                    case {'0','disabled'}
                        obj.State=0;
                    case {'1','armed','unpulled'}
                        obj.State=1;
                    case {'2'}
                        obj.State=2;
                    case {'p'}
                        Info.Flags.StopRequest=1;
                        Info.Flags.Stop=1;
                        obj.State=2;
                        obj.Send('r');
                        obj.Plot;
                        try TelegramSend('Arduino Pull!');end
                    %case 
                end
            end
            %Monitor
        end
        function Plot(obj)
            if(isempty(obj.PlotObj) || ~isvalid(obj.PlotObj))
                obj.FigureObj=figure();
                obj.PlotObj=plot(0,0);
            end
            obj.UpdatePlot();
        end
        function UpdatePlot(obj)
            if(~isempty(obj.PlotObj) && isvalid(obj.PlotObj))
                obj.PlotObj.YData=obj.Buffer;
                obj.PlotObj.XData=1:numel(obj.Buffer); 
            end
        end
         function out=Toc(obj)
            out=toc(obj.Tic);
        end
        function SaveBuffer(obj)
            allOneString = ['Arduiono_Buffer_',sprintf('%d_' , obj.InputTime)];
            allOneString=[allOneString(1:end-1),'.mat'];
            Buffer=obj.Buffer;
            BaseFolder=regexp(pwd,'Documents','split');
            BaseFolder=[BaseFolder{1},'Documents\ArduinoProtectionBuffer\'];
            if(~exist(BaseFolder))
                mkdir(BaseFolder);
            end
            obj2=obj;
            obj2.FigureObj=[];
            obj2.PlotObj=[];
            save([BaseFolder,allOneString],'Buffer','obj2');
        end
        function delete(obj)
            try fclose(obj.Comunication);delete(obj.Comunication);end
            try stop(obj.Timer);delete(obj.Timer);end
            try delete(obj.PlotObj);end
        end
    end
end