function figureN(N)
    global Info
    if(~isfield(Info,'Fig'));Info.Fig={};end
    if(numel(Info.Fig)>=N && ~isempty(Info.Fig{N}) && ishandle(Info.Fig{N}))
        set(0,'CurrentFigure',Info.Fig{N})
    else
        Info.Fig{N}=figure(N);
       
        try Info.Fig{N}.Position=Info.MoreParameters.FigPosition{N};
        
        end
        set(0,'CurrentFigure',Info.Fig{N})
    end
end