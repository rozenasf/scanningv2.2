
Y={};Traces={};
%ScanList=1017:1083; %1017:1083 LONG OVERNIGHT LOW EXITATION SUM=7.5mV
%ScanList=1105:1136; % Low Voltage
% ScanList=1162:1172;
%  ScanList=[1400:1403]; 
 ScanList=[1462:1477]; %as function of BG 7.5mV on graphene

%%
clf
BGList=[-9.9,-8.9,-6.9,-5.0,-3.4,-2.7,-2.0,-1.4,-0.9,-0.4,0.0];
Range=[-1.25,1.25];
CurvetureSTDList=[];SumList=[];
for j=1:numel(ScanList)
CurvetureSTDList(j)=GetCurvetureFromDSB4(ScanList(j),Range);
SumList(j)=Info.Channels.Sum
end
%%
clf
SumVec=[10e-3,15e-3,20e-3];CurvetureMean=[];CurvetureSTD=[];
for i=1:numel(SumVec)
CurvetureMean(i)=mean(CurvetureSTDList(find(SumList==SumVec(i))));
CurvetureSTD(i)=std(CurvetureSTDList(find(SumList==SumVec(i))))./sqrt(sum(SumList==SumVec(i))-1);
end
errorbar(SumVec,CurvetureMean,CurvetureSTD,'linewidth',3)

errorbar(1e3*SumVec*(1-0.4609),CurvetureMean/4,CurvetureSTD/4,'m','linewidth',3)
hold on
%Based on files F405, F402, F406
%BG @ 4K is -2V. example scan is 1007.01
%BG @ 75K is -5V. example scan is 1462
%Roughly the same RhoXX (0.5391 factor compared with 0.5743)
errorbar(1e3*[0.0075,0.015,0.025]*(1-0.4257),[0.15,0.78,1.18]/4,[0.18/4,0.09/4,0.11/4],'b-','markersize',30,'linewidth',3)
Ax=gca
Ax.FontSize=16
grid
TXYL('','$V_{ex} $[mV]','$E_y $ Curvature,$\kappa$','T=75K','T=4K')
xlim([2.5,15])
%The error bars folowes:
% F405_Curveture_Multi_Side_Bridge_WRc_1_6_Lmr_1_5W_Low
% F402_Multi_Side_Bridge_WRc_1_6_Lmr_1_5W
% F406_Curveture_Multi_Side_Bridge_WRc_1_6_Lmr_1_5W_High
% figure(8)
% STD=std(Fit.Y-Fit.Func(Fit.X));STDOUT=[];
% figure(9);clf
% for i=1:100
% FakeY=Fit.Func(Fit.X)+STD*randn(1,numel(Fit.X));
% plot(Fit.X,FakeY);hold on
% end
% Fit_Line(2)
% for i=1:100
%    STDOUT(i)= std(Fit(i).Y-Fit(i).Func(Fit(i).X));
% end
% CurvetureSTD=std(-[Fit.a]./[Fit.Y_0]*(5)^2)
% For 7.5mV (F405): 0.18
% For 15mV (F402):  0.09
% For 25mV (F406):  0.11
% plot(Fit.X,Fit.Y-Fit.Func(Fit.X))