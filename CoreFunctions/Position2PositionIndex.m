function PositionIndex=Position2PositionIndex(Position)
    global Info;
    %needs to be checked... 24.2.2018
    MaxPosition=Info.Scan.Runtime.MaxPosition;
    a=cumprod(MaxPosition);
    a=[1,a(1:end-1)];
    PositionIndex=1+a*(Position-1)';
end