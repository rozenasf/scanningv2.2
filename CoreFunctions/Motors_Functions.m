function S=Motors_Functions(S)
%% Initialization Of S for motors
S.ClosedLoop=                  @ClosedLoop;
S.Motors.MoveCloser_Steps=     @MoveCloser_Steps;
S.Motors.MoveFarther_Steps=    @MoveFarther_Steps;
S.Motors.MoveNorth_Steps=      @MoveNorth_Steps;
S.Motors.MoveSouth_Steps=      @MoveSouth_Steps;
S.Motors.MoveEast_Steps=       @MoveEast_Steps;
S.Motors.MoveWest_Steps=       @MoveWest_Steps;
S.Motors.MoveCloser_Microns=   @MoveCloser_Microns;
S.Motors.MoveFarther_Microns=  @MoveFarther_Microns;
S.Motors.MoveNorth_Microns=    @MoveNorth_Microns;
S.Motors.MoveSouth_Microns=    @MoveSouth_Microns;
S.Motors.MoveEast_Microns=     @MoveEast_Microns;
S.Motors.MoveWest_Microns=     @MoveWest_Microns;
%%
    function ClosedLoop(Ax,Target)
        global Info;
        Constants=BuildStruct(...
            'MaxSteps',300,...
            'WaitTime',0.1,...
            'StepSize',[0.5e-6,0.5e-6,0.5e-6,0.1],...
            'Safty',[0.4,0.4,0.4,0.4], ...
            'Tolerance',[2e-6,2e-6,2e-6,1e-1]);
        %timeout=20 S
        NumberOfAxis=find(strcmp(Info.ChannelType.ClosedLoop,Ax));
        NameOfGetAxis   =Info.ChannelType.CourseLocation{NumberOfAxis};
        NameOfStepAxis  =Info.ChannelType.CourseSteps{NumberOfAxis};
            S.Get(NameOfGetAxis);
                InitialSign=sign(Info.GetChannels.(NameOfGetAxis)-Target);
                TimeOutSteps=1e9;Count=0;
                TimeOut=tic;
                while(sign(Info.GetChannels.(NameOfGetAxis)-Target)==InitialSign ...
                        && abs(Info.GetChannels.(NameOfGetAxis)-Target)>Constants.Tolerance(NumberOfAxis)...
                        && Count<TimeOutSteps && toc(TimeOut)<20)    
                    delta=Info.GetChannels.(NameOfGetAxis)-Target;
                    steps=-sign(delta)*ceil(abs(Constants.Safty(NumberOfAxis)*delta./Constants.StepSize(NumberOfAxis)));
                    if(abs(steps)>Constants.MaxSteps)
                        steps=Constants.MaxSteps*sign(steps);
                    end
                    S.Set(NameOfStepAxis,steps);
                    pause(Constants.WaitTime); 
                    Count=Count+1;
                    S.Get(NameOfGetAxis);
                end
                if(sign(Info.GetChannels.(NameOfGetAxis)-Target)~=InitialSign);disp(['warning:Overshoot ',NameOfGetAxis]);end
                if(toc(TimeOut)>40);input('Interlock! Closed loop doesnt converge!');end
                S.Get(NameOfGetAxis);
    end
%% Low Level movement
        function MoveCloser_Steps(steps);S.Set('Zo',steps);end
        function MoveFarther_Steps(steps);S.Set('Zo',-steps);end
        function MoveNorth_Steps(steps);S.Set('Yo',steps);end
        function MoveSouth_Steps(steps);S.Set('Yo',-steps);end
        function MoveEast_Steps(steps);S.Set('Xo',steps);end
        function MoveWest_Steps(steps);S.Set('Xo',-steps);end
    
        function MoveCloser_Microns(micorns);   global Info;if(micorns~=0);S.Get('Zr');S.Set('Zc',Info.GetChannels.Zr + micorns);end;end
        function MoveFarther_Microns(micorns);  global Info;if(micorns~=0);S.Get('Zr');S.Set('Zc',Info.GetChannels.Zr - micorns);end;end
        function MoveNorth_Microns(micorns);    global Info;if(micorns~=0);S.Get('Yr');S.Set('Yc',Info.GetChannels.Yr + micorns);end;end
        function MoveSouth_Microns(micorns);    global Info;if(micorns~=0);S.Get('Yr');S.Set('Yc',Info.GetChannels.Yr - micorns);end;end
        function MoveEast_Microns(micorns);     global Info;if(micorns~=0);S.Get('Xr');S.Set('Xc',Info.GetChannels.Xr + micorns);end;end
        function MoveWest_Microns(micorns);     global Info;if(micorns~=0);S.Get('Xr');S.Set('Xc',Info.GetChannels.Xr - micorns);end;end

end