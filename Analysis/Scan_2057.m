LoadScan([2056],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
%% %30mV
%% 2mV - 9.6 SNR
LoadScan([2057],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope
%% 1mV - 13.1
LoadScan([2059],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope
%% 0.75mV - 15.7
LoadScan([2062],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope
%% 0.65mV - 12.14
LoadScan([2063],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope
%% 0.5 mV - 16.4
LoadScan([2060],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope
%% 0.5 mV - 17
LoadScan([2064],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope
%% 0.3 mV - 12.8
LoadScan([2061],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope
%%
%% @0.5mV -> 31mV - 16.2
LoadScan([2065],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope
%% @0.5mV -> 33mV - 11.2
LoadScan([2066],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%% @0.5mV -> 29mV @ WP 6 - 11.1
LoadScan([2067],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%% @0.5mV -> 29mV @ WP 7 - 12.8
LoadScan([2068],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%% @0.5mV -> 31mV @ WP 7 - 19.5
LoadScan([2069],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%% @0.5mV -> 31mV @ WP 6.5 - 14.7
LoadScan([2070],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%% @0.5mV -> 31mV @ WP 7 - 13.67
LoadScan([2071],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%%  @0.5mV -> 30mV @ WP 7.5 - 11
LoadScan([2072],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope

%% @0.5mV -> 31mV @ WP 7 - 13.75
LoadScan([2073],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%% @0.5mV -> 31mV @ WP 7 -ratio 10.8 -  13.8
LoadScan([2076],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%% @0.5mV -> 31mV @ WP 7 -ratio 10.8 @25 Vsd -  11.7
LoadScan([2077],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%% @0.5mV -> 31mV @ WP 7 -ratio 10.8 @28 Vsd -  11.7
LoadScan([2078],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%% @0.5mV -> 31mV @ WP 7 -ratio 10.8 @40 Vsd -  16.4
LoadScan([2079],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%% @0.5mV -> 31mV @ WP 7.5 -ratio 10.8 @40 Vsd -  17.5
LoadScan([2081],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%% @0.5mV -> 31mV @ WP 7 -ratio 10.8 @40 Vsd - 15
LoadScan([2083],L);[Slope,Std,Zero]=GetRho(Beta_HS14_Hset);plot(Beta_HS14_Hset);Fit_Line(1);title(num2str(Slope/Std))
Slope/Std
%%
LoadScan(2084,L);figure(1);plot(Beta_HS14_Hset);
hold on
LoadScan(2085,L);figure(1);plot(Beta_HS14_Hset);
LoadScan(2086,L);figure(1);plot(Beta_HS14_Hset);
hold off