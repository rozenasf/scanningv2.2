function out = BuildStruct( varargin )
if(numel(varargin)==0)
    out=struct();
else
    for i=1:(nargin/2)
        out.(varargin{2*i-1})=varargin{2*i};
    end
end
end

