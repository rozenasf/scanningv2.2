function Number=FindLastScanNumber()
    global  L
    a=dir([L.MainPath,L.ActiveNotebook,'\*.mat']);
        c={a.name};
        Max=1;
        for i=1:numel(c)
            if(regexp(c{i},'Scan_.+_.+'))
                b=regexp(c{i},'_','split');
                num=str2num(b{2});
                Max=max([Max,num]);
            end
        end
        Number=Max;
end