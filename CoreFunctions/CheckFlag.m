function out=CheckFlag(Struct,field)
    out=(isfield(Struct,field) && Struct.(field));
end