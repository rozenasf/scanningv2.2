function y=ErfFit(x,a,b,c,d)
    y=d+c.*erf(b.*(x-a));
end