%% 
LoadScan([2267],L);
% N_B=length(Info.Scan.idgr.Beta_HS15_Hset.axes{2});
% N_x=length(Info.Scan.idgr.Beta_HS15_Hset.axes{1});
N_B=numel(Magnet_I);
N_x=numel(Xs);

%% parameters
VSenseGain=1e3;RSense=120;
W=5*um_;
Vsd=Info.Dac.DispExs.VS15.Amp;
v_dirac=-1; %approximate for now
Magnet_Constant=1/10*120e-3;

B=Magnet_I*Magnet_Constant;

n= Vbg_to_n(Info.Channels.BG, v_dirac);
k_F_vec = sqrt(pi*abs(n));

Rc=r_cyclo(n, B).*sign(B);Rc(isnan(Rc))=inf;

%%
Phi=0.5+0.5*(Beta_HS15_Hset-Beta_HS2_Hset)./(Beta_HS15_Hset+Beta_HS2_Hset);Phi.name='Phi';
% Phi=Phi.roi(1,-33e-6,-25e-6);

Phi.axes{2}.data=Phi.axes{2}.data*Magnet_Constant;Phi.axes{2}.name='B';


%%
figure(5);
plot_with_color(Phi,1)
% Fit_Line(1,'all')

% R=slice_index(map_vectors(Rgr15,@(x)mean(x),1),1,1);R.name='R';
PhiR=Phi.*Rgr15;
PhiR.name='PhiR';

DPhi_DXs=Rgr15.slice_index(1,1)*0;
for i=1:N_B
   F=polyfit(Xs',Phi.data(:,i),1);
   DPhi_DXs.data(i)=F(1);
end

% dPhidX=[Fit.a, zeros(1,N_B-length([Fit.a]))];

RhoXX=DPhi_DXs.*mean(Rgr15,1)*W; RhoXX.name='RhoXX';
RhoXX.data=(fliplr(RhoXX.data)+RhoXX.data)/2;
l_mfp=1./RhoXX/k_F_vec/2/qe_^2*h_/um_;
l_mfp.name='l_mfp[um]';

figure(24);
subplot(211);l_mfp.plot;
subplot(212);RhoXX.plot;

return;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
for i=1:size(Phi.data,2)
   F=polyfit(Phi.axes{1}.data',Phi.data(:,i),1);
   DPhi_DXs.data(i)=F(1);
end
DPhi_DXs.data=0.5*(DPhi_DXs.data+fliplr(DPhi_DXs.data))
%%

figure(1)
plot(mean(Rgr,1))
AddTitle('Density: 4.4417e+11 1/cm^2')
set(gca,'XTick',[-120:30:120]*1e-3)
grid
%%
RhoXX=DPhi_DXs*Vsd*W./I1;
Idata('RhoXX',RhoXX);
figure(2)
plot(RhoXX)
AddTitle('Density: 4.4417e+11 1/cm^2')
set(gca,'XTick',[-120:30:120]*1e-3)
grid
%%
RhoXX=DPhi_DXs*Vsd*W./I1;
Idata('RhoXX',RhoXX,'W/Rc',W./Rc);
figure(3)
plot(RhoXX)
AddTitle('Density: 4.4417e+11 1/cm^2')
set(gca,'XTick',[-8,-6,-4,-2,-1,0,1,2,4,6,8])
grid
%%
figure(4)
Idata('l_tr_um',1e6*( h_./(2*qe_^2*k_F_vec.*RhoXX))); % in um
plot(l_tr_um)
AddTitle('Density: 4.4417e+11 1/cm^2')
set(gca,'XTick',[-8,-6,-4,-2,-1,0,1,2,4,6,8])
grid