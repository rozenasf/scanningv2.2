LoadScan([2570],L);
Average_Beta_HS15_Hset=Beta_HS15_Hset.slice_index(3,1)*0;
for i=1:9
    Average_Beta_HS15_Hset=Average_Beta_HS15_Hset+Beta_HS15_Hset.slice_index(3,i);
end

Average_Beta_HS15_Hset=Average_Beta_HS15_Hset/7;
Average_Beta_HS15_Hset.name='Average_Beta_HS15_Hset';
%
plot_with_color(Average_Beta_HS15_Hset,1)
Ax=gca;
e=Ax.Children;
for i=1:numel(e)
   e(i).LineWidth=3; 
end
title('BG@8V, Scan 2570');
Ax.FontSize=20;
xlim([-29e-6,-26.5e-6])
%%

plot_with_color(Average_Beta_HS15_Hset,2)
Ax=gca;
e=Ax.Children;
for i=1:numel(e)
   e(i).LineWidth=3; 
end
title('BG@8V, Scan 2570');
Ax.FontSize=20;
xlim([-12e-6,-7.5e-6])
%%
pcolor(permute(Average_Beta_HS15_Hset,[2,1]))
AddGDS(GDS,[1,0,0,1,-3.8,-3.65]*1e-6,0);
axis equal
ylim([-30,-26.5]*1e-6);
xlim([-12,-6.5]*1e-6)