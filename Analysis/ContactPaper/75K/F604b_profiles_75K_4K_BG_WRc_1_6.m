Based on F411
Scans [1014.01:0.01:1014.04], [1015.01:0.01:1015.04]
[[1:0.25:3,3.5:0.5:6],[1:0.25:3],[1:0.25:2.5],[1:0.25:2]];
figure(2);clf
Y={};Traces={};
ScanList=1017:1083; %SUM=7.5mV and BG=-2 as function of W/Rc
ScanList={};
ScanList{1}=[1173:1235]; % LowExitation dinamic
ScanList{2}=1105:1136; % Low Voltage Sum=7.5mV
ScanList{3}=1162:1172; % High Voltage
ScanList{4}=1406:1460; % High Temperature 75K
Output={};
figure(4113);clf
for ScanExitationVelueIndex=4:-1:1
Range=[-1.28,1.28];
Curveture=[];CurvetureSTD=[];
BGList=[];BGListFull=[];BetaSumList=[];

for i=1:numel(ScanList{ScanExitationVelueIndex})
    LoadScan(ScanList{ScanExitationVelueIndex}(i)+0.01,L,'NoScale');BGListFull(i)=Info.Scan.UserData.BG;
    BetaSumList(i)=1./Info.Scan.UserData.Initial_Beta_Sum;
end
BGList_unique=uniquetol(BGListFull,1e-3);
for i=1:numel(BGList_unique)
    figure(4111);clf
        index=find( abs(BGList_unique(i)-BGListFull)<1e-3 );
        
        CurvetureSTDList=[];
        for j=1:numel(index)
            CurvetureSTDList(j)=GetCurvetureFromDSB4(ScanList{ScanExitationVelueIndex}(index(j)),Range);
        end
        [Curveture(i),WRcList(i),BG]=GetCurvetureFromDSB4(ScanList{ScanExitationVelueIndex}(index),Range);
        BGList(i)=BG;
        if(numel(index)>1)
        CurvetureSTD(i)=std(CurvetureSTDList)./sqrt(numel(index)-1);
        else
            CurvetureSTD(i)=nan;
        end
        
end
    
figure(4113);hold on
WRcList_unique=unique(WRcList);Curveture_AVR=[];
Lmr_BG_Fit=[-9.38362e-05,-4.20564e-03,-6.39272e-02,-4.72690e-01,-3.20175e+00,2.54587e+00];
Lmr=polyval(Lmr_BG_Fit,BGList_unique);
errorbar(Lmr/5,Curveture,CurvetureSTD,'.','markersize',20,'linewidth',2);
errorbar(BGList_unique,Curveture,CurvetureSTD,'.','markersize',20,'linewidth',2);
Output{ScanExitationVelueIndex}.BG=BGList_unique;
Output{ScanExitationVelueIndex}.Curveture=Curveture;
Output{ScanExitationVelueIndex}.CurvetureSTD=CurvetureSTD;
TXYL('curveture in Ey','lMR/W','curveture')
TXYL('curveture in Ey','BG','curveture')
Export.Figure3.Panel3.Curve1=[BGList;Curveture;CurvetureSTD];
1016 is the first one
ylim([-2,4]);grid
end
legend({'75K','High','Low Constant','Low dinamic'},'location','northwest')
return

clf
LmrPoly_4K=[5.69814893e-06,1.96910484e-04,2.67801986e-03,1.77549882e-02,5.48655391e-02,2.84913971e-02,-5.50675966e-01,5.11791320e-01];
LmrPoly_75K=[-1.05305119e-06,-4.38744380e-05,-7.57166633e-04,-7.14722794e-03,-4.19879822e-02,-1.71069158e-01,-4.94630562e-01,4.27898584e-01];
for i=[4,1]
    if(i==4)
        errorbar(polyval(LmrPoly_75K,Output{i}.BG),Output{i}.Curveture,Output{i}.CurvetureSTD,'.','markersize',20,'linewidth',2);
    else
        errorbar(polyval(LmrPoly_4K,Output{i}.BG),Output{i}.Curveture,Output{i}.CurvetureSTD,'.','markersize',20,'linewidth',2);
    end
    hold on

end
grid
legend({'75K','Low dinamic'},'location','northeast');
xlim([0.25,2.5]);ylim([0,3.75])
TXYL('Curveture as function of Lmr','Lmr/W','Curveture')

clf
LmrPoly_4K=[5.69814893e-06,1.96910484e-04,2.67801986e-03,1.77549882e-02,5.48655391e-02,2.84913971e-02,-5.50675966e-01,5.11791320e-01];
For W/Rc=4 Lmr Calibration:
LmrPoly_75K=[-1.05305119e-06,-4.38744380e-05,-7.57166633e-04,-7.14722794e-03,-4.19879822e-02,-1.71069158e-01,-4.94630562e-01,4.27898584e-01];
For W/Rc=6 Lmr Calibration:
 LmrPoly_75K=[1.64360558e-06,6.01400983e-05,7.99861526e-04,4.04857659e-03,-4.43483169e-03,-1.35371171e-01,-5.78646532e-01,4.56910421e-01];
for i=[1,4]
    if(i==4)
        errorbar(log10(polyval(LmrPoly_75K,Output{i}.BG)),Output{i}.Curveture,Output{i}.CurvetureSTD,'.','markersize',20,'linewidth',2);
        
    else
        errorbar(log10(polyval(LmrPoly_4K,Output{i}.BG)),(Output{i}.Curveture),Output{i}.CurvetureSTD,'.','markersize',20,'linewidth',2);
        Fit=Fit_Line(4);
    end
    hold on

end
ClearFit
plot(linspace(-0.4,0.4,100),Fit.Func(linspace(-0.4,0.4,100)),'b--','linewidth',2)
grid
legend({'4K','75K - Calibrated At W/Rc=4'},'location','northeast');
 xlim([-0.2,0.4]);ylim([0,3])
TXYL('Curveture as function of Lmr','log10(Lmr/W)','Curveture')