function UpdateTimeAxes(Value)
global Info S
if(Info.Flags.RunningScan)
    TimeAxisIndex=find(strcmp(Info.Scan.Runtime.AxisNames,'Time'));
    RelevantPosition=Info.Scan.Runtime.Position(TimeAxisIndex);
    Info.Scan.Axis.Time(RelevantPosition)=Value;
    if(isstruct(Info.Scan.idgr))
        names=fieldnames(Info.Scan.idgr);
        for i=1:numel(names)
            Info.Scan.idgr.(names{i}).axes{TimeAxisIndex}.data(RelevantPosition)=Value;
        end
    end
end
end