function Index=GetIndexFromPosition(Size,Position)
    Cum_Size=cumprod(Size);
    Cum_Size=[1,Cum_Size(1:end-1)];
    Index=Cum_Size*(Position-1)'+1;