
[idgr,~]=LoadScan(893);

width=[];
for i=size(idgr.Beta_Hm_Hset,2):-1:1;
    [width(i),Plot]=FitDataToStep(idgr.Beta_Hm_Hset,i);
end
x=idgr.Beta_Hm_Hset.axes{2}.data;
y=width;
figure(111);plot(idgr.Beta_Hm_Hset.axes{2}.data,width,'o')
hold on
fit=polyfit(x,y,1);Crash=roots(fit);
plot([min(x),Crash],polyval(fit,[min(x),Crash]),'m');
hold off
title(sprintf('Crash at:%0.3f um, slope is %0.3f', Crash*1e6,fit(1)))
xlabel('Zs [m]');ylabel('Width[m]');