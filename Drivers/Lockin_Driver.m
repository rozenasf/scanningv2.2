classdef Lockin_Driver < handle
    properties
        Freq
        Vac
        Vauxout
        Vauxin
        Iac
        X
        Y
        Communication
        Phase
    end
    methods
        function obj=Lockin_Driver(address)
           obj.Freq=0;
           obj.Vac=0;
           obj.Iac=0;
           obj.X = 0;
           obj.Y = 0;
           obj.Phase = 0;
           obj.Communication=gpib('adlink',address(1),address(2));
           fopen(obj.Communication);
           fprintf(obj.Communication, 'AOFF 2');
           disp('Connection to Lock-in established!');
        end
        function [Value,Text]=GetFromMemory(obj,Channel)
            Text=[];Value=0;
            switch Channel(1)
                case 'V'
                    if Channel(2) == 'a'
                        Value=obj.Vac;
                    else
                        Value = obj.Vauxin;
                    end
                case 'I'
                    Value=obj.Iac;
                case 'F'
                    Value=obj.Freq;
                case 'X'
                Value=obj.X;
                case 'Y'
                Value=obj.Y;
                case 'P'
                Value=obj.Phase;
            end
        end
        %-----
        function out=Get(obj,Channel)
            switch Channel(1)
                case 'X'
                    %fprintf(obj.Communication, 'AOFF 2');
                    %pause(0.01);
                    fprintf(obj.Communication, 'REST');
                    fprintf(obj.Communication, 'OUTP?1');
                    obj.X=str2double(fscanf(obj.Communication));
                    out = obj.X;
                case 'Y'
                    fprintf(obj.Communication, 'REST');
                    fprintf(obj.Communication, 'OUTP?2');
                    obj.Y=str2double(fscanf(obj.Communication));
                    out = obj.Y;
                case 'P'
                    fprintf(obj.Communication, 'REST');
                    fprintf(obj.Communication, 'PHAS?');
                    obj.Phase=str2double(fscanf(obj.Communication));
                    out = obj.Phase;
                case 'V'
                    if Channel(2) == 'a'
                        out=obj.Vac;
                    else
                        fprintf(obj.Communication, 'OAUX?1');
                        obj.Vauxin = str2double(fscanf(obj.Communication));
                        out = obj.Vauxin;
                    end
                case 'F'
                    out=obj.Freq;
            end                    
        end
        function Set(obj,Channel,Value)
            switch Channel(1)
                case 'V'
                    if Channel(2) == 'a'
                        obj.Vac = Value;
                        fprintf(obj.Communication, 'SLVL%f',Value);
                    else
                        obj.Vauxout = Value;
                        fprintf(obj.Communication, 'AUXV1,%f',Value);
                    end
                case 'F'
                    obj.Freq=Value;
                    fprintf(obj.Communication, 'FREQ%f',Value);
            end
        end
        function delete(obj)
            fclose(obj.Communication);
            try delete(obj);end
            disp('Lockin disconnected!');
        end
    end
end