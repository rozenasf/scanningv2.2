
%% Active Range
%-------------------------------%
%-------------------------------%
Info.Range=BuildStruct(                                 ...
    'Xs0',[0e-6,19e-6],'Ys0',[0e-6,52e-6],'Zs0',[-1e-9,13.01e-6], ...
    'Xc',[3200e-6,4600e-6],'Yc',[3700e-6,4100e-6],'Zc',[3300e-6,5299e-6],        ...
    'DSL',[-0.05,0.05],'DSM',[-0.08,0.08],'DSR',[-0.05,0.05],...
    'Beta',[0,1/1],'IdcL',[-1e-7,1e-7],'IdcR',[-3e-7,3e-7]...
    );
UpdateMarkerPosition();
DocSend({'Range Set:',Info.Range});
%-------------------------------%
%-------------------------------%
%% enable course
Info.Flags.CourseEnable=1;
%% Retract
S.Set('Retract');
DocSend({'Retract'});
%% Position above BG Area, really safe hight (0.5mm)
S.Set('Xc',3.7e-3,'Yc',3.5e-3,'Zc',5000e-6);
DocSend({'Position above BG Area. Moved to:',S.Get('Location')});
%% Check signal integrity by gate depandance
DocSend({'Checkign signal integrity:'});
D_GateDepandenceOfLargeDevice('Gate2',linspace(1000e-3,300e-3,31),'Vsd',30e-3);
%% Approaching 
DocSend({'Far Approach:',S.Get('Location')});
D_ZApprCoarse('Gate2',linspace(750e-3,500e-3,7),'Zc',linspace(S.Get('Zr'),5240e-6,10),'Vsd',30e-3);
%% Approaching (100 um, about 100 from surface at the end)
DocSend({'Macro Approach:',S.Get('Location')});
D_ZApprCoarse('Gate2',linspace(750e-3,500e-3,7),'Zc',linspace(S.Get('Zr'),5340e-6,10),'Vsd',30e-3);
%% Slice the large Pad Y direction
DocSend({'Slicing Y course from position:',S.Get('Location')});
D_YCourseScan('Gate2',linspace(750e-3,500e-3,5),'Yc',linspace(3500e-6,4100e-6,10),'Vsd',30e-3);
%% Go to middle of Pad
S.Set('Yc',3950e-6);
DocSend('Going to middle of pad');
%% Get Closer
DocSend({'Get Closer:',S.Get('Location')});
D_ZApprCoarseAll('Gate2',linspace(600e-3,400e-3,13),'Zc',linspace(S.Get('Zr'),5286e-6,10),'Vsd',30e-3);
%% Slice the large Pad Y direction, small area

DocSend({'Slicing Y course from position, will do a small window:',S.Get('Location')});
D_YCourseScan('Gate2',linspace(750e-3,500e-3,4),'Yc',linspace(3850e-6,4050e-6,10),'Vsd',30e-3);
%% fast Y scan
%D_YCourseScanFast('Gate2',500e-3,'Yc',linspace(S.Get('Yr'),4050e-6,50),'Vsd',30e-3);
%D_YCourseScanFast('Gate2',500e-3,'Yc',linspace(S.Get('Yr'),3850e-6,10),'Vsd',30e-3);
D_YCourseScanFast('Gate2',520e-3,'Yc',linspace(S.Get('Yr'),3940e-6,20),'Vsd',30e-3);
%% fast X scan
D_XCourseScanFast('Gate2',520e-3,'Xc',linspace(S.Get('Xr'),3640e-6,3),'Vsd',30e-3);
%% find best signal to noise value
%S.Set('Xc',3.67e-03,'Yc',3.95e-3,'Zc',5350e-6);%above the CX pad
D_NoiseAsFunctionOfGate('Gate2',linspace(600e-3,300e-3,6),'Dummy',1:20,'Vsd',30e-3);
%%
%D_YCourseScanFast('Gate2',590e-3,'Yc',linspace(S.Get('Yr'),3950e-6,30),'Vsd',30e-3);
D_XCourseScanFast('Gate2',520e-3,'Xc',linspace(S.Get('Xr'),4025e-6,20),'Vsd',30e-3);

%% Approach Z coarse!!!
D_ZApprCoarseAll('Gate2',linspace(800e-3,600e-3,13),'Zc',linspace(S.Get('Zr'),5298e-6,10),'Vsd',30e-3);

%% ##############################Calibrations##############################

%% Large Devices
DocSend({'Large Devices:'});
D_GateDepandenceOfLargeDevice('Gate2',[linspace(1200e-3,600e-3,31),linspace(600e-3,1200e-3,31)],'Vsd',30e-3);
%% Small Devices
DocSend({'Small Devices:'});
D_GateDepandenceOfSmallDevice('Gate1',[linspace(1600e-3,1300e-3,31),linspace(1300e-3,1600e-3,31)],'Vsd',60e-3);


%% ##############################Scanner##############################

Info.Flags.CourseEnable=0;

% ------------------------------------------------------
%% Scanner Z ##########################################################################################
D_ZApprScanner('Gate2',linspace(600e-3,400e-3,11),'Zs0',linspace(S.Get('Zs0'),2e-6,20),'Vsd',30e-3);
%% ##########################################################################################
%% Scanner X  
D_XScannerScanLargeFast('Gate2',520e-3,'Xs0',linspace(S.Get('Xs0'),30e-6,100),'Vsd',30e-3);
%%
D_XScannerScanSmallFast('Gate1',1400e-3,'Xs0',linspace(S.Get('Xs0'),0e-6,100),'Vsd',20e-3);
%% ##########################################################################################
%% Scanner Y 
D_YScannerScanLargeFast('Gate2',520e-3,'Ys0',linspace(S.Get('Ys0'),60e-6,100),'Vsd',30e-3);
%%
D_YScannerScanSmallFast('Gate1',1400e-3,'Ys0',linspace(S.Get('Ys0'),60e-6,100),'Vsd',20e-3);
%%
D_YScannerScanSmall('Gate1',linspace(1400e-3,1000e-3,13),'Ys0',linspace(60e-6,40e-6,10),'Vsd',20e-3);
%% ##########################################################################################
%% Scanner XY Large
D_XY_ScannerScanLargeFast('Gate2',520e-3,'Xs0',linspace(0,30e-6,20),'Ys0',linspace(40e-6,60e-6,20),'Vsd',30e-3);
%%
D_XY_ScannerScanSmallFast('Gate1',1300e-3,'Xs0',linspace(0,30e-6,20),'Ys0',linspace(40e-6,60e-6,20),'Vsd',20e-3);

%% Approach ->2D ########################################################################################## Small
ZsNow=S.Get('Zs0');ZsTarget=ZsNow-400e-9;
input(sprintf('will move from %e to %e, OK?', ZsNow, ZsTarget));
D_ZApprScannerSmall('Gate1',linspace(1600e-3,1300e-3,13),'Zs0',linspace(ZsNow,ZsTarget,20),'Vsd',60e-3);
%%
D_XScannerScanSmallFast('Gate1',1400e-3,'Xs0',linspace(S.Get('Xs0'),13e-6,30),'Vsd',60e-3);
%D_XScannerScanSmallFast('Gate1',1400e-3,'Xs0',[linspace(16e-6,19e-6,30),linspace(19e-6,16e-6,30)],'Vsd',70e-3);
%%
D_YScannerScanSmallFast('Gate1',1400e-3,'Ys0',linspace(S.Get('Ys0'),48e-6,10),'Vsd',60e-3);
%D_YScannerScanSmallFast('Gate1',1300e-3,'Ys0',[linspace(48e-6,52e-6,30),linspace(52e-6,48e-6,30)],'Vsd',20e-3);
%%
    D_XY_ScannerScanSmallFast('Gate1',1400e-3,'Xs0',linspace(13e-6,19e-6,40),'Ys0',linspace(48e-6,52e-6,40),'Vsd',60e-3);
%%
S.Set('Xs0',18e-6,'Ys0',50e-6);UpdateMarkerPosition()

%% LARGE!! LARGE!!LARGE!!LARGE!!LARGE!!LARGE!!LARGE!! Approach ->2D ########################################################################################## Small
 ZsNow=S.Get('Zs0');ZsTarget=ZsNow+200e-9;
 input(sprintf('will move from %e to %e, OK?', ZsNow, ZsTarget));
 D_ZApprScannerLarge('Gate2',linspace(700e-3,500e-3,13),'Zs0',linspace(ZsNow,ZsTarget,10),'Vsd',30e-3);
%%
D_XScannerScanLargeFast('Gate2',600e-3,'Xs0',linspace(S.Get('Xs0'),14e-6,30),'Vsd',30e-3);
%D_XScannerScanSmallFast('Gate1',1400e-3,'Xs0',[linspace(16e-6,19e-6,30),linspace(19e-6,16e-6,30)],'Vsd',70e-3);
%%
D_YScannerScanLargeFast('Gate2',600e-3,'Ys0',linspace(S.Get('Ys0'),46e-6,30),'Vsd',30e-3);
%D_YScannerScanSmallFast('Gate1',1300e-3,'Ys0',[linspace(48e-6,52e-6,30),linspace(52e-6,48e-6,30)],'Vsd',20e-3);
%%

    D_XY_ScannerScanLargeFast('Gate2',600e-3,'Xs0',linspace(14e-6,18e-6,20),'Ys0',linspace(46e-6,51e-6,20),'Vsd',30e-3);
%    if(Info.Flags.Stop);return;end


Info.Opt=BuildStruct(...
    'ActiveDevice','HLargeL',...
    'DeviceSignals',{'HLBG','HLCX','HLCY'},...
    'ToMeasure',{Info.ChannelType.TuneSignals{:},'NoiseL','NoiseR'},...
    'InitialSet',{'DSL',30e-3,'DSM',0,'DSR',0},...
    'SaveDimention',2,...
    'AutomaticCalculateBeta',{'Gate1','Gate2'});

S.Set(Info.Opt.InitialSet{:});

Info.Scan=BuildStruct(...
    'Axis',struct('Xs0',linspace(14e-6,18e-6,20),'Ys0',linspace(46e-6,51e-6,20)),...
    'Start',struct('Gate2',600e-3,'Gate1',1.2),'End',struct('Xs0',14e-6,'Ys0',46e-6),...
    'Get',{{...
        @()Interlock({'IdcL','IdcR','Beta_Sum'},{'DSL',0,'DSM',0,'DSR',0}),...
        @()Plot('(?!.*Err)Beta_.*')...
    },{...
        @()Plot1D_with_color(Info.Opt.ToMeasure{:}),...
        @()Plot2D('(?!.*Err)Beta_.*','Idc.',Info.Opt.ActiveDevice),...
    }});

        S.RunScan();
%%
S.Set('Xs0',15.7e-6,'Ys0',48.9e-6);UpdateMarkerPosition()



%% Dummy
D_DummySmallBeta('Gate1',linspace(1600e-3,1300e-3,11),'Dummy',1:30,'Vsd',20e-3);


%% ########################      Final Crash    #########################
ZsNow=S.Get('Zs0');ZsTarget=ZsNow+2000e-9;
input(sprintf('will move from %e to %e, OK?', ZsNow, ZsTarget));
D_Landing('LCY',linspace(0,2,20),'Zs0',linspace(ZsNow,ZsTarget,200),'Vsd',30e-3);
%%    
%D_Burning('LCY',[linspace(0,10,201)],'Vsd',30e-3);
    S.Set('LCY',0);
    
    Info.Keithley.Compliance=1e-7;
    Info.Keithley.Range_M=1e-6;
    Info.Keithley.Range_S=10;
    Info.Keithley.V=0;
    %Info.Keithley.Serial=1;

    fprintf(Info.Keithley.Serial,'SENS:CURR:RANG:AUTO OFF');
    fprintf(Info.Keithley.Serial,'SOUR:VOLT:RANG:AUTO OFF');
    fprintf(Info.Keithley.Serial,'SOUR:VOLT:LEV %E\n',Info.Keithley.V);
    fprintf(Info.Keithley.Serial,'SENS:CURR:RANG %E\n',Info.Keithley.Range_M);
    fprintf(Info.Keithley.Serial,'SOUR:VOLT:RANG %E\n',Info.Keithley.Range_S);
    fprintf(Info.Keithley.Serial,'SENS:CURR:PROT %E\n',Info.Keithley.Compliance);
    %% matrix manual change!!!!!
    %-------------------------------
    %-------------------------------
    %-------------------------------
    %-------------------------------
    %%
    Info.Keithley.Compliance=1e-4;
    Info.Keithley.Range_M=1e-4;
    Info.Keithley.Range_S=10;
    Info.Keithley.V=0;
    %Info.Keithley.Serial=1;

    fprintf(Info.Keithley.Serial,'SENS:CURR:RANG:AUTO OFF');
    fprintf(Info.Keithley.Serial,'SOUR:VOLT:RANG:AUTO OFF');
    fprintf(Info.Keithley.Serial,'SOUR:VOLT:LEV %E\n',Info.Keithley.V);
    fprintf(Info.Keithley.Serial,'SENS:CURR:PROT %E\n',Info.Keithley.Compliance);
    fprintf(Info.Keithley.Serial,'SENS:CURR:RANG %E\n',Info.Keithley.Range_M);
    fprintf(Info.Keithley.Serial,'SOUR:VOLT:RANG %E\n',Info.Keithley.Range_S);
    
   
%%
Vlist=[linspace(0,20,200),0];
Ilist=Vlist*nan;
figure(145);
h=plot(0,0,'.-');xlabel('Voltage');ylabel('Current');
for i=1:numel(Vlist)
    fprintf(Info.Keithley.Serial,'SOUR:VOLT:LEV %E\n',Vlist(i));
    fprintf(Info.Keithley.Serial,'READ?\n');RAW=fgets(Info.Keithley.Serial);RAW_IDX=find(RAW==',');
    Ilist(i)=str2num( RAW((RAW_IDX(1)+1):(RAW_IDX(2)-1)));
    h.XData=Vlist;h.YData=Ilist;
    drawnow;
end
h.XData=Vlist(1:end-1);h.YData=Ilist(1:end-1);
DocSend(145)

%%
 ZsNow=S.Get('Zs0');ZsTarget=ZsNow-500e-9;
 %input(sprintf('will move from %e to %e, OK?', ZsNow, ZsTarget));
 D_ZApprScannerLargeNoInterlock('LCY',linspace(0,2,13),'Zs0',linspace(ZsNow,ZsTarget,20),'Vsd',30e-3);