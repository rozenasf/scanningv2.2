1
%pause(7.5*60*60);

LoadScan(950,L)

%% 
Vsd=-24e-3; %???
Vbg=BG;


W=4e-6;
e=1.6e-19;
VDirac=-4.47;
CapacitiveBgFactor=5.946e14;
hbar=6.62e-34/(2*pi);

n=CapacitiveBgFactor*(Vbg-VDirac);
B=Magnet_I/10*120e-3;
%Av=10;
%SpreadOut=[3,0.015,1.5];
SpreadOut=[0,0,0];
Av=1;
%%
    Idata('Phi',0.5+0.5*(Beta_HS14_Hset-Beta_HS6_Hset)./(Beta_HS14_Hset+Beta_HS6_Hset));
    Idata('I',(Ggr6/VSenseGain/Info.Scaling.Ggr6)/RSense);
    n= Vbg_to_n(BG, v_dirac);
    k_F_vec = sqrt(pi*abs(n));
%%
figure(1);
    I1=mean(I,1);
%     Idata('V',Phi*Vsd);
Idata('V',Beta_HS14_Hset*Vsd/0.3);

plot_with_color(V.slice_index(3,1),1)

%%
    DPhi_DXs=I1*0;
    %Idx=1:10;
    %Idx=12:21;
%     Idx
    for i=1:size(V.data,2)
        for j=1:size(V.data,3)
            F=polyfit(V.axes{1}.data',V.data(:,i,j),1);
            %F=polyfit(Xs(Idx)',V.data(Idx,i,j),1);
            DPhi_DXs.data(i,j)=F(1);
        end
    end
    
    DPhi_DXs.data=(DPhi_DXs.data+flipud(DPhi_DXs.data))/2
    %DPhi_DXs=DPhi_DXs.map_vectors(@(x)smooth(x,Av),1);
    %%
    RhoXX=DPhi_DXs*W./(I1);
   n= Vbg_to_n(BG, v_dirac);
    l=1e6*( h_./(2*qe_^2*k_F_vec.*RhoXX));

%%
figure(2);
% Temp={};for u=1:numel(n);Temp{end+1}=sprintf('n [1/cm^2] = %0.2e',n(u)/1e4);end;legend(Temp,'Location','northwest')
subplot(2,3,1);
plot((B'*ones(1,10)),l.data+SpreadOut(3)*[1:10],'-');

xlabel('B [T]');ylabel('l [um]');
% Temp={};for u=1:numel(n);Temp{end+1}=sprintf('n [1/cm^2] = %0.2e',n(u)/1e4);end;legend(Temp,'Location','north')
grid
title(sprintf('  Scan 950',Av))
%%
%figure(3)
subplot(2,3,2);
plot((B'*ones(1,10)),1./RhoXX.data+SpreadOut(2)*[1:10],'-');xlabel('B [T]');ylabel('Conductivity [1/Ohm]');
% Temp={};for u=1:numel(n);Temp{end+1}=sprintf('n [1/cm^2] = %0.2e',n(u)/1e4);end;legend(Temp,'Location','north')
grid
title(sprintf('  Scan 950',Av))
%%
% figure(4)
subplot(2,3,3);
plot((B'*ones(1,10)),RhoXX.data+SpreadOut(1)*[1:10],'-');xlabel('B [T]');ylabel('Resistivity [1/Ohm]');
% Temp={};for u=1:numel(n);Temp{end+1}=sprintf('n [1/cm^2] = %0.2e',n(u)/1e4);end;legend(Temp,'Location','northwest')
grid
title(sprintf('  Scan 950',Av))
%%
% figure(5)
subplot(2,3,4);
for i=1:10
    Rc=r_cyclo(n(i), B).*sign(B);Rc(isnan(Rc))=inf;W_Rc=W./Rc;
    plot(W_Rc,l.data(:,i)+SpreadOut(3)*i,'-');
hold on
end
hold off
xticks([-4,-3,-2,-1,-0.5,0,0.5,1,2,3,4])
grid
xlabel('W/Rc');ylabel('l [um]');
title(sprintf('  Scan 950',Av))
%%
% figure(6)
subplot(2,3,5);
for i=1:10
    Rc=r_cyclo(n(i), B).*sign(B);Rc(isnan(Rc))=inf;W_Rc=W./Rc;
    plot(W_Rc,1./RhoXX.data(:,i)+SpreadOut(2)*i,'-');
hold on
end
hold off
xticks([-4,-3,-2,-1,-0.5,0,0.5,1,2,3,4])
grid
xlabel('W/Rc');ylabel('Conductivity [1/Ohm]');
title(sprintf('  Scan 950',Av))
%%
% figure(7)
subplot(2,3,6);
for i=1:10
    Rc=r_cyclo(n(i), B).*sign(B);Rc(isnan(Rc))=inf;W_Rc=W./Rc;
    plot(W_Rc,RhoXX.data(:,i)+SpreadOut(1)*i,'-');
hold on
end
hold off
xticks([-4,-3,-2,-1,-0.5,0,0.5,1,2,3,4])
grid
xlabel('W/Rc');ylabel('Resistivity [Ohm]');
title(sprintf('  Scan 950',Av))
%%
%TelegramSend(2)