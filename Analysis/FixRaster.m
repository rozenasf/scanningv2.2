function idgr=FixRaster(idgr)
    names=fieldnames(idgr);
    for i=1:numel(names)
        [~,IDX]=sort(idgr.(names{i}).axes{1}.data);
        idgr.(names{i})=idgr.(names{i}).slice_index_range(1,IDX);
    end
end