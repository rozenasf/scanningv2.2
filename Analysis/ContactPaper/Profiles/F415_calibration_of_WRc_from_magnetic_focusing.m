% while(1)
% TelegramGetLoop();
clf
set(gca, 'ColorOrder', hsv(19), 'NextPlot', 'replacechildren');
LoadScan(392,L,'NoScale'); figure(1);
Center=2.05e-5;
Beta_Sum=358.492e-3;
% try
V=Beta_HS15_Hset./Beta_Sum.*AVS15;%./IgrAC15
VMatrix=[];Magnet=V.axes{2}.data(1:2:end);
MaxLocationRough=[-2.9,-3.3,-3.7,-4.1,-4.1,-4.5,-5,-5,-5.4,-5.4,-5.4,-5.8,-6,-6.1,-6.1,-6.1,-6.1,-6.1,-6.2];
out={};
for i=2:size(V.data,2)/2
    clf
    LineIData=(-V.slice_index(2,2*i-1)+V.slice_index(2,2*i))./2;
    %LineIData.data=LineIData.data+fliplr(LineIData.data);
    plot((21+LineIData.axes{1}.data*1e6*0.8385),(LineIData.data)*1e6,'linewidth',3)
    out{i}=Fit_Line(2,'X',MaxLocationRough(i-1)+[-1,1]);
pause(0.0)
end
 clf
 dXList=[];dYList=[];MagnetList=[];
for i=2:size(V.data,2)/2
    LineIData=(-V.slice_index(2,2*i-1)+V.slice_index(2,2*i))./2;
    %LineIData.data=LineIData.data+fliplr(LineIData.data);
    plot((21+LineIData.axes{1}.data*1e6*0.8385),(LineIData.data)*1e6,'linewidth',3)
%     out{i}=Fit_Line(2,'X',MaxLocationRough(i-1)+[-1,1])
          hold on
          dXList=[dXList,out{i}.dX_0];
          dYList=[dYList,out{i}.dY_0];
          MagnetList=[MagnetList,Magnet(i)];
          plot(out{i}.dX_0,out{i}.dY_0,'o')
    VMatrix=[LineIData.data;VMatrix];
end
hold off
TXYL('Scan 392, BG=-8.5, Hall Voltage - W/R_c = [0.5:0.25:5]','X [\mum]','V [\muV]')
% xlim([-10,10])

set(gca,'FontSize',16)
%%

BG=-8.5;
Width = 5e-6;
qe_=1.6022e-19;
hbar_=1.0546e-34;
n = abs(Vbg_to_n(BG,0.7));
p_F=hbar_*sqrt(pi*n);
B=MagnetList/10*120e-3;
r_c=p_F./qe_./B;
WrcList=Width./r_c;
plot(5./WrcList,dXList,'o')
% n=abs(5.946e14*(BG-0.5));
Fit_Line(1,'X',[1.5,3.5]);
TXYL('Magnetic focusing peak','Rc (assumed)','Location [um]')


