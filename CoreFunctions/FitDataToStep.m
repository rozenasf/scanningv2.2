function [Width,Plot]=FitDataToStep(idgr,Index)
q=idgr.slice_index(2,Index);

x=q.axes{1}.data';y=q.data';
NormParms=[mean(x),std(x),mean(y),std(y)];
y=y-NormParms(3);y=y/NormParms(4);
x=x-NormParms(1);x=x/NormParms(2);
%a*tanh((x-c)/b)+d
fo = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[-2,0,0,0],...
    'Upper',[2,6,Inf,Inf],...
    ...'Normalize','on',...
    'StartPoint',[0,1,0.5,0.5]);
%ft = fittype('ErfFit(x,a,b,c,d)','options',fo);
ft = fittype('TanhFit(x,a,b,c,d)','options',fo);
fitobject = fit(x,y,ft);%x=b;y=a;
a=fitobject.a;b=fitobject.b;c=fitobject.c;d=fitobject.d;
a=a*NormParms(2)+NormParms(1);b=b/NormParms(2);c=c*NormParms(4);d=d*NormParms(4)+NormParms(3);
Width=1/b;X=q.axes{1}.data';
    Plot=@()Plotfit();
    function Plotfit()
        plot(q,'o');
        hold on
        %plot(X,ErfFit(X,a,b,c,d),'--');
        plot(X,TanhFit(X,a,b,c,d),'--');
        figure(33);
        plot(X(1:end-1),diff(TanhFit(X,a,b,c,d)),'--');
        hold off
        title(sprintf('Width is %0.3f um',1e6*Width));
    end
end