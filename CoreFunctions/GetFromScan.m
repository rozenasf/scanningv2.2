function out=GetFromScan(Channel,Position)
    global Info
    if(~exist('Position'));Position=Info.Scan.Runtime.Position;end
    out=Info.Scan.idgr.(Channel).data(Position2PositionIndex(Position));
end