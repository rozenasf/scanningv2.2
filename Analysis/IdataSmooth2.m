function r = IdataSmooth2(obj, N)
    % idata_obj = idata_obj.smooth(N,dimension) : smooth along the selected
    % dimension for N point
    % Arguments: 
    %  - D-dimensioned idata object,
    %  - N number to smooth
    %  - averaging dimension (1,2,3...)
    % Example:
    % r = s.smooth(3,2);
        r = obj.map(@(x) smooth(x, N), dimension);
end
                
