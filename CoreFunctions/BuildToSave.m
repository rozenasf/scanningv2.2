function ToSave=BuildToSave(IDX)
global Info;
switch(IDX)
    case 1
        ToSave=[];
%         try ToSave.Title=[Info.Scan.Description]; end
%         try ToSave.SaveName=Info.Scan.SaveName; end
        try ToSave.Scan=Info.Scan;end
        try ToSave.GetChannels=Info.GetChannels;end
        try ToSave.SetChannels=Info.SetChannels;end
        try ToSave.Range=Info.Range;end
        try ToSave.InitialTolerance=Info.InitialTolerance;end
        try ToSave.DispExp=Info.Dac.DispExs;end
        try ToSave.Sampling_time=Info.Dac.General_Parameters.Sampling_time;end
        try ToSave.Tone_Signals=Info.Dac.Tone_Signals;end
        try ToSave.Band_Signals=Info.Dac.Band_Signals;end
end
end