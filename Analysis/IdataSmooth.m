function r = IdataSmooth(obj, N, dimension)
    % idata_obj = idata_obj.smooth(N,dimension) : smooth along the selected
    % dimension for N point
    % Arguments: 
    %  - D-dimensioned idata object,
    %  - N number to smooth
    %  - averaging dimension (1,2,3...)
    % Example:
    % r = s.smooth(3,2);
    if(~exist('dimension'));dimension=1;end
        r = obj.map_vectors(@(x) smooth(x, N), dimension);
end
                
