out=load('C:\Users\asafr\Google Drive\witz\phd\measurments\Scanning SET technique\B2_Manchester5B\HallEffect_291218_75K_BG_N5.mat');
out=out.CalibrationListWRc;
clf
U=out([out.UD]=='U');
D=out([out.UD]=='D');
Scale=1;
WRcList=-[U.WRc]*Scale;
MeasuredRhoXY=([U.AVS2]-[D.AVS2])./mean([[U.IgrAC15];[D.IgrAC15]]);
plot(WRcList,MeasuredRhoXY);hold on

kf=sqrt(pi*abs(Vbg_to_n(-5,0.6)));W=5e-6;
Classical=pi*hbar_/(W*kf*qe_^2)*(WRcList);
plot(WRcList,Classical);
%
clf
Shift=-4.5e-2;
 
 k=0.15;
% Scale=1;
Classical=pi*hbar_/(W*kf*qe_^2)*(WRcList-Shift);
plot(WRcList,MeasuredRhoXY./Classical);
ylim([0.8,1.2])
hold on

 plot(WRcList,(1-6*k^2./(1+(2*k*(WRcList-Shift)).^2)))
% Vbg_to_n(-5,0.6)
%%
LoadScan(471);
clf
Shift=6.4e-3;
plot(Feedback+Shift);hold on
%
DiracLocation=0.547;
Vf=1.15e6;
kf=sqrt(pi*abs(Vbg_to_n(BG,DiracLocation))).*sign(BG-DiracLocation);
plot(BG,-Vf*hbar_.*kf./qe_.*Beta_HS15_Hset.data);
title('Free parameter Vf = 1.15e6')
legend({'measurement','theory'})
%%
clf
Data=load('Q:\Measurements\Asaf R\Scanning SET technique\B2_Manchester5B\Scan_411_Xs_Ramp_RampBG_2018_10_20_11_41_16.mat');
Data=Data.Info_Saved;
idgr=Data.Scan.idgr;
Phi=(idgr.Beta_HS15_Hset-idgr.Beta_HS2_Hset)./(2*(idgr.Beta_HS15_Hset+idgr.Beta_HS2_Hset))+0.5;
R=Phi.*Data.Channels.AVS15./idgr.IgrAC15;R.name='R [Ohm]';
R=R.slice_index(2,20).sir(1,[2:41]);
plot(0.8385*1e6*(R.axes{1}.data-mean(R.axes{1}.data)),R.data,'linewidth',3);
title('BG=-9.8 [V]','interpreter','latex');xlabel('$X \ [\mu m]$','interpreter','latex');
ylabel('$R \ [Ohm]$','interpreter','latex');
xlim([-10,10]);
Fit_Line(1,'X',[-4,4]);
Fit.line.LineWidth=2;Fit.line.LineStyle='--';
Fit.Markers.delete
grid