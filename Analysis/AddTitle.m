function AddTitle(More_Text)
global Info
newline=sprintf('\n');
if(exist('More_Text'))
suptitle(['\fontsize{16} Scan ',num2str(Info.ScanNumber),newline,'\fontsize{10}',Info.Scan.Description,newline,More_Text]);
else
    suptitle(['\fontsize{16} Scan ',num2str(Info.ScanNumber),newline,'\fontsize{10}',Info.Scan.Description]);
end
end