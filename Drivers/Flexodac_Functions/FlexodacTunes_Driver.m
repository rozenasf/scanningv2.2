classdef FlexodacTunes_Driver < handle
    properties
        Tunes
        TimeWindow
    end
    
    methods
        function obj=FlexodacTunes_Driver()
            TimeWindow=1;
        end
        function Map(obj,varargin)
            for i=1:2:numel(varargin)
                obj.Tunes.(varargin{i})=struct('Sampler',varargin{i+1}{1},'Frequency',varargin{i+1}{2},'Phase',varargin{i+1}{3});
            end
        end
        function init(obj)
            global Info
            Info.Flexodac.S.capture(obj.TimeWindow);
        end
% 
%         %-----
         function out=Get(obj,Channel)
             global Info
                Res=Info.Flexodac.S.fetch({readerTone(obj.Tunes.(Channel).Sampler,obj.Tunes.(Channel).Frequency)});
                out=real(Res{1}*exp(1i*obj.Tunes.(Channel).Phase));
% %             out=nan;
         end
%         function Set(obj,Channel,Value)
%             
%         end
       
    end
end