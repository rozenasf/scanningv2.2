function [BinnaryOutput,Length]=Data2Format(Format,Data)
        Length=FormatLength(Format);
        Message=zeros(1,Length);
        Location=1;
        for i=1:numel(Format)
            switch(Format(i))
                case 'B'
                    Message(Location)=Data(i);Location=Location+1;
                case 'f'
                    Message(Location:(Location+3))=typecast(single(Data(i)),'uint8');Location=Location+4;
                case 'L'
                    Message(Location:(Location+3))=typecast(int32(Data(i)),'uint8');Location=Location+4;
                otherwise
                    error('Unknown format');
            end
        end
        BinnaryOutput=sprintf('%02X',Message);
    end