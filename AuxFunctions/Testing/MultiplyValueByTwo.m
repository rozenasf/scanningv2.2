function out=MultiplyValueByTwo(inname,outname)
    global Info S;
    S.Set(outname,2*Info.SetChannels.(inname));
    out=1;
end