classdef DCDecadac_Driver < handle
   properties
      Voltage %[1:10]
      Comunication
   end
   %M3
   methods
      function obj=DCDecadac_Driver(ComPort)
          if(isnumeric(ComPort))
            obj.Comunication=serial(['COM',num2str(ComPort)],'BaudRate',9600);
          else
             obj.Comunication=serial(ComPort,'BaudRate',9600);
          end
          fopen(obj.Comunication);
          obj.Voltage=nan(1,10);
          for i=1:10;disp(obj.ReadChannelVoltage(i));disp('Communicating about channel');end
          disp('Connection to DCDecadac OK!');
      end
      function Set(obj,Channel,Value)
          global Info
          DCDACChannelIndex=find(strcmp(Info.ChannelType.DCDecadac,Channel));
          obj.SetVoltage(DCDACChannelIndex,Value);
      end
      function Init(obj) %will set the mode to 3, the correct one
          for Channel=1:10
             fprintf(obj.Comunication,sprintf('B%d;C%d;M3;D32767.5;',ceil(Channel/2)-1,mod(Channel-1,2))); %mid range
             disp(fgets(obj.Comunication));
          end
      end
      function delete(obj)
         fclose(obj.Comunication);
         delete(obj.Comunication);
      end
      function [Voltage]=ReadChannelVoltage(obj,Channel) %
        fprintf(obj.Comunication,sprintf('B%d;C%d;d;',ceil(Channel/2)-1,mod(Channel-1,2)));
        Voltage=obj.ReadFromInput/65535*20-10;
        disp('Warning! This shows only coarse, resulution of only 0.3mV!');
      end
      function ValueRead=ReadFromInput(obj)
        Read=fgets(obj.Comunication);
        p=regexp(Read,'[dD].+!','match');
        ValueRead=str2num(p{1}(2:end-1));
      end
      function ZeroAll(obj)
          for i=1:10
              obj.SetVoltage(i,0)
          end
      end
      function SetVoltage(obj,Channel,Value) %-10-10
          if(numel(Channel)~=1);error('SetVoltage works on a single channel at a time');end
         NumberToSend=( (Value+10)/20 *65535);
         if(NumberToSend>65535 || NumberToSend<0);error('OUT OF RANGE DCDecadac Voltage!');end
         flushinput(obj.Comunication);
         fprintf(obj.Comunication,sprintf('B%d;C%d;M3;D%f;',ceil(Channel/2)-1,mod(Channel-1,2),NumberToSend));
         NumberRead=ReadFromInput(obj);
         if(abs(NumberToSend-NumberRead)*20/65535>1e-3);
             error(['BAD COMMUNICATION with DCDecadac!',num2str(Channel)]);
         end
         obj.Voltage(Channel)=Value;
      end
   end
end