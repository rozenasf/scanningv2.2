classdef Keithley2000_Driver < handle
    properties
        Range
        Raw
        Communication
    end
    methods
        function obj=Keithley2000_Driver(varargin)
           %vendor,GPIB no, Address (example: 'ni',0,2)
           obj.Communication=gpib(varargin{1},varargin{2},varargin{3});
           fopen(obj.Communication);
           disp('Keithly2000 connected!');
        end
        %---
        function out=Get(obj,Channel)
           flushinput(obj.Communication);
           fprintf(obj.Communication,':FETCh?');
           obj.Raw=fgets(obj.Communication);
           out=sscanf(obj.Raw,'%e');
           
        end
        function delete(obj)
            fclose(obj.Communication);
            try delete(obj);end
            disp('Keithly2000 removed');
        end
    end
end