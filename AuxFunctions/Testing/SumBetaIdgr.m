    function out=SumBetaIdgr(varargin)
        global Info;out=0;
        Info.Scan.idgr.Beta_Sum=Info.Scan.idgr.(varargin{1});
        for i=2:numel(varargin)
            Info.Scan.idgr.Beta_Sum=Info.Scan.idgr.Beta_Sum+Info.Scan.idgr.(varargin{i});
        end
    end