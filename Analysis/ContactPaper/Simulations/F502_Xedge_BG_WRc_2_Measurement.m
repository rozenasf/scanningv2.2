 Extra=3;
    j=1+(Extra-1)*2;
clf
PeaksLocations=[-5.2/0.8,-2.75/0.8,0];
    RelevantBGList=[];
for i=2:10
%2:10
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j,i});Beta_Sum=0.51;
R1 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
UnPackScan(Scans.XEdge_BGlist_Holes_wrc{j+1,i});Beta_Sum=0.51;
R2 = CenterIDataAxis(BreakIData(Beta_HS15_Hset)./Beta_Sum .*AVS15./IgrAC15,1,1e6);
IData=R1-R2;
RelevantBGList(i-1)=Info.Channels.BG;
% plot(IData.roi(1,-6.5,6.5),'-','linewidth',3)
IData.data=(IData.data+fliplr(IData.data))/2;
plot(IData,'-','linewidth',3);
hold on;
end
ChangePlotColor(@jet);
% PeakList=[];
out={};
for i=1:numel(PeaksLocations)
   out{i}=Fit_Line(2,'X',[PeaksLocations(i)-1,PeaksLocations(i)+1]) ;
   PeakList{i}=[out{i}.dY_0];
   
end
for i=1:numel(PeaksLocations)
PeakList{i}=smooth(PeakList{i}./PeakList{3},3);
end
Ax=gca;Lines=Ax.Children;for i=1:numel(Lines);Lines(i).LineWidth=3;,Lines(i).MarkerSize=20;end;
hold on
Q1=patch([-10,-7.5,-7.5,-10],[0,0,70,70],[0,0,0]);Q1.FaceAlpha=0.3;Q1.EdgeAlpha=0;
Q2=patch(-[-10,-7.5,-7.5,-10],[0,0,70,70],[0,0,0]);Q2.FaceAlpha=0.3;Q2.EdgeAlpha=0;
grid;ylim([0,70]);TXYL('W/Rc=2','X[um]','$R_{Hall}$ [Ohm]','BG=%0.2f',{RelevantBGList},'location','southeast')