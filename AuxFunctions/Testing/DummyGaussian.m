function out=DummyGaussian(inname1,inname2,outname)
    global Info S;
    S.Set(outname,  exp(-0.5*(Info.SetChannels.(inname1).^2+Info.SetChannels.(inname2).^2))  );
    out=1;
end