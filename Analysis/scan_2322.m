%
LoadScan([],L)
j=4;
Beta_Sum=0.307
Phi=Beta_HS15_Hset/Beta_Sum.*AVS15_e./(AVS15_e-AVS2_e)-AVS2_e./(AVS15_e-AVS2_e); Phi.name='Phi';
%
Phi_A=(Phi.slice_index(4,j).slice_index(3,1)-Phi.slice_index(4,j).slice_index(3,5))/2;
Phi_S=(Phi.slice_index(4,j).slice_index(3,1)+Phi.slice_index(4,j).slice_index(3,5))/2;
Phi_S=Phi.slice_index(4,j).slice_index(3,3);
N=1;
[X,Y]=meshgrid(linspace(-3,3,N),linspace(-3,3,N));
contour(Phi_A,10,'linewidth',5);hold on%
%caxis([min(min(Phi_A.data)),max(max(Phi_A.data))])
contour(Phi_S,30,'linewidth',5);hold on
caxis([min(min(Phi_S.data)),max(max(Phi_S.data))])

colormap(jet)
hold off
axis equal
TelegramSend(999)
%%
clf;
for i=1:5
   subplot(2,5,i);
   pcolor(Phi.slice_index(4,1).slice_index(3,i))
   %pcolor(Hset.slice_index(4,1).slice_index(3,i))
   %axis equal
      subplot(2,5,i+5);
    pcolor(Phi.slice_index(4,2).slice_index(3,i))
%pcolor(Hset.slice_index(4,2).slice_index(3,i))
  %axis equal
end