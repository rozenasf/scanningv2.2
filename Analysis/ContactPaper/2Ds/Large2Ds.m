%% Nice 2D as function of W/Rc = 0,1,2,4,6.3
% BG=-8.5 - "XY_BG_Holes" and BG=9.9 - "XY_BG_Electrons"
%BG = 2.47 -'XY_BG_Electrons_spec' BG = -1.07 'XY_BG_Holes_BG_spec'

ScanNameList={'XY_BG_Holes','XY_BG_Electrons','XY_BG_Holes_BG_spec','XY_BG_Electrons_spec'};

for j=1:numel(ScanNameList)
    ScanName=ScanNameList{j};
    figure(j+100);clf;colormap(jet(16));shading interp
    suptitle(ScanNameList{j}(7:end))
    
    for i=1:numel(Scans.(ScanName))
        subplot(5,2,i+1*(i>1))
        UnPackScan(Scans.(ScanName){i})
        Beta_Sum=0.51;
        R = CenterIDataAxis(BreakIData(Beta_HS15_Hset./Beta_Sum .*AVS15./IgrAC15),[1,2],1e6);
        pcolor(R);shading interp;axis equal;
        hold on
        contour(R,40,'LineColor','k')
        TXYL(sprintf('R [ohm], W/Rc=%0.2f',SuperAxis.(ScanName).W_Rc(i)),'X(scaled) [um]','Y [um]');
        ScaleColorFromIData(R.roi(1,-7,7))
    end
end
