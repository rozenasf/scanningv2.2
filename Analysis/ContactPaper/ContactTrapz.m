function Y=ContactTrapz(X,Slope)
     Y=X*0;
     SlopePar=40;
     Y(abs(X)>2) =Slope.*(abs(X(abs(X)>2))+SlopePar-2);
     Y(abs(X)<=2)=Slope*SlopePar*(1+find((abs(X)<=2))*0);
end