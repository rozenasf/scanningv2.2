function ShiftGDSToMatchLocation(xM,yM)
    global Info;
    xD=mean(xlim());
    yD=mean(ylim());
    Info.Design.Matrix(1,3)=Info.Design.Matrix(1,3)-(xD-xM);
    Info.Design.Matrix(2,3)=Info.Design.Matrix(2,3)-(yD-yM);
end