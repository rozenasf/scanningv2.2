function out=Get_Names_Flexodac_AC
global Info

for i=1:numel(Info.ChannelType.FlexodacVoltages)
    out{2*i-1}=['A_',Info.ChannelType.FlexodacVoltages{i}];
    out{2*i}=['F_',Info.ChannelType.FlexodacVoltages{i}];
end
end