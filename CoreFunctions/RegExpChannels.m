function Out=RegExpChannels(In)
Out={};
if(iscell(In))
    for i=1:numel(In)
        Out=[Out,RegExpChannels(In{i})];
    end
else
       List=regexp(AllChannelsList(),['^',In,'$'],'match');
       Out=[Out,List{:}];
end
end