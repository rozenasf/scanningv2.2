function OK=InChannelRange(Channel,Value)
global Info
    OK=1;
    if(isfield(Info.Range,Channel))
        OK=InRange(Value,Info.Range.(Channel)(1),Info.Range.(Channel)(2));
        if(~OK)
            TEXT=sprintf('out of range %s = %e\n Range IS: %s\n',...
                        Channel,Value,SprintfA(Info.Range.(Channel)));
            Info.Scan.OutOfRangeEvent=TEXT;
            disp(TEXT);
            DocSend(TEXT);
        end
    end
end