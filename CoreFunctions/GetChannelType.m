function Type=GetChannelType(Channel)
global Info
Type=[];
    names=fieldnames(Info.ChannelType);
    for i=1:numel(names);
        name=names{i};
        if(ismember(Channel,Info.ChannelType.(name)));
            Type=name;
            break;
        end
    end
end