function Max=GetMaxSuperScanNumber(ScanNumber)
global L;
if(exist('ScanNumber'))
    Dir=dir([L.MainPath,L.ActiveNotebook,'\Scan_',num2str(ScanNumber),'_*']);
    Max=0;
    for i=1:numel(Dir)
        Split=regexp(Dir(i).name,'_','split');
        Max=max([Max,str2num(Split{3})]);
    end
else
    Dir=dir([L.MainPath,L.ActiveNotebook,'\Scan_*']);
    Max=0;
    for i=1:numel(Dir)
        Split=regexp(Dir(i).name,'_','split');
        Max=max([Max,str2num(Split{2})]);
    end
end
end