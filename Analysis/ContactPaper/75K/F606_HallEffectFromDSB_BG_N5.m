load('C:\Users\asafr\Google Drive\witz\phd\measurments\Scanning SET technique\B2_Manchester5B\HallEffect_291218_75K_BG_N5.mat');
I1=([CalibrationListWRc.UD]=='U');
I2=([CalibrationListWRc.UD]=='D');
% clf;plot([CalibrationListWRc(I1).WRc],[CalibrationListWRc(I1).AVS2]./[CalibrationListWRc(I1).IgrAC15])
% hold on;plot([CalibrationListWRc(I2).WRc],[CalibrationListWRc(I2).AVS2]./[CalibrationListWRc(I1).IgrAC15])
%%
clf;
RHall=2*([CalibrationListWRc(I1).AVS2]-[CalibrationListWRc(I2).AVS2])./([CalibrationListWRc(I1).IgrAC15]+[CalibrationListWRc(I2).IgrAC15]);
Fit_Line(1)
plot([CalibrationListWRc(I1).WRc],-RHall,'.','linewidth',3)
hold on
n=abs(Vbg_to_n(-5,0.6));
WRc_List=[CalibrationListWRc(I1).WRc];
Wrc = 0.8*WRc_List-0.05*0.8;
r_c=5e-6./Wrc;
    p_F=hbar_*sqrt(pi*n*1.2);
    B=p_F./qe_./r_c;
   RHallTheory=B./(n*qe_);
   hold on
%    Fit_Line(1)
   plot(WRc_List,RHallTheory)
     clf
  plot(WRc_List,-RHall./RHallTheory,'linewidth',3)
 TXYL('Hall Resistance normelized by theory @BG=-5V','W/Rc','R/R_theory','Measurement','Ohmic theory')
%% 
TXYL('Hall Resistance @BG=-5V','W/Rc','R [Ohm]','Measurement','Ohmic theory')