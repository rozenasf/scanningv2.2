%%
SetActiveNotebook('F5_ManchesterA');
%% cooldown
List=[];ListCapY=[];Graphene=[];,T=[];
for i=1:numel(id)
   if(~isempty(id{i}) && isfield(id{i},'Time') && isfield(id{i},'Temperature'))
       T=[T,id{i}.Time.data(:)'+id{i}.Now*3600*24];
       List=[List,id{i}.Temperature.data(:)'];
       ListCapY=[ListCapY,id{i}.ZsCapY.data(:)'];
       Graphene=[Graphene,id{i}.Ggr10.data(:)'*1000/120];
   end
end
Temperature=Idata('Temperature',List,'Time [Hours]',(T-T(1))/3600);
ZscapY=Idata('ZscapY',ListCapY,'Time [Hours]',(T-T(1))/3600);
Graphene=Idata('Graphene[Ggr10*1000/120]',Graphene,'Time [Hours]',(T-T(1))/3600);
BatchPlot(1,'Cooldown',1,2,@(x)plot(x),Temperature,ZscapY)
%%
figure(2)
plot(Idata('ZsCapY',ZscapY.data,'Temperature',Temperature.data))
ylim([-8.8,-1.7]*1e-6);
Fit_Line(8);
%%
figure(3)
plot(Graphene)
%%
plot_with_color(Idata('Idc',id{11}.Idc.data(:,1:130),'Vg',id{11}.Vg,'Dummy',1:130),1)
title('Scan 11 - 5mV Vsd')
%%

plot_with_color(Idata('Idc',id{12}.Idc.data(:,1:1140),'Vg',id{12}.Vg,'Dummy',1:1140),1)
title('Scan 12 - 5mV Vsd')
%% Heater - Magnet - BG on S15-S2
figure(8);clf;
subplot(3,3,1);
plot_with_color(id{122}.Ggr10.transpose,1);
title(sprintf('Heater@0, T=%0.2fK ',i,mean(id{122}.Temperature.data(:)))) 
%print('0','-dpng')
for i=1:8
    subplot(3,3,i+1);
plot_with_color(id{123}.Ggr10.slice_index(3,i).transpose,1);colormap(jet)
title(sprintf('Heater@%d, T=%0.2fK ',i,mean(id{123}.Temperature.slice_index(3,i).data(:)))) 
end
suptitle('G Scan 122,123 - S15->S2')
%% 